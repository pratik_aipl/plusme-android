package com.plus.me.fragments;


import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.plus.me.R;
import com.plus.me.adapter.PostAdapter;
import com.plus.me.listener.DeleteBlogListener;
import com.plus.me.model.Post;
import com.plus.me.others.App;
import com.plus.me.utils.VerticalSpacingDecoration;
import com.plus.me.ws.AsyncTaskListner;
import com.plus.me.ws.CallRequest;
import com.plus.me.ws.Constant;
import com.plus.me.ws.JsonParserUniversal;
import com.plus.me.ws.MyConstants;
import com.plus.me.ws.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * A simple {@link Fragment} subclass.
 */
public class PostFragment extends Fragment implements AsyncTaskListner, DeleteBlogListener {


    public RecyclerView rvPost;
    public JsonParserUniversal jParser;
    private PostAdapter postAdapter;
    private LinearLayoutManager layoutManager;
    private TextView txtNoValue;
    private ArrayList<Post> postArrayList = new ArrayList<>();
    int position;

    public PostFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_post, container, false);

        setHasOptionsMenu(true);

        bindWidgetReference(view);

        initializeData();

        getMyPosts();

        return view;
    }

    private void initializeData() {
        jParser = new JsonParserUniversal();
    }

    private void getMyPosts() {
        Map<String, String> map = new HashMap<>();
        map.put("url", MyConstants.BASE_URL + "GetMyPosts");
        map.put("header", "");
        if (MyConstants.isUserProfile) {
            map.put("UserProfileId", MyConstants.otherUserId);
        } else {
            map.put("UserProfileId", "");
        }
        map.put("Auth", App.user.getAuth());

        new CallRequest(PostFragment.this).getMyPosts(map);
    }

    private void getDeletePost(String BlogID) {
        Map<String, String> map = new HashMap<>();
        map.put("url", MyConstants.BASE_URL + "RemoveBlog");
        map.put("header", "");
        map.put("BlogID", BlogID);
        map.put("Auth", App.user.getAuth());

        new CallRequest(PostFragment.this).getDeletePost(map);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        //    tabs.setVisibility(View.VISIBLE);
    }

    private void setData() {
        layoutManager = new LinearLayoutManager(getActivity());
        rvPost.setLayoutManager(layoutManager);

        postAdapter = new PostAdapter(getActivity(), postArrayList, PostFragment.this);
        rvPost.setHasFixedSize(true);
        rvPost.addItemDecoration(new VerticalSpacingDecoration(20));
        rvPost.setItemViewCacheSize(20);
        rvPost.setDrawingCacheEnabled(true);
        //rvCompleteChat.addItemDecoration(new DividerItemDecoration(getActivity(), R.drawable.divider));
        rvPost.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
        rvPost.setAdapter(postAdapter);

        if (postArrayList.size() == 0) {
            txtNoValue.setVisibility(View.VISIBLE);
        } else {
            txtNoValue.setVisibility(View.GONE);
        }
    }

    private void bindWidgetReference(View view) {
        rvPost = view.findViewById(R.id.rvPost);
        txtNoValue = view.findViewById(R.id.txtNoValue);
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        try {

            Utils.hideProgressDialog();
            if (result != null && !result.isEmpty()) {
                Log.i("TAG", "TAG Result : " + result);

                switch (request) {
                    case getMyPosts:
                        postArrayList.clear();
                        try {
                            JSONObject object = new JSONObject(result);
                            boolean success = object.getBoolean("success");
                            if (success) {
                                // TODO parse JSON
                                JSONArray array = object.getJSONArray("data");

                                for (int i = 0; i < array.length(); i++) {
                                    JSONObject jsonObject = array.getJSONObject(i);

                                    Post post = (Post) jParser.parseJson(jsonObject, new Post());

                                    postArrayList.add(post);
                                }

                            } else {
                                String error_string = object.getString("message");
                                //Utils.showToast(error_string, getActivity());
                            }

                            setData();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;
                    case getDeletePost:
                        try {
                            JSONObject object = new JSONObject(result);
                            boolean success = object.getBoolean("success");
                            if (success) {
                                postArrayList.remove(position);
                                postAdapter.notifyDataSetChanged();
                                Utils.showToast(object.getString("message"), getActivity());
                            } else {
                                Utils.showToast(object.getString("message"), getActivity());
                            }

                            setData();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onProgressUpdate(String uniqueMessageId, int progres) {

    }

    @Override
    public void onProgressComplete(String uniqueMessageId, String result, Constant.REQUESTS request) {

    }

    @Override
    public void onDelete(String blogID, int pos) {
        getDeletePost(blogID);
        position = pos;
    }
}
