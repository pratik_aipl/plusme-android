package com.plus.me.adapter;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.plus.me.R;
import com.plus.me.fragments.MyClosetFragment;
import com.plus.me.listener.GetItemPosition;
import com.plus.me.model.MyCloset;
import com.plus.me.others.PicassoTrustAll;
import com.squareup.picasso.Callback;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class MyClosetAdapter extends RecyclerView.Adapter<MyClosetAdapter.MyViewHolder> {

    Context mContext;
    LayoutInflater inflater;
    ArrayList<MyCloset> categoryList;
    private List<MyCloset> categoryFilterList = new ArrayList<>();
    GetItemPosition getItemPosition;
    public MyClosetAdapter(Context mContext, ArrayList<MyCloset> categoryList, MyClosetFragment myClosetFragment) {
        this.mContext = mContext;
        this.categoryList = categoryList;
        this.getItemPosition = (GetItemPosition) myClosetFragment;
        this.categoryFilterList.addAll(categoryList);
        setHasStableIds(true);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_my_closet, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        MyCloset MyCloset = categoryList.get(position);

      //  holder.tvCategoryName.setText(MyCloset.getCategoryImageUrl());

     //   Picasso.with(mContext.getApplicationContext()).load(MyCloset.getClosetImage()).into(holder.ivImage);
        try {
            PicassoTrustAll.getInstance(mContext)
                    .load(MyCloset.getClosetImage())
                    .into(holder.ivImage, new Callback() {
                        @Override
                        public void onSuccess() {
                            holder.progressBar.setVisibility(View.GONE);
                        }

                        @Override
                        public void onError() {
                        }
                    });
        } catch (Exception e) {
            e.printStackTrace();
        }
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getItemPosition.onIteamPos(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return categoryList.size();
    }

    public MyCloset getItem(int position) {
        return categoryList.get(position);
    }

    public void filters(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());
        categoryList.clear();
        if (charText.length() == 0) {
            categoryList.addAll(categoryFilterList);
        } else {
            for (MyCloset bean : categoryFilterList) {
                /*String Contanint = bean.getFirstName();
                if (bean.getFirstName().toLowerCase(Locale.getDefault())
                        .contains(charText)) {
                    postList.add(bean);
                }*/
            }
        }
        notifyDataSetChanged();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        public ImageView ivImage;
        public TextView tvCategoryName;
        public ProgressBar progressBar;

        public MyViewHolder(View itemView) {
            super(itemView);

            tvCategoryName = itemView.findViewById(R.id.tvCategoryName);
            ivImage = itemView.findViewById(R.id.ivImage);
            progressBar = itemView.findViewById(R.id.progressBar);
        }
    }
}
