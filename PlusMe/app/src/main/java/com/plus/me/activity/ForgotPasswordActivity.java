package com.plus.me.activity;

import android.content.Intent;
import androidx.core.app.ActivityCompat;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.plus.me.R;
import com.plus.me.others.Internet;
import com.plus.me.ws.AsyncTaskListner;
import com.plus.me.ws.CallRequest;
import com.plus.me.ws.Constant;
import com.plus.me.ws.MyConstants;
import com.plus.me.ws.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class ForgotPasswordActivity extends AppCompatActivity implements AsyncTaskListner{

    private EditText etEmail;
    private Button btnSend;
    private TextView tvLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);

        bindWidgetReference();

        bindWidgetEvents();
    }

    private void bindWidgetReference() {

        etEmail = findViewById(R.id.etEmail);
        btnSend = findViewById(R.id.btnSend);
        tvLogin = findViewById(R.id.tvLogin);
    }

    private void bindWidgetEvents() {

        tvLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                forgotPassword();
            }
        });
    }

    private void forgotPassword() {
        String email = etEmail.getText().toString().trim();

        if (!Utils.isValidEmail(email)) {
            etEmail.requestFocus();
            etEmail.setError("Enter valid email");
        } else {
            Map<String, String> map = new HashMap<>();
            map.put("url", MyConstants.BASE_URL + "ForgotPassword");
            map.put("Email", email);

            if (!Internet.isAvailable(ForgotPasswordActivity.this)) {
                Internet.showAlertDialog(ForgotPasswordActivity.this, "Error!", "No Internet Connection", false);
            } else {
                new CallRequest(ForgotPasswordActivity.this).forgotPassword(map);
            }
        }
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        try {

            Utils.hideProgressDialog();
            if (result != null && !result.isEmpty()) {
                Log.i("TAG", "TAG Result : " + result);

                switch (request) {
                    case forgotPassword:
                        try {
                            JSONObject object = new JSONObject(result);
                            boolean success = object.getBoolean("success");
                            if (success) {
                                // TODO parse JSON
                                Utils.showToast(object.getString("message"), this);

                                Intent intent = new Intent(ForgotPasswordActivity.this, LoginActivity.class);
                                startActivity(intent);
                                ActivityCompat.finishAffinity(ForgotPasswordActivity.this);
                            } else {
                                String error_string = object.getString("message");
                                Toast.makeText(ForgotPasswordActivity.this, error_string, Toast.LENGTH_SHORT).show();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onProgressUpdate(String uniqueMessageId, int progres) {

    }

    @Override
    public void onProgressComplete(String uniqueMessageId, String result, Constant.REQUESTS request) {

    }
}
