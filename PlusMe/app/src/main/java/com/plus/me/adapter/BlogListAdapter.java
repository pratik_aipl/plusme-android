package com.plus.me.adapter;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;

import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.PopupMenu;
import android.widget.TextView;

import com.jsibbold.zoomage.ZoomageView;
import com.plus.me.R;
import com.plus.me.fragments.BlogDetailFragment;
import com.plus.me.fragments.BlogListFragment;
import com.plus.me.listener.BlogListener;
import com.plus.me.listener.DeleteBlogListener;
import com.plus.me.model.BlogList;
import com.plus.me.others.App;
import com.plus.me.others.PicassoTrustAll;
import com.plus.me.ws.Utils;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import static com.plus.me.activity.HomeActivity.replaceFragment;

/**
 * Created by Krupa Kakkad on 04 July 2018
 */

public class BlogListAdapter extends RecyclerView.Adapter<BlogListAdapter.MyViewHolder> {

    private static final String TAG = "BlogListAdapter";
    public Context mContext;
    public LayoutInflater inflater;
    public ArrayList<BlogList> blogLists;
    public List<BlogList> blogFilterList = new ArrayList<BlogList>();
    public BlogListener blogListener;
    public BlogListFragment blogListFragment;
    public DeleteBlogListener deleteBlogListener;

    public BlogListAdapter(Context mContext, ArrayList<BlogList> blogLists, BlogListFragment blogListFragment) {
        this.mContext = mContext;
        this.blogLists = blogLists;
        this.blogFilterList.addAll(blogLists);
        this.blogListener = blogListFragment;
        this.blogListFragment = blogListFragment;
        setHasStableIds(true);
        this.deleteBlogListener = blogListFragment;

    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item__sub_bloag, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final BlogList blogList = blogLists.get(position);

       /* holder.tvCommentCount.setText(blogList.getTotalComment() + "Comments");
        holder.tvDescription.setText(blogList.getDescription());
        String jobModifieDate = blogList.getPostTime();
        Utils.getDatesDifferenceInDays(jobModifieDate, holder.tvTime);
        holder.tvTitle.setText(blogList.getBlogTitle());
        holder.tvUserName.setText("by " + blogList.getPostedByName());
        Collections.reverse(blogList.commentList);
        BlogCommentAdapter adapter = new BlogCommentAdapter(mContext, blogList.commentList, blogListFragment);

        LinearLayoutManager layoutManager = new LinearLayoutManager(mContext);
        holder.rvcomment.setLayoutManager(layoutManager);
        holder.rvcomment.setHasFixedSize(true);
        holder.rvcomment.setAdapter(adapter);



        holder.rel_comment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                BlogDetailFragment fragment = new BlogDetailFragment();
                Bundle b = new Bundle();
                b.putString("intent", "blog");
                b.putSerializable("blog", getItem(position).getBlogId());
                b.putSerializable("user", getItem(position).getBlogByID());
                fragment.setArguments(b);
                replaceFragment(fragment, false);
            }
        });
        holder.tvTitle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        holder.tvTitle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ProfileFragment profileFragment = new ProfileFragment();
                Bundle bundle = new Bundle();
                bundle.putString("intent", "profile");
                bundle.putString("id", blogList.getBlogId());
                profileFragment.setArguments(bundle);
                HomeActivity.replaceFragment(profileFragment, false);
            }
        });

        if (!TextUtils.isEmpty(blogList.getPostImage())) {
            try {
                PicassoTrustAll.getInstance(mContext)
                        .load(blogList.getPostImage())
                        .into(holder.ivImage, new Callback() {
                            @Override
                            public void onSuccess() {
                                holder.progressBar.setVisibility(View.GONE);
                            }

                            @Override
                            public void onError() {
                            }
                        });
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (!TextUtils.isEmpty(blogList.getProfileImage())) {
            try {
                PicassoTrustAll.getInstance(mContext)
                        .load(blogList.getProfileImage())
                        .placeholder(R.drawable.avatar)
                        .error(R.drawable.avatar)
                        .into(holder.ivUser, new Callback() {
                            @Override
                            public void onSuccess() {
                            }

                            @Override
                            public void onError() {

                            }
                        });
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        holder.ivImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createDialog(blogLists.get(position).getPostImage());
            }
        });*/

        String jobModifieDate = blogList.getPostTime();
        Utils.getDatesDifferenceInDays(jobModifieDate, holder.tvTime);
        holder.tvDescription.setText(blogList.getDescription());
        holder.tv_title.setText(blogList.getBlogTitle());


        if (!App.user.getId().equalsIgnoreCase(blogLists.get(position).getBlogByID())) {
            holder.tv_Tdots.setVisibility(View.GONE);
        }
        holder.tv_Tdots.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PopupMenu popup = new PopupMenu(mContext, holder.tv_Tdots);
                popup.getMenuInflater().inflate(R.menu.delete_menu, popup.getMenu());

                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    public boolean onMenuItemClick(MenuItem item) {
                        //Toast.makeText(mContext, "You Clicked : " + item.getTitle(), Toast.LENGTH_SHORT).show();
                        deleteBlogListener.onDelete(blogLists.get(position).getBlogId(), position);
                        return true;
                    }
                });

                popup.show();
            }
        });

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                BlogDetailFragment fragment = new BlogDetailFragment();
                Bundle b = new Bundle();
                b.putString("intent", "blog");
                b.putSerializable("blog", getItem(position).getBlogId());
                b.putSerializable("user", getItem(position).getBlogByID());
                fragment.setArguments(b);
                replaceFragment(fragment, false);
            }
        });
    }

    @Override
    public int getItemCount() {
        return blogLists.size();
    }

    public BlogList getItem(int position) {
        return blogLists.get(position);
    }

    public void filters(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());
        blogLists.clear();
        if (charText.length() == 0) {
            blogLists.addAll(blogFilterList);
        } else {
            for (BlogList bean : blogFilterList) {
                String Contanint = bean.getCompanyName();
                if (Contanint.toLowerCase(Locale.getDefault())
                        .contains(charText)) {
                    blogLists.add(bean);
                }
            }
        }
        notifyDataSetChanged();
    }

    private void createDialog(String image) {
        Dialog dialog = new Dialog(mContext, R.style.ChatDialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        // Include dialog.xml file
        dialog.setContentView(R.layout.dialog_view_fullimage);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        lp.copyFrom(window.getAttributes());
        //This makes the dialog take up the full width
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
        ZoomageView zoomageView = dialog.findViewById(R.id.imgDialog);
        try {
            PicassoTrustAll.getInstance(mContext)
                    .load(image)
                    .placeholder(R.drawable.no_image)
                    .error(R.drawable.no_image)
                    .into(zoomageView);
        } catch (Exception e) {
            e.printStackTrace();
        }
        dialog.show();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        public TextView tvTime, tvDescription,tv_title,tv_Tdots;

        public MyViewHolder(View itemView) {
            super(itemView);

            tv_title= itemView.findViewById(R.id.tv_title);
            tvTime = itemView.findViewById(R.id.tvTime);
            tvDescription = itemView.findViewById(R.id.tvDescription);
            tv_Tdots = itemView.findViewById(R.id.tv_Tdots);

        }
    }
}
