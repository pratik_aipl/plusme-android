package com.plus.me.fragments;


import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.core.content.ContextCompat;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.plus.me.R;
import com.plus.me.adapter.NothingSelectedSpinnerAdapter;
import com.plus.me.others.App;
import com.plus.me.utils.FileUtils;
import com.plus.me.utils.ImagePathUtils;
import com.plus.me.utils.Utilities;
import com.plus.me.ws.AsyncTaskListner;
import com.plus.me.ws.CallRequest;
import com.plus.me.ws.Constant;
import com.plus.me.ws.MyConstants;
import com.plus.me.ws.Utils;
import com.squareup.picasso.Picasso;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import static android.app.Activity.RESULT_OK;
import static com.plus.me.activity.HomeActivity.replaceFragment;
import static com.plus.me.activity.HomeActivity.toolbar;
import static com.plus.me.activity.HomeActivity.txtSave;

/**
 * A simple {@link Fragment} subclass.
 */
public class PostNewFragment extends Fragment implements AsyncTaskListner {
    public ImageView ivCamera;

    private static final String TAG = "PostNewFragment";
    public CropImage.ActivityResult result;
    public Uri selectedUri;
    public String filePath, catagory, postTitle;
    public ImageView ivPostImage;
    public TextView txtHint, txtCancel;
    public Spinner spCatagory;
    public Button btnPost;
    public EditText edtPostTitle;
    public ArrayAdapter aaCatagory;
    public PostNewFragment instnce;
    private Dialog dialog;
    public String intent;

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_post_new, container, false);
        App.isFromEditProfile = false;
        App.isFromCreateALook = false;
        App.isAppFromVisionBoard = false;
        App.isFromNewPost = true;
        instnce = this;
        setHasOptionsMenu(true);

        bindWidgetReference(view);

        bindWidgetEvents();

        initializeData();
        if (isReadStorageAllowed(1)) {
            if (getArguments() != null) {
                intent = getArguments().getString("intent");
                if (intent.equalsIgnoreCase("share")) {
                    //FileUtils.getPath(getActivity(),Uri.parse(getArguments().getString("uri")));
                    Uri url = Uri.parse(FileUtils.getPath(getActivity(), Uri.parse(getArguments().getString("uri"))));
                    Log.i("TAG", "url :-> " + url.toString());
                    ivPostImage.setVisibility(View.VISIBLE);
                    MyConstants.imageType = "0";
                    filePath = url.getEncodedPath();
                    //  ivPostImage.setImageURI(url);
                    Picasso.with(getContext()).load(url).into(ivPostImage);

                }
            }
        }
        //   ((HomeActivity)getActivity()).setToolbarTitleFragment("Post New");
        toolbar.setText("Post New");
        return view;
    }

    private boolean isReadStorageAllowed(int ID) {

        //If permission is granted returning true
        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE)
                + ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE)
                + ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA)
                == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA}, ID);
            return false;
        }


    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        Log.i("Requestcode:", "Code :->? " + requestCode);


        if (grantResults.length > 0) {

            boolean readExternalFile = grantResults[0] == PackageManager.PERMISSION_GRANTED;
            boolean writeExternalFile = grantResults[1] == PackageManager.PERMISSION_GRANTED;
            boolean cameraPermission = grantResults[2] == PackageManager.PERMISSION_GRANTED;
            if (readExternalFile && writeExternalFile && cameraPermission) {
                if (requestCode == 1) {
                    try {
                        intent = getArguments().getString("intent");
                        if (intent.equalsIgnoreCase("share")) {
                            //FileUtils.getPath(getActivity(),Uri.parse(getArguments().getString("uri")));
                            Uri url = Uri.parse(FileUtils.getPath(getActivity(), Uri.parse(getArguments().getString("uri"))));
                            Log.i("TAG", "url :-> " + url.toString());
                            ivPostImage.setVisibility(View.VISIBLE);
                            MyConstants.imageType = "0";
                            filePath = url.getEncodedPath();
                            // ivPostImage.setImageURI(url);
                            Picasso.with(getContext()).load(url).into(ivPostImage);

                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    private void initializeData() {

        System.out.println("category String" + MyConstants.categoriesListString.size());
        aaCatagory = new ArrayAdapter<>(getContext(), R.layout.spinner_text, MyConstants.categoriesListString);
        aaCatagory.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spCatagory.setAdapter(new NothingSelectedSpinnerAdapter(
                aaCatagory, R.layout.spinner_text,
                getActivity(), "Select Category"));
    }

    private void createDialog() {
        dialog = new Dialog(getActivity(), R.style.ChatDialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        // Include dialog.xml file
        dialog.setContentView(R.layout.dialog_image_closet_chooser);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        lp.copyFrom(window.getAttributes());
        //This makes the dialog take up the full width
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
        LinearLayout llCloset = dialog.findViewById(R.id.llCloset);
        LinearLayout llGallery = dialog.findViewById(R.id.llGallery);
        ImageView ivVisionBoard = dialog.findViewById(R.id.ivVisionBoard);
        TextView txtMyVisionBoard = dialog.findViewById(R.id.txtMyVisionBoard);

        ivVisionBoard.setImageResource(R.drawable.vision);
        txtMyVisionBoard.setText("My Visionboard");
        llCloset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (TextUtils.isEmpty(catagory)) {
                    Utils.showToast("Select Category ID", getContext());
                } else {
                    Bundle bundle = new Bundle();
                    bundle.putString("CatagoryId", catagory);
                    Log.d(TAG, "CatID: " + catagory);
                    SelectVisionBoardFragment fragment = new SelectVisionBoardFragment();
                    fragment.setArguments(bundle);
                    replaceFragment(fragment, false);


                }
                dialog.dismiss();
            }
        });
        llGallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CropImage.startPickImageActivity(getActivity());
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    private void bindWidgetReference(View view) {
        ivCamera = view.findViewById(R.id.ivCamera);
        ivPostImage = view.findViewById(R.id.ivPostImage);
        txtHint = view.findViewById(R.id.txtHint);
        spCatagory = view.findViewById(R.id.spCatagory);
        btnPost = view.findViewById(R.id.btnPost);
        edtPostTitle = view.findViewById(R.id.edtPostTitle);
        txtCancel = view.findViewById(R.id.txtCancel);
    }

    private void bindWidgetEvents() {
        ivCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createDialog();
            }
        });
        spCatagory.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position > 0) {
                    try {
                        catagory = MyConstants.categoriesList.get(position - 1).getCategoryID();
                    } catch (NullPointerException e) {
                        e.printStackTrace();
                    }
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        ivPostImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // CropImage.startPickImageActivity(getActivity());

                createDialog();
            }
        });
        btnPost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println("MyConstants.imageType===" + MyConstants.imageType);
                postTitle = edtPostTitle.getText().toString();
                if (TextUtils.isEmpty(catagory)) {
                    spCatagory.requestFocus();
                    Utils.showToast("Please Select Category.", getActivity());
                } else if (TextUtils.isEmpty(postTitle)) {
                    edtPostTitle.requestFocus();
                    Utils.showToast("Please Enter Post Title.", getActivity());
                } else if (TextUtils.isEmpty(filePath) && TextUtils.isEmpty(MyConstants.selectedImage)) {
                    Utils.showToast("Please Select Image.", getActivity());
                }/*else if (MyConstants.imageType.equalsIgnoreCase("0")){

                }*/
                /*else if (MyConstants.imageType.equalsIgnoreCase("0")&& !MyConstants.selectedImage.contains("https://")) {
                    if (TextUtils.isEmpty(filePath))
                        Utils.showToast("Please Select Image.", getActivity());
                } else if (MyConstants.imageType.equalsIgnoreCase("1")) {
                    if (TextUtils.isEmpty(MyConstants.selectedImage))
                        Utils.showToast("Please Select Image." + MyConstants.selectedImage + ">>" + MyConstants.imageType, getActivity());
                }*/
                else {
                    Map<String, String> map = new HashMap<>();
                    map.put("url", MyConstants.BASE_URL + "add_new_blog");
                    map.put("PostTitle",  Utilities.convertStringToUTF8(postTitle));
                    map.put("CategoryID", catagory);
                    //Todo
                    map.put("Description", "NA");
                    map.put("header", "");
                    map.put("Auth", App.user.getAuth());
                    if (MyConstants.imageType.equals("0")) {
                        map.put("ImageType", "0");
                        map.put("PostImage", filePath);
                        new CallRequest(PostNewFragment.this).addNewPost(map);
                    } else {
                        map.put("ImageType", "1");
                        map.put("image_url", MyConstants.selectedImage);
                        new CallRequest(PostNewFragment.this).addNewPost(map);
                    }

                }
            }
        });
        txtCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });
    }

    @SuppressLint("NewApi")
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CropImage.PICK_IMAGE_CHOOSER_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            Uri imageUri = CropImage.getPickImageResultUri(getActivity(), data);

            if (CropImage.isReadExternalStoragePermissionsRequired(getActivity(), imageUri)) {
//                selectedUri = imageUri;
                selectedUri = data.getData();
                filePath = new File((ImagePathUtils.getFilePath(getActivity(), selectedUri))).getAbsolutePath();
                System.out.println("file path===" + filePath);
                MyConstants.imageType = "0";
                requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 0);
            } else {
                startCropImageActivity(imageUri);
            }
        }
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                ivPostImage.setVisibility(View.VISIBLE);
                // ivPostImage.setImageURI(result.getUri());
                Picasso.with(getContext()).load(result.getUri()).resize(550, 550).into(ivPostImage);
                selectedUri = result.getUri();
//                filePath = selectedUri.getPath();

                //selectedUri = data.getData();
                filePath = new File((ImagePathUtils.getFilePath(getActivity(), selectedUri))).getAbsolutePath();

           /*     try {
                    filePath =  new Compressor(getActivity()).compressToFile(new File(filePath));
                } catch (IOException e) {
                    e.printStackTrace();
                }*/
                //   Picasso.with(getContext()).load(filePath).into(ivPostImage);


                System.out.println("file path===" + filePath);
                //   ivPostImage.setImageBitmap(BitmapFactory.decodeFile(filePath));
                ivCamera.setVisibility(View.GONE);
                txtHint.setVisibility(View.GONE);
                MyConstants.imageType = "0";
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
            }
        }

    }

    private void startCropImageActivity(Uri imageUri) {
        CropImage.activity(imageUri)
                .setGuidelines(CropImageView.Guidelines.ON)
                .setAspectRatio(1, 1)
                .setFixAspectRatio(false)
                .setMultiTouchEnabled(true)
                .start(getActivity());

    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        txtSave.setVisibility(View.GONE);
        menu.getItem(0).setVisible(true);
        menu.getItem(1).setVisible(false);
        menu.getItem(2).setVisible(true);
        menu.getItem(3).setVisible(false);
        menu.getItem(4).setVisible(false);
        super.onPrepareOptionsMenu(menu);
    }

    @Override
    public void onResume() {
        super.onResume();
        System.out.println("My constant==" + MyConstants.selectedImage);
        if (!TextUtils.isEmpty(MyConstants.selectedImage)) {
            ivCamera.setVisibility(View.GONE);
            txtHint.setVisibility(View.GONE);
            ivPostImage.setVisibility(View.VISIBLE);
            Picasso.with(getContext()).load(MyConstants.selectedImage).into(ivPostImage);
        }
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        try {

            Utils.hideProgressDialog();
            if (result != null && !result.isEmpty()) {
                Log.i("TAG", "TAG Result : " + result);

                switch (request) {
                    case addNewPost:
                        try {
                            JSONObject object = new JSONObject(result);
                            boolean success = object.getBoolean("success");
                            if (success) {
                                // TODO parse JSON
                                Utils.showToast(object.getString("message"), getActivity());
                                MyConstants.selectedImage = "";
                                getActivity().runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        replaceFragment(new HomeFragment(), true);
                                    }
                                });
                            } else {
                                MyConstants.selectedImage = "";
                                Utils.showToast(object.getString("message"), getActivity());
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        App.isFromNewPost = false;
        MyConstants.selectedImage = "";
    }

    @Override
    public void onProgressUpdate(String uniqueMessageId, int progres) {

    }

    @Override
    public void onProgressComplete(String uniqueMessageId, String result, Constant.REQUESTS request) {

    }
}
