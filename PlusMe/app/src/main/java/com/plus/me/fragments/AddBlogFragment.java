package com.plus.me.fragments;

import android.Manifest;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.core.view.MenuItemCompat;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.SearchView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.MultiAutoCompleteTextView;
import android.widget.TextView;

import com.plus.me.R;
import com.plus.me.others.App;
import com.plus.me.ws.AsyncTaskListner;
import com.plus.me.ws.CallRequest;
import com.plus.me.ws.Constant;
import com.plus.me.ws.JsonParserUniversal;
import com.plus.me.ws.MyConstants;
import com.plus.me.ws.Utils;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import static android.app.Activity.RESULT_OK;
import static com.plus.me.activity.HomeActivity.replaceFragment;
import static com.plus.me.activity.HomeActivity.toolbar;
import static com.plus.me.activity.HomeActivity.txtSave;

public class AddBlogFragment extends Fragment implements AsyncTaskListner {
    public AddBlogFragment instance;

    public JsonParserUniversal jParser;
    public Menu menu;
    public SearchView searchView;
    private RecyclerView rvHome, rvCatagoryFilter;

    private static final String ARG_PARAM1 = "param1";
    private static String profImagePath = "";
    public View view;
    private String mParam1, postTitle, postDetail = "";
    public ImageView ivPostImage, ivCamera;

    private static final int PICK_IMAGE_REQUEST_CODE = 1;
    private static final int PICK_IMAGE_PERMISSION_CODE = 1;
    private OnFragmentInteractionListener mListener;
    public Uri selectedUri;
    public Button btnPost;
    public TextView txtCancel;
    public EditText et_title;
    public MultiAutoCompleteTextView et_dis;

    public AddBlogFragment() {
        // Required empty public constructor
    }


    public static AddBlogFragment newInstance(String param1, String param2) {
        AddBlogFragment fragment = new AddBlogFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_add_blog, container, false);
        instance = this;
        ivPostImage = view.findViewById(R.id.ivPostImage);
        ivCamera = view.findViewById(R.id.ivCamera);
        et_dis = view.findViewById(R.id.et_dis);
        et_title = view.findViewById(R.id.et_title);
        btnPost = view.findViewById(R.id.btnPost);
        txtCancel = view.findViewById(R.id.txtCancel);
        MyConstants.imageType = "0";

        setHasOptionsMenu(true);

        ivCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                CropImage.startPickImageActivity(getActivity(), instance);
                //  pickImage();
            }
        });

        btnPost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println("MyConstants.imageType===" + MyConstants.imageType);
                postTitle = et_title.getText().toString();
                postDetail = et_dis.getText().toString();
                if (TextUtils.isEmpty(postTitle)) {

                    et_title.setError("Please Enter Post Title.");
                    et_title.requestFocus();
                } else if (TextUtils.isEmpty(postDetail)) {
                    et_dis.setError("Please Enter Post DIscription.");
                    et_dis.requestFocus();
                } else if (MyConstants.imageType.equalsIgnoreCase("0") && TextUtils.isEmpty(profImagePath) ||
                        MyConstants.imageType.equalsIgnoreCase("1") && TextUtils.isEmpty(MyConstants.selectedImage)) {
                    ivCamera.requestFocus();
                    Utils.showToast("Please Select Image.", getActivity());
                } else {
                    Map<String, String> map = new HashMap<>();
                    map.put("url", MyConstants.BASE_URL + "add_new_blog_feed");
                    map.put("BlogTitle", postTitle);
                    //Todo
                    map.put("Description", postDetail);
                    map.put("header", "");
                    map.put("Auth", App.user.getAuth());
                    if (MyConstants.imageType.equals("0")) {
                        map.put("ImageType", "0");
                        map.put("PostImage", profImagePath);
                        new CallRequest(instance).addNewBlog(map);
                    } else {
                        map.put("ImageType", "1");
                        map.put("image_url", MyConstants.selectedImage);
                        new CallRequest(instance).addNewBlog(map);
                    }

                }
            }
        });
        txtCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });

        return view;
    }


    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        //super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
        inflater.inflate(R.menu.home, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }
    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        txtSave.setVisibility(View.GONE);
        toolbar.setText("Add Blog");
        MyConstants.editImageUrl = "";
        menu.getItem(0).setVisible(true);
        menu.getItem(1).setVisible(false);
        menu.getItem(2).setVisible(false);
        menu.getItem(3).setVisible(false);
        menu.getItem(4).setVisible(false);

        //tabs.setVisibility(View.VISIBLE);
        super.onPrepareOptionsMenu(menu);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CropImage.PICK_IMAGE_CHOOSER_REQUEST_CODE && resultCode == RESULT_OK) {
            Uri imageUri = CropImage.getPickImageResultUri(getActivity(), data);

            if (CropImage.isReadExternalStoragePermissionsRequired(getActivity(), imageUri)) {
                selectedUri = imageUri;
                profImagePath = selectedUri.getPath().toString();
                requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 0);
            } else {
                startCropImageActivity(imageUri);
            }
        }

        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                ivPostImage.setImageURI(result.getUri());
                selectedUri = result.getUri();
                profImagePath = selectedUri.getPath();
                MyConstants.imageType = "0";
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
            }
        }

    }

    private void startCropImageActivity(Uri imageUri) {
        CropImage.activity(imageUri)
                .setGuidelines(CropImageView.Guidelines.ON)
                .setAspectRatio(1, 1)
                .setFixAspectRatio(false)

                .setMultiTouchEnabled(true)
                .start(getActivity(), instance);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        try {

            Utils.hideProgressDialog();
            if (result != null && !result.isEmpty()) {
                Log.i("TAG", "TAG Result : " + result);

                switch (request) {
                    case addNewBlog:
                        try {
                            JSONObject object = new JSONObject(result);
                            boolean success = object.getBoolean("success");
                            if (success) {
                                // TODO parse JSON
                                Utils.showToast(object.getString("message"), getActivity());
                                MyConstants.selectedImage = "";
                                replaceFragment(new UsreWiseBlogListFragment(), true);
                            } else {
                                MyConstants.selectedImage = "";
                                Utils.showToast(object.getString("message"), getActivity());
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onProgressUpdate(String uniqueMessageId, int progres) {

    }

    @Override
    public void onProgressComplete(String uniqueMessageId, String result, Constant.REQUESTS request) {

    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
