package com.plus.me.adapter;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.plus.me.fragments.FollowersFragment;
import com.plus.me.fragments.FollowingFragment;
import com.plus.me.fragments.PostFragment;


/**
 * Created by User on 17-04-2018.
 */

public class ProfileTabsPagerAdapter extends FragmentStatePagerAdapter {

    int mNumOfTabs;

    public ProfileTabsPagerAdapter(FragmentManager fm, int NumOfTabs) {
        super(fm);
        this.mNumOfTabs = NumOfTabs;
    }

    @Override
    public Fragment getItem(int i) {
        switch (i) {
            case 0:
                PostFragment tab1 = new PostFragment();
                return tab1;
            case 1:
                FollowersFragment tab2 = new FollowersFragment();
                return tab2;
            case 2:
                FollowingFragment tab3 = new FollowingFragment();
                return tab3;

        }
        return null;

    }
    @Override
    public int getItemPosition(Object object) {
        // POSITION_NONE makes it possible to reload the PagerAdapter
        return 3;
    }
    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return 3; //No of Tabs
    }


}
