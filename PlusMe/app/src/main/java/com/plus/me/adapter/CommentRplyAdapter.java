package com.plus.me.adapter;

import android.content.Context;

import androidx.recyclerview.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.github.siyamed.shapeimageview.CircularImageView;
import com.plus.me.R;
import com.plus.me.fragments.ViewAllCommentFragment;
import com.plus.me.listener.BlogListener;
import com.plus.me.model.BlogComment;
import com.plus.me.others.PicassoTrustAll;
import com.plus.me.utils.Utilities;
import com.plus.me.ws.Utils;
import com.squareup.picasso.Callback;

import java.util.ArrayList;

public class CommentRplyAdapter extends RecyclerView.Adapter<CommentRplyAdapter.MyViewHolder> {

    public Context mContext;
    public LayoutInflater inflater;
    public ArrayList<BlogComment> blogLists;
    public BlogListener blogListener;
    public ViewAllCommentFragment blogListFragment;


    public CommentRplyAdapter(Context mContext, ArrayList<BlogComment> blogLists, ViewAllCommentFragment blogListFragment) {
        this.mContext = mContext;
        this.blogLists = blogLists;
        this.blogListener = (BlogListener) blogListFragment;
        this.blogListFragment = blogListFragment;
        //
        // Collections.reverse(this.blogLists);
        setHasStableIds(true);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_bloag_comment, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final BlogComment obj = blogLists.get(position);

        holder.tvUserName.setText(obj.getPostedByName());
        holder.tvchattime.setText(Utils.ConvertTime(obj.getTime()));
        holder.tvComment.setText(Utilities.convertUTF8ToString(obj.getComment()));
        if (!TextUtils.isEmpty(obj.getProfileImage())) {
            try {
                PicassoTrustAll.getInstance(mContext)
                        .load(obj.getProfileImage())
                        .placeholder(R.drawable.avatar)
                        .error(R.drawable.avatar)
                        .into(holder.ivUser, new Callback() {
                            @Override
                            public void onSuccess() {
                            }

                            @Override
                            public void onError() {

                            }
                        });
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        holder.img_rply.setVisibility(View.GONE);

    }

    @Override
    public int getItemCount() {

        return blogLists.size();


    }

    public BlogComment getItem(int position) {
        return blogLists.get(position);
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        public CircularImageView ivUser;
        public TextView tvUserName, tvComment,tvchattime;
        public ProgressBar progressBar;
        public ImageView img_rply;

        public MyViewHolder(View itemView) {
            super(itemView);
            ivUser = itemView.findViewById(R.id.ivUser);
            tvUserName = itemView.findViewById(R.id.tvUserName);
            tvComment = itemView.findViewById(R.id.tvComment);
            tvchattime = itemView.findViewById(R.id.tvdatetime);
            progressBar = itemView.findViewById(R.id.progressBar);
            img_rply = itemView.findViewById(R.id.img_rply);

        }
    }
}
