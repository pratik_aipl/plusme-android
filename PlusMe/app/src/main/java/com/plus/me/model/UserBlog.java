package com.plus.me.model;

import java.io.Serializable;
import java.util.ArrayList;

public class UserBlog implements Serializable {

    public String Id = "", PostedByName = "", UserProfileImage = "", CompanyName = "",SearchName="",Searchtittle="",SearchDesc="";

    public String getSearchName() {
        return SearchName;
    }

    public void setSearchName(String searchName) {
        SearchName = searchName;
    }

    public String getSearchtittle() {
        return Searchtittle;
    }

    public void setSearchtittle(String searchtittle) {
        Searchtittle = searchtittle;
    }

    public String getSearchDesc() {
        return SearchDesc;
    }

    public void setSearchDesc(String searchDesc) {
        SearchDesc = searchDesc;
    }

    public ArrayList<BlogList> getBlogArray() {
        return blogArray;
    }

    public void setBlogArray(ArrayList<BlogList> blogArray) {
        this.blogArray = blogArray;
    }

    public String getId() {
        return Id;
    }

    public String getPostedByName() {
        return PostedByName;
    }

    public void setPostedByName(String postedByName) {
        PostedByName = postedByName;
    }

    public String getUserProfileImage() {
        return UserProfileImage;
    }

    public void setUserProfileImage(String userProfileImage) {
        UserProfileImage = userProfileImage;
    }

    public String getCompanyName() {
        return CompanyName;
    }

    public void setCompanyName(String companyName) {
        CompanyName = companyName;
    }

    public void setId(String id) {
        Id = id;
    }

    public ArrayList<BlogList> blogArray = new ArrayList<>();
}
