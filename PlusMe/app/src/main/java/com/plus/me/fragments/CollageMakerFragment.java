package com.plus.me.fragments;


import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.media.ThumbnailUtils;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Environment;
import androidx.transition.TransitionManager;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.plus.me.R;
import com.plus.me.activity.CropActivity;
import com.plus.me.activity.CropMultiActivity;
import com.plus.me.activity.GalleryActivity;
import com.plus.me.activity.HomeActivity;
import com.plus.me.adapter.NothingSelectedSpinnerAdapter;
import com.plus.me.others.App;
import com.plus.me.utils.ImageDownloaderCache;
import com.plus.me.ws.AsyncTaskListner;
import com.plus.me.ws.CallRequest;
import com.plus.me.ws.Constant;
import com.plus.me.ws.MyConstants;
import com.plus.me.ws.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import static android.app.Activity.RESULT_OK;
import static com.plus.me.activity.HomeActivity.ivDown;
import static com.plus.me.activity.HomeActivity.toolbar;
import static com.plus.me.activity.HomeActivity.txtSave;
import static com.plus.me.ws.MyConstants.INTENT_KEY_GALLERY_SELECTMULTI;
import static com.plus.me.ws.MyConstants.INTENT_RESULT_GALLERY;

/**
 * A simple {@link Fragment} subclass.
 */
public class CollageMakerFragment extends Fragment implements AsyncTaskListner {
    private static final String TAG = "CollageMakerFragment";
    public CollageMakerFragment instace;
    View vSelect;
    public static Dialog dialogCollage;
    private Button btnSaveCollage, btnCancelCollage;
    private Button btnVisionBoardSave, btnMyClosetSave, btnCancel, btnSaveAsMyCloset;
    ImageView imgView;
    int miScreenWidth;
    int miScreenHeight;
    String masterImagePath;
    String workingImagePath;
    String[] masterImageList = null;
    Bitmap workingBitmap = null;
    String selectedOutputPath = "";
    public boolean doOwerWrite = false;
    public String catagory;
    public boolean toVisionBoar = false, toMyCloset = false;
    private FrameLayout flCropImage;
    MenuItem itemSave;
    private View.OnClickListener btnChangePhoto_OnClickListener = new View.OnClickListener() {
        public void onClick(View v) {
            //check for read external storage permision
            if (ActivityCompat.checkSelfPermission(getActivity(), android.Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                Intent intent = new Intent(getActivity(), GalleryActivity.class);
                intent.putExtra(INTENT_KEY_GALLERY_SELECTMULTI, true);
                startActivityForResult(intent, INTENT_RESULT_GALLERY);
            } else {
                ActivityCompat.requestPermissions(getActivity(), new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE}, MyConstants.PERMISSIONS_REQUEST_READ_STORAGE);
            }
        }
    };


    public CollageMakerFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_collage_maker, container, false);
        instace = this;
        setHasOptionsMenu(true);

        bindWidgetReference(view);

        loadImagesFromGallery();

        toolbar.setText("Collage Maker");

        return view;
    }

    private void bindWidgetReference(View view) {
        flCropImage = view.findViewById(R.id.flCropImage);
    }

    private void loadImagesFromGallery() {
        ViewTreeObserver vto3 = flCropImage.getViewTreeObserver();
        vto3.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {

            @Override
            public void onGlobalLayout() {
                if (flCropImage.getWidth() > 0) {
                    miScreenWidth = flCropImage.getWidth();
                }
                if (flCropImage.getHeight() > 0) {
                    miScreenHeight = flCropImage.getHeight();
                }

                LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

                //add the CropViews to the frame
                //int borderSize = getActivity().getResources().getDimensionPixelSize(R.dimen.merge_photo_border);
                //int overlayHeight = getActivity().getResources().getDimensionPixelSize(R.dimen.progress_photo_overlay_height);
                //width and height the same to make view square
                int imgW = miScreenWidth;
                int imgH = miScreenWidth;
                int xPos = 0;
                int yPos = 0;

                //Add views
                FrameLayout.LayoutParams paramsBefore = new FrameLayout.LayoutParams(imgW, imgH);
                /*paramsBefore.leftMargin = xPos;
                paramsBefore.topMargin = yPos;*/
                vSelect = inflater.inflate(R.layout.include_photo_select, null);
                RelativeLayout rlSelectPhotoBefore = vSelect.findViewById(R.id.rlSelectPhoto);
                rlSelectPhotoBefore.setOnClickListener(btnChangePhoto_OnClickListener);

                int centreX = (miScreenWidth - vSelect.getWidth()) / 2;

                int centreY = (miScreenHeight - vSelect.getHeight()) / 2;

                paramsBefore.leftMargin = xPos;
                paramsBefore.topMargin = yPos;
                paramsBefore.gravity = Gravity.CENTER;

                flCropImage.addView(vSelect, paramsBefore);

                //flCropImage.setLayoutParams(new FrameLayout.LayoutParams(vSelect.getWidth(), vSelect.getHeight()));

                //add the image view
                imgView = new ImageView(getActivity());
                imgView.setVisibility(View.INVISIBLE);
                //use the image view to change the image - maybe add abutton to do this
                imgView.setOnClickListener(btnChangePhoto_OnClickListener);
                flCropImage.addView(imgView, paramsBefore);

                ViewTreeObserver obs = flCropImage.getViewTreeObserver();
                obs.removeOnGlobalLayoutListener(this);
            }

        });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        //make sure to delete the working file
        if (workingImagePath != null && !workingImagePath.isEmpty()) {
            getActivity().deleteFile(MyConstants.IMAGE_WORKING_FILE);
        }

        if (workingBitmap != null && !workingBitmap.isRecycled()) {
            workingBitmap.recycle();
            workingBitmap = null;
        }


    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        // Make sure the request was successful
        if (resultCode == RESULT_OK) {
            if (requestCode == INTENT_RESULT_GALLERY) {
                if (data != null) {
                    Log.d(TAG, "dattttttaaa: " + data.toString());
                    masterImageList = null;
                    masterImagePath = null;
                    String imgpath = null;
                    boolean isBitmap = false;

                    if (data.hasExtra("isBitmap")) {
                        isBitmap = true;
                    }

                    if (data.hasExtra("image_path")) {
                        imgpath = data.getStringExtra("image_path");
                    } else if (data.hasExtra("image_path_list")) {
                        masterImageList = data.getStringArrayExtra("image_path_list");
                        if (masterImageList != null && masterImageList.length == 1) {
                            //only one image
                            imgpath = masterImageList[0];
                        }
                    }


                    if (imgpath != null && !imgpath.isEmpty()) {
                        masterImagePath = imgpath;
                        if (isBitmap) {
                            try {
                                URL url = new URL(masterImagePath);
                                workingBitmap = BitmapFactory.decodeStream(url.openConnection().getInputStream());
                            } catch (IOException e) {
                                System.out.println(e);
                            }
                        } else {
                            workingBitmap = ImageDownloaderCache.decodeSampledBitmapFromPath(masterImagePath, ImageDownloaderCache.maxImageSize, ImageDownloaderCache.maxImageSize);
                        }
                    } else if (masterImageList != null && masterImageList.length > 0) {

                        /*  adjust image demisions to fit  */
                        if (!getActivity().isFinishing()) {
                            ProgressDialog progressDialog1 = ProgressDialog.show(getActivity(), "", "Merging Photos...", true, false);

                            //int borderSize = 10;
                            int borderSize = getActivity().getResources().getDimensionPixelSize(R.dimen.margin_4);
                            int count = masterImageList.length;
                            //don't allow more than 6 images
                            if (count > 6) {
                                count = 6;
                            }
                            Bitmap[] bitmapList = new Bitmap[count];
                            for (int i = 0; i < count; i++) {
                                Bitmap bmImage = null;
                                if (isBitmap) {

                                    try {
                                        URL url = new URL(masterImagePath);
                                        bmImage = BitmapFactory.decodeStream(url.openConnection().getInputStream());
                                    } catch (IOException e) {
                                        System.out.println(e);
                                    }
                                } else {
                                    bmImage = ImageDownloaderCache.decodeSampledBitmapFromPath(masterImageList[i], ImageDownloaderCache.maxImageSize, ImageDownloaderCache.maxImageSize, false);

                                }
                                if (bmImage != null) {
                                    int imgW = Math.min(bmImage.getWidth(), bmImage.getHeight()) - borderSize;
                                    int imgH = imgW;
                                    if (count <= 3) {
                                        //int dimension = Math.min(bmImage.getWidth(), bmImage.getHeight());
                                        imgW = (ImageDownloaderCache.maxImageSize - ((count - 1) * borderSize)) / count;
                                        imgH = ImageDownloaderCache.maxImageSize;
                                        //bitmapList[i] = ThumbnailUtils.extractThumbnail(bmImage, imgW, imgDownloader.maxImageSize, ThumbnailUtils.OPTIONS_RECYCLE_INPUT);
                                    } else if (count == 4) {
                                        //int dimension = Math.min(bmImage.getWidth(), bmImage.getHeight()) - borderSize;
                                        imgW = ((ImageDownloaderCache.maxImageSize - borderSize) / 2);
                                        imgH = imgW;
                                        //bitmapList[i] = ThumbnailUtils.extractThumbnail(bmImage, imgWH, imgWH, ThumbnailUtils.OPTIONS_RECYCLE_INPUT);
                                    } else if (count == 5) {
                                        if (i < 2) {
                                            imgW = ((ImageDownloaderCache.maxImageSize - borderSize) / 2);
                                            imgH = imgW;
                                            //bitmapList[i] = ThumbnailUtils.extractThumbnail(bmImage, imgWH, imgWH, ThumbnailUtils.OPTIONS_RECYCLE_INPUT);
                                        } else {
                                            imgW = ((ImageDownloaderCache.maxImageSize - borderSize) / 2);
                                            //imgH = (imgDownloader.maxImageSize - (2 * borderSize)) / 3;
                                            //note this formula returns the ceil value instead of the floor - from https://stackoverflow.com/questions/7139382/java-rounding-up-to-an-int-using-math-ceil
                                            int a = (ImageDownloaderCache.maxImageSize - (2 * borderSize));
                                            int b = 3;
                                            imgH = (a - 1) / b + 1;
                                            //bitmapList[i] = ThumbnailUtils.extractThumbnail(bmImage, imgW, imgH, ThumbnailUtils.OPTIONS_RECYCLE_INPUT);
                                        }
                                    } else if (count == 6) {
                                        if (i < 1) {
                                            int img3d = ((ImageDownloaderCache.maxImageSize - borderSize) / 3);
                                            //int a = (imgDownloader.maxImageSize - borderSize);
                                            //int b = 3;
                                            //int img3d = (a - 1) / b + 1;
                                            //imgW = (img3d*2) - borderSize; //size is 2/3
                                            imgW = (img3d * 2); //size is 2/3
                                            //imgH = imgW + borderSize;
                                            //imgW = (img3d*2) + (borderSize / 2); //size is 2/3
                                            //calculate the differnce facter and add it to the large image
                                            int imgW1 = (ImageDownloaderCache.maxImageSize - (2 * borderSize)) / 3;
                                            int dif1 = ImageDownloaderCache.maxImageSize - (imgW + borderSize + imgW1);
                                            //int dif2 = imgDownloader.maxImageSize - ((imgW1*3)+(borderSize*2));
                                            //add the extra to the single large image
                                            //imgW += dif1+dif2;
                                            imgW += dif1;
                                            imgH = imgW;
                                            //if (MyConstants.DEBUG) Log.d(TAG, "borderSize : "+borderSize);
                                            //if (MyConstants.DEBUG) Log.d(TAG, "one third : "+img3d);
                                            //if (MyConstants.DEBUG) Log.d(TAG, "two thirds - border : "+imgW);
                                            //if (MyConstants.DEBUG) Log.d(TAG, "dif1 : "+dif1);
                                            //if (MyConstants.DEBUG) Log.d(TAG, "dif2 : "+dif2);
                                        } else {
                                            imgW = (ImageDownloaderCache.maxImageSize - (2 * borderSize)) / 3;
                                            if (i == 5) {
                                                int dif2 = ImageDownloaderCache.maxImageSize - ((imgW * 3) + (borderSize * 2));
                                                if (dif2 > 0) {
                                                    //add extra into last image
                                                    imgW += dif2;
                                                }
                                            }
                                            //note this formula returns the ceil value instead of the floor - from https://stackoverflow.com/questions/7139382/java-rounding-up-to-an-int-using-math-ceil
                                            //int a = (imgDownloader.maxImageSize - (2 * borderSize));
                                            //int b = 3;
                                            //imgW = (a - 1) / b + 1;
                                            //imgW = (img3d) - (borderSize*2/3);  //size is 1/3;
                                            //imgW = img3d - borderSize;  //size is 1/3;
                                            //imgW = img3d;  //size is 1/3;
                                            imgH = imgW;
                                        }
                                    }
                                    bitmapList[i] = ThumbnailUtils.extractThumbnail(bmImage, imgW, imgH, ThumbnailUtils.OPTIONS_RECYCLE_INPUT);
                                    //if (MyConstants.DEBUG) {
                                    //    Log.d(TAG, "Image : "+(i+1)+" width: " +bitmapList[i].getWidth());
                                    //    Log.d(TAG, "Image : "+(i+1)+" height: " + bitmapList[i].getHeight());
                                    //}
                                }
                            }
                            //workingBitmap = Bitmap.createBitmap(bitmapList[0].getWidth() * 2, bitmapList[0].getHeight() * 2, Bitmap.Config.ARGB_8888);
                            workingBitmap = Bitmap.createBitmap(ImageDownloaderCache.maxImageSize, ImageDownloaderCache.maxImageSize, Bitmap.Config.ARGB_8888);

                            Canvas canvas = new Canvas(workingBitmap);
                            canvas.drawColor(Color.WHITE);
                            Paint paint = new Paint();
                            int xPos = 0;
                            int yPos = 0;
                            count = bitmapList.length;
                            for (int i = 0; i < bitmapList.length; i++) {
                                //if (MyConstants.DEBUG) {
                                //    Log.d(TAG, "Image : "+(i)+" xPos: " +xPos);
                                //    Log.d(TAG, "Image : "+(i)+" yPos: " + yPos);
                                //    //Log.d(TAG, "even? : "+(i % 2 ));
                                //    Log.d(TAG, "even? : "+(i % 2 ));
                                //}
                                //canvas.drawBitmap(bitmapList[i], bitmapList[i].getWidth() * (i % 2), bitmapList[i].getHeight() * (i / 2), paint);
                                if (bitmapList[i] != null) {
                                    canvas.drawBitmap(bitmapList[i], xPos, yPos, paint);
                                }
                                //if (count <= 3 && i == 2) {
                                //    yPos = bitmapList[i].getHeight()+borderSize;
                                //}
                                //if (count == 4 && i == 1) {
                                //    yPos = bitmapList[i].getHeight()+borderSize;
                                //}
                                if (count == 4) {
                                    //if (i % 2 == 0) {
                                    //    //move to next column
                                    //    xPos += bitmapList[i].getWidth()+borderSize;
                                    //}
                                    //else {
                                    //    xPos = 0;
                                    //}
                                    /*
                                    if (i == 0 || i == 2) {
                                        //move next row
                                        yPos += bitmapList[i].getHeight()+borderSize;
                                    }
                                    if (i == 1) {
                                        //move to next column
                                        yPos = 0;
                                        xPos += bitmapList[i].getWidth()+borderSize;
                                    }
                                    */
                                    if (i % 2 == 0) {
                                        //move next row
                                        yPos += bitmapList[i].getHeight() + borderSize;
                                    } else {
                                        //move to next column
                                        yPos = 0;
                                        xPos += bitmapList[i].getWidth() + borderSize;
                                    }
                                } else if (count == 5) {
                                    if (i == 1) {
                                        //move to next column
                                        yPos = 0;
                                        xPos += bitmapList[i].getWidth() + borderSize;
                                    } else {
                                        //move next row
                                        yPos += bitmapList[i].getHeight() + borderSize;
                                    }
                                } else if (count == 6) {
                                    if (i == 1) {
                                        //move to next column in the same row
                                        xPos += bitmapList[i].getWidth() + borderSize;
                                    } else if (i == 2) {
                                        //move to next column
                                        yPos = 0;
                                        xPos += bitmapList[i].getWidth() + borderSize;
                                        int xPos2 = bitmapList[0].getWidth() + borderSize;
                                        if (xPos2 > xPos) {
                                            xPos = xPos2;
                                        }
                                    } else {
                                        //move next row
                                        yPos += bitmapList[i].getHeight() + borderSize;
                                    }
                                } else {
                                    //this is for 2 or 3 images
                                    //move to next column
                                    xPos += bitmapList[i].getWidth() + borderSize;
                                }
                            }

                            if (progressDialog1 != null && progressDialog1.isShowing()) {
                                progressDialog1.dismiss();
                            }

                        }
                    }

                    if (workingBitmap != null) {
                        TransitionManager.beginDelayedTransition(flCropImage);

                        if (imgView != null) {
                            if (vSelect != null) {
                                vSelect.setVisibility(View.INVISIBLE);
                            }
                            imgView.setImageBitmap(workingBitmap);
                            imgView.setVisibility(View.VISIBLE);
                        }


                        //save the working image
                        try {
                            FileOutputStream out = getActivity().openFileOutput(MyConstants.IMAGE_WORKING_FILE, Context.MODE_PRIVATE);
                            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                            workingBitmap.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
                            out.write(bytes.toByteArray());
                            out.close();
                            File workingImageFile = getActivity().getFileStreamPath(MyConstants.IMAGE_WORKING_FILE);
                            if (workingImageFile != null) {
                                workingImagePath = workingImageFile.getAbsolutePath();
                            }
                        } catch (Exception e) {

                        }

                        //call to crop image - maybe do as button later
                        if (masterImagePath != null && !masterImagePath.isEmpty()) {
                            Intent pcIntent = new Intent(getActivity(), CropActivity.class);
                            pcIntent.putExtra(MyConstants.INTENT_KEY_CROP_IMGPATH, masterImagePath);
                            startActivityForResult(pcIntent, MyConstants.INTENT_RESULT_CROP);

                        } else if (masterImageList != null && masterImageList.length > 0) {
                            //open multi image crop
                            Intent pcIntent = new Intent(getActivity(), CropMultiActivity.class);
                            pcIntent.putExtra(MyConstants.INTENT_KEY_CROP_IMGPATHLIST, masterImageList);
                            startActivityForResult(pcIntent, MyConstants.INTENT_RESULT_CROP);

                        }


                    }
                    itemSave.setVisible(true);

                }
            } else if (requestCode == MyConstants.INTENT_RESULT_CROP) {
                try {
                    String workingImagePath = null;
                    if (data.hasExtra("image_path")) {
                        workingImagePath = data.getStringExtra("image_path");
                    }
                    //File workingImageFile = getActivity().getFileStreamPath(MyConstants.IMAGE_WORKING_FILE);
                    if (workingImagePath != null) {
                        // workingImagePath = workingImageFile.getAbsolutePath();
                        if (!workingImagePath.isEmpty()) {
                            //workingBitmap = BitmapFactory.decodeStream(getActivity().openFileInput(MyConstants.IMAGE_WORKING_FILE));
                          /*  if (workingBitmap != null && !workingBitmap.isRecycled()) {
                                TransitionManager.beginDelayedTransition(flCropImage);

                                if (imgView != null) {
                                    if (vSelect != null) {
                                        vSelect.setVisibility(View.INVISIBLE);
                                    }*/
                            Bitmap myBitmap = BitmapFactory.decodeFile(workingImagePath);

                            imgView.setImageBitmap(myBitmap);
                            imgView.setVisibility(View.VISIBLE);
                            //  }
                            // }

                        }
                    }

                } catch (Exception e) {
                    //Log.d(TAG,e.getMessage()+e.getStackTrace());
                    //return null;
                }

            }


        }
    }


    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        try {
            txtSave.setVisibility(View.GONE);
            ivDown.setVisibility(View.GONE);
            itemSave = menu.findItem(R.id.item_save);
            itemSave.setVisible(false);
           /* menu.getItem(0).setVisible(true);
            menu.getItem(1).setVisible(false);
            menu.getItem(2).setVisible(false);
            menu.getItem(3).setVisible(false);*/
        } catch (Exception e) {
            e.printStackTrace();
        }
        super.onPrepareOptionsMenu(menu);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
        inflater.inflate(R.menu.menu_collage_maker, menu);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.item_home) {

        } else if (id == R.id.item_save) {
            openSaveDialogCollage();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void openSaveDialogCollage() {
        createSaveDialogCollage();
        initSaveDialogComponentsCollage();
    }

    private void createSaveDialogCollage() {
        dialogCollage = new Dialog(getActivity(), R.style.ChatDialog);
        dialogCollage.requestWindowFeature(Window.FEATURE_NO_TITLE);
        // Include dialog.xml file
        dialogCollage.setContentView(R.layout.dialog_save_image);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialogCollage.getWindow();
        lp.copyFrom(window.getAttributes());
        lp.dimAmount = 0.0f; // Dim level. 0.0 - no dim, 1.0 - completely opaque
        //This makes the dialog take up the full width
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
        dialogCollage.show();
    }

    private void initSaveDialogComponentsCollage() {
        btnVisionBoardSave = dialogCollage.findViewById(R.id.btnVisonboardSave);
        btnMyClosetSave = dialogCollage.findViewById(R.id.btnMyCloset);
        btnMyClosetSave.setVisibility(View.GONE);
        btnSaveAsMyCloset = dialogCollage.findViewById(R.id.btnNewMyCloset);
        btnCancel = dialogCollage.findViewById(R.id.btnCancel);
        btnVisionBoardSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toVisionBoar = true;
                returnBackWithSavedImageCollage();
            }
        });
        btnMyClosetSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toVisionBoar = false;
                toMyCloset = true;
                returnBackWithSavedImageCollage();

            }
        });
        btnSaveAsMyCloset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toVisionBoar = false;
                toMyCloset = true;
                doOwerWrite = true;
                returnBackWithSavedImageCollage();

            }
        });
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogCollage.dismiss();
            }
        });

    }

    private void returnBackWithSavedImage() {
        //updateView(View.GONE);
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(
                RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
        layoutParams.addRule(RelativeLayout.CENTER_IN_PARENT, RelativeLayout.TRUE);

        new CountDownTimer(1000, 500) {
            public void onTick(long millisUntilFinished) {

            }

            public void onFinish() {
                String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
                String imageName = "IMG_" + timeStamp + ".jpg";
                /*Intent returnIntent = new Intent();
                returnIntent.putExtra("imagePath", photoEditorSDK.saveImage("PlusMe", imageName));
                getActivity().setResult(Activity.RESULT_OK, returnIntent);*/

                System.out.println("path:::" + selectedOutputPath);
              /*  if (!TextUtils.isEmpty(savedPath)) {
                    Toast.makeText(getActivity(), "File saved successfully", Toast.LENGTH_SHORT).show();
                }*/

                if (toVisionBoar) {
                    dialogViewImage(selectedOutputPath);
                }
                if (toMyCloset) {
                    moveToMyCloset(selectedOutputPath);
                }
            }
        }.start();
    }

    private void moveToMyCloset(String savedPath) {
        Map<String, String> map = new HashMap<>();
        map.put("url", MyConstants.BASE_URL + "MoveToCloset");
        map.put("PostImage", savedPath);
        map.put("Title", "");
        if (doOwerWrite) {
            map.put("BaseImageID", MyConstants.baseImageId);
        } else {
            map.put("BaseImageID", "");
        }
        map.put("header", "");
        map.put("Type", "IMAGE");
        map.put("Auth", App.user.getAuth());
        new CallRequest(instace).moveToCloset(map);
    }

    private void dialogViewImage(final String image) {

        final Dialog dialogSaveVisionBoard = new Dialog(getActivity());
        dialogSaveVisionBoard.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogSaveVisionBoard.setCancelable(true);
        dialogSaveVisionBoard.setContentView(R.layout.dialog_image_move_tocloset);
        dialogSaveVisionBoard.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        ImageView ivImage = dialogSaveVisionBoard.findViewById(R.id.ivImage);
        Button btnMoveToVisionBoard = dialogSaveVisionBoard.findViewById(R.id.btnMoveToCloset);

        Spinner spCatagory = dialogSaveVisionBoard.findViewById(R.id.spCatagory);

        System.out.println("country list==" + MyConstants.categoriesList.size());
        ArrayAdapter aaCountry = new ArrayAdapter<>(getActivity(), R.layout.spinner_text_black, MyConstants.categoriesListString);
        aaCountry.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spCatagory.setAdapter(new NothingSelectedSpinnerAdapter(
                aaCountry, R.layout.spinner_text_black,
                getContext(), "Select Category"));
        try {
            Bitmap bitmap = BitmapFactory.decodeFile(image);
            ivImage.setImageBitmap(bitmap);
        } catch (Exception e) {
            e.printStackTrace();
        }

        btnMoveToVisionBoard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (TextUtils.isEmpty(catagory)) {
                    Utils.showToast("Please Select Category", getActivity());
                } else {
                    dialogSaveVisionBoard.dismiss();
                    //  doOwerWrite = true;
                    moveToVisionBoard(image);


                }
            }
        });
        spCatagory.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                try {
                    catagory = MyConstants.categoriesList.get(position - 1).getCategoryID();
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        dialogSaveVisionBoard.show();
    }

    private void moveToVisionBoard(String selectedOutputPath) {
        Map<String, String> map = new HashMap<>();
        map.put("url", MyConstants.BASE_URL + "MoveToVisionBoard");
        map.put("PostImage", selectedOutputPath);
        map.put("CategoryID", catagory);
        map.put("header", "");
        map.put("Type", "IMAGE");
        map.put("Auth", App.user.getAuth());
        new CallRequest(instace).addToVisionBoard(map);
    }

    private void returnBackWithSavedImageCollage() {
        String imageName = "Collage_" + System.currentTimeMillis() + ".jpg";

        File mediaStorageDir = new File(
                Environment.getExternalStorageDirectory(), "PlusMe");
        // Create a storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Log.d("PhotoEditorSDK", "Failed to create directory");
            }
        }
        // Create a media file name
        selectedOutputPath = mediaStorageDir.getPath() + File.separator + imageName;
        Log.d("PhotoEditorSDK", "selected camera path " + selectedOutputPath);
        File file = new File(selectedOutputPath);
        try {
            FileOutputStream out = new FileOutputStream(file);
            if (flCropImage != null) {
                flCropImage.setDrawingCacheEnabled(true);
                flCropImage.getDrawingCache().compress(Bitmap.CompressFormat.JPEG, 90, out);
            }
            out.flush();
            out.close();

            Toast.makeText(getActivity(), "File saved successfully", Toast.LENGTH_SHORT).show();

        } catch (Exception e) {
            e.printStackTrace();
        }
        returnBackWithSavedImage();

    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        try {

            Utils.hideProgressDialog();
            if (result != null && !result.isEmpty()) {
                Log.i("TAG", "TAG Result : " + result);

                switch (request) {
                    case addToVisionBoard:
                        try {
                            JSONObject object = new JSONObject(result);
                            boolean success = object.getBoolean("success");
                            if (success) {
                                // TODO parse JSON
                                Utils.showToast(object.getString("message"), getContext());
                                dialogCollage.dismiss();
                                //dialogSaveVisionBoard.dismiss();
                                MyConstants.isFromCreateLook = true;
                                HomeActivity.replaceFragment(new VisionBoardLatestFragment(), false);
                                App.edit = false;

                            } else {
                                Utils.showToast(object.getString("message"), getContext());
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;

                    case moveTocloset:
                        try {
                            JSONObject object = new JSONObject(result);
                            boolean success = object.getBoolean("success");
                            if (success) {
                                // TODO parse JSON
                                Utils.showToast(object.getString("message"), getContext());
                                dialogCollage.dismiss();
                                MyConstants.isFromCreateLook = true;
                                HomeActivity.replaceFragment(new HomeFragment(), false);
                                App.edit = false;
                            } else {
                                Utils.showToast(object.getString("message"), getContext());
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        dialogCollage.dismiss();
                        break;
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onProgressUpdate(String uniqueMessageId, int progres) {

    }

    @Override
    public void onProgressComplete(String uniqueMessageId, String result, Constant.REQUESTS request) {

    }

}
