package com.plus.me.fragments;


import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Environment;
import android.provider.MediaStore;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager.widget.ViewPager;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.ahmedadeltito.photoeditorsdk.BrushDrawingView;
import com.ahmedadeltito.photoeditorsdk.OnPhotoEditorSDKListener;
import com.ahmedadeltito.photoeditorsdk.PhotoEditorSDK;
import com.ahmedadeltito.photoeditorsdk.ViewType;
import com.plus.me.R;
import com.plus.me.activity.ChooseImageActivity;
import com.plus.me.activity.GalleryActivity;
import com.plus.me.activity.HomeActivity;
import com.plus.me.adapter.NothingSelectedSpinnerAdapter;
import com.plus.me.custom_views.SlidingUpPanelLayout;
import com.plus.me.others.App;
import com.plus.me.others.PicassoTrustAll;
import com.plus.me.ws.AsyncTaskListner;
import com.plus.me.ws.CallRequest;
import com.plus.me.ws.Constant;
import com.plus.me.ws.MyConstants;
import com.plus.me.ws.Utils;
import com.squareup.picasso.Callback;
import com.viewpagerindicator.PageIndicator;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.plus.me.activity.HomeActivity.ivDown;
import static com.plus.me.activity.HomeActivity.replaceFragment;
import static com.plus.me.activity.HomeActivity.toolbar;
import static com.plus.me.activity.HomeActivity.txtSave;
import static com.plus.me.ws.MyConstants.INTENT_KEY_GALLERY_SELECTMULTI;
import static com.plus.me.ws.MyConstants.INTENT_RESULT_GALLERY;
import static com.plus.me.ws.MyConstants.isEditedNowInCreateLook;

/**
 * A simple {@link Fragment} subclass.
 */
public class CreateALookFragment extends MediaFragment implements OnPhotoEditorSDKListener, AsyncTaskListner {
    public static Dialog dialogCollage;
    private Button btnSaveVisionBoardCollage, btnSaveAsMyCloset, btnCancelCollage, btnSaveNewMyClosetCollage;
    public static ImageView imgView;
    public static int miScreenWidth;
    public static int miScreenHeight;
    public static String masterImagePath;
    public static String workingImagePath;
    public static String masterImageList[] = null;
    private ArrayList<Bitmap> masterImageBitmapList = new ArrayList<Bitmap>();
    public static Bitmap workingBitmap = null;
    public static View vSelect;
    public static Dialog dialog, dialog1;
    public LinearLayout llFromMyCloset;
    public LinearLayout llCollageMaker;
    public static String subImage;
    private static SlidingUpPanelLayout mLayout;
    private static PhotoEditorSDK photoEditorSDK;
    private ImageView photoEditImageView;
    private Activity activity;
    private LinearLayout llCamera, llGallery;
    private Button btnVisionBoardSave, btnMyClosetSave, btnCancel;
    private boolean isSubImage = false;
    private PageIndicator indicator;
    private ViewPager pager;
    private List<Fragment> fragmentsList = new ArrayList<>();
    private RelativeLayout parentImageRelativeLayout;
    public static FrameLayout flCropImage;
    MenuInflater inflater1;
    private boolean isCollageMaker = false;
    public static String imgpath = null;
    MenuItem itemReset;
    // MenuItem itemAddNew;
    MenuItem itemSave;
    Dialog dialogSaveVisionBoard;
    MenuItem itemAddNewCollage;
    MenuItem itemSaveCollage;
    public String catagory;
    protected String selectedImagePath;

    public boolean doOwerWrite = false;
    public boolean toVisionBoar = false, toMyCloset = false;
    FloatingActionButton fabAdd, fabEdit;


    public CreateALookFragment() {
        // Required empty public constructor
    }

    public static void addImage(Bitmap image) {

        if (isEditedNowInCreateLook) {
            photoEditorSDK.parentView.removeViewAt(0);
        }
        photoEditorSDK.addImage(image);
        if (mLayout != null) {
        /*    if (mLayout.getDrawingCache() != null && !mLayout.getDrawingCache().isRecycled()) {
                mLayout.getDrawingCache().recycle();

            }*/
            mLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);

        }
    }

    private View.OnClickListener btnChangePhoto_OnClickListener = new View.OnClickListener() {
        public void onClick(View v) {


            createDilogForMyvisionBordOrGallery();
            //check for read external storage permision

        }
    };

    private void createDilogForMyvisionBordOrGallery() {
        final Dialog dialogCollage = new Dialog(this.activity, R.style.ChatDialog);
        dialogCollage.requestWindowFeature(Window.FEATURE_NO_TITLE);
        // Include dialogCollage.xml file
        dialogCollage.setContentView(R.layout.dialog_choose_collage_maker);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialogCollage.getWindow();
        lp.copyFrom(window.getAttributes());
        //This makes the dialogCollage take up the full width
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
        LinearLayout llFromMyVisionBoard = dialogCollage.findViewById(R.id.llFromMyVisionBoard);
        LinearLayout llGallery = dialogCollage.findViewById(R.id.llGallery);
        llGallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogCollage.dismiss();
                if (ActivityCompat.checkSelfPermission(getActivity(), android.Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {

                    Intent intent = new Intent(getActivity(), GalleryActivity.class);
                    intent.putExtra(INTENT_KEY_GALLERY_SELECTMULTI, true);
                    startActivityForResult(intent, INTENT_RESULT_GALLERY);

                } else {
                    ActivityCompat.requestPermissions(getActivity(), new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE}, MyConstants.PERMISSIONS_REQUEST_READ_STORAGE);
                }
            }
        });

       /* llFromMyVisionBoard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ActivityCompat.checkSelfPermission(getActivity(), android.Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {


                    Intent intent = new Intent(getActivity(), GalleryActivityVisionBoard.class);
                    intent.putExtra(INTENT_KEY_GALLERY_SELECTMULTI, true);
                    startActivityForResult(intent, INTENT_RESULT_VISIONBOARD);


                } else {
                    ActivityCompat.requestPermissions(getActivity(), new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE}, MyConstants.PERMISSIONS_REQUEST_READ_STORAGE);
                }
            }
        });*/
        dialogCollage.show();
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_vision_board, container, false);

        Toast.makeText(activity, "Look", Toast.LENGTH_SHORT).show();
        setHasOptionsMenu(true);
        bindWidgetReference(view);
        initializeData();

        return view;
    }

    private void openCollageMaker() {
        itemReset.setVisible(false);
        //  itemAddNew.setVisible(false);
        itemSave.setVisible(false);
        itemSaveCollage.setVisible(true);
        toolbar.setText("Collage Maker");
        loadImagesFromGallery();
        isCollageMaker = true;
    }

    private void loadImagesFromGallery() {
        ViewTreeObserver vto3 = flCropImage.getViewTreeObserver();
        vto3.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {

            @Override
            public void onGlobalLayout() {
                if (flCropImage.getWidth() > 0) {
                    miScreenWidth = flCropImage.getWidth();
                }
                if (flCropImage.getHeight() > 0) {
                    miScreenHeight = flCropImage.getHeight();
                }

                LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

                //add the CropViews to the frame
                //int borderSize = getActivity().getResources().getDimensionPixelSize(R.dimen.merge_photo_border);
                //int overlayHeight = getActivity().getResources().getDimensionPixelSize(R.dimen.progress_photo_overlay_height);
                //width and height the same to make view square
                int imgW = miScreenWidth;
                int imgH = miScreenWidth;
                int xPos = 0;
                int yPos = 0;

                //Add views
                FrameLayout.LayoutParams paramsBefore = new FrameLayout.LayoutParams(imgW, imgH);
                /*paramsBefore.leftMargin = xPos;
                paramsBefore.topMargin = yPos;*/
                vSelect = inflater.inflate(R.layout.include_photo_select, null);
                RelativeLayout rlSelectPhotoBefore = (RelativeLayout) vSelect.findViewById(R.id.rlSelectPhoto);
                rlSelectPhotoBefore.setOnClickListener(btnChangePhoto_OnClickListener);

                int centreX = (miScreenWidth - vSelect.getWidth()) / 2;

                int centreY = (miScreenHeight - vSelect.getHeight()) / 2;

                paramsBefore.leftMargin = xPos;
                paramsBefore.topMargin = yPos;
                paramsBefore.gravity = Gravity.CENTER;

                flCropImage.addView(vSelect, paramsBefore);

                //flCropImage.setLayoutParams(new FrameLayout.LayoutParams(vSelect.getWidth(), vSelect.getHeight()));

                //add the image view
                imgView = new ImageView(getActivity());
                imgView.setVisibility(View.INVISIBLE);
                //use the image view to change the image - maybe add abutton to do this
                imgView.setOnClickListener(btnChangePhoto_OnClickListener);
                flCropImage.addView(imgView, paramsBefore);

                ViewTreeObserver obs = flCropImage.getViewTreeObserver();
                obs.removeOnGlobalLayoutListener(this);
            }

        });
    }

    private void initializeData() {
        toolbar.setText("Vision Board");
        activity = getActivity();
        isSubImage = true;
        photoEditImageView.setImageResource(R.drawable.white_squere);
        fabAdd.setVisibility(View.VISIBLE);
        if (!TextUtils.isEmpty(MyConstants.editImageUrl)) {
            isSubImage = true;
            //  Picasso.with(getContext()).load(MyConstants.editImageUrl).into(photoEditImageView);

            Utils.showProgressDialog(getContext());
            try {
                PicassoTrustAll.getInstance(getContext())
                        .load(MyConstants.editImageUrl)
                        .into(photoEditImageView, new Callback() {
                            @Override
                            public void onSuccess() {
                                Utils.hideProgressDialog();
                            }

                            @Override
                            public void onError() {
                            }
                        });
            } catch (Exception e) {
                e.printStackTrace();
            }
            FileOutputStream out = null;


            System.out.println("subImage===" + subImage);
            MyConstants.isEditable = true;
            fabEdit.setVisibility(View.VISIBLE);
            fabEdit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    isEditedNowInCreateLook = true;
                    photoEditImageView.setImageResource(R.drawable.white_squere);
                    Intent mIntent = new Intent(getActivity(), ChooseImageActivity.class);
                    Bundle mBundle = new Bundle();
                    mBundle.putString("image", MyConstants.editImageUrl);
                    mBundle.putString("isEdit", "yes");
                    mIntent.putExtras(mBundle);
                    startActivity(mIntent);

                }
            });

        } else {
            photoEditImageView.setImageResource(R.drawable.white_squere);
        }
    }

    private String saveToInternalStorage(String path) {

        FileOutputStream fos = null;

        ContextWrapper cw = new ContextWrapper(getContext());
        File directory = cw.getDir("imageDir", Context.MODE_PRIVATE);
        try {
            URL url = new URL(path);
            Bitmap image = BitmapFactory.decodeStream(url.openConnection().getInputStream());

            File mypath = new File(directory, "profile.jpg");


            fos = new FileOutputStream(mypath);
            // Use the compress method on the BitMap object to write image to the OutputStream
            image.compress(Bitmap.CompressFormat.JPEG, 100, fos);
            fos.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return directory.getAbsolutePath();
    }


    private void bindWidgetReference(View view) {
        photoEditImageView = view.findViewById(R.id.photo_edit_iv);
        parentImageRelativeLayout = view.findViewById(R.id.parent_image_rl);
        pager = view.findViewById(R.id.image_emoji_view_pager);
        indicator = view.findViewById(R.id.image_emoji_indicator);
        RelativeLayout deleteRelativeLayout = view.findViewById(R.id.delete_rl);
        TextView deleteTextView = view.findViewById(R.id.delete_tv);
        BrushDrawingView brushDrawingView = view.findViewById(R.id.drawing_view);
        mLayout = view.findViewById(R.id.sliding_layout);
        flCropImage = view.findViewById(R.id.flCropImage);
        fabAdd = view.findViewById(R.id.fabAdd);
        fabEdit = view.findViewById(R.id.fabEdit);
        if (TextUtils.isEmpty(MyConstants.selectedImage)) {
            fabAdd.setVisibility(View.GONE);
        } else {
            fabAdd.setVisibility(View.VISIBLE);
        }
        fabAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                isEditedNowInCreateLook = false;
              /*  if (photoEditImageView.getDrawable() != null) {
                    isSubImage = true;
                } else {
                    isSubImage = false;
                }*/
                openImageChooserDialog();
            }
        });

        Typeface newFont = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Eventtus-Icons.ttf");

        photoEditorSDK = new PhotoEditorSDK.PhotoEditorSDKBuilder(getActivity())
                .parentView(parentImageRelativeLayout) // add parent image view
                .childView(photoEditImageView) // add the desired image view
                .deleteView(deleteRelativeLayout) // add the deleted view that will appear during the movement of the views
                .brushDrawingView(brushDrawingView) // add the brush drawing view that is responsible for drawing on the image view
                .buildPhotoEditorSDK(); // build photo editor sdk
        photoEditorSDK.setOnPhotoEditorSDKListener(this);

        deleteTextView.setTypeface(newFont);
    }

    private void openImageChooserDialog() {

        // dialog.dismiss();
        flCropImage.setVisibility(View.GONE);
        mLayout.setVisibility(View.VISIBLE);
        photoEditImageView.setVisibility(View.VISIBLE);
        isCollageMaker = false;
        createDialog();
        initDialogComponents();
    }

    private void createDialog() {
        dialog = new Dialog(this.activity, R.style.ChatDialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        // Include dialog.xml file
        dialog.setContentView(R.layout.dialog_image_chooser);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        lp.copyFrom(window.getAttributes());
        //This makes the dialog take up the full width
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
        dialog.show();
    }

    private void initDialogComponents() {
        llCamera = dialog.findViewById(R.id.llCamera);
        llGallery = dialog.findViewById(R.id.llGallery);
        llFromMyCloset = dialog.findViewById(R.id.llFromMyCloset);
        llCollageMaker = dialog.findViewById(R.id.llCollageMaker);

        if (MyConstants.isEditable) {
            llFromMyCloset.setVisibility(View.GONE);
            llCollageMaker.setVisibility(View.GONE);
        }
        llCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //startCameraActivity();
                if (isReadStorageAllowed()) {
                    Intent photoPickerIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    photoPickerIntent.putExtra(MediaStore.EXTRA_OUTPUT,
                            getOutputMediaFile());
                    photoPickerIntent.putExtra("outputFormat",
                            Bitmap.CompressFormat.JPEG.toString());
                    startActivityForResult(
                            Intent.createChooser(photoPickerIntent, getString(R.string.upload_picker_title)),
                            CAMERA_CODE);
                } else {
                    fabAdd.setVisibility(View.GONE);

                }

            }
        });

        llGallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //openGallery();
                fabAdd.setVisibility(View.VISIBLE);
            }
        });


        llFromMyCloset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                replaceFragment(new SelectClosetImageFragment(), false);
                dialog.dismiss();
                fabAdd.setVisibility(View.VISIBLE);
            }
        });
        llCollageMaker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                itemSave.setVisible(false);
                itemSaveCollage.setVisible(true);
                mLayout.setVisibility(View.GONE);
                fabAdd.setVisibility(View.VISIBLE);
                openCollageMaker();
                flCropImage.setVisibility(View.VISIBLE);
                fabAdd.setVisibility(View.VISIBLE);
                //  replaceFragment(new CollageMakerFragment(),false);
            }
        });
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        txtSave.setVisibility(View.GONE);
        ivDown.setVisibility(View.GONE);
        toolbar.setText("Create a Look");
        itemReset = menu.findItem(R.id.item_reset);
        // itemAddNew = menu.findItem(R.id.item_add_new);
        itemSave = menu.findItem(R.id.item_save);
        itemSaveCollage = menu.findItem(R.id.item_save_collage);
        itemReset.setVisible(true);
        // itemAddNew.setVisible(true);
        itemSave.setVisible(true);
        itemSaveCollage.setVisible(false);

        if (MyConstants.imageType.equalsIgnoreCase("1")) {
            setMyClosetData();
            MyConstants.imageType = "";
        }

        /*menu.getItem(0).setVisible(false);
        menu.getItem(1).setVisible(false);
        menu.getItem(2).setVisible(false);
        menu.getItem(3).setVisible(false);*/

        super.onPrepareOptionsMenu(menu);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
        inflater = getActivity().getMenuInflater();
        inflater.inflate(R.menu.menu_vision_board, menu);
    }


    public void setMyClosetData() {
        if (photoEditImageView != null)
            if (!TextUtils.isEmpty(MyConstants.selectedImage)) {
                //  Picasso.with(getContext()).load(MyConstants.selectedImage).into(photoEditImageView);
                try {
                    PicassoTrustAll.getInstance(getContext())
                            .load(MyConstants.editImageUrl)
                            .into(photoEditImageView, new Callback() {
                                @Override
                                public void onSuccess() {
                                    Utils.hideProgressDialog();
                                }

                                @Override
                                public void onError() {
                                }
                            });
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
    }

    private void openSaveDialogCollage() {
        createSaveDialogCollage();
        initSaveDialogComponentsCollage();
    }

    private void createSaveDialogCollage() {
        dialogCollage = new Dialog(getActivity(), R.style.ChatDialog);
        dialogCollage.requestWindowFeature(Window.FEATURE_NO_TITLE);
        // Include dialog.xml file
        dialogCollage.setContentView(R.layout.dialog_save_image);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialogCollage.getWindow();
        lp.copyFrom(window.getAttributes());
        lp.dimAmount = 0.0f; // Dim level. 0.0 - no dim, 1.0 - completely opaque
        //This makes the dialog take up the full width
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        dialogCollage.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        window.setAttributes(lp);
        dialogCollage.show();
    }

    private void dialogViewImage(final String image) {

        dialogSaveVisionBoard = new Dialog(getActivity());
        dialogSaveVisionBoard.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogSaveVisionBoard.setCancelable(true);
        dialogSaveVisionBoard.setContentView(R.layout.dialog_image_move_tocloset);
        dialogSaveVisionBoard.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        ImageView ivImage = dialogSaveVisionBoard.findViewById(R.id.ivImage);
        Button btnMoveToVisionBoard = dialogSaveVisionBoard.findViewById(R.id.btnMoveToCloset);

        Spinner spCatagory = dialogSaveVisionBoard.findViewById(R.id.spCatagory);

        System.out.println("country list==" + MyConstants.categoriesList.size());
        ArrayAdapter aaCountry = new ArrayAdapter<>(getActivity(), R.layout.spinner_text_black, MyConstants.categoriesListString);
        aaCountry.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spCatagory.setAdapter(new NothingSelectedSpinnerAdapter(
                aaCountry, R.layout.spinner_text_black,
                getContext(), "Select Category"));
        try {
            Bitmap bitmap = BitmapFactory.decodeFile(image);
            ivImage.setImageBitmap(bitmap);
        } catch (Exception e) {
            e.printStackTrace();
        }

        btnMoveToVisionBoard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (TextUtils.isEmpty(catagory)) {
                    Utils.showToast("Please Select Category", getActivity());
                } else {

                    //  doOwerWrite = true;
                    moveToVisionBoard(image);
                    dialogSaveVisionBoard.dismiss();


                }
            }
        });
        spCatagory.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                try {
                    catagory = MyConstants.categoriesList.get(position - 1).getCategoryID();
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        dialogSaveVisionBoard.show();
    }

    private void initSaveDialogComponentsCollage() {
        btnSaveVisionBoardCollage = dialogCollage.findViewById(R.id.btnVisonboardSave);
        btnSaveAsMyCloset = dialogCollage.findViewById(R.id.btnMyCloset);
        btnSaveNewMyClosetCollage = dialogCollage.findViewById(R.id.btnNewMyCloset);
        btnCancelCollage = dialogCollage.findViewById(R.id.btnCancel);
        if (!MyConstants.isEditable) {
            btnSaveNewMyClosetCollage.setVisibility(View.GONE);
        }

        btnSaveVisionBoardCollage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                toVisionBoar = true;
                toMyCloset = false;
                doOwerWrite = false;
                returnBackWithSavedImageCollage();
                dialogCollage.dismiss();

            }
        });
        btnSaveAsMyCloset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toVisionBoar = false;
                toMyCloset = true;
                doOwerWrite = false;
                returnBackWithSavedImageCollage();
                dialogCollage.dismiss();
            }
        });
        btnSaveNewMyClosetCollage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toVisionBoar = false;
                toMyCloset = true;
                doOwerWrite = true;
                returnBackWithSavedImageCollage();
                dialogCollage.dismiss();
            }
        });


        btnCancelCollage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogCollage.dismiss();
            }
        });
    }

    private void returnBackWithSavedImageCollage() {
        String imageName = "Collage_" + System.currentTimeMillis() + ".jpg";
        String selectedOutputPath = "";
        File mediaStorageDir = new File(
                Environment.getExternalStorageDirectory(), "PlusMe");
        // Create a storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Log.d("PhotoEditorSDK", "Failed to create directory");
            }
        }
        // Create a media file name
        selectedOutputPath = mediaStorageDir.getPath() + File.separator + imageName;
        Log.d("PhotoEditorSDK", "selected camera path " + selectedOutputPath);

        File file = new File(selectedOutputPath);
        try {
            FileOutputStream out = new FileOutputStream(file);
            if (flCropImage != null) {
                flCropImage.setDrawingCacheEnabled(true);
                flCropImage.getDrawingCache().compress(Bitmap.CompressFormat.JPEG, 90, out);
            }
            out.flush();
            out.close();

            //  Toast.makeText(getActivity(), "File saved successfully", Toast.LENGTH_SHORT).show();

        } catch (Exception e) {
            e.printStackTrace();
        }
        if (toVisionBoar) {
            dialogViewImage(selectedOutputPath);
        }
        if (toMyCloset) {
            moveToMyCloset(selectedOutputPath);
        }
    }

    private void moveToVisionBoard(String selectedOutputPath) {
        Map<String, String> map = new HashMap<>();
        map.put("url", MyConstants.BASE_URL + "MoveToVisionBoard");
        map.put("PostImage", selectedOutputPath);
        map.put("CategoryID", catagory);
        map.put("header", "");
        map.put("Type", "IMAGE");

        map.put("Auth", App.user.getAuth());
        new CallRequest(CreateALookFragment.this).addToVisionBoard(map);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

            /*if (id == R.id.item_home) {
                replaceFragment(new HomeFragment(),false);
            } else*/


        if (id == R.id.item_reset) {
            clearAllViews();
            return true;
        } else if (id == R.id.item_add_new) {
         /*   if (photoEditImageView.getDrawable() != null) {
                isSubImage = true;
            } else {
                isSubImage = false;
            }
            openImageChooserDialog();*/
            return true;
        } else if (id == R.id.item_save) {
            openSaveDialog();
            return true;
        } else if (id == R.id.item_save_collage) {

            openSaveDialogCollage();
            return true;
        } else if (id == R.id.item_home) {
            replaceFragment(new HomeFragment(), true);
        }

        return super.onOptionsItemSelected(item);
    }

    private void clearAllViews() {
        photoEditorSDK.clearAllViews();
    }

    private void openSaveDialog() {
        createSaveDialog();
        initSaveDialogComponents();
    }

    private void createSaveDialog() {
        dialog1 = new Dialog(this.activity, R.style.ChatDialog);
        dialog1.requestWindowFeature(Window.FEATURE_NO_TITLE);
        // Include dialog.xml file
        dialog1.setContentView(R.layout.dialog_save_image);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog1.getWindow();
        lp.copyFrom(window.getAttributes());
        dialog1.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        lp.dimAmount = 0.0f; // Dim level. 0.0 - no dim, 1.0 - completely opaque
        //This makes the dialog take up the full width
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
        dialog1.show();
    }

    private void initSaveDialogComponents() {
        btnVisionBoardSave = dialog1.findViewById(R.id.btnVisonboardSave);
        btnMyClosetSave = dialog1.findViewById(R.id.btnMyCloset);
        btnSaveAsMyCloset = dialog1.findViewById(R.id.btnNewMyCloset);
        btnCancel = dialog1.findViewById(R.id.btnCancel);
        if (!MyConstants.isEditable) {
            btnSaveAsMyCloset.setVisibility(View.GONE);
        }


        btnVisionBoardSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toVisionBoar = true;
                returnBackWithSavedImage();
            }
        });
        btnMyClosetSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toVisionBoar = false;
                toMyCloset = true;
                returnBackWithSavedImage();

            }
        });
        btnSaveAsMyCloset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toVisionBoar = false;
                toMyCloset = true;
                doOwerWrite = true;
                returnBackWithSavedImage();

            }
        });
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog1.dismiss();
            }
        });
    }

    @Override
    protected void onPhotoTaken() {
        dialog.dismiss();

        System.out.println("sub image::" + selectedImagePath);
        subImage = selectedImagePath;
        //pager.setCurrentItem(0);
        Intent mIntent = new Intent(getActivity(), ChooseImageActivity.class);
        Bundle mBundle = new Bundle();
        mBundle.putString("image", subImage);
        mBundle.putString("isEdit", "No");
        mIntent.putExtras(mBundle);
        startActivity(mIntent);
        //mLayout.setPanelState(SlidingUpPanelLayout.PanelState.EXPANDED);

    }

    private void returnBackWithSavedImage() {
        //updateView(View.GONE);
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(
                RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
        layoutParams.addRule(RelativeLayout.CENTER_IN_PARENT, RelativeLayout.TRUE);
        parentImageRelativeLayout.setBackgroundColor(Color.parseColor("#ffffff"));
        parentImageRelativeLayout.setLayoutParams(layoutParams);

        new CountDownTimer(1000, 500) {
            public void onTick(long millisUntilFinished) {

            }

            public void onFinish() {
                String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
                String imageName = "IMG_" + timeStamp + ".jpg";
                /*Intent returnIntent = new Intent();
                returnIntent.putExtra("imagePath", photoEditorSDK.saveImage("PlusMe", imageName));
                getActivity().setResult(Activity.RESULT_OK, returnIntent);*/
                String savedPath = photoEditorSDK.saveImage("PlusMe", imageName);
                System.out.println("path:::" + savedPath);
              /*  if (!TextUtils.isEmpty(savedPath)) {
                    Toast.makeText(getActivity(), "File saved successfully", Toast.LENGTH_SHORT).show();
                }*/

                if (toVisionBoar) {
                    dialogViewImage(savedPath);
                }
                if (toMyCloset) {
                    moveToMyCloset(savedPath);
                }
            }
        }.start();
    }

    private void moveToMyCloset(String savedPath) {

        Map<String, String> map = new HashMap<>();
        map.put("url", MyConstants.BASE_URL + "MoveToCloset");
        map.put("PostImage", savedPath);
        map.put("Title", "");
        if (doOwerWrite) {
            map.put("BaseImageID", MyConstants.baseImageId);
        } else {
            map.put("BaseImageID", "");
        }
        map.put("header", "");
        map.put("Auth", App.user.getAuth());
        new CallRequest(CreateALookFragment.this).moveToCloset(map);
    }

    private void setPhotoAndEditing(String mainImage) {
        BitmapFactory.Options options = new BitmapFactory.Options();
        //  options.inSampleSize = 1;
        Bitmap bitmap = BitmapFactory.decodeFile(mainImage);
        //  Bitmap bitmap = BitmapFactory.decodeFile(mainImage, options);
        System.out.println("bitmap image===" + bitmap);
        photoEditImageView.setVisibility(View.VISIBLE);
        photoEditImageView.setImageBitmap(bitmap);

       /* if(bitmap !=null && !bitmap.isRecycled()) {
            bitmap.recycle();
            bitmap = null;
        }*/
    }

    @Override
    public void onEditTextChangeListener(String text, int colorCode) {

    }

    @Override
    public void onAddViewListener(ViewType viewType, int numberOfAddedViews) {

    }

    @Override
    public void onRemoveViewListener(int numberOfAddedViews) {

    }

    @Override
    public void onStartViewChangeListener(ViewType viewType) {

    }

    @Override
    public void onStopViewChangeListener(ViewType viewType) {

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        MyConstants.editImageUrl = "";
        MyConstants.selectedImage = "";
        MyConstants.baseImageId = "";
        if (workingImagePath != null && !workingImagePath.isEmpty()) {
            getActivity().deleteFile(MyConstants.IMAGE_WORKING_FILE);
        }
        if (workingBitmap != null && !workingBitmap.isRecycled()) {
            workingBitmap.recycle();
            workingBitmap = null;
        }
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        try {

            Utils.hideProgressDialog();
            if (result != null && !result.isEmpty()) {
                Log.i("TAG", "TAG Result : " + result);

                switch (request) {
                    case addToVisionBoard:
                        try {
                            JSONObject object = new JSONObject(result);
                            boolean success = object.getBoolean("success");
                            if (success) {
                                // TODO parse JSON
                                Utils.showToast(object.getString("message"), getContext());
                                dialog1.dismiss();
                                dialogSaveVisionBoard.dismiss();
                                MyConstants.isFromCreateLook = true;
                                HomeActivity.replaceFragment(new VisionBoardLatestFragment(), false);


                            } else {
                                Utils.showToast(object.getString("message"), getContext());
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;

                    case moveTocloset:
                        try {
                            JSONObject object = new JSONObject(result);
                            boolean success = object.getBoolean("success");
                            if (success) {
                                // TODO parse JSON
                                Utils.showToast(object.getString("message"), getContext());
                                dialog1.dismiss();
                                MyConstants.isFromCreateLook = true;
                                HomeActivity.replaceFragment(new HomeFragment(), false);

                            } else {
                                Utils.showToast(object.getString("message"), getContext());
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        dialog1.dismiss();
                        break;
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onProgressUpdate(String uniqueMessageId, int progres) {

    }

    @Override
    public void onProgressComplete(String uniqueMessageId, String result, Constant.REQUESTS request) {

    }


    private class PreviewSlidePagerAdapter extends FragmentStatePagerAdapter {
        private List<Fragment> mFragments;

        PreviewSlidePagerAdapter(FragmentManager fm, List<Fragment> fragments) {
            super(fm);
            mFragments = fragments;
        }

        @Override
        public Fragment getItem(int position) {
            if (mFragments == null) {
                return (null);
            }
            return mFragments.get(position);
        }

        /*@Override
        public int getItemPosition(Object object) {
            // POSITION_NONE makes it possible to reload the PagerAdapter
            return POSITION_NONE;
        }*/

        @Override
        public int getCount() {
            return mFragments.size();
        }
    }
}
