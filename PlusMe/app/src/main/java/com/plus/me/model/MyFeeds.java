package com.plus.me.model;

import java.io.Serializable;

public class MyFeeds implements Serializable {
    public String BlogId="";
    public String ProfileImage = "";
    public String PostTitle = "";
    public String PostedByName = "";
    public String PostTime = "";
    public String TotalComment = "";
    public String PostImage = "";
    public String IsCloset = "";
    public String Description = "";
    public String RegisterUserTypeByName = "";
    public String PostedByID="";
    public String isLike="";
    public String TotalLike="";

    public String getTotalLike() {
        return TotalLike;
    }

    public void setTotalLike(String totalLike) {
        TotalLike = totalLike;
    }

    public String getIsLike() {
        return isLike;
    }

    public void setIsLike(String isLike) {
        this.isLike = isLike;
    }

    public String getProfileImage() {
        return ProfileImage;
    }

    public void setProfileImage(String profileImage) {
        ProfileImage = profileImage;
    }

    public String getPostTitle() {
        return PostTitle;
    }

    public void setPostTitle(String postTitle) {
        PostTitle = postTitle;
    }

    public String getPostedByName() {
        return PostedByName;
    }

    public void setPostedByName(String postedByName) {
        PostedByName = postedByName;
    }

    public String getPostTime() {
        return PostTime;
    }

    public void setPostTime(String postTime) {
        PostTime = postTime;
    }

    public String getTotalComment() {
        return TotalComment;
    }

    public void setTotalComment(String totalComment) {
        TotalComment = totalComment;
    }

    public String getPostImage() {
        return PostImage;
    }

    public void setPostImage(String postImage) {
        PostImage = postImage;
    }

    public String getIsCloset() {
        return IsCloset;
    }

    public void setIsCloset(String isCloset) {
        IsCloset = isCloset;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public String getRegisterUserTypeByName() {
        return RegisterUserTypeByName;
    }

    public void setRegisterUserTypeByName(String registerUserTypeByName) {
        RegisterUserTypeByName = registerUserTypeByName;
    }

    public String getBlogId() {
        return BlogId;
    }

    public void setBlogId(String blogId) {
        BlogId = blogId;
    }

    public String getPostedByID() {
        return PostedByID;
    }

    public void setPostedByID(String postedByID) {
        PostedByID = postedByID;
    }
}
