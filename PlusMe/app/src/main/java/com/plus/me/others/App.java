package com.plus.me.others;

import android.app.Application;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.StrictMode;
import androidx.multidex.MultiDex;
import androidx.multidex.MultiDexApplication;


import com.androidnetworking.AndroidNetworking;
import com.crashlytics.android.Crashlytics;

import com.onesignal.OneSignal;
import com.plus.me.model.User;
import com.plus.me.one_signal.NotificationOpenedHandler;
import com.plus.me.one_signal.NotificationReceivedHandler;
import com.plus.me.utils.MySharedPref;

import io.fabric.sdk.android.Fabric;

import java.lang.reflect.Method;
import java.util.ArrayList;

import static com.plus.me.ws.MyConstants.MY_PREF_NAME;

/**
 * Created by Sagar Sojitra on 5/16/2016.
 */
public class App extends Application {

    public static App getInstance() {
        return instance;
    }

    public static App instance;
    public SharedPreferences sharedPref;
    public String recentChatId = "";
    public String messageBadgeVal = "";
    public static User user = null;
    public static String  Category= "";
    public static String  noti_type= "",noti_redirect_id="";

    public static String HEALTH_TOPIC = "health_tips",intent="",SHARE_URI="";
    public static boolean isHomeFragment=false;
    public static boolean isFromHomeFragment=false;
    public static boolean isFromNewPost=false;
    public static boolean isFromEditProfile=false, isFromCreateALook =false,isFromPostNew=false;
    public static boolean isAppFromVisionBoard = false;
    public static ArrayList<Bitmap> bitmapArrayList = new ArrayList<>();
    public static Bitmap bitmap,bitmapCemera;
    public static int mnotificationItemCount = 0;
    public static boolean crop=false, inMuCloset=false,edit =false,inMuClosetEdit = false,firstTimeEdit=false,isFromNoti=false;
    /*public ArrayList<Discount> discountArray = new ArrayList<>();
    public ArrayList<ChatMessage> chatArray = new ArrayList<>();
    public ArrayList<FireMessage> fireChatArray = new ArrayList<>();*/

    @Override
    public void onCreate() {
        super.onCreate();
        MySharedPref.MySharedPref(getApplicationContext());
        Fabric.with(this, new Crashlytics());
        OneSignal.startInit(this)
                .inFocusDisplaying(OneSignal.OSInFocusDisplayOption.Notification)
                .setNotificationReceivedHandler(new NotificationReceivedHandler())
                .setNotificationOpenedHandler(new NotificationOpenedHandler(getApplicationContext()))
                .unsubscribeWhenNotificationsAreDisabled(true)
                .init();


        AndroidNetworking.initialize(getApplicationContext());

        if(Build.VERSION.SDK_INT>=24){
            try{
                Method m = StrictMode.class.getMethod("disableDeathOnFileUriExposure");
                m.invoke(null);
            }catch(Exception e){
                e.printStackTrace();
            }
        }

        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2) {
            builder.detectFileUriExposure();
        }

        try {
            instance = this;
            user = new User();
            sharedPref = getSharedPreferences(MY_PREF_NAME, 0);
            MultiDex.install(this);


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onTerminate() {

        super.onTerminate();
    }

    public App() {
        setInstance(this);
    }


    public static void setInstance(App instance) {
        App.instance = instance;
    }
}
