package com.plus.me.model;

import java.io.Serializable;


public class CommentDetail implements Serializable{

   public String CommentedByName="",
           UserProfileImage="",
           Comment="",UserId="";
    public String BlogCommentId="";
    public String ParentCommentID="";
    public String BlogId="";
    public String Time="";

    public String getTime() {
        return Time;
    }

    public void setTime(String time) {
        Time = time;
    }

    public String getBlogId() {
        return BlogId;
    }

    public void setBlogId(String blogId) {
        BlogId = blogId;
    }

    public String getParentCommentID() {
        return ParentCommentID;
    }

    public void setParentCommentID(String parentCommentID) {
        ParentCommentID = parentCommentID;
    }

    public String getBlogCommentId() {
        return BlogCommentId;
    }

    public void setBlogCommentId(String blogCommentId) {
        BlogCommentId = blogCommentId;
    }

    public String getName() {
        return CommentedByName;
    }

    public void setName(String name) {
        this.CommentedByName = name;
    }

    public String getUserProfileImage() {
        return UserProfileImage;
    }


    public void setUserProfileImage(String userProfileImage) {
        this.UserProfileImage = userProfileImage;
    }

    public String getComment() {
        return Comment;
    }

    public void setComment(String comment) {
        this.Comment = comment;
    }

    public String getCommentedByName() {
        return CommentedByName;
    }

    public void setCommentedByName(String commentedByName) {
        CommentedByName = commentedByName;
    }

    public String getUserId() {
        return UserId;
    }

    public void setUserId(String userId) {
        UserId = userId;
    }
}
