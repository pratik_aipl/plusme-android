package com.plus.me.activity;

import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import com.google.android.material.navigation.NavigationView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.core.content.res.ResourcesCompat;
import androidx.core.view.GravityCompat;
import androidx.core.view.MenuItemCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.github.siyamed.shapeimageview.CircularImageView;
import com.plus.me.R;
import com.plus.me.adapter.NothingSelectedSpinnerAdapter;
import com.plus.me.fragments.BlogDetailFragment;
import com.plus.me.fragments.CreateALookFragment;
import com.plus.me.fragments.DesignersFragment;
import com.plus.me.fragments.EditProfileFragment;
import com.plus.me.fragments.HomeFragment;
import com.plus.me.fragments.MyFeedDetailFragment;
import com.plus.me.fragments.NotificationFragment;
import com.plus.me.fragments.PostNewFragment;
import com.plus.me.fragments.ProfileFragment;
import com.plus.me.fragments.SettingFragment;
import com.plus.me.fragments.StoresFragment;
import com.plus.me.fragments.TermsOfUseFragment;
import com.plus.me.fragments.TrendsetterFragment;
import com.plus.me.fragments.UsreWiseBlogListFragment;
import com.plus.me.fragments.VisionBoardLatestFragment;
import com.plus.me.model.Categories;
import com.plus.me.model.City;
import com.plus.me.model.Country;
import com.plus.me.model.State;
import com.plus.me.model.UserProfile;
import com.plus.me.others.App;
import com.plus.me.others.PicassoTrustAll;
import com.plus.me.utils.FileUtils;
import com.plus.me.utils.MySharedPref;
import com.plus.me.ws.AsyncTaskListner;
import com.plus.me.ws.CallRequest;
import com.plus.me.ws.Constant;
import com.plus.me.ws.JsonParserUniversal;
import com.plus.me.ws.MyConstants;
import com.plus.me.ws.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class HomeActivity extends AppCompatActivity implements View.OnClickListener, AsyncTaskListner {

    // tags used to attach the fragments
    public static final String TAG_HOME = "home";
    public static final String TAG_POST_NEW = "post_new";
    public static final String TAG_BLOG_DETAIL = "blog_detail";
    public static final String TAG_PROFILE = "profile";
    // index to identify current nav menu item
    public static int navItemIndex = 0;
    public static String CURRENT_TAG = TAG_HOME;
    //public static FragmentTabHost mTabHost;
    // public static TabWidget tabs;
    public static DrawerLayout drawer;
    public static TextView toolbar;
    public static TextView txtSave;
    public static Toolbar Toolbar;
    public static FragmentManager manager;
    public static HomeActivity activity;
    public static ImageView ivDown;
    public LinearLayout ll_post_new, ll_my_feeds, ll_stylist, ll_setting, ll_terms_of_use, ll_privacy_policy, ll_blog_list, ll_profile, ll_collage_maker, ll_designer;
    public static CircularImageView ivUser;
    public static TextView tvName, tvPost;
    public LinearLayout llAppVersion, ll_visionboard;
    public JsonParserUniversal jParser;
    private NavigationView navigationView, navigationFilter;
    private View navHeader, navHaderFilter;
    // toolbar titles respected to selected nav menu item
    private String[] activityTitles;
    // flag to load home fragment when user presses back key
    private boolean shouldLoadHomeFragOnBackPress = true;
    private Handler mHandler;
    public static HomeActivity Homenstance;
    public static TextView tvNotificationItemCount;

    public static Spinner spCountry, spState, spCity;
    public static EditText edtZipCode;
    public ImageView ivRight, ivReset;
    LinearLayout lin_city;
    String countryId = "";

    public static void replaceFragment(Fragment fragment, boolean doAddToBackStack) {
        MyConstants.selectedImage = "";
        MyConstants.isEditable = false;
        if (App.isHomeFragment) {
            toolbar.setClickable(false);
        }
        App.isHomeFragment = false;
        ivDown.setVisibility(View.GONE);
        drawer.closeDrawers();
        FragmentTransaction transaction = manager.beginTransaction();
        transaction.replace(R.id.frame, fragment);
        if (doAddToBackStack) {
            activity.getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        } else {
            transaction.addToBackStack(fragment.getClass().getName());
            activity.getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        }
        transaction.commitAllowingStateLoss();
        activity.getSupportFragmentManager().executePendingTransactions();
    }

    public static void setBackButton() {
        activity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        Toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activity.onBackPressed();
            }
        });
        drawer.closeDrawers();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        activity = this;
        Homenstance = this;
        bindWidgetReference();
        MySharedPref.MySharedPref(this);
        setSupportActionBar(Toolbar);

        initializeData();

        setUserDataFromSharedPreference();

        loadNavHeader();

        loadFilterDrawer();

        setUpNavigationView();

        clodeDrawer();

        if (App.isFromNoti) {
            //App.isFromNoti = false;
            if (App.noti_type.equals("6")) {
                ProfileFragment profileFragment = new ProfileFragment();
                Bundle bundle = new Bundle();
                bundle.putString("intent", "profile");
                bundle.putString("id", App.noti_redirect_id);
                profileFragment.setArguments(bundle);
                replaceFragment(profileFragment, false);
                App.noti_type = "";
                App.noti_redirect_id = "";
            } else if (App.noti_type.equals("8")) {
                MyFeedDetailFragment fragment = new MyFeedDetailFragment();
                Bundle b = new Bundle();
                b.putString("intent", "feeds");
                b.putSerializable("feeds", App.noti_redirect_id);
                fragment.setArguments(b);
                replaceFragment(fragment, false);
            } else if (App.noti_type.equals("9")) {
                MyFeedDetailFragment fragment = new MyFeedDetailFragment();
                Bundle b = new Bundle();
                b.putString("intent", "feeds");
                b.putSerializable("feeds", App.noti_redirect_id);
                fragment.setArguments(b);
                replaceFragment(fragment, false);
            }else if (App.noti_type.equals("10")) {
                BlogDetailFragment fragment = new BlogDetailFragment();
                Bundle b = new Bundle();
                b.putString("intent", "blog");
                b.putSerializable("blog", App.noti_redirect_id);
                b.putSerializable("user","6");
                fragment.setArguments(b);
                replaceFragment(fragment, false);
            }  else if (App.noti_type.equals("12")) {
                MyFeedDetailFragment fragment = new MyFeedDetailFragment();
                Bundle b = new Bundle();
                b.putString("intent", "feeds");
                b.putSerializable("feeds", App.noti_redirect_id);
                fragment.setArguments(b);
                replaceFragment(fragment, false);
            }
        } else {
            if (MyConstants.categoriesList.size() == 0) {
                getCatagory();
            } else {
                replaceFragment(new HomeFragment(), true);
                setUserData();
            }
            if (MyConstants.countryList.size() < 1)
                getCountry();
            setSpinners();
        }

        if (App.intent.equals("share")) {
            App.intent = "";
            Uri imageUri = Uri.parse(App.SHARE_URI);
            if (imageUri != null) {
                PostNewFragment profileFragment = new PostNewFragment();
                Bundle bundle = new Bundle();
                bundle.putString("intent", "share");
                bundle.putString("uri", FileUtils.getPath(this, imageUri));
                profileFragment.setArguments(bundle);
                replaceFragment(profileFragment, false);
                // Update UI to reflect image being shared
            }
        }
        HandelShareIntent();

    }

    public void HandelShareIntent() {
        Intent intent = getIntent();
        String action = intent.getAction();
        String type = intent.getType();

        if (Intent.ACTION_SEND.equals(action) && type != null) {
            if ("text/plain".equals(type)) {
                handleSendText(intent); // Handle text being sent
            } else if (type.startsWith("image/")) {
                handleSendImage(intent); // Handle single image being sent
            }
        } else if (Intent.ACTION_SEND_MULTIPLE.equals(action) && type != null) {
            if (type.startsWith("image/")) {
                handleSendMultipleImages(intent); // Handle multiple images being sent
            }
        } else {
            // Handle other intents, such as being started from the home screen
        }


    }

    public void handleSendText(Intent intent) {
        String sharedText = intent.getStringExtra(Intent.EXTRA_TEXT);
        if (sharedText != null) {
            // Update UI to reflect text being shared
        }
    }

    public void handleSendImage(Intent intent) {
        Uri imageUri = (Uri) intent.getParcelableExtra(Intent.EXTRA_STREAM);
        if (imageUri != null) {
            PostNewFragment profileFragment = new PostNewFragment();
            Bundle bundle = new Bundle();
            bundle.putString("intent", "share");
            bundle.putString("uri", FileUtils.getPath(this, imageUri));
            profileFragment.setArguments(bundle);
            replaceFragment(profileFragment, false);
            // Update UI to reflect image being shared
        }
    }

    public void handleSendMultipleImages(Intent intent) {
        ArrayList<Uri> imageUris = intent.getParcelableArrayListExtra(Intent.EXTRA_STREAM);
        if (imageUris != null) {


            // Update UI to reflect multiple images being shared
        }
    }

    private void setSpinners() {
        if (MyConstants.countryList.size() == 0) {
            //getCatagory();
        } else {
            //getUserProfile();

            // getCatagory();
            System.out.println("country list==" + MyConstants.countryList.size());
            ArrayAdapter aaCountry = new ArrayAdapter<>(getApplicationContext(), R.layout.spinner_text_white, MyConstants.countryListString);
            aaCountry.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spCountry.setAdapter(new NothingSelectedSpinnerAdapter(
                    aaCountry, R.layout.spinner_text_white,
                    getApplicationContext(), "Select Country"));

            ArrayAdapter aaState = new ArrayAdapter<>(getApplicationContext(), R.layout.spinner_text_white, MyConstants.stateListString);
            aaState.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spState.setAdapter(new NothingSelectedSpinnerAdapter(
                    aaState, R.layout.spinner_text_white,
                    getApplicationContext(), "Select State"));
            System.out.println("state list==" + MyConstants.stateList.size());

            ArrayAdapter aaCity = new ArrayAdapter<>(getApplicationContext(), R.layout.spinner_text_white, MyConstants.cityListString);
            aaCity.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spCity.setAdapter(new NothingSelectedSpinnerAdapter(
                    aaCity, R.layout.spinner_text_white,
                    getApplicationContext(), "Select City"));


        }

        spState.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                try {
                    if (position != 0) {
                        MyConstants.stateFilterId = MyConstants.stateList.get(position - 1).getId();
                        getCity();
                    }

                } catch (Exception e) {

                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        spCity.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                try {
                    if (position != 0) {
                        MyConstants.cityFilterId = MyConstants.cityList.get(position - 1).getId();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        spCountry.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                try {
                    countryId = MyConstants.countryList.get(position - 1).getId();
                    MyConstants.countryFilterId = MyConstants.countryList.get(position - 1).getId();

                    System.out.println("country id==" + countryId);
               /*     MyConstants.stateListString.clear();
                    spState.setSelection(0);
                    MyConstants.cityListString.clear();
                    spCity.setSelection(0);*/
                    getState();
                } catch (Exception e) {

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }

    private void loadFilterDrawer() {
        navigationFilter = findViewById(R.id.navigationFilter);
        spCountry = navigationFilter.findViewById(R.id.spCountry);
        spCity = navigationFilter.findViewById(R.id.spCity);
        lin_city = navigationFilter.findViewById(R.id.lin_city);
        spState = navigationFilter.findViewById(R.id.spState);
        edtZipCode = navigationFilter.findViewById(R.id.edtZipCode);
        ivRight = navigationFilter.findViewById(R.id.ivRight);
        ivReset = navigationFilter.findViewById(R.id.ivReset);
        ivRight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawer.openDrawer(GravityCompat.END);
                MyConstants.zipCode = edtZipCode.getText().toString();
                if (MyConstants.isDesignerScreen) {
                   // manager.popBackStack();
                    replaceFragment(new DesignersFragment(), false);
                } else if (MyConstants.isStylistScreen) {
                  //  manager.popBackStack();
                    replaceFragment(new TrendsetterFragment(), false);
                } else if (MyConstants.isStoresScreen) {
                    manager.popBackStack();
                    replaceFragment(new StoresFragment(), false);
                }
            }
        });
        ivReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MyConstants.countryFilterId = "";
                MyConstants.cityFilterId = "";
                MyConstants.stateFilterId = "";
                spCity.setSelection(0);
                //spCountry.setSelection(0);
                spState.setSelection(0);
                edtZipCode.getText().clear();
                MyConstants.zipCode = "";

            }
        });
    }

    public static void clodeDrawer() {
        drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
    }

    public static void unlockDrawer() {
        drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
    }

    public static void openDraer() {
        drawer.openDrawer(GravityCompat.END);
    }

    private void getState() {
        Map<String, String> map = new HashMap<>();
        map.put("url", MyConstants.BASE_URL + "GetStates");
        map.put("CountryID", countryId);
        map.put("show", "");
        new CallRequest(HomeActivity.this).getState(map);

    }

    private void getCity() {
        Map<String, String> map = new HashMap<>();
        map.put("url", MyConstants.BASE_URL + "GetCity");
        map.put("StateID", MyConstants.stateFilterId);
        map.put("show", "");
        new CallRequest(HomeActivity.this).getCity(map);

    }

    private void getCountry() {
        Map<String, String> map = new HashMap<>();
        map.put("url", MyConstants.BASE_URL + "GetCountry");
        map.put("header", "");
        map.put("Auth", App.user.getAuth());
        map.put("show", "");
        new CallRequest(HomeActivity.this).getCountry(map);
    }

    private void getCatagory() {
        Map<String, String> map = new HashMap<>();
        map.put("url", MyConstants.BASE_URL + "GetCategoriesList");
        map.put("header", "");
        map.put("Auth", App.user.getAuth());
        //map.put("show", "");
        new CallRequest(HomeActivity.this).getCatagory(map);
    }

    private void getUserProfile() {
        Map<String, String> map = new HashMap<>();
        map.put("url", MyConstants.BASE_URL + "get_profile");
        map.put("header", "");
        map.put("Auth", App.user.getAuth());
        map.put("show", "");
        new CallRequest(HomeActivity.this).getUserProfile(map);
    }

    private void setUserDataFromSharedPreference() {
        if (MySharedPref.getIsLogin()) {
            App.user.setEmail(MySharedPref.getString(HomeActivity.this, MyConstants.EMAIL, ""));
            App.user.setAuth(MySharedPref.getString(HomeActivity.this, MyConstants.AUTH, ""));
            App.user.setId(MySharedPref.getString(HomeActivity.this, MyConstants.USER_ID, ""));
            App.user.setPassword(MySharedPref.getString(HomeActivity.this, MyConstants.PASSWORD, ""));
            App.user.setFirstName(MySharedPref.getString(HomeActivity.this, MyConstants.FIRST_NAME, ""));
            App.user.setLastName(MySharedPref.getString(HomeActivity.this, MyConstants.LAST_NAME, ""));
            App.user.setName(MySharedPref.getString(HomeActivity.this, MyConstants.NAME, ""));

            System.out.println("AUTH :::: " + App.user.getAuth());
        }
    }

    private void initializeData() {
        activity = HomeActivity.this;
        manager = getSupportFragmentManager();
        mHandler = new Handler();
        jParser = new JsonParserUniversal();
        navHeader = navigationView.getHeaderView(0);
        activityTitles = getResources().getStringArray(R.array.nav_item_activity_titles);
    }

    private void bindWidgetReference() {
        Toolbar = findViewById(R.id.toolbar);
        toolbar = findViewById(R.id.txtTitle);
        drawer = findViewById(R.id.drawer_layout);
        navigationView = findViewById(R.id.nav_view);
        txtSave = findViewById(R.id.txtSave);
        ivDown = findViewById(R.id.spinner);

    }

    public boolean isTablet() {
        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);

        float yInches = metrics.heightPixels / metrics.ydpi;
        float xInches = metrics.widthPixels / metrics.xdpi;
        double diagonalInches = Math.sqrt(xInches * xInches + yInches * yInches);
        if (diagonalInches >= 6.5) {
            // 6.5inch device or bigger
            return true;
        } else {
            // smaller device
            return false;
        }
    }

    private void loadNavHeader() {
        ivUser = navHeader.findViewById(R.id.ivUser);
        tvName = navHeader.findViewById(R.id.tvName);
        tvPost = navHeader.findViewById(R.id.tvPost);
        ll_post_new = navHeader.findViewById(R.id.ll_post_new);

        ll_blog_list = navHeader.findViewById(R.id.ll_blog_list);
        ll_stylist = navHeader.findViewById(R.id.ll_stylist);
        ll_setting = navHeader.findViewById(R.id.ll_setting);
        ll_profile = navHeader.findViewById(R.id.llProfile);
        ll_designer = navHeader.findViewById(R.id.ll_designer);
        llAppVersion = navHeader.findViewById(R.id.ll_app_version);
        ll_visionboard = navHeader.findViewById(R.id.ll_visionboard);
        TextView txtVersion = navHeader.findViewById(R.id.txtVersion);
        ll_post_new.setOnClickListener(this);

        ll_setting.setOnClickListener(this);
        ll_profile.setOnClickListener(this);
        ll_designer.setOnClickListener(this);
        ll_blog_list.setOnClickListener(this);
        ll_stylist.setOnClickListener(this);
        ll_visionboard.setOnClickListener(this);
        PackageInfo pInfo = null;
        try {
            pInfo = this.getPackageManager().getPackageInfo(getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        String version = pInfo.versionName;
        txtVersion.setText("App Version: " + version);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.home, menu);

        final MenuItem menuItem = menu.findItem(R.id.item_notification);

        View actionView = MenuItemCompat.getActionView(menuItem);
        tvNotificationItemCount = actionView.findViewById(R.id.notification_badge);
        FrameLayout frameNotification = actionView.findViewById(R.id.frameNotification);
        frameNotification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println("Notification====");
                replaceFragment(new NotificationFragment(), false);
            }
        });
        setupBadge();
        return true;
    }


    public static void setupBadge() {

        if (tvNotificationItemCount != null) {
            if (App.mnotificationItemCount == 0) {
                if (tvNotificationItemCount.getVisibility() != View.GONE) {
                    tvNotificationItemCount.setVisibility(View.GONE);
                }
            } else {

                System.out.println("notification count===" + String.valueOf(Math.min(App.mnotificationItemCount, 99)));
                tvNotificationItemCount.setText(String.valueOf(Math.min(App.mnotificationItemCount, 99)));
                if (tvNotificationItemCount.getVisibility() != View.VISIBLE) {
                    tvNotificationItemCount.setVisibility(View.VISIBLE);
                }
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        System.out.println("id====" + id);
        //noinspection SimplifiableIfStatement
        if (id == R.id.item_notification) {
            System.out.println("******going notification*********");
            replaceFragment(new NotificationFragment(), false);
            return true;
        } else if (id == R.id.item_search) {
            return true;
        } else if (id == R.id.item_home) {
            replaceFragment(new HomeFragment(), true);
            return true;
        } else if (id == R.id.item_edit_profile) {
            System.out.println("******going EditProfileFragment*********");
            replaceFragment(new EditProfileFragment(), false);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void setIndex(int navItemIndex, String currentTag) {
        this.navItemIndex = navItemIndex;
        CURRENT_TAG = currentTag;
    }

    public void disableKeyBoard() {
        //  getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(INPUT_METHOD_SERVICE);

        if (inputMethodManager.isAcceptingText()) {
            inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        }
    }

    @Override
    public void onBackPressed() {
        toolbar.setVisibility(View.VISIBLE);

        FragmentManager manager = getSupportFragmentManager();
        MyConstants.editImageUrl = "";
        if (drawer.isDrawerOpen(GravityCompat.END)) {
            clodeDrawer();
        } else {
            if (MyConstants.profileCount != 0) {
                MyConstants.profileCount = MyConstants.profileCount - 1;
            } else {
                MyConstants.isUserProfile = false;
            }

            MyConstants.isStylistScreen = false;
            MyConstants.isStoresScreen = false;
            MyConstants.isFromCreateLook = false;
            MyConstants.isDesignerScreen = false;
            if (manager.getBackStackEntryCount() == 1) {
                activity.getSupportActionBar().setDisplayHomeAsUpEnabled(false);
                manager.popBackStack();
                setUpNavigationView();
                disableKeyBoard();
            } else if (manager.getBackStackEntryCount() > 1) {
                manager.popBackStack();
                setUpNavigationView();
                disableKeyBoard();
            } else {
                new AlertDialog.Builder(this)
                        .setTitle("Really Exit?")
                        .setMessage("Are you sure want to exit?")
                        .setNegativeButton(android.R.string.no, null)
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

                            public void onClick(DialogInterface arg0, int arg1) {
                                finish();
                                moveTaskToBack(true);
                            }
                        }).create().show();
            }
        }

    }

    private void setUpNavigationView() {

        ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawer, Toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close) {

            @Override
            public void onDrawerClosed(View drawerView) {
                // Code here will be triggered once the drawer closes as we dont want anything to happen so we leave this blank
                super.onDrawerClosed(drawerView);
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                // Code here will be triggered once the drawer open as we dont want anything to happen so we leave this blank
                setUserData();
                super.onDrawerOpened(drawerView);
            }
        };

        //Setting the actionbarToggle to drawer layout
        drawer.setDrawerListener(actionBarDrawerToggle);

        //calling sync state is necessary or else your hamburger icon wont show up
        actionBarDrawerToggle.syncState();

        actionBarDrawerToggle.setDrawerIndicatorEnabled(false);
        Drawable drawable = ResourcesCompat.getDrawable(getResources(), R.drawable.menu, getTheme());
        actionBarDrawerToggle.setHomeAsUpIndicator(drawable);

        actionBarDrawerToggle.setToolbarNavigationClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (drawer.isDrawerVisible(GravityCompat.START)) {
                    drawer.closeDrawer(GravityCompat.START);
                } else {
                    drawer.openDrawer(GravityCompat.START);
                }
            }
        });
    }

    @Override
    public void onClick(View v) {
        if (v == ll_post_new) {
            replaceFragment(new PostNewFragment(), false);
        } else if (v == ll_stylist) {
            System.out.println("stylist====");
            MyConstants.countryFilterId = "";
            MyConstants.cityFilterId = "";
            MyConstants.stateFilterId = "";
            //spCountry.setSelection(0);
            spState.setSelection(0);
            edtZipCode.getText().clear();
            MyConstants.zipCode = "";
            replaceFragment(new TrendsetterFragment(), false);
        } /*else if (v == ll_my_feeds) {
            replaceFragment(new MyFeedsFragment(), false);
        }*/ else if (v == ll_blog_list) {
            replaceFragment(new UsreWiseBlogListFragment(), false);
        } else if (v == ll_setting) {
            replaceFragment(new SettingFragment(), false);
        } else if (v == ll_privacy_policy) {

        } else if (v == ll_terms_of_use) {
            replaceFragment(new TermsOfUseFragment(), false);
        } else if (v == ll_visionboard) {
            replaceFragment(new VisionBoardLatestFragment(), false);
        } else if ((v == ll_profile)) {
            ProfileFragment profileFragment = new ProfileFragment();
            Bundle bundle = new Bundle();
            bundle.putString("intent", "home");
            profileFragment.setArguments(bundle);
            replaceFragment(profileFragment, false);
        } else if (v == ll_collage_maker) {
            //replaceFragment(new CollageMakerFragment(), false);
        } else if (v == ll_designer) {
            MyConstants.cityFilterId = "";
            MyConstants.stateFilterId = "";
            spState.setSelection(0);
            edtZipCode.getText().clear();
            MyConstants.zipCode = "";
            replaceFragment(new DesignersFragment(), false);
        }
    }

    private void doLogOut() {

        drawer.closeDrawers();

        new AlertDialog.Builder(HomeActivity.this)
                .setTitle("Alert!")
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setMessage("Would you like to logout?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                        dialog.dismiss();

                        Map<String, String> map = new HashMap<>();
                        map.put("url", MyConstants.BASE_URL + "logout");
                        map.put("header", "");
                        map.put("Auth", App.user.getAuth());

                        new CallRequest(HomeActivity.this).doLogOut(map);
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .show();
    }

    public void setToolbarTitleFragment(String title) {
        getSupportActionBar().setTitle(title);
    }

    @SuppressLint("NewApi")
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (App.isFromEditProfile) {
            try {
                EditProfileFragment editFragment = (EditProfileFragment) getSupportFragmentManager().findFragmentById(R.id.frame);
                if (editFragment != null && editFragment.isVisible()) {
                    editFragment.onActivityResult(requestCode, resultCode, data);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (App.isFromNewPost) {
            try {
                PostNewFragment postNewFragment = (PostNewFragment) getSupportFragmentManager().findFragmentById(R.id.frame);
                if (postNewFragment != null && postNewFragment.isVisible()) {
                    postNewFragment.onActivityResult(requestCode, resultCode, data);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (App.isFromCreateALook) {
            try {
                CreateALookFragment createAlookFragment = (CreateALookFragment) getSupportFragmentManager().findFragmentById(R.id.frame);
                if (createAlookFragment != null && createAlookFragment.isVisible()) {
                    createAlookFragment.onActivityResult(requestCode, resultCode, data);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (App.isAppFromVisionBoard) {
            try {
                VisionBoardLatestFragment visionBoardFragment = (VisionBoardLatestFragment) getSupportFragmentManager().findFragmentById(R.id.frame);
                if (visionBoardFragment != null && visionBoardFragment.isVisible()) {
                    visionBoardFragment.onActivityResult(requestCode, resultCode, data);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        try {
            if (result != null && !result.isEmpty()) {
                Log.i("TAG", "TAG Result : " + result);

                switch (request) {
//                    case logout:
//                        try {
//                            JSONObject object = new JSONObject(result);
//                            boolean success = object.getBoolean("success");
//                            if (success) {
//                                Utils.showToast(object.getString("message"), this);
//
//                                MySharedPref.setIsLogin(false);
//                                MySharedPref.sharedPrefClear(HomeActivity.this);
//
//                                Intent intent = new Intent(HomeActivity.this, LoginActivity.class);
//                                startActivity(intent);
//                                ActivityCompat.finishAffinity(HomeActivity.this);
//                            } else {
//                                String error_string = object.getString("message");
//                                Toast.makeText(HomeActivity.this, error_string, Toast.LENGTH_SHORT).show();
//                            }
//
//                        } catch (JSONException e) {
//                            e.printStackTrace();
//                        }
//                        break;

                    case getCategory:
                        MyConstants.categoriesList.clear();
                        MyConstants.categoriesListString.clear();
                        try {
                            JSONObject object = new JSONObject(result);
                            boolean success = object.getBoolean("success");
                            String notificationCount = object.getString("unread_count");
                            App.mnotificationItemCount = Integer.parseInt(notificationCount);
                            if (success) {
                                // TODO parse JSON
                                JSONArray userDataObj = object.getJSONArray("data");
                                for (int i = 0; userDataObj.length() > i; i++) {
                                    MyConstants.category = (Categories) jParser.parseJson(userDataObj.getJSONObject(i), new Categories());
                                    MyConstants.categoriesList.add(MyConstants.category);
                                    MyConstants.categoriesListString.add(MyConstants.category.getCategoryName());
                                }
                            } else {
                                String error_string = object.getString("message");
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        getUserProfile();

                        break;
                    case getUserProfile:
                        try {
                            JSONObject object = new JSONObject(result);
                            boolean success = object.getBoolean("success");
                            if (success) {
                                // TODO parse JSON
                                JSONObject userDataObj = object.getJSONObject("user_data");
                                MyConstants.userProfile = (UserProfile) jParser.parseJson(userDataObj, new UserProfile());
                                setUserData();
                            } else {
                                String error_string = object.getString("message");
                                Toast.makeText(HomeActivity.this, error_string, Toast.LENGTH_SHORT).show();
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        replaceFragment(new HomeFragment(), true);

                        // Utils.hideProgressDialog();

                        break;
                    case getCountry:
                        MyConstants.countryList.clear();
                        MyConstants.countryListString.clear();
                        int selection = 0;
                        try {
                            Utils.hideProgressDialog();
                            JSONObject object = new JSONObject(result);
                            boolean success = object.getBoolean("status");
                            if (success) {
                                // TODO parse JSON
                                JSONArray userDataObj = object.getJSONArray("data");
                                for (int i = 0; userDataObj.length() > i; i++) {
                                    MyConstants.country = (Country) jParser.parseJson(userDataObj.getJSONObject(i), new Country());
                                    MyConstants.countryList.add(MyConstants.country);
                                    MyConstants.countryListString.add(MyConstants.country.getName());
                                    if (MyConstants.country.getName().equalsIgnoreCase("United States")) {
                                        countryId = MyConstants.country.getId();
                                        selection = i + 1;
                                    }

                                }
                            } else {
                                String error_string = object.getString("message");
                            }
                            ArrayAdapter aaCountry = new ArrayAdapter<>(getApplicationContext(), R.layout.spinner_text_white, MyConstants.countryListString);
                            aaCountry.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            spCountry.setAdapter(new NothingSelectedSpinnerAdapter(
                                    aaCountry, R.layout.spinner_text_white,
                                    getApplicationContext(), "Select Country"));
                            spCountry.setSelection(selection);
                            // spCountry.setAdapter(aaCountry);
                            spCountry.setClickable(false);
                            spCountry.setFocusable(false);
                            spCountry.setEnabled(false);
                            System.out.println("country list==" + MyConstants.countryList.size());
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        Utils.hideProgressDialog();
                        break;

                    case getState:
                        MyConstants.stateList.clear();
                        MyConstants.stateListString.clear();
                        try {
                            JSONObject object = new JSONObject(result);
                            boolean success = object.getBoolean("status");
                            if (success) {
                                // TODO parse JSON
                                JSONArray userDataObj = object.getJSONArray("data");
                                for (int i = 0; userDataObj.length() > i; i++) {
                                    MyConstants.state = (State) jParser.parseJson(userDataObj.getJSONObject(i), new State());
                                    MyConstants.stateList.add(MyConstants.state);
                                    MyConstants.stateListString.add(MyConstants.state.getName());
                                }
                            } else {
                                String error_string = object.getString("message");
                            }

                            ArrayAdapter aaState = new ArrayAdapter<>(getApplicationContext(), R.layout.spinner_text_white, MyConstants.stateListString);
                            aaState.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            spState.setAdapter(new NothingSelectedSpinnerAdapter(
                                    aaState, R.layout.spinner_text_white,
                                    getApplicationContext(), "Select State"));
                            System.out.println("state list==" + MyConstants.stateList.size());

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        getCity();
                        break;

                    case getCity:
                        MyConstants.cityList.clear();
                        MyConstants.cityListString.clear();
                        try {
                            JSONObject object = new JSONObject(result);
                            boolean success = object.getBoolean("status");
                            if (success) {
                                // TODO parse JSON
                                JSONArray userDataObj = object.getJSONArray("data");
                                for (int i = 0; userDataObj.length() > i; i++) {
                                    MyConstants.city = (City) jParser.parseJson(userDataObj.getJSONObject(i), new City());
                                    MyConstants.cityList.add(MyConstants.city);
                                    MyConstants.cityListString.add(MyConstants.city.getName());
                                }
                            } else {
                                String error_string = object.getString("message");
                            }

                            ArrayAdapter aaCity = new ArrayAdapter<>(getApplicationContext(), R.layout.spinner_text_white, MyConstants.cityListString);
                            aaCity.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            spCity.setAdapter(new NothingSelectedSpinnerAdapter(
                                    aaCity, R.layout.spinner_text_white,
                                    getApplicationContext(), "Select City"));
                            System.out.println("city list==" + MyConstants.cityList.size());
                            if (MyConstants.cityList.size() == 0) {
                                lin_city.setVisibility(View.GONE);
                            } else {
                                lin_city.setVisibility(View.VISIBLE);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        Utils.hideProgressDialog();
                        break;
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public static void setUserData() {
        if (MyConstants.userProfile != null) {
            if (!TextUtils.isEmpty(MyConstants.userProfile.getUserProfileImage())) {
                try {
                    PicassoTrustAll.getInstance(activity)
                            .load(MyConstants.userProfile.getUserProfileImage())
                            .placeholder(R.drawable.avatar)
                            .error(R.drawable.avatar)
                            .into(ivUser);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            tvName.setText(MyConstants.userProfile.getFirstName() + " " + MyConstants.userProfile.getLastName());
            tvPost.setText(MyConstants.userProfile.getRegisterUserTypeByName());
        }
    }

    @Override
    public void onProgressUpdate(String uniqueMessageId, int progres) {

    }

    @Override
    public void onProgressComplete(String uniqueMessageId, String result, Constant.REQUESTS request) {

    }
}
