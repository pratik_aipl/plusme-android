package com.plus.me.model;

import java.io.Serializable;

public class Following implements Serializable {

    public String Id = "", FirstName = "", LastName = "", ProfileImage = "", RegisterUserTypeByName = "";

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public String getFirstName() {
        return FirstName;
    }

    public void setFirstName(String firstName) {
        FirstName = firstName;
    }

    public String getLastName() {
        return LastName;
    }

    public void setLastName(String lastName) {
        LastName = lastName;
    }

    public String getProfileImage() {
        return ProfileImage;
    }

    public void setProfileImage(String profileImage) {
        ProfileImage = profileImage;
    }

    public String getRegisterUserTypeByName() {
        return RegisterUserTypeByName;
    }

    public void setRegisterUserTypeByName(String registerUserTypeByName) {
        RegisterUserTypeByName = registerUserTypeByName;
    }
}
