package com.plus.me.utils;

public class Point {

    public float dy;

    public float dx;
    public float x, y;

    @Override
    public String toString() {
        return x + ", " + y;
    }
}