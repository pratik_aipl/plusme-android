package com.plus.me.fragments;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.core.content.ContextCompat;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;

import com.plus.me.R;
import com.plus.me.activity.ChooseImageActivity;
import com.plus.me.activity.GalleryActivity;
import com.plus.me.activity.HomeActivity;
import com.plus.me.adapter.NothingSelectedSpinnerAdapter;
import com.plus.me.custom_views.CropView;
import com.plus.me.others.App;
import com.plus.me.others.PicassoTrustAll;
import com.plus.me.photoeditor.OnPhotoEditorListener;
import com.plus.me.photoeditor.PhotoEditor;
import com.plus.me.photoeditor.PhotoEditorView;
import com.plus.me.photoeditor.ViewType;
import com.plus.me.ws.AsyncTaskListner;
import com.plus.me.ws.CallRequest;
import com.plus.me.ws.Constant;
import com.plus.me.ws.MyConstants;
import com.plus.me.ws.Utils;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static android.app.Activity.RESULT_OK;
import static com.plus.me.activity.HomeActivity.ivDown;
import static com.plus.me.activity.HomeActivity.replaceFragment;
import static com.plus.me.activity.HomeActivity.toolbar;
import static com.plus.me.activity.HomeActivity.txtSave;
import static com.plus.me.utils.ImagePathUtils.getFilePath;
import static com.plus.me.ws.MyConstants.INTENT_KEY_GALLERY_SELECTMULTI;
import static com.plus.me.ws.MyConstants.INTENT_RESULT_GALLERY;
import static com.plus.me.ws.MyConstants.isEditedNowInCreateLook;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link CreateALookFragmentNew.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link CreateALookFragmentNew#newInstance} factory method to
 * create an instance of this fragment.
 */
public class CreateALookFragmentNew extends BaseFragment implements OnPhotoEditorListener, AsyncTaskListner {
    // TODO: Rename parameter arguments, choose names that match
    private static final String TAG = "CreateALookFragmentNew";
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    public CreateALookFragmentNew instace;
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    boolean isCamera = false;
    //changes
    private Uri mCurrentPhotoPathUri;
    private String mCurrentPhotoPath;
    private static final int REQUEST_IMAGE_CAPTURE = 11;
    private static final int REQUEST_IMAGE_SELECT = 12;

    File imgFile = null;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    FloatingActionButton fabAdd, fabEdit;
    private OnFragmentInteractionListener mListener;
    public static Dialog dialog, dialog1, dialogSaveVisionBoard;
    private LinearLayout llCamera, llGallery;
    private boolean isSubImage = false;
    public LinearLayout llFromMyCloset;
    public LinearLayout llCollageMaker;
    public static String masterImageList[] = null;
    public static String masterImagePath;
    public static Bitmap workingBitmap = null;
    public static FrameLayout flCropImage;
    public static String workingImagePath;

    public String catagory;
    public boolean doOwerWrite = false;
    public boolean toVisionBoar = false, toMyCloset = false;

    private PhotoEditorView mPhotoEditorView;
    private PhotoEditor mPhotoEditor;
    Bitmap bitmap;
    ProgressDialog mProgressDialog;

    private Activity activity;
    MenuItem itemReset;
    MenuItem itemSave;
    MenuItem itemSaveCollage;
    private Button btnSaveVisionBoardCollage, btnSaveAsMyCloset, btnCancelCollage;
    private Button btnVisionBoardSave, btnMyClosetSave, btnCancel;
    public ImageView img_priview;
    public int count = 0, tempCount = 0;
    boolean frsttime = false;
    //public Bitmap temp;
    public static final int PICK_IMAGE_PERMISSION_CODE_MYCLOSET = 1, PICK_VIDEO_PERMISSION_VISIONBOARD = 2, RECORD_VIDEO_PERMISSION_CODE = 3;
    public static ArrayList<Bitmap> tmepBitmapArrayList = new ArrayList<>();

    RelativeLayout rel_data;
    // public String selectedImagePath;

    public CreateALookFragmentNew() {
        // Required empty public constructor

    }


    // TODO: Rename and change types and number of parameters
    public static CreateALookFragmentNew newInstance(String param1, String param2) {
        CreateALookFragmentNew fragment = new CreateALookFragmentNew();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view = inflater.inflate(R.layout.fragment_create_alook_fragment_new, container, false);
        setHasOptionsMenu(true);
        instace = this;
        fabAdd = view.findViewById(R.id.fabAdd);
        fabEdit = view.findViewById(R.id.fabEdit);
        rel_data = view.findViewById(R.id.rel_data);
        img_priview = view.findViewById(R.id.img_priview);
        flCropImage = view.findViewById(R.id.flCropImage);
        activity = getActivity();
        mPhotoEditorView = view.findViewById(R.id.photoEditorView);
        DisplayMetrics dm = new DisplayMetrics();
        try {
            getActivity().getWindowManager().getDefaultDisplay().getMetrics(dm);
        } catch (Exception e) {
            e.printStackTrace();
        }
        Bitmap newBitmap = Bitmap.createBitmap(dm.widthPixels, dm.heightPixels, Bitmap.Config.ARGB_4444);
        Canvas canva = new Canvas(newBitmap);
        canva.drawColor(Color.WHITE);
        canva.drawBitmap(newBitmap, 0, 0, null);


        if (getArguments() != null && getArguments().getString("frommyclose").equalsIgnoreCase("myclose")) {
            rel_data.setVisibility(View.GONE);

        }
        mPhotoEditor = new PhotoEditor.Builder(getActivity(), mPhotoEditorView)
                .setPinchTextScalable(true) // set flag to make text scalable when pinch
                //.setDefaultTextTypeface(mTextRobotoTf)
                //.setDefaultEmojiTypeface(mEmojiTypeFace)
                .build(); // build photo editor sdk

        mPhotoEditor.setOnPhotoEditorListener(instace);

        initializeData();
        fabAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isEditedNowInCreateLook = false;
                App.crop = false;
                openImageChooserDialog();
            }
        });

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();

        //final DisplayMetrics dm = new DisplayMetrics();
        try {
            // getActivity().getWindowManager().getDefaultDisplay().getMetrics(dm);


            Bitmap bitmap2 = App.bitmap;

            Bitmap resultingImage = Bitmap.createBitmap(App.bitmap.getWidth(),
                    App.bitmap.getHeight(),
                    bitmap2.getConfig());
            Canvas canvas = new Canvas(resultingImage);

            if (App.crop) {

                Paint paint = new Paint();
                paint.setAntiAlias(true);
                Path path = new Path();
                for (int i = 0; i < CropView.points.size(); i++) {
                    path.lineTo(CropView.points.get(i).x, CropView.points.get(i).y);
                }
                canvas.drawPath(path, paint);
                paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
                canvas.drawBitmap(bitmap2, 0, 0, paint);
                frsttime = true;
            } else {
                canvas.drawBitmap(bitmap2, 0, 0, null);
            }
       /*   Bitmap newBitmap = Bitmap.createBitmap(resultingImage.getWidth(), resultingImage.getHeight(), Bitmap.Config.ARGB_4444);
            Canvas canva = new Canvas(newBitmap);
            canva.drawColor(Color.WHITE);
            canva.drawBitmap(newBitmap, 0, 0, null);*/

            // mPhotoEditorView.getSource().setImageBitmap(newBitmap);
            Log.d(TAG, "bitmappp>>>: " + resultingImage.getWidth() + "/" + resultingImage.getHeight());

            try {
                App.bitmapArrayList.add(createTrimmedBitmap(resultingImage));

            } catch (Exception e) {
                e.printStackTrace();
            }
            if (App.edit && App.inMuClosetEdit) {
                //  mPhotoEditor.clearAllViews();
            }
            if (App.bitmapArrayList.size() == 1)
                frsttime = true;
            //mPhotoEditor.addImage(resultingImage);

            //mPhotoEditorView.getSource().setImageBitmap(resultingImage);
       /*     if (App.firstTimeEdit) {
                //mPhotoEditor.addImage(resultingImage);
                saveImage();
            }*/

        } catch (Exception ex) {
        }

        if (App.inMuCloset) {
            if (count == 0)
                count++;
            Thread thread = new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        URL url = new URL(MyConstants.editImageUrl);
                        System.out.println("image url====" + MyConstants.editImageUrl);
                        bitmap = BitmapFactory.decodeStream(url.openConnection().getInputStream());
                        _taken = true;


                    } catch (MalformedURLException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                }
            });
            thread.start();

            img_priview.setVisibility(View.VISIBLE);
            try {

                PicassoTrustAll.getInstance(getActivity())
                        .load(MyConstants.editImageUrl)
                        .placeholder(R.drawable.no_image)
                        .error(R.drawable.no_image)
                        .into(img_priview);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            if (count == 1) {
                count++;
                //if (!App.edit) {
                mPhotoEditor.addImage(bitmap);
                //}
            }
            img_priview.setVisibility(View.GONE);
        }

 /*       if (frsttime) {

            if (App.bitmapArrayList.size() > 0) {
                mPhotoEditor.addImage(App.bitmapArrayList.get(App.bitmapArrayList.size() - 1));
            } else if (App.bitmapArrayList.size() != 0) {
                mPhotoEditor.addImage(App.bitmapArrayList.get(0)); //original image
            }


            frsttime = false;
        } else {*/
            /*if (tempCount == 1) {
                tempCount = 0;
            } else {*/

        if (frsttime) {
            Log.d(TAG, "onResume: " + App.bitmapArrayList.size());
           /* if (App.bitmapArrayList.size() > 1) {
                //  mPhotoEditor.addImage(App.bitmapArrayList.get(App.bitmapArrayList.size() - 1));
                mPhotoEditor.addImage(App.bitmapArrayList.get(0));

            } else */
            if (App.bitmapArrayList.size() > 0) {
                mPhotoEditor.addImage(App.bitmapArrayList.get(0)); //original image
                App.bitmapArrayList = new ArrayList<>();
            }
            frsttime = false;
            App.bitmap = null;
        }

              /*  }
                tempCount++;*/

        //  }


    }

    public static Bitmap createTrimmedBitmap(Bitmap bmp) {
        int imgHeight = bmp.getHeight();
        int imgWidth = bmp.getWidth();
        int smallX = 0, largeX = imgWidth, smallY = 0, largeY = imgHeight;
        int left = imgWidth, right = imgWidth, top = imgHeight, bottom = imgHeight;
        for (int i = 0; i < imgWidth; i++) {
            for (int j = 0; j < imgHeight; j++) {
                if (bmp.getPixel(i, j) != Color.TRANSPARENT) {
                    if ((i - smallX) < left) {
                        left = (i - smallX);
                    }
                    if ((largeX - i) < right) {
                        right = (largeX - i);
                    }
                    if ((j - smallY) < top) {
                        top = (j - smallY);
                    }
                    if ((largeY - j) < bottom) {
                        bottom = (largeY - j);
                    }
                }
            }
        }
        Log.d("TAG", "left:" + left + " right:" + right + " top:" + top + " bottom:" + bottom);
        bmp = Bitmap.createBitmap(bmp, left, top, imgWidth - left - right, imgHeight - top - bottom);

        return bmp;

    }

    private void openImageChooserDialog() {

        createDialog();
        initDialogComponents();
        // dialog.dismiss();

    }

    private void createDialog() {
        dialog = new Dialog(getActivity(), R.style.ChatDialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        // Include dialog.xml file
        dialog.setContentView(R.layout.dialog_image_chooser);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        lp.copyFrom(window.getAttributes());
        //This makes the dialog take up the full width
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
        isReadStorageAllowed();
        dialog.show();

    }

    private void initDialogComponents() {
        llCamera = dialog.findViewById(R.id.llCamera);
        llGallery = dialog.findViewById(R.id.llGallery);
        llFromMyCloset = dialog.findViewById(R.id.llFromMyCloset);
        llCollageMaker = dialog.findViewById(R.id.llCollageMaker);

        if (MyConstants.isEditable) {
            llFromMyCloset.setVisibility(View.GONE);
            llCollageMaker.setVisibility(View.GONE);
        }
        llCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (isReadStorageAllowed()) {
                    File f = new File(Environment.getExternalStorageDirectory() + "/Plus_Me/Images");
                    if (!f.exists()) {
                        f.mkdirs();
                    }
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    File file = new File(Environment.getExternalStorageDirectory(), "/Plus_Me/Images" + System.currentTimeMillis() + ".jpg");
                    mCurrentPhotoPathUri = Uri.fromFile(file);
                    mCurrentPhotoPath = file.getAbsolutePath();
                    intent.putExtra(MediaStore.EXTRA_OUTPUT, mCurrentPhotoPathUri);
                    startActivityForResult(intent, REQUEST_IMAGE_CAPTURE);


                }
                fabAdd.setVisibility(View.VISIBLE);

            }
        });

        llGallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (isReadStorageAllowed()) {
                    Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                    intent.setType("image/*");
                    startActivityForResult(intent, REQUEST_IMAGE_SELECT);
                }
                // openGallery();
                fabAdd.setVisibility(View.VISIBLE);


            }
        });


        llFromMyCloset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                replaceFragment(new SelectClosetImageFragment(), false);
                dialog.dismiss();
                fabAdd.setVisibility(View.VISIBLE);
                //   rel_data.setVisibility(View.GONE);

            }
        });
        llCollageMaker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
    /*            itemSave.setVisible(false);
                itemSaveCollage.setVisible(true);

                fabAdd.setVisibility(View.VISIBLE);
                openCollageMaker();
                flCropImage.setVisibility(View.VISIBLE);
                fabAdd.setVisibility(View.VISIBLE);*/
                replaceFragment(new CollageMakerFragment(), false);

            }
        });
    }


    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            isCamera = true;
            Log.d(TAG, "onActivityResult: " + mCurrentPhotoPath);
            MyConstants.editImageUrl = "";
            setCapturedImage(mCurrentPhotoPath);
            itemSave.setVisible(true);
            itemReset.setVisible(true);

            if (dialog != null)
                dialog.dismiss();
        } else if (requestCode == REQUEST_IMAGE_SELECT && resultCode == RESULT_OK) {
            isCamera = false;
            //  if (selectedImage != null) {
            Uri selectedImg = data.getData();
            imgFile = new File(getFilePath(getContext(), selectedImg));
            MyConstants.editImageUrl = "";
            setCapturedImage(imgFile.getPath());
            itemSave.setVisible(true);
            itemReset.setVisible(true);
            //    }
            if (dialog != null)
                dialog.dismiss();
        }


    }

//    @SuppressLint("NewApi")
//    public static String getFilePath(Context context, Uri uri) {
//        String selection = null;
//        String[] selectionArgs = null;
//        // Uri is different in versions after KITKAT (Android 4.4), we need to
//        if (Build.VERSION.SDK_INT >= 19 && DocumentsContract.isDocumentUri(context, uri)) {
//            if (isExternalStorageDocument(uri)) {
//                final String docId = DocumentsContract.getDocumentId(uri);
//                final String[] split = docId.split(":");
//                return Environment.getExternalStorageDirectory() + "/" + split[1];
//            } else if (isDownloadsDocument(uri)) {
//                final String id = DocumentsContract.getDocumentId(uri);
//                uri = ContentUris.withAppendedId(
//                        Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));
//            } else if (isMediaDocument(uri)) {
//                final String docId = DocumentsContract.getDocumentId(uri);
//                final String[] split = docId.split(":");
//                final String type = split[0];
//                if (MyConstants.image.equals(type)) {
//                    uri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
//                }
//                selection = "_id=?";
//                selectionArgs = new String[]{
//                        split[1]
//                };
//            }
//        }
//        if (MyConstants.content.equalsIgnoreCase(uri.getScheme())) {
//            String[] projection = {
//                    MediaStore.Images.Media.DATA
//            };
//            Cursor cursor = null;
//            try {
//                cursor = context.getContentResolver()
//                        .query(uri, projection, selection, selectionArgs, null);
//                int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
//                if (cursor.moveToFirst()) {
//                    return cursor.getString(column_index);
//                }
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//        } else if (MyConstants.file.equalsIgnoreCase(uri.getScheme())) {
//            return uri.getPath();
//        }
//        return null;
//    }


    private void setCapturedImage(final String imagePath) {
        new AsyncTask<Void, Void, String>() {
            @Override
            protected String doInBackground(Void... params) {

                //Last Chnages here 18/12/2019 by ayaz for webp file

                if (!imagePath.contains(".webp")) {
                    try {
                        return getRightAngleImage(imagePath);
//                        return imagePath;
                    } catch (Throwable e) {
                        e.printStackTrace();
                    }
                } else {
                    return imagePath;
                }
                return imagePath;
            }

            @Override
            protected void onPostExecute(String imagePath) {
                super.onPostExecute(imagePath);
                //  Picasso.with(AddPeepView.this).load(mCurrentPhotoPathUri).into(mSelectedTheme);
                // Picasso.with(AddPeepView.this).load(mCurrentPhotoPathUri).into(mUploadImagePreview);
                //  imgFile = new File(imagePath);//= new Compressor(AddPeepView.this).compressToFile(new File(imagePath));
                Intent mIntent = new Intent(getActivity(), ChooseImageActivity.class);
                Bundle mBundle = new Bundle();
                mBundle.putString("image", imagePath);
                mBundle.putBoolean("isCamera", isCamera);
                mBundle.putString("isEdit", "No");
                System.out.println("image path from camera>>" + imagePath);

                mIntent.putExtras(mBundle);

                startActivity(mIntent);
                rel_data.setVisibility(View.GONE);
            }
        }.execute();
    }

    private String getRightAngleImage(String photoPath) {

        try {
            ExifInterface ei = new ExifInterface(photoPath);
            int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);
            int degree = 0;

            switch (orientation) {
                case ExifInterface.ORIENTATION_NORMAL:
                case ExifInterface.ORIENTATION_UNDEFINED:
                    degree = 0;
                    break;
                case ExifInterface.ORIENTATION_ROTATE_90:
                    degree = 90;
                    break;
                case ExifInterface.ORIENTATION_ROTATE_180:
                    degree = 180;
                    break;
                case ExifInterface.ORIENTATION_ROTATE_270:
                    degree = 270;
                    break;
                default:
                    degree = 90;
            }

            return rotateImage(degree, photoPath);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return photoPath;
    }


    private String rotateImage(int degree, String imagePath) {


        try {
            Bitmap b = BitmapFactory.decodeFile(imagePath);

            Matrix matrix = new Matrix();
            if (b.getWidth() > b.getHeight()) {
                matrix.setRotate(degree);
                b = Bitmap.createBitmap(b, 0, 0, b.getWidth(), b.getHeight(),
                        matrix, true);
            }

            FileOutputStream fOut = new FileOutputStream(imagePath);
            String imageName = imagePath.substring(imagePath.lastIndexOf("/") + 1);
            String imageType = imageName.substring(imageName.lastIndexOf(".") + 1);

            FileOutputStream out = new FileOutputStream(imagePath);
            if (imageType.equalsIgnoreCase("png")) {
                b.compress(Bitmap.CompressFormat.PNG, 100, out);
            } else if (imageType.equalsIgnoreCase("jpeg") || imageType.equalsIgnoreCase("jpg")) {
                b.compress(Bitmap.CompressFormat.JPEG, 100, out);
            }
            fOut.flush();
            fOut.close();
            App.bitmapCemera = b;

            b.recycle();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return imagePath;
    }

    private View.OnClickListener btnChangePhoto_OnClickListener = new View.OnClickListener() {
        public void onClick(View v) {


            createDilogForMyvisionBordOrGallery();
            //check for read external storage permision

        }
    };

    private void createDilogForMyvisionBordOrGallery() {
        final Dialog dialogCollage = new Dialog(this.activity, R.style.ChatDialog);
        dialogCollage.requestWindowFeature(Window.FEATURE_NO_TITLE);
        // Include dialogCollage.xml file
        dialogCollage.setContentView(R.layout.dialog_choose_collage_maker);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialogCollage.getWindow();
        lp.copyFrom(window.getAttributes());
        //This makes the dialogCollage take up the full width
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
        LinearLayout llFromMyVisionBoard = dialogCollage.findViewById(R.id.llFromMyVisionBoard);
        LinearLayout llGallery = dialogCollage.findViewById(R.id.llGallery);
        llGallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogCollage.dismiss();
                if (ActivityCompat.checkSelfPermission(getActivity(), android.Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {

                    Intent intent = new Intent(getActivity(), GalleryActivity.class);
                    intent.putExtra(INTENT_KEY_GALLERY_SELECTMULTI, true);
                    startActivityForResult(intent, INTENT_RESULT_GALLERY);

                } else {
                    ActivityCompat.requestPermissions(getActivity(), new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE}, MyConstants.PERMISSIONS_REQUEST_READ_STORAGE);
                }
            }
        });

       /* llFromMyVisionBoard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ActivityCompat.checkSelfPermission(getActivity(), android.Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {


                    Intent intent = new Intent(getActivity(), GalleryActivityVisionBoard.class);
                    intent.putExtra(INTENT_KEY_GALLERY_SELECTMULTI, true);
                    startActivityForResult(intent, INTENT_RESULT_VISIONBOARD);


                } else {
                    ActivityCompat.requestPermissions(getActivity(), new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE}, MyConstants.PERMISSIONS_REQUEST_READ_STORAGE);
                }
            }
        });*/
        dialogCollage.show();
    }


    /* @Override
     protected void onPhotoTaken() {
         dialog.dismiss();

         System.out.println("sub image::" + selectedImagePath);

         //pager.setCurrentItem(0);
         Intent mIntent = new Intent(getActivity(), ChooseImageActivity.class);
         Bundle mBundle = new Bundle();
         mBundle.putString("image", selectedImagePath);
         mBundle.putString("isEdit", "No");
         mBundle.putString("BitmapImage", "No");

         mIntent.putExtras(mBundle);
         startActivity(mIntent);
         //mLayout.setPanelState(SlidingUpPanelLayout.PanelState.EXPANDED);

     }
 */
    private void initializeData() {
        toolbar.setText("Vision Board");
        fabAdd.setVisibility(View.VISIBLE);
        if (!TextUtils.isEmpty(MyConstants.editImageUrl)) {
            isSubImage = true;
            //  Picasso.with(getContext()).load(MyConstants.editImageUrl).into(photoEditImageView);
            FileOutputStream out = null;
            MyConstants.isEditable = true;
            fabEdit.setVisibility(View.VISIBLE);
            fabEdit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    isEditedNowInCreateLook = true;
                    Intent mIntent = new Intent(getActivity(), ChooseImageActivity.class);
                    Bundle mBundle = new Bundle();
                    mBundle.putString("image", MyConstants.editImageUrl);
                    mBundle.putString("isEdit", "yes");
                    mIntent.putExtras(mBundle);
                    startActivity(mIntent);

                }
            });

        }
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        txtSave.setVisibility(View.GONE);
        ivDown.setVisibility(View.GONE);
        toolbar.setText("Create a Look");
        itemReset = menu.findItem(R.id.item_reset);
        itemSaveCollage = menu.findItem(R.id.item_save_collage);
        // itemAddNew = menu.findItem(R.id.item_add_new);
        itemSave = menu.findItem(R.id.item_save);
        //itemReset.setVisible(true);
        // itemAddNew.setVisible(true);
        itemSaveCollage.setVisible(false);

        if (!TextUtils.isEmpty(MyConstants.editImageUrl) && getArguments() != null && getArguments().getString("frommyclose").equalsIgnoreCase("myclose")) {
            itemSave.setVisible(true);
            itemReset.setVisible(true);
        } else {
            itemSave.setVisible(false);
            itemReset.setVisible(false);

        }

        if (MyConstants.imageType.equalsIgnoreCase("1")) {
            //setMyClosetData();
            MyConstants.imageType = "";
        }

        /*menu.getItem(0).setVisible(false);
        menu.getItem(1).setVisible(false);
        menu.getItem(2).setVisible(false);
        menu.getItem(3).setVisible(false);*/

        super.onPrepareOptionsMenu(menu);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
        inflater = getActivity().getMenuInflater();
        inflater.inflate(R.menu.menu_vision_board, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();


        if (id == R.id.item_reset) {
            mPhotoEditor.clearAllViews();
            App.bitmapArrayList.clear();
            itemReset.setVisible(false);
            itemSave.setVisible(false);
            rel_data.setVisibility(View.VISIBLE);
            return true;
        } else if (id == R.id.item_add_new) {
            return true;
        } else if (id == R.id.item_save) {

            openSaveDialog();
            return true;
        } else if (id == R.id.item_save_collage) {


            return true;
        } else if (id == R.id.item_home) {
            replaceFragment(new HomeFragment(), true);
        }

        return super.onOptionsItemSelected(item);
    }

    private void openSaveDialog() {
        createSaveDialog();
        initSaveDialogComponents();
    }

    private void createSaveDialog() {
        dialog1 = new Dialog(this.activity, R.style.ChatDialog);
        dialog1.requestWindowFeature(Window.FEATURE_NO_TITLE);
        // Include dialog.xml file
        dialog1.setContentView(R.layout.dialog_save_image);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog1.getWindow();
        lp.copyFrom(window.getAttributes());
        dialog1.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        lp.dimAmount = 0.0f; // Dim level. 0.0 - no dim, 1.0 - completely opaque
        //This makes the dialog take up the full width
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
        dialog1.show();
    }

    private void initSaveDialogComponents() {
        btnVisionBoardSave = dialog1.findViewById(R.id.btnVisonboardSave);
        btnMyClosetSave = dialog1.findViewById(R.id.btnMyCloset);
        btnSaveAsMyCloset = dialog1.findViewById(R.id.btnNewMyCloset);
        btnCancel = dialog1.findViewById(R.id.btnCancel);
        if (!MyConstants.isEditable) {
            btnSaveAsMyCloset.setVisibility(View.GONE);
        }


        btnVisionBoardSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toVisionBoar = true;
                if (isReadStorageAllowed(PICK_IMAGE_PERMISSION_CODE_MYCLOSET))
                    returnBackWithSavedImage();
            }
        });
        btnMyClosetSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toVisionBoar = false;
                toMyCloset = true;
                if (isReadStorageAllowed(PICK_IMAGE_PERMISSION_CODE_MYCLOSET))
                    returnBackWithSavedImage();

            }
        });
        btnSaveAsMyCloset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toVisionBoar = false;
                toMyCloset = true;
                doOwerWrite = true;
                if (isReadStorageAllowed(PICK_IMAGE_PERMISSION_CODE_MYCLOSET))
                    returnBackWithSavedImage();

            }
        });
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog1.dismiss();
            }
        });
    }

    private boolean isReadStorageAllowed(int ID) {

        //If permission is granted returning true
        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE)
                + ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE)
                + ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA)
                == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA}, ID);
            return false;
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        Log.i("Requestcode:", "Code :->? " + requestCode);


        if (grantResults.length > 0) {

            boolean readExternalFile = grantResults[0] == PackageManager.PERMISSION_GRANTED;
            boolean writeExternalFile = grantResults[1] == PackageManager.PERMISSION_GRANTED;
            boolean cameraPermission = grantResults[2] == PackageManager.PERMISSION_GRANTED;
            if (readExternalFile && writeExternalFile && cameraPermission) {
                if (requestCode == PICK_IMAGE_PERMISSION_CODE_MYCLOSET) {
                    // returnBackWithSavedImage();

                }
            }
        }
    }

    private void returnBackWithSavedImage() {
        //updateView(View.GONE);

        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(
                RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
        layoutParams.addRule(RelativeLayout.CENTER_IN_PARENT, RelativeLayout.TRUE);
        String savedPath;
        if (!TextUtils.isEmpty(MyConstants.editImageUrl) && getArguments() != null && getArguments().getString("frommyclose").equalsIgnoreCase("myclose")) {
            savedPath = MyConstants.editImageUrl;
        } else {
            savedPath = saveImage();
        }
        Log.d(TAG, "returnBackWithSavedImage: " + savedPath);

        if (!TextUtils.isEmpty(MyConstants.editImageUrl)) {
            if (toVisionBoar) {
                dialogViewImage(savedPath);
            }
            if (toMyCloset) {
                moveToMyCloset(savedPath);
            }
        }


    }

    private void moveToMyCloset(String savedPath) {

        Log.d(TAG, "moveToMyCloset: " + savedPath);
        Map<String, String> map = new HashMap<>();
        map.put("url", MyConstants.BASE_URL + "MoveToCloset");

        map.put("PostImage", savedPath);
        map.put("Title", "");
        if (doOwerWrite) {
            map.put("BaseImageID", MyConstants.baseImageId);
        } else {
            map.put("BaseImageID", "");
        }
        map.put("header", "");
        map.put("Auth", App.user.getAuth());

        if (!TextUtils.isEmpty(MyConstants.editImageUrl) && getArguments() != null && getArguments().getString("frommyclose").equalsIgnoreCase("myclose")) {
            map.put("Type", "URL");
            new CallRequest(instace).moveToClosetusingUrl(map);
        } else {
            map.put("Type", "IMAGE");
            new CallRequest(instace).moveToCloset(map);
        }

    }

    private void dialogViewImage(final String image) {

        Log.d(TAG, "image>>>: " + image);
        dialogSaveVisionBoard = new Dialog(getActivity());
        dialogSaveVisionBoard.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogSaveVisionBoard.setCancelable(true);
        dialogSaveVisionBoard.setContentView(R.layout.dialog_image_move_tocloset);
        dialogSaveVisionBoard.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        ImageView ivImage = dialogSaveVisionBoard.findViewById(R.id.ivImage);
        Button btnMoveToVisionBoard = dialogSaveVisionBoard.findViewById(R.id.btnMoveToCloset);

        Spinner spCatagory = dialogSaveVisionBoard.findViewById(R.id.spCatagory);

        System.out.println("country list==" + MyConstants.categoriesList.size());
        ArrayAdapter aaCountry = new ArrayAdapter<>(getActivity(), R.layout.spinner_text_black, MyConstants.categoriesListString);
        aaCountry.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spCatagory.setAdapter(new NothingSelectedSpinnerAdapter(
                aaCountry, R.layout.spinner_text_black,
                getContext(), "Select Category"));

        if (!image.contains("http"))
            Picasso.with(getActivity()).load(new File(image)).into(ivImage);
        else
            Picasso.with(getActivity()).load(image).into(ivImage);
        btnMoveToVisionBoard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (TextUtils.isEmpty(catagory)) {
                    Utils.showToast("Please Select Category", getActivity());
                } else {
                    dialogSaveVisionBoard.dismiss();
                    //  doOwerWrite = true;
                    moveToVisionBoard(image);

                }
            }
        });
        spCatagory.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                try {
                    catagory = MyConstants.categoriesList.get(position - 1).getCategoryID();
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        dialogSaveVisionBoard.show();
    }

    private void moveToVisionBoard(String selectedOutputPath) {
        Log.d(TAG, "moveToVisionBoard: " + selectedOutputPath);
        Map<String, String> map = new HashMap<>();
        map.put("url", MyConstants.BASE_URL + "MoveToVisionBoard");
        map.put("PostImage", selectedOutputPath);
        map.put("CategoryID", catagory);
        map.put("header", "");
        map.put("Auth", App.user.getAuth());
        if (!TextUtils.isEmpty(MyConstants.editImageUrl) && getArguments() != null && getArguments().getString("frommyclose").equalsIgnoreCase("myclose")) {
            map.put("Type", "URL");
            new CallRequest(instace).moveimageclosettovision(map);
        } else {
            map.put("Type", "IMAGE");
            new CallRequest(instace).addToVisionBoard(map);
        }
    }

    @SuppressLint("MissingPermission")
    private String saveImage() {
        File file = null;
        if (requestPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            showLoading("Saving...");
            file = new File(Environment.getExternalStorageDirectory()
                    + File.separator + ""
                    + System.currentTimeMillis() + ".png");
            Log.d(TAG, "file 1>>: " + file.getAbsolutePath());
            Bitmap bitmap = BitmapFactory.decodeFile(file.getAbsolutePath());
            mPhotoEditorView.getSource().setImageBitmap(bitmap);
            try {
                file.createNewFile();

                mPhotoEditor.saveImage(file.getAbsolutePath(), new PhotoEditor.OnSaveListener() {
                    @Override
                    public void onSuccess(@NonNull String imagePath) {
                        hideLoading();
                        // mPhotoEditorView.getSource().setImageURI(Uri.fromFile(new File(imagePath)));


                        if (toVisionBoar) {
                            dialogViewImage(imagePath);
                        }
                        if (toMyCloset) {
                            moveToMyCloset(imagePath);
                        }
                    }

                    @Override
                    public void onFailure(@NonNull Exception exception) {
                        hideLoading();
                    }
                });
            } catch (ClassCastException e) {
                e.printStackTrace();
                hideLoading();
            } catch (IOException e) {
                e.printStackTrace();
                hideLoading();
            }
        }
        return file.getAbsolutePath();
    }

    protected void showLoading(@NonNull String message) {
        mProgressDialog = new ProgressDialog(this.activity);
        mProgressDialog.setMessage(message);
        mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        mProgressDialog.setCancelable(false);
        mProgressDialog.show();
    }

    protected void hideLoading() {
        if (mProgressDialog != null) {
            mProgressDialog.dismiss();
        }
    }


    public boolean requestPermission(String permission) {
        boolean isGranted = ContextCompat.checkSelfPermission(this.activity, permission) == PackageManager.PERMISSION_GRANTED;
        if (!isGranted) {
            ActivityCompat.requestPermissions(
                    this.activity,
                    new String[]{permission},
                    1);
        }
        return isGranted;
    }


    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
        MyConstants.editImageUrl = "";

    /*    if (App.bitmapCemera != null) {
            App.bitmapCemera.recycle();
            App.bitmapCemera = null;
            App.bitmap.recycle();
            App.bitmap = null;

        }*/
        if (App.bitmap != null) {
            App.bitmap.recycle();
            App.bitmap = null;
        }

    }

    @Override
    public void onDestroyView() {
        App.bitmapArrayList.clear();
        MyConstants.editImageUrl = "";
        if (App.bitmap != null) {
            App.bitmap.recycle();
            App.bitmap = null;
        }
      /*  if (App.bitmapCemera != null) {
            App.bitmapCemera.recycle();
            App.bitmapCemera = null;
        }*/

        super.onDestroyView();
    }

    @Override
    public void onDestroy() {
        App.bitmapArrayList.clear();
        MyConstants.editImageUrl = "";
    /*    if (App.bitmapCemera != null) {
            App.bitmapCemera.recycle();
            App.bitmapCemera = null;
        }*/
        if (App.bitmap != null) {
            App.bitmap.recycle();
            App.bitmap = null;
        }
        super.onDestroy();
    }

    @Override
    public void onEditTextChangeListener(View rootView, String text, int colorCode) {

    }

    @Override
    public void onAddViewListener(ViewType viewType, int numberOfAddedViews) {

    }

    @Override
    public void onRemoveViewListener(int numberOfAddedViews) {

    }

    @Override
    public void onRemoveViewListener(ViewType viewType, int numberOfAddedViews) {

    }

    @Override
    public void onStartViewChangeListener(ViewType viewType) {

    }

    @Override
    public void onStopViewChangeListener(ViewType viewType) {

    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        try {

            Utils.hideProgressDialog();
            if (result != null && !result.isEmpty()) {
                Log.i("TAG", "TAG Result : " + result);

                switch (request) {
                    case addToVisionBoard:
                        try {
                            JSONObject object = new JSONObject(result);
                            boolean success = object.getBoolean("success");
                            if (success) {
                                // TODO parse JSON
                                // dialogSaveVisionBoard.dismiss();

                                dialog1.dismiss();

                                Utils.showToast(object.getString("message"), getContext());


                                MyConstants.isFromCreateLook = false;
                                HomeActivity.replaceFragment(new VisionBoardLatestFragment(), false);
                                App.edit = false;

                            } else {
                                Utils.showToast(object.getString("message"), getContext());
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;

                    case moveTocloset:
                        try {
                            JSONObject object = new JSONObject(result);
                            boolean success = object.getBoolean("success");
                            if (success) {
                                // TODO parse JSON
                                Utils.showToast(object.getString("message"), getContext());
                                dialog1.dismiss();
                                MyConstants.isFromCreateLook = true;
                                HomeActivity.replaceFragment(new HomeFragment(), false);
                                App.edit = false;
                            } else {
                                Utils.showToast(object.getString("message"), getContext());
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        dialog1.dismiss();
                        break;
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onProgressUpdate(String uniqueMessageId, int progres) {

    }

    @Override
    public void onProgressComplete(String uniqueMessageId, String result, Constant.REQUESTS request) {

    }

}
