package com.plus.me.model;

import java.io.Serializable;

public class Post implements Serializable {

    public String BlogId = "", PostedByName = "", PostImage = "", ProfileImage = "", PostTime = "", PostTitle = "", Description = "", RegisterUserTypeByName = "",PostedByID="";

    public String getBlogId() {
        return BlogId;
    }

    public void setBlogId(String blogId) {
        BlogId = blogId;
    }

    public String getPostedByName() {
        return PostedByName;
    }

    public void setPostedByName(String postedByName) {
        PostedByName = postedByName;
    }

    public String getPostImage() {
        return PostImage;
    }

    public void setPostImage(String postImage) {
        PostImage = postImage;
    }

    public String getProfileImage() {
        return ProfileImage;
    }

    public void setProfileImage(String profileImage) {
        ProfileImage = profileImage;
    }

    public String getPostTime() {
        return PostTime;
    }

    public void setPostTime(String postTime) {
        PostTime = postTime;
    }

    public String getPostTitle() {
        return PostTitle;
    }

    public void setPostTitle(String postTitle) {
        PostTitle = postTitle;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public String getRegisterUserTypeByName() {
        return RegisterUserTypeByName;
    }

    public void setRegisterUserTypeByName(String registerUserTypeByName) {
        RegisterUserTypeByName = registerUserTypeByName;
    }

    public String getPostedByID() {
        return PostedByID;
    }

    public void setPostedByID(String postedByID) {
        PostedByID = postedByID;
    }
}
