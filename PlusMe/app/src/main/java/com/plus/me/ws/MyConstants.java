package com.plus.me.ws;

import com.plus.me.BuildConfig;
import com.plus.me.model.Categories;
import com.plus.me.model.City;
import com.plus.me.model.Country;
import com.plus.me.model.MyCloset;
import com.plus.me.model.State;
import com.plus.me.model.UserProfile;
import com.plus.me.model.VisionBoard;

import java.util.ArrayList;

public class MyConstants {

    //old url
    // public static final String MAIN_URL = "  http://clientworkstore.us/plusme/";

    //test URLs
    public static final String MAIN_URL = "https://plusme.blenzabi.com/admin/";

    //LIVE URL
   // public static final String MAIN_URL = "http://plusmestyle.com/admin/";



    public static final String BASE_URL = MAIN_URL + "api/web_service/";

    //  public static final String TEST_URL="http://www.humbolt.xyz/";
    // device id
    public static String DEVICE_ID = "", IMEI = "", MODEL_NAME;

    public static final String MY_PREF_NAME = "PLUS_ME";
    public static final String EMAIL = "Email";
    public static final String AUTH = "Auth";
    public static final String USER_ID = "UserId";
    public static final String PASSWORD = "Password";
    public static final String NAME = "Name";
    public static final String FIRST_NAME = "FirstName";
    public static final String LAST_NAME = "LastName";
    public static final String PROFILE_IMAGE = "ProfileImage";
    public static UserProfile userProfile;
    public static Categories category;
    public static Country country;
    public static State state;
    public static City city;
    public static String categoryId = "";
    public static String categoryName = "";
    public static boolean isEditable = false;
    public static boolean isEditedNowInCreateLook = false;
    public static boolean isNotCroped = false;
    public static ArrayList<Categories> categoriesList = new ArrayList<>();

    public static ArrayList<String> categoriesListString = new ArrayList<>();
    public static ArrayList<byte[]> selectedBitmapInString = new ArrayList<byte[]>();

    public static ArrayList<Country> countryList = new ArrayList<>();
    public static ArrayList<String> countryListString = new ArrayList<>();

    public static ArrayList<City> cityList = new ArrayList<>();
    public static ArrayList<String> cityListString = new ArrayList<>();

    public static ArrayList<State> stateList = new ArrayList<>();
    public static ArrayList<String> stateListString = new ArrayList<>();
    public static ArrayList<MyCloset> myClosetList = new ArrayList<>();

    public static ArrayList<VisionBoard> myVisionBoardList = new ArrayList<>();

    public static boolean isUserProfile = false;
    public static int profileCount = 0;
    public static String otherUserId, selectdClosetBlogId, selectedImage;
    public static String imageType = "";
    public static String countryFilterId = "", stateFilterId = "", cityFilterId = "", zipCode = "";
    public static final boolean DEBUG = true;
    public static boolean isDesignerScreen = false, isStylistScreen = false, isStoresScreen = false;
    /* working image file */
    public static final String IMAGE_WORKING_FILE = BuildConfig.APPLICATION_ID + ".workingimage";

    public static final int PERMISSIONS_REQUEST_CAMERA_EXTERNAL_STORAGE = 102;
    public static final int PERMISSIONS_REQUEST_READ_STORAGE = 103;
    /* activty intent parm keys */
    public static final String INTENT_KEY_GALLERY_SELECTMULTI = BuildConfig.APPLICATION_ID + ".gallery.selectmulti";
    public static final String INTENT_KEY_CROP_IMGPATHLIST = BuildConfig.APPLICATION_ID + ".crop.imagepathlist";
    public static final String INTENT_KEY_CROP_IMGPATH = BuildConfig.APPLICATION_ID + ".crop.imagepath";
    public static String editImageUrl = "", baseImageId = "";
    public static boolean isFromCreateLook;
    /* activty result ids */
    public static final int INTENT_RESULT_VISIONBOARD = 100;
    public static final int INTENT_RESULT_GALLERY = 101;
    public static final int INTENT_RESULT_CAMERA = 102;
    public static final int INTENT_RESULT_CROP = 103;
    public static String content = "content";
    public static String image = "image";
    public static String file = "file";

    public static final String PUBLIC_PHOTO_FOLDER = "PlusMe";
}
