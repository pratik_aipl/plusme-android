package com.plus.me.model;

import java.util.ArrayList;

public class BlogDetail {

    public String BlogId,PostedByName,PostImage,ProfileImage,
            PostTime,PostTitle,Description,TotalComment,TotalLike,IsCloset,isLike,PostedByID,RegisterUserTypeByName;
    public ArrayList<CommentDetail>commentDetails;

    public String getTotalLike() {
        return TotalLike;
    }

    public void setTotalLike(String totalLike) {
        TotalLike = totalLike;
    }

    public String getBlogId() {
        return BlogId;
    }

    public void setBlogId(String blogId) {
        BlogId = blogId;
    }

    public String getPostedByName() {
        return PostedByName;
    }

    public void setPostedByName(String postedByName) {
        PostedByName = postedByName;
    }

    public String getPostImage() {
        return PostImage;
    }

    public void setPostImage(String postImage) {
        PostImage = postImage;
    }

    public String getProfileImage() {
        return ProfileImage;
    }

    public void setProfileImage(String profileImage) {
        ProfileImage = profileImage;
    }

    public String getPostTime() {
        return PostTime;
    }

    public void setPostTime(String postTime) {
        PostTime = postTime;
    }

    public String getPostTitle() {
        return PostTitle;
    }

    public void setPostTitle(String postTitle) {
        PostTitle = postTitle;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public String getTotalComment() {
        return TotalComment;
    }

    public void setTotalComment(String totalComment) {
        TotalComment = totalComment;
    }

    public String getIsCloset() {
        return IsCloset;
    }

    public void setIsCloset(String isCloset) {
        IsCloset = isCloset;
    }

    public String getIsLike() {
        return isLike;
    }

    public void setIsLike(String isLike) {
        this.isLike = isLike;
    }

    public String getPostedByID() {
        return PostedByID;
    }

    public void setPostedByID(String postedByID) {
        PostedByID = postedByID;
    }

    public ArrayList<CommentDetail> getCommentDetails() {
        return commentDetails;
    }

    public void setCommentDetails(ArrayList<CommentDetail> commentDetails) {
        this.commentDetails = commentDetails;
    }

    public String getRegisterUserTypeByName() {
        return RegisterUserTypeByName;
    }

    public void setRegisterUserTypeByName(String registerUserTypeByName) {
        RegisterUserTypeByName = registerUserTypeByName;
    }
}
