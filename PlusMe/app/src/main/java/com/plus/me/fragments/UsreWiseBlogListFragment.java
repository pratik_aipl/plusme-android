package com.plus.me.fragments;


import android.app.Dialog;
import android.os.Bundle;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import androidx.fragment.app.Fragment;
import androidx.core.view.MenuItemCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.SearchView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import com.plus.me.R;
import com.plus.me.activity.HomeActivity;
import com.plus.me.adapter.CatagoryPopupAdapter;
import com.plus.me.adapter.UserBlogListAdapter;
import com.plus.me.listener.BlogListener;
import com.plus.me.listener.CatagoriesPopupListener;
import com.plus.me.listener.FilterList;
import com.plus.me.model.BlogList;
import com.plus.me.model.UserBlog;
import com.plus.me.others.App;
import com.plus.me.others.Internet;
import com.plus.me.ws.AsyncTaskListner;
import com.plus.me.ws.CallRequest;
import com.plus.me.ws.Constant;
import com.plus.me.ws.JsonParserUniversal;
import com.plus.me.ws.MyConstants;
import com.plus.me.ws.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static com.plus.me.activity.HomeActivity.ivDown;
import static com.plus.me.activity.HomeActivity.replaceFragment;
import static com.plus.me.activity.HomeActivity.setupBadge;
import static com.plus.me.activity.HomeActivity.toolbar;
import static com.plus.me.activity.HomeActivity.txtSave;

/**
 * A simple {@link Fragment} subclass.
 */
public class UsreWiseBlogListFragment extends Fragment implements AsyncTaskListner, CatagoriesPopupListener, BlogListener, FilterList {
    UserBlog blogList;
    private static final String TAG = "UsreWiseBlogListFragmen";
    public JsonParserUniversal jParser;
    public Menu menu;
    public SearchView searchView;
    public String categoryId = "";
    public TextView txtNoValue;
    Button imgnodata;
    private RecyclerView rvHome, rvCatagoryFilter;
    private UserBlogListAdapter homeAdapter;
    private CatagoryPopupAdapter categoriesAdapter;
    private ArrayList<UserBlog> userBlogArray = new ArrayList<>();
    Dialog materialDialog;

    private PopupWindow popup;
    public int position = 0;
    public FloatingActionButton fabAdd;

    public UsreWiseBlogListFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_blog_list, container, false);
        //tabs.setVisibility(View.VISIBLE);
        setHasOptionsMenu(true);

        bindWidgetReference(view);

        initializeData(view);
        fabAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                replaceFragment(new AddBlogFragment(), false);
            }
        });
        imgnodata.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getBlogList();
            }
        });


        // TODO comment addData(); and uncomment getBlogList();
        getBlogList();
        return view;
    }

    private void initializeData(View view) {
        App.isHomeFragment = true;
        toolbar.setClickable(true);
        if (App.isHomeFragment) {
            ivDown.setVisibility(View.GONE);
        }

        userBlogArray.clear();
        jParser = new JsonParserUniversal();

        toolbar.setText("Blogs");

        //setActionSpinner(getActivity(), view);

        if (!TextUtils.isEmpty(MyConstants.categoryId)) {
            categoryId = MyConstants.categoryId;
            MyConstants.categoryId = "";
            toolbar.setText(MyConstants.categoryName);
            MyConstants.categoryName = "";
        } else {
            categoryId = "";
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        //super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
        inflater.inflate(R.menu.home, menu);
        MenuItem menusearch = menu.findItem(R.id.item_serch);
        menusearch.setVisible(true);

//        EditText searchEditText = (EditText) searchView.findViewById(android.support.v7.appcompat.R.id.search_src_text);
//        searchEditText.setTextColor(getResources().getColor(R.color.black));

        menusearch.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {

                AddMDialog();
                return false;
            }
        });
/*
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {

                if (homeAdapter != null && userBlogArray.size() > 0) {
                    homeAdapter.getFilter().filter(newText);
                    Log.d(TAG, "onQueryTextChange: " + newText);
                }
                return true;
            }
        });*/


        final MenuItem menuItem = menu.findItem(R.id.item_notification);

        View actionView = MenuItemCompat.getActionView(menuItem);
        HomeActivity.tvNotificationItemCount = actionView.findViewById(R.id.notification_badge);
        FrameLayout frameNotification = actionView.findViewById(R.id.frameNotification);
        frameNotification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println("Notification====");
                replaceFragment(new NotificationFragment(), false);
            }
        });
        setupBadge();
        super.onCreateOptionsMenu(menu, inflater);
    }


    private void bindWidgetReference(View view) {

        fabAdd = view.findViewById(R.id.fabAdd);
        rvHome = view.findViewById(R.id.rvHome);
        txtNoValue = view.findViewById(R.id.txtNoValue);
        imgnodata = view.findViewById(R.id.imgnodata);
    }

    public void AddMDialog() {

        materialDialog = new Dialog(getActivity(), R.style.Theme_Dialog);
        materialDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        materialDialog.setCancelable(true);
        materialDialog.setContentView(R.layout.searchdatadialog);

        Button btnsearch;
        final EditText edtbyname, edtbytittlecontent;


        btnsearch = materialDialog.findViewById(R.id.btn_Bysearch);

        edtbyname = materialDialog.findViewById(R.id.edt_byName);
        edtbytittlecontent = materialDialog.findViewById(R.id.edt_byTittleContent);

        btnsearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (TextUtils.isEmpty(edtbyname.getText().toString()) && TextUtils.isEmpty(edtbytittlecontent.getText().toString().trim())) {
                    Toast.makeText(getActivity(), "Please Input Atlist One Filed", Toast.LENGTH_SHORT).show();
                } else {
                    if (homeAdapter != null && userBlogArray.size() > 0) {

                        if (!TextUtils.isEmpty(edtbyname.getText().toString()) && !TextUtils.isEmpty(edtbytittlecontent.getText().toString())) {
                            homeAdapter.getFilter().filter(edtbyname.getText().toString().trim() + "," + edtbytittlecontent.getText().toString().trim() + "," + "ALL");
                        } else if (!TextUtils.isEmpty(edtbyname.getText().toString())) {
                            homeAdapter.getFilter().filter(edtbyname.getText().toString().trim() + "," + "" + "," + "NAME");
                        } else if (!TextUtils.isEmpty(edtbytittlecontent.getText().toString())) {
                            homeAdapter.getFilter().filter(edtbytittlecontent.getText().toString().trim() + "," + "" + "," + "TC");
                        }
                    }

                    materialDialog.hide();

                    Log.d(TAG, "onQueryTextChange: " + edtbyname.getText().toString().trim()
                            + edtbytittlecontent.getText().toString().trim());
                }

            }
        });

        materialDialog.show();

    }

    private void addData() {
//        for (int i = 0; i < 3; i++) {
//            BlogList blogList = new BlogList();
//            blogList.setTotalComment("31 Comments");
//            blogList.setPostDescription("Lorem Ipsum is simply dummy text of the printing and typesetting industry.");
//            blogList.setPostTime("1 hour ago");
//            blogList.setTitle("Fashion Hub");
//            blogList.setPostedByName("by Adam Clark");
//            blogList.setPostImageURL("http://urbanmediatoday.com/wp-content/uploads/2016/07/fashion-world-768x371.jpg");
//            blogList.setUserProfileImage("http://urbanmediatoday.com/wp-content/uploads/2016/07/fashion-world-768x371.jpg");
//
//            blogLists.add(blogList);
//        }
    }

    private void getBlogList() {
        Map<String, String> map = new HashMap<>();
        map.put("url", MyConstants.BASE_URL + "UserBlogsList");
        // map.put("CategoryID", categoryId); // TODO add category id
        map.put("header", "");
        map.put("Auth", App.user.getAuth());

        if (!Internet.isAvailable(getActivity())) {
            Internet.showAlertDialog(getActivity(), "Error!", "No Internet Connection", false);
        } else {
            new CallRequest(UsreWiseBlogListFragment.this).getBlogList(map);
        }
    }

    private void setData() {
        System.out.println("Blog list size==" + userBlogArray.size());
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        rvHome.setLayoutManager(layoutManager);
        homeAdapter = new UserBlogListAdapter(getActivity(), userBlogArray, UsreWiseBlogListFragment.this);

        rvHome.setHasFixedSize(true);
        rvHome.setAdapter(homeAdapter);

        if (userBlogArray.size() == 0) {
            txtNoValue.setVisibility(View.VISIBLE);
        } else {
            txtNoValue.setVisibility(View.GONE);
            imgnodata.setVisibility(View.GONE);
        }
        rvHome.getRecycledViewPool().clear();
        homeAdapter.notifyDataSetChanged();
        rvHome.scrollToPosition(position);
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        txtSave.setVisibility(View.GONE);
        menu.getItem(0).setVisible(true);
        menu.getItem(1).setVisible(false);
        menu.getItem(2).setVisible(false);
        menu.getItem(3).setVisible(true);
        menu.getItem(4).setVisible(false);
        super.onPrepareOptionsMenu(menu);
    }


    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        try {

            Utils.hideProgressDialog();
            if (result != null && !result.isEmpty()) {
                Log.i("TAG", "TAG Result : " + result);

                switch (request) {
                    case getBlogList:
                        userBlogArray.clear();
                        try {
                            JSONObject object = new JSONObject(result);
                            boolean success = object.getBoolean("success");
                            if (success) {
                                // TODO parse JSON
                                JSONArray data = object.getJSONArray("data");
                                if (data != null && data.length() > 0) {
                                    for (int i = 0; i < data.length(); i++) {
                                        JSONObject jsonObject = data.getJSONObject(i);
                                        blogList = (UserBlog) jParser.parseJson(jsonObject, new UserBlog());

                                        JSONArray cData = jsonObject.getJSONArray("Blogs");
                                        if (cData.length() > 0) {
                                            for (int j = 0; j < cData.length(); j++) {
                                                JSONObject cObject = cData.getJSONObject(j);
                                                BlogList commetnList = (BlogList) jParser.parseJson(cObject, new BlogList());
                                                blogList.setSearchtittle(commetnList.getBlogTitle());
                                                blogList.setSearchDesc(commetnList.getDescription());
                                                blogList.blogArray.add(commetnList);
                                            }
                                        }
                                        userBlogArray.add(blogList);
                                        setData();
                                    }
                                } else {
                                    //  Utils.showToast(object.getString("message"), getActivity());
                                }
                            } else {
                                String error_string = object.getString("message");
                                Toast.makeText(getActivity(), error_string, Toast.LENGTH_SHORT).show();
                                setData();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;

                    case addToCloset:

                        try {
                            JSONObject object = new JSONObject(result);
                            boolean success = object.getBoolean("success");
                            if (success) {
                                // TODO parse JSON
                                Utils.showToast(object.getString("message"), getActivity());
                                getBlogList();

                            } else {
                                Utils.showToast(object.getString("message"), getActivity());
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;
                    case getLike:

                        try {
                            JSONObject object = new JSONObject(result);
                            boolean success = object.getBoolean("success");
                            if (success) {
                                // TODO parse JSON
                                //Utils.showToast(object.getString("message"), getActivity());
                                getBlogList();

                            } else {
                                Utils.showToast(object.getString("message"), getActivity());
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onProgressUpdate(String uniqueMessageId, int progres) {

    }

    @Override
    public void onProgressComplete(String uniqueMessageId, String result, Constant.REQUESTS request) {

    }

    @Override
    public void onPopupItemClick(int pos, View v) {
        popup.dismiss();
        categoryId = MyConstants.categoriesList.get(pos).getCategoryID();
        toolbar.setText(MyConstants.categoriesList.get(pos).getCategoryName());
        getBlogList();
    }

    @Override
    public void onClosetClick(int pos) {
        position = pos;

    }

    @Override
    public void onLikeClick(int pos) {
        position = pos;
    }


    @Override
    public void onTotalData(int pos) {
        if (pos == 0) {
            imgnodata.setVisibility(View.VISIBLE);
            txtNoValue.setVisibility(View.VISIBLE);
            txtNoValue.setText("Data Not Found...!");

        } else {
            imgnodata.setVisibility(View.GONE);
        }
    }
}
