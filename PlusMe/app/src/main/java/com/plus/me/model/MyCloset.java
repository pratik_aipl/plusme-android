package com.plus.me.model;

import java.io.Serializable;

public class MyCloset implements Serializable{

 public String BlogId,PostImage,PostTitle,ClosetImage,ClosetID;

    public String getClosetID() {
        return ClosetID;
    }

    public void setClosetID(String closetID) {
        ClosetID = closetID;
    }

    public String getBlogId() {
        return BlogId;
    }

    public void setBlogId(String blogId) {
        BlogId = blogId;
    }

    public String getPostImage() {
        return PostImage;
    }

    public void setPostImage(String postImage) {
        PostImage = postImage;
    }

    public String getPostTitle() {
        return PostTitle;
    }

    public void setPostTitle(String postTitle) {
        PostTitle = postTitle;
    }

    public String getClosetImage() {
        return ClosetImage;
    }

    public void setClosetImage(String closetImage) {
        ClosetImage = closetImage;
    }
}
