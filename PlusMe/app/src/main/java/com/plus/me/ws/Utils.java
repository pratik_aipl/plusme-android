package com.plus.me.ws;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Parcelable;
import android.provider.Settings.Secure;
import androidx.core.app.ActivityCompat;
import androidx.core.app.NotificationCompat;
import androidx.appcompat.app.AlertDialog;
import android.telephony.TelephonyManager;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.format.DateUtils;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.ANRequest;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.AnalyticsListener;
import com.androidnetworking.interfaces.BitmapRequestListener;
import com.androidnetworking.interfaces.DownloadListener;
import com.androidnetworking.interfaces.DownloadProgressListener;
import com.plus.me.R;

import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.lang.reflect.Field;
import java.net.HttpURLConnection;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import cc.cloudist.acplibrary.ACProgressConstant;
import cc.cloudist.acplibrary.ACProgressFlower;
import fr.castorflex.android.circularprogressbar.CircularProgressDrawable;

public class Utils {


    /* for GCM */
    static final String EXTRA_MESSAGE = "message";
    private static final String TAG = "DownloadFile";
    public static SimpleDateFormat dateFormat = new SimpleDateFormat("dd-mm-yyyy");
    public static SimpleDateFormat dayOFWeekFormat = new SimpleDateFormat("EEEE");
    public static ProgressDialog pDialog;
    public static Dialog dialog;
    public static ACProgressFlower dialog1;
    public static NotificationManager mNotifyManager;
    public static NotificationCompat.Builder build;
    static float density = 1;
    private static ProgressDialog mProgressDialog;

    public static String bytesToHex(byte[] bytes) {
        StringBuilder sbuf = new StringBuilder();
        for (int idx = 0; idx < bytes.length; idx++) {
            int intVal = bytes[idx] & 0xff;
            if (intVal < 0x10) sbuf.append("0");
            sbuf.append(Integer.toHexString(intVal).toUpperCase());
        }
        return sbuf.toString();
    }

    /**
     * Get utf8 byte array.
     *
     * @param str
     * @return array of NULL if error was found
     */
    public static byte[] getUTF8Bytes(String str) {
        try {
            return str.getBytes("UTF-8");
        } catch (Exception ex) {
            return null;
        }
    }

    /**
     * Load UTF8withBOM or any ansi text file.
     *
     * @param filename
     * @return
     * @throws IOException
     */
    public static String loadFileAsString(String filename) throws IOException {
        final int BUFLEN = 1024;
        BufferedInputStream is = new BufferedInputStream(new FileInputStream(filename), BUFLEN);
        try {
            ByteArrayOutputStream baos = new ByteArrayOutputStream(BUFLEN);
            byte[] bytes = new byte[BUFLEN];
            boolean isUTF8 = false;
            int read, count = 0;
            while ((read = is.read(bytes)) != -1) {
                if (count == 0 && bytes[0] == (byte) 0xEF && bytes[1] == (byte) 0xBB && bytes[2] == (byte) 0xBF) {
                    isUTF8 = true;
                    baos.write(bytes, 3, read - 3); // drop UTF8 bom marker
                } else {
                    baos.write(bytes, 0, read);
                }
                count += read;
            }
            return isUTF8 ? new String(baos.toByteArray(), "UTF-8") : new String(baos.toByteArray());
        } finally {
            try {
                is.close();
            } catch (Exception ex) {
            }
        }
    }

    public static CharSequence printDifference(String start) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        //  sdf.setTimeZone(Locale.getDefault());
        long time = 0;

        try {
            time = sdf.parse(start).getTime();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        long now = System.currentTimeMillis();

        CharSequence ago =
                DateUtils.getRelativeTimeSpanString(time, now, DateUtils.MINUTE_IN_MILLIS);
        return ago;
    }


    /*
    "PostTime":"2019-07-26 13:11:24"
    */
    public static void getDatesDifferenceInDays(String startDate, TextView txt) {
        Date date1 = null, date2 = null;
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.", Locale.getDefault());
        format.setTimeZone(TimeZone.getTimeZone("UTC"));
        String dateCurrent = format.format(new Date());

        SimpleDateFormat dates = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        dates.setTimeZone(TimeZone.getDefault());
        try {
            date1 = dates.parse(dateCurrent);
            date2 = dates.parse(startDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        long difference = 0;
        if (date1 != null) {
            if (date2 != null) {
                difference = Math.abs(date1.getTime() - date2.getTime());
                Log.d(TAG, "TIME ZONE>>>: " + date1 + ">>>>" + date2 + ">>>>>" + difference);
                //(6:51:20-13:11:24)
            }
        }

        long seconds = difference / 1000;
        long minutes = seconds / 60;
        long hours = minutes / 60;

        long differenceDates = difference / (24 * 60 * 60 * 1000);
        //Convert long to String
        String dayDifference = Long.toString(differenceDates);
        Log.d("date:--------", dayDifference);


        String time;
        if (seconds < 60) {
            if (seconds == 1) {
                time = seconds + "  " + "second ago ";
            } else {
                time = seconds + "  " + "seconds ago ";
            }
            System.out.println("********" + seconds + "  " + "seconds ago");
        } else if (minutes < 60) {
            if (minutes == 1) {
                time = minutes + "  " + "minute ago ";
            } else {
                time = minutes + "  " + "minutes ago ";
            }
            System.out.println("********" + minutes + "  " + " minutes ago");
        } else if (hours < 24) {
            if (hours == 1) {
                time = hours + "  " + "hour ago ";
            } else {
                time = hours + "  " + "hours ago ";
            }

            System.out.println("********" + hours + "  " + " hours ago");
        } else {
            if (Integer.parseInt(dayDifference) == 1) {
                time = dayDifference + "  " + "day ago ";
            } else {
                time = dayDifference + "  " + "days ago ";
            }

            System.out.println("********" + dayDifference + " " + " days ago");
        }

        txt.setText(time);
    }

    /**
     * Returns MAC address of the given interface name.
     *
     * @param interfaceName eth0, wlan0 or NULL=use first interface
     * @return mac address or empty string
     */
    public static String getMACAddress(String interfaceName) {
        try {
            List<NetworkInterface> interfaces = Collections.list(NetworkInterface.getNetworkInterfaces());
            for (NetworkInterface intf : interfaces) {
                if (interfaceName != null) {
                    if (!intf.getName().equalsIgnoreCase(interfaceName)) continue;
                }
                byte[] mac = intf.getHardwareAddress();
                if (mac == null) return "";
                StringBuilder buf = new StringBuilder();
                for (int idx = 0; idx < mac.length; idx++)
                    buf.append(String.format("%02X:", mac[idx]));
                if (buf.length() > 0) buf.deleteCharAt(buf.length() - 1);
                return buf.toString();
            }
        } catch (Exception ex) {
        } // for now eat exceptions
        return "";
        /*try {
            // this is so Linux hack
            return loadFileAsString("/sys/class/net/" +interfaceName + "/address").toUpperCase().trim();
        } catch (IOException ex) {
            return null;
        }*/
    }

    /*public static void setKeyboardVisibilityListener(Activity activity, final KeyboardVisibilityListener keyboardVisibilityListener) {
        final View contentView = activity.findViewById(android.R.id.content);
        contentView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            private int mPreviousHeight;

            @Override
            public void onGlobalLayout() {
                int newHeight = contentView.getHeight();
                if (mPreviousHeight != 0) {
                    if (mPreviousHeight > newHeight) {
                        // Height decreased: keyboard was shown
                        keyboardVisibilityListener.onKeyboardVisibilityChanged(true);
                    } else if (mPreviousHeight < newHeight) {
                        // Height increased: keyboard was hidden
                        keyboardVisibilityListener.onKeyboardVisibilityChanged(false);
                    } else {
                        // No change
                    }
                }
                mPreviousHeight = newHeight;
            }
        });
    }*/

    /**
     * Get IP address from first non-localhost interface
     *
     * @param
     * @return address or empty string
     */
    public static String getIPAddress(boolean useIPv4) {
        try {
            List<NetworkInterface> interfaces = Collections.list(NetworkInterface.getNetworkInterfaces());
            for (NetworkInterface intf : interfaces) {
                List<InetAddress> addrs = Collections.list(intf.getInetAddresses());
                for (InetAddress addr : addrs) {
                    if (!addr.isLoopbackAddress()) {
                        String sAddr = addr.getHostAddress();
                        //boolean isIPv4 = InetAddressUtils.isIPv4Address(sAddr);
                        boolean isIPv4 = sAddr.indexOf(':') < 0;

                        if (useIPv4) {
                            if (isIPv4)
                                return sAddr;
                        } else {
                            if (!isIPv4) {
                                int delim = sAddr.indexOf('%'); // drop ip6 zone suffix
                                return delim < 0 ? sAddr.toUpperCase() : sAddr.substring(0, delim).toUpperCase();
                            }
                        }
                    }
                }
            }
        } catch (Exception ex) {
        } // for now eat exceptions
        return "";
    }

    public static int getScreenWidth(Context context) {
        WindowManager windowManager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        Display display = windowManager.getDefaultDisplay();
        return display.getWidth();
    }

    public static int validateDate(int date) {
        return (date < 10) ? date : Integer.parseInt("0" + date);
    }

    public static void logUser() {
        // You can call any combination of these three methods


        /*Crashlytics.setUserIdentifier("Doctor Pocket");
        Crashlytics.setUserEmail("sojitrasagar108@gmail.com");
        Crashlytics.setUserName("Sagar Sojitra");*/
    }

    public static void printMap(Map<String, String> map) {

        for (String key : map.keySet()) {
            System.out.println(key + "====>" + map.get(key));
        }

    }

    public static String getTodayDay() {

        return dayOFWeekFormat.format(new Date());
    }

    public static String intentToString(Intent intent) {
        if (intent == null) {
            return null;
        }

        return intent.toString() + " " + bundleToString(intent.getExtras());
    }

    public static String bundleToString(Bundle bundle) {
        StringBuilder out = new StringBuilder("Bundle[");

        if (bundle == null) {
            out.append("null");
        } else {
            boolean first = true;
            for (String key : bundle.keySet()) {
                if (!first) {
                    out.append(", ");
                }

                out.append(key).append('=');

                Object value = bundle.get(key);

                if (value instanceof int[]) {
                    out.append(Arrays.toString((int[]) value));
                } else if (value instanceof byte[]) {
                    out.append(Arrays.toString((byte[]) value));
                } else if (value instanceof boolean[]) {
                    out.append(Arrays.toString((boolean[]) value));
                } else if (value instanceof short[]) {
                    out.append(Arrays.toString((short[]) value));
                } else if (value instanceof long[]) {
                    out.append(Arrays.toString((long[]) value));
                } else if (value instanceof float[]) {
                    out.append(Arrays.toString((float[]) value));
                } else if (value instanceof double[]) {
                    out.append(Arrays.toString((double[]) value));
                } else if (value instanceof String[]) {
                    out.append(Arrays.toString((String[]) value));
                } else if (value instanceof CharSequence[]) {
                    out.append(Arrays.toString((CharSequence[]) value));
                } else if (value instanceof Parcelable[]) {
                    out.append(Arrays.toString((Parcelable[]) value));
                } else if (value instanceof Bundle) {
                    out.append(bundleToString((Bundle) value));
                } else {
                    out.append(value);
                }

                first = false;
            }
        }

        out.append("]");
        return out.toString();
    }

    public static String getValidateDate() {

        final Calendar cal = Calendar.getInstance();
        int mYear = cal.get(Calendar.YEAR);
        int mMonth = cal.get(Calendar.MONTH);
        int mDay = cal.get(Calendar.DAY_OF_MONTH);


        return Utils.validateDate(mDay) + "-" + Utils.validateDate(mMonth + 1) + "-" + mYear;
    }

    public static boolean checkValidation(EditText et) {
        if (et.getText().toString().isEmpty()) {
            return true;
        } else {
            return false;
        }
    }

    public static String getValue(EditText et) {
        return et.getText().toString();
    }

    public static boolean isEmpty(EditText et) {
        return et.getText().toString().equalsIgnoreCase("");
    }

    public static void showProgressDialog(Context context, String msg) {
        try {
            /*if (dialog == null) {
                dialog = new Dialog(context);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setCancelable(false);
                dialog.setContentView(R.layout.custom_progress_dialog);
            }

            TextView txtView = (TextView) dialog.findViewById(R.id.textView);
            txtView.setText(msg);
            if (!dialog.isShowing())
                dialog.show();*/

            if (dialog1 == null) {
                dialog1 = new ACProgressFlower.Builder(context)
                        .direction(ACProgressConstant.DIRECT_CLOCKWISE)
                        .themeColor(Color.WHITE)
                        .fadeColor(Color.DKGRAY)
                        .build();
                dialog1.setCancelable(false);
                dialog1.setCanceledOnTouchOutside(false);
            }
            if (!dialog1.isShowing())
                dialog1.show();

        } catch (IllegalArgumentException ie) {
            ie.printStackTrace();
        } catch (RuntimeException re) {
            re.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void showProgressDialog(Context context) {
        showProgressDialog(context, "Please Wait...");
    }

    public static void hideProgressDialog() {
        try {

            /*if (dialog != null) {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                    dialog = null;
                }
            }*/

            if (dialog1 != null) {
                if (dialog1.isShowing()) {
                    dialog1.dismiss();
                    dialog1 = null;
                }
            }
        } catch (IllegalArgumentException ie) {
            ie.printStackTrace();

        } catch (RuntimeException re) {
            re.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void showColorProgressDialog(Context context) {
        try {
            if (dialog == null) {
                dialog = new Dialog(context);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setCancelable(false);
                dialog.setContentView(R.layout.custom_progress_dialog);
            }

            ProgressBar progressBar = dialog.findViewById(R.id.progressBar1);
            progressBar.setIndeterminateDrawable(new CircularProgressDrawable
                    .Builder(context)
                    .colors(context.getResources().getIntArray(R.array.gplus_colors))
                    .sweepSpeed(1f)
                    .strokeWidth(5)
                    .style(CircularProgressDrawable.Style.ROUNDED)
                    .build());

            if (!dialog.isShowing())
                dialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void hideColorProgressDialog() {
        try {

            if (dialog != null) {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                    dialog = null;
                }
            }

        } catch (IllegalArgumentException ie) {
            ie.printStackTrace();

        } catch (RuntimeException re) {
            re.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static String getUpperCase(String name) {
        StringBuilder sb = new StringBuilder(name); // one StringBuilder object
        sb.setCharAt(0, Character.toUpperCase(sb.charAt(0)));
        return sb.toString(); // one String object
    }

    public static void addValueToEditor(SharedPreferences.Editor et, Object o, Activity act) {
        Class c;
        try {
            c = Class.forName(o.getClass().getCanonicalName(), false, o.getClass().getClassLoader());

            Field[] fields = c.getFields();
            for (Field f : fields) {
                f.setAccessible(true);
                if (f.getType() == String.class) {
                    et.putString(f.getName(), f.get(o).toString());

                } else if (f.getType() == Integer.class) {
                    et.putInt(f.getName(), Integer.parseInt(f.get(o).toString()));

                } else if (f.getType() == Boolean.class) {
                    et.putBoolean(f.getName(), Boolean.valueOf(f.get(o).toString()));
                }
            }

            et.commit();

            showToast("prefrence saved", act);
        } catch (Exception e) {
            e.printStackTrace();


        }

    }

    public static boolean isValidEmail(CharSequence target) {
        if (target == null || TextUtils.isEmpty(target)) {
            return false;
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
        }
    }

    public static void showSimpleSpinProgressDialog(Context context, String message) {

        showProgressDialog(context, message);
/*
        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(context);
            mProgressDialog.setMessage(message);
            mProgressDialog.setCancelable(true);
            mProgressDialog.setIndeterminate(true);
            mProgressDialog.show();
        } else {
            if (mProgressDialog.isShowing()) {
                mProgressDialog.show();
            }
        }*/
    }

    public static void removeSimpleSpinProgressDialog() {
        hideProgressDialog();
       /* if (mProgressDialog != null) {
            mProgressDialog.cancel();
            mProgressDialog.dismiss();
            mProgressDialog = null;
        }*/
    }

    public static String getTodayDate() {
        final Calendar cal = Calendar.getInstance();
        int mYear = cal.get(Calendar.YEAR);
        int mMonth = cal.get(Calendar.MONTH);
        int mDay = cal.get(Calendar.DAY_OF_MONTH);


        return Utils.validateDate(mDay) + "-" + Utils.validateDate(mMonth + 1) + "-" + mYear;

    }

    public static float getDisplayMetricsDensity(Context context) {
        density = context.getResources().getDisplayMetrics().density;

        return density;
    }

    public static int getPixel(Context context, int p) {
        if (density != 1) {
            return (int) (p * density + 0.5);
        }
        return p;
    }

    public static Animation FadeAnimation(float nFromFade, float nToFade) {
        Animation fadeAnimation = new AlphaAnimation(nToFade, nToFade);

        return fadeAnimation;
    }

    public static String getDeviceId(Context context) {
        TelephonyManager tManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return null;
        }
        String uuid = tManager.getDeviceId();
        return uuid;
    }

    public static Animation inFromRightAnimation() {
        Animation inFromRight = new TranslateAnimation(Animation.RELATIVE_TO_PARENT, +1.0f,
                Animation.RELATIVE_TO_PARENT, 0.0f, Animation.RELATIVE_TO_PARENT, 0.0f, Animation.RELATIVE_TO_PARENT,
                0.0f);

        return inFromRight;
    }

    public static Animation inFromLeftAnimation() {
        Animation inFromLeft = new TranslateAnimation(Animation.RELATIVE_TO_PARENT, -1.0f,
                Animation.RELATIVE_TO_PARENT, 0.0f, Animation.RELATIVE_TO_PARENT, 0.0f, Animation.RELATIVE_TO_PARENT,
                0.0f);

        return inFromLeft;
    }

    public static Animation inFromBottomAnimation() {
        Animation inFromBottom = new TranslateAnimation(Animation.RELATIVE_TO_PARENT, 0.0f,
                Animation.RELATIVE_TO_PARENT, 0.0f, Animation.RELATIVE_TO_PARENT, +1.0f, Animation.RELATIVE_TO_PARENT,
                0.0f);

        return inFromBottom;
    }

    public static Animation outToLeftAnimation() {
        Animation outToLeft = new TranslateAnimation(Animation.RELATIVE_TO_PARENT, 0.0f, Animation.RELATIVE_TO_PARENT,
                -1.0f, Animation.RELATIVE_TO_PARENT, 0.0f, Animation.RELATIVE_TO_PARENT, 0.0f);
        return outToLeft;
    }

    public static Animation outToRightAnimation() {
        Animation outToRight = new TranslateAnimation(Animation.RELATIVE_TO_PARENT, 0.0f, Animation.RELATIVE_TO_PARENT,
                +1.0f, Animation.RELATIVE_TO_PARENT, 0.0f, Animation.RELATIVE_TO_PARENT, 0.0f);

        return outToRight;
    }

    // public static String convert24HoursTo12HoursFormat(String twelveHourTime)
    // throws ParseException {
    // return outputformatter.format(inputformatter.parse(twelveHourTime));
    // }

    public static Animation outToBottomAnimation() {
        Animation outToBottom = new TranslateAnimation(Animation.RELATIVE_TO_PARENT, 0.0f,
                Animation.RELATIVE_TO_PARENT, 0.0f, Animation.RELATIVE_TO_PARENT, 0.0f, Animation.RELATIVE_TO_PARENT,
                +1.0f);

        return outToBottom;
    }

    public static boolean isNetworkAvailable(Context activity) {
        ConnectivityManager connectivity = (ConnectivityManager) activity
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivity == null) {
            showToast("Please connect to Internet", activity);
            return false;
        } else {
            NetworkInfo[] info = connectivity.getAllNetworkInfo();
            if (info != null) {
                for (int i = 0; i < info.length; i++) {
                    if (info[i].getState() == NetworkInfo.State.CONNECTED) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    public static boolean eMailValidation(String emailstring) {
        if (null == emailstring || emailstring.length() == 0) {
            return false;
        }
        Pattern emailPattern = Pattern.compile(".+@.+\\.[a-z]+");
        Matcher emailMatcher = emailPattern.matcher(emailstring);
        return emailMatcher.matches();
    }

    public static void showToast(final String msg, final Context ctx) {

        new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
            @Override
            public void run() {
                // Run your task here
                Toast toast = Toast.makeText(ctx, msg, Toast.LENGTH_SHORT);
                // toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();
            }
        }, 1000);

    }

    public static double round(double value, int places) {
        if (places < 0)
            throw new IllegalArgumentException();

        long factor = (long) Math.pow(10, places);
        value = value * factor;
        long tmp = Math.round(value);
        return (double) tmp / factor;
    }

    /**
     * You can integrate interstitials in any placement in your app, but for
     * testing purposes we present integration tied to a button click
     */
   /* public static Typeface setNormalFontFace(Context context) {
        Typeface font = Typeface.createFromAsset(context.getAssets(), "fonts/DroidSans.ttf");
        return font;
    }

    public static Typeface setBoldFontFace(Context context) {
        Typeface font = Typeface.createFromAsset(context.getAssets(), "fonts/DroidSans-Bold.ttf");
        return font;
    }*/
    public static View setTabCaption() {

        return null;
    }


    public static String manageDecimal(String temp) {
        String displayRiskDecimal = null, before = null, after = null;
        if (temp.contains(".")) {
            int in = temp.indexOf(".") + 1;
            before = temp.substring(0, in);
            after = temp.substring(in, temp.length());
            if (after.length() == 1) {
                displayRiskDecimal = before + after + "0";
            } else {
                displayRiskDecimal = before + after;
            }
            return displayRiskDecimal;
        } else {
            displayRiskDecimal = temp + ".00";
            return displayRiskDecimal;
        }
    }

    public static String manageDecimalPointForGetLines(String temp) {
        String displayRiskDecimal = null, before = null, after = null;
        int in = temp.indexOf(".") + 1;
        before = temp.substring(0, in);
        after = temp.substring(in, temp.length());
        if (after.equals("00")) {
            displayRiskDecimal = before.substring(0, before.length() - 1);
        } else if (after.length() == 2) {
            if (after.startsWith("0", 1)) {
                displayRiskDecimal = after.substring(0, after.length() - 1);
                displayRiskDecimal = before + displayRiskDecimal;
            }
        }
        return displayRiskDecimal;
    }

    public static String manageSign(String temp) {
        String displayRiskDecimal = null;
        if (temp.contains("-") || temp.contains("+")) {
            displayRiskDecimal = temp;
        } else {
            displayRiskDecimal = "+" + temp;
        }
        return displayRiskDecimal;
    }

    public static SpannableStringBuilder displayNameValue(String battingTeamName, String displayBaseValue) {
        SpannableStringBuilder builder = new SpannableStringBuilder();
        SpannableString WordtoSpan = new SpannableString(displayBaseValue);
        WordtoSpan.setSpan(new ForegroundColorSpan(Color.parseColor("#1298DE")), 0, displayBaseValue.length(), 0);
        builder.append(battingTeamName);
        builder.append(WordtoSpan);
        return builder;
    }

    private static String convertStreamToString(InputStream is) {
        /*
         * To convert the InputStream to String we use the
         * BufferedReader.readLine() method. We iterate until the BufferedReader
         * return null which means there's no more data to read. Each line will
         * appended to a StringBuilder and returned as String.
         */
        BufferedReader reader = new BufferedReader(new InputStreamReader(is), 8192);
        StringBuilder sb = new StringBuilder();

        String line = null;
        try {
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                is.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return sb.toString();
    }

  /*  public static String removeSpecialChar(String value) {
        try {
            if (value.contains("("))
                value = value.replaceAll("(", "");
            if (value.contains(")"))
                value = value.replaceAll(")", "");
            if (value.contains("-"))
                value = value.replaceAll("-", "");
            if (value.contains(" "))
                value = value.replaceAll(" ", "");
            if (value.contains("."))
                value = value.replaceAll(".", "");
            if (value.contains(":"))
                value = value.replaceAll(":", "");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return value;
    }*/

    public static String getUrlVideoRTSP(String urlYoutube, String video_id) {
        try {
            String gdy = "http://gdata.youtube.com/feeds/api/videos/";
            DocumentBuilder documentBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
            String id = video_id;
            URL url = new URL(gdy + id);

            System.out.println("Gdata Url :::" + url);

            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            Document doc = documentBuilder.parse(connection.getInputStream());
            Element el = doc.getDocumentElement();
            NodeList list = el.getElementsByTagName("media:content");// /media:content
            String cursor = urlYoutube;
            for (int i = 0; i < list.getLength(); i++) {
                Node node = list.item(i);
                if (node != null) {
                    NamedNodeMap nodeMap = node.getAttributes();
                    HashMap<String, String> maps = new HashMap<String, String>();
                    for (int j = 0; j < nodeMap.getLength(); j++) {
                        Attr att = (Attr) nodeMap.item(j);
                        maps.put(att.getName(), att.getValue());
                    }
                    if (maps.containsKey("yt:format")) {
                        String f = maps.get("yt:format");
                        if (maps.containsKey("url")) {
                            cursor = maps.get("url");
                        }
                        if (f.equals("1"))
                            return cursor;
                    }
                }
            }
            return cursor;
        } catch (Exception ex) {
            Log.e("Get ======>>", ex.toString());
        }
        return urlYoutube;

    }

    public static void showAlert(String msg, final Context ctx) {
        try {
            new AlertDialog.Builder(ctx)
                    .setMessage(msg)
                    .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                            dialog.dismiss();
                        }
                    })
                    .show();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static void hideKeyboard(Activity context, View view) {
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    public static void CopyStream(InputStream is, OutputStream os) {
        final int buffer_size = 1024;
        try {
            byte[] bytes = new byte[buffer_size];
            for (; ; ) {
                int count = is.read(bytes, 0, buffer_size);
                if (count == -1)
                    break;
                os.write(bytes, 0, count);
            }
        } catch (Exception ex) {
        }
    }

    public static String getcurrentTime() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("KK:mm a");
        return dateFormat.format(new Date());
    }

    public static String ConvertTime(String Time) {
        try {
            SimpleDateFormat _24HourSDF = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
            SimpleDateFormat _12HourSDF = new SimpleDateFormat("dd-MM-yyyy hh:mm aa");
            Date _24HourDt = _24HourSDF.parse(Time);
            System.out.println(_24HourDt);
            System.out.println(_12HourSDF.format(_24HourDt));
            return _12HourSDF.format(_24HourDt);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    public static String findDeviceID(Context cnt) {
        return Secure.getString(cnt.getContentResolver(), Secure.ANDROID_ID);
    }

    public static void downloadFileWithNotification(String url, String directoryPath, String fileName, final Context context) {

        final int ID = 1;

        mNotifyManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        build = new NotificationCompat.Builder(context, "Channel_ID");
        build.setContentTitle("Download")
                .setContentText("Download in progress")
                .setOngoing(true)
                .setSmallIcon(android.R.drawable.stat_sys_download);
        build.setOngoing(true);
        build.setProgress(0, 0, true);
        mNotifyManager.notify(ID, build.build());

        AndroidNetworking.download(url, directoryPath, fileName)
                .setTag("downloadTest")
                .setPriority(Priority.MEDIUM)
                //.setExecutor(Executors.newSingleThreadExecutor()) // setting an executor to get response or completion on that executor thread
                .build()
                .setAnalyticsListener(new AnalyticsListener() {
                    @Override
                    public void onReceived(long timeTakenInMillis, long bytesSent, long bytesReceived, boolean isFromCache) {
                        Log.d(TAG, " timeTakenInMillis : " + timeTakenInMillis);
                        Log.d(TAG, " bytesSent : " + bytesSent);
                        Log.d(TAG, " bytesReceived : " + bytesReceived);
                        Log.d(TAG, " isFromCache : " + isFromCache);
                    }
                })
                .setDownloadProgressListener(new DownloadProgressListener() {
                    @Override
                    public void onProgress(long bytesDownloaded, long totalBytes) {
                        // do anything with progress

                        int progressPercent = ((int) bytesDownloaded * 100) / (int) totalBytes;

                        /*build.setOngoing(true);
                        build.setProgress(100, progressPercent, true);
                        mNotifyManager.notify(ID, build.build());*/

                        /*if (bytesDownloaded == totalBytes){
                            build.setContentText("Download complete");
                            // Removes the progress bar
                            build.setProgress(0, 0, false);
                            build.setSmallIcon(android.R.drawable.stat_sys_download_done);
                            mNotifyManager.notify(1, build.build());
                        }*/

                        //Log.d(TAG, "bytesDownloaded : " + bytesDownloaded + " totalBytes : " + totalBytes);
                    }
                })
                .startDownload(new DownloadListener() {
                    @Override
                    public void onDownloadComplete() {
                        // do anything after completion

                        build.setContentText("Download complete");
                        build.setOngoing(false);
                        // Removes the progress bar
                        build.setProgress(0, 0, false);
                        build.setSmallIcon(android.R.drawable.stat_sys_download_done);
                        Notification notification = build.build();
                        mNotifyManager.notify(ID, notification);


                        showToast("File downloaded successfully", context);
                    }

                    @Override
                    public void onError(ANError error) {
                        // handle error
                        System.out.println("error:::" + error);
                        showToast("Something went wrong", context);
                    }
                });
    }

    public static void downloadFileWithDialog(String url, String directoryPath, String fileName, final Context context) {

        final ProgressDialog progressDialog = new ProgressDialog(context);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        progressDialog.setIndeterminate(false);
        progressDialog.setProgressNumberFormat(null);
        progressDialog.setMessage("Downloading file...");
        progressDialog.show();


        AndroidNetworking.download(url, directoryPath, fileName)
                .setTag("downloadTest")
                .setPriority(Priority.MEDIUM)
                .setExecutor(Executors.newSingleThreadExecutor()) // setting an executor to get response or completion on that executor thread
                .build()
                .setAnalyticsListener(new AnalyticsListener() {
                    @Override
                    public void onReceived(long timeTakenInMillis, long bytesSent, long bytesReceived, boolean isFromCache) {
                        Log.d(TAG, " timeTakenInMillis : " + timeTakenInMillis);
                        Log.d(TAG, " bytesSent : " + bytesSent);
                        Log.d(TAG, " bytesReceived : " + bytesReceived);
                        Log.d(TAG, " isFromCache : " + isFromCache);
                    }
                })
                .setDownloadProgressListener(new DownloadProgressListener() {
                    @Override
                    public void onProgress(long bytesDownloaded, long totalBytes) {
                        // do anything with progress
                        progressDialog.setMax((int) totalBytes);
                        progressDialog.setProgress((int) bytesDownloaded);
                        Log.d(TAG, "bytesDownloaded : " + bytesDownloaded + " totalBytes : " + totalBytes);
                    }
                })
                .startDownload(new DownloadListener() {
                    @Override
                    public void onDownloadComplete() {
                        // do anything after completion
                        progressDialog.dismiss();
                        showToast("File downloaded successfully", context);
                    }

                    @Override
                    public void onError(ANError error) {
                        // handle error
                        System.out.println("error:::" + error);
                        progressDialog.dismiss();
                        showToast("Something went wrong", context);
                    }
                });
    }

    public static void setImageFromServer(ImageView imageView, String url) {

        setImageFromServer(imageView, url, null, 0, 0, null, null, 0, 0);
    }

    public static void setImageFromServer(ImageView imageView, String url, ImageView.ScaleType scaleType) {

        setImageFromServer(imageView, url, scaleType, 0, 0, null, null, 0, 0);
    }

    public static void setImageFromServer(final ImageView imageView, String url, ImageView.ScaleType scaleType, int maxWidth, int maxHeight,
                                          final ProgressBar progressBar, final Drawable defaultImageDrawable, final int newWidth, final int newHeight) {

        ANRequest.GetRequestBuilder anRequest = new ANRequest.GetRequestBuilder(url);

        if (scaleType != null) {
            anRequest.setImageScaleType(scaleType);
        }

        if (maxWidth != 0) {
            anRequest.setBitmapMaxWidth(maxWidth);
        }

        if (maxHeight != 0) {
            anRequest.setBitmapMaxHeight(maxHeight);
        }

        anRequest.setTag("ImageRequestTag")
                .setPriority(Priority.MEDIUM)
                .setBitmapConfig(Bitmap.Config.ARGB_8888)
                .setMaxAgeCacheControl(2, TimeUnit.HOURS)
                .build()
                .setAnalyticsListener(new AnalyticsListener() {
                    @Override
                    public void onReceived(long timeTakenInMillis, long bytesSent, long bytesReceived, boolean isFromCache) {
                        Log.d(TAG, " timeTakenInMillis : " + timeTakenInMillis);
                        Log.d(TAG, " bytesSent : " + bytesSent);
                        Log.d(TAG, " bytesReceived : " + bytesReceived);
                        Log.d(TAG, " isFromCache : " + isFromCache);
                    }
                })
                .getAsBitmap(new BitmapRequestListener() {
                    @Override
                    public void onResponse(Bitmap bitmap) {
                        // do anything with bitmap
                        if (imageView != null) {

                            if (newWidth != 0 && newHeight != 0) {
                                imageView.setImageBitmap(getResizedBitmap(bitmap, newWidth, newHeight));
                            } else {
                                imageView.setImageBitmap(bitmap);
                            }
                        }
                        if (progressBar != null) {
                            progressBar.setVisibility(View.GONE);
                        }
                    }

                    @Override
                    public void onError(ANError error) {
                        // handle error
                        if (imageView != null && defaultImageDrawable != null) {
                            imageView.setImageDrawable(defaultImageDrawable);
                        }
                        if (progressBar != null) {
                            progressBar.setVisibility(View.GONE);
                        }
                        System.out.println("Error:::" + error);
                    }
                });
    }

    public static Bitmap getResizedBitmap(Bitmap bm, int newWidth, int newHeight) {
        int width = bm.getWidth();
        int height = bm.getHeight();
        float scaleWidth = ((float) newWidth) / width;
        float scaleHeight = ((float) newHeight) / height;
        // CREATE A MATRIX FOR THE MANIPULATION
        Matrix matrix = new Matrix();
        // RESIZE THE BIT MAP
        matrix.postScale(scaleWidth, scaleHeight);

        // "RECREATE" THE NEW BITMAP
        Bitmap resizedBitmap = Bitmap.createBitmap(
                bm, 0, 0, width, height, matrix, false);
        bm.recycle();
        return resizedBitmap;
    }

    // demo: Utils.setSpinnerItem(spSurname, surnameArrayList, "5", "mSurnameid");
    public static void setSpinnerItem(Spinner spinner, ArrayList<? extends Object> arrayList, String value, String paramName) {

        for (int i = 0; i < arrayList.size(); i++) {
            try {
                if (arrayList.get(i).getClass().getField(paramName).getName().equals(paramName)) {
                    try {
                        if (arrayList.get(i).getClass().getField(paramName).get(arrayList.get(i)).toString().equalsIgnoreCase(value)) {
                            spinner.setSelection(i);
                        }
                    } catch (IllegalAccessException e) {
                        e.printStackTrace();
                    }
                }
            } catch (NoSuchFieldException e) {
                e.printStackTrace();
            }
        }
    }
}