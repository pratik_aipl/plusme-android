package com.plus.me.activity;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.provider.Settings;
import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.appcompat.app.AppCompatActivity;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.onesignal.OneSignal;
import com.plus.me.R;
import com.plus.me.model.User;
import com.plus.me.model.UserProfile;
import com.plus.me.others.App;
import com.plus.me.utils.MySharedPref;
import com.plus.me.ws.AsyncTaskListner;
import com.plus.me.ws.CallRequest;
import com.plus.me.ws.Constant;
import com.plus.me.ws.JsonParserUniversal;
import com.plus.me.ws.MyConstants;
import com.plus.me.ws.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import io.fabric.sdk.android.Fabric;

public class LoginActivity extends AppCompatActivity implements AsyncTaskListner {

    public JsonParserUniversal jParser;
    public LoginActivity instance;
    private EditText etEmail, etPassword;
    private Button btnLogin;
    private TextView tvSignUp, tvForgotPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_login);

        getOneSignalId();

        initializeData();

        bindWidgetReference();

        bindWidgetEvents();
      /*  if(BuildConfig.DEBUG){
            etEmail.setText("ricky@blenzabi.com");
            etPassword.setText("123456");
        }*/
    }

    private void initializeData() {
        jParser = new JsonParserUniversal();
        instance = this;
        MyConstants.IMEI = getDeviceIMEI();
    }

    private void getOneSignalId() {
        OneSignal.idsAvailable(new OneSignal.IdsAvailableHandler() {
            @Override
            public void idsAvailable(String userId, String registrationId) {
                if (userId != null)
                    MyConstants.DEVICE_ID = userId;
                Log.d("debug", "User:" + userId);
                Log.d("debug", "registrationId:" + registrationId);
            }
        });
    }

    private void bindWidgetReference() {
        etEmail = findViewById(R.id.etEmail);
        etPassword = findViewById(R.id.etPassword);
        btnLogin = findViewById(R.id.btnLogin);
        tvSignUp = findViewById(R.id.tvSignup);
        tvForgotPassword = findViewById(R.id.tvForgotPassword);
    }

    private void bindWidgetEvents() {

        tvSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LoginActivity.this, PreRegisterActivity.class);
                startActivity(intent);
                finish();
            }
        });

        tvForgotPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LoginActivity.this, ForgotPasswordActivity.class);
                startActivity(intent);
            }
        });

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // TODO uncomment doLogin();
                doLogin();
                /*Intent intent = new Intent(LoginActivity.this, HomeActivity.class);
                startActivity(intent);
                ActivityCompat.finishAffinity(LoginActivity.this);*/
            }
        });
    }

    /**
     * Returns the unique identifier for the device
     *
     * @return unique identifier for the device
     */
    public String getDeviceIMEI() {
        String deviceUniqueIdentifier = null;
        TelephonyManager tm = (TelephonyManager) this.getSystemService(Context.TELEPHONY_SERVICE);
        if (null != tm) {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                ActivityCompat.requestPermissions(LoginActivity.this, new String[]{android.Manifest.permission.READ_PHONE_STATE}, 123);
            } else {
                deviceUniqueIdentifier = tm.getDeviceId();
            }
        }
        if (null == deviceUniqueIdentifier || 0 == deviceUniqueIdentifier.length()) {
            deviceUniqueIdentifier = Settings.Secure.getString(this.getContentResolver(), Settings.Secure.ANDROID_ID);
        }
        return deviceUniqueIdentifier;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 123) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                MyConstants.IMEI = getDeviceIMEI();
            } else {
                MyConstants.IMEI = "";
            }
        }
    }

    private void doLogin() {

        String email = etEmail.getText().toString().trim();
        String password = etPassword.getText().toString().trim();
        if (TextUtils.isEmpty(email)) {
            etEmail.requestFocus();
            etEmail.setError("Enter email");
        } else if (!Utils.isValidEmail(email)) {
            etEmail.requestFocus();
            etEmail.setError("Enter valid email");
        } else if (TextUtils.isEmpty(password)) {
            etPassword.requestFocus();
            etPassword.setError("Enter password");
        } else {
            Map<String, String> map = new HashMap<>();
            map.put("url", MyConstants.BASE_URL + "login");
            map.put("Email", email);
            map.put("Password", password);
            map.put("Devicetoken", MyConstants.DEVICE_ID);
            map.put("IMEI", MyConstants.IMEI);
            map.put("DeviceName", MyConstants.MODEL_NAME);
            map.put("OSVersion", "Android");

            new CallRequest(LoginActivity.this).doLogin(map);
        }
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        try {

            Utils.hideProgressDialog();
            if (result != null && !result.isEmpty()) {
                Log.i("TAG", "TAG Result : " + result);

                switch (request) {
                    case login:
                        try {
                            JSONObject object = new JSONObject(result);
                            boolean success = object.getBoolean("status");
                            if (success) {
                                //Utils.showToast(object.getString("message"), this);
                                JSONObject userDataObj = object.getJSONObject("user_data");

                                User user = (User) jParser.parseJson(userDataObj, new User());
                                MyConstants.userProfile = (UserProfile) jParser.parseJson(userDataObj, new UserProfile());
                                setUserDataToPreference(user);


                            } else {
                                String error_string = object.getString("message");
                                Toast.makeText(LoginActivity.this, error_string, Toast.LENGTH_SHORT).show();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setUserDataToPreference(User user) {

        MySharedPref.setString(instance, MyConstants.EMAIL, user.getEmail());
        MySharedPref.setString(instance, MyConstants.AUTH, user.getAuth());
        MySharedPref.setString(instance, MyConstants.USER_ID, user.getId());
        MySharedPref.setString(instance, MyConstants.PASSWORD, user.getPassword());
        MySharedPref.setString(instance, MyConstants.FIRST_NAME, user.getFirstName());
        MySharedPref.setString(instance, MyConstants.LAST_NAME, user.getLastName());
        MySharedPref.setString(instance, MyConstants.NAME, user.getFirstName() + " " + user.getLastName());
        MySharedPref.setString(instance, MyConstants.PROFILE_IMAGE, "");
        MySharedPref.setIsLogin(true);

        App.user = user;
        App.user.setName(user.getFirstName() + " " + user.getLastName());

        Intent intent = new Intent(LoginActivity.this, HomeActivity.class);
        startActivity(intent);
        ActivityCompat.finishAffinity(LoginActivity.this);
    }

    @Override
    public void onProgressUpdate(String uniqueMessageId, int progres) {

    }

    @Override
    public void onProgressComplete(String uniqueMessageId, String result, Constant.REQUESTS request) {

    }
}
