package com.plus.me.ws;

import android.content.Context;
import android.os.AsyncTask;
import androidx.fragment.app.Fragment;
import android.util.Log;


import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.conn.ssl.X509HostnameVerifier;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.SingleClientConnManager;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HTTP;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.security.KeyStore;
import java.util.Map;
import java.util.regex.Pattern;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;

/**
 * Created by Karan-Pitroda on 01/02/2019.
 */
public class MySecureAsynchHttpRequest {

    public Fragment ft;
    public Constant.REQUESTS request;
    public Map<String, String> map;
    public AsyncTaskListner aListner;
    public Constant.POST_TYPE post_type;
    public boolean header = false;
    public String tempData = "";

    String api_key = "admin";
    String api_password = "API@TEACHER!#APP$";
    String encodeString = (api_key + ":" + api_password);
    public String token, user_id;
    public int respCode = 0;

    public MySecureAsynchHttpRequest(Fragment ft, Constant.REQUESTS request, Constant.POST_TYPE post_type, Map<String, String> map) {
        this.ft = ft;
        this.ct = ft.getActivity();
        this.request = request;
        this.map = map;
        this.aListner = (AsyncTaskListner) ft;
        this.post_type = post_type;

        if (Utils.isNetworkAvailable(ft.getActivity())) {
            if (!map.containsKey("show")) {
                Utils.showProgressDialog(ft.getActivity());
            } else {
                this.map.remove("show");
            }
            new MyRequest().execute(this.map);
        } else {
            aListner.onTaskCompleted(null, request);
            Utils.showToast("No Internet, Please try again later", ft.getActivity());
        }
    }

    public Context ct;

    public MySecureAsynchHttpRequest(Context ft, Constant.REQUESTS request, Constant.POST_TYPE post_type, Map<String, String> map) {
        this.ct = ft;
        this.request = request;
        this.map = map;
        this.aListner = (AsyncTaskListner) ft;
        this.post_type = post_type;

        if (Utils.isNetworkAvailable(ct)) {

            if (!map.containsKey("show")) {
                Utils.showSimpleSpinProgressDialog(ct, "Please Wait...");
            }

            if (map.containsKey("header")) {
                header = true;
                token = map.get("Auth");
            }

            new MyRequest().execute(this.map);
        } else {
            aListner.onTaskCompleted(null, request);
            Utils.showToast("No Internet, Please try again later", ct);
        }
    }


    class MyRequest extends AsyncTask<Map<String, String>, Void, String>

    {
        MyCustomMultiPartEntity reqEntity;

        @Override
        protected void onPreExecute() {

            super.onPreExecute();
        }

        @Override
        protected String doInBackground(Map<String, String>... map) {
            System.out.println("::urls ::" + map[0].get("url"));

            final String basicAuth = "Basic " + "YWRtaW46YWRtaW4="; //Base64.encodeToString("(username : admin , Password : admin)".getBytes(), Base64.NO_WRAP);

            String responseBody = "";
            try {
                switch (post_type) {



                    case POST_WITH_IMAGE:

                        String charset = "UTF-8";
                        try {
                         HttpClient httpClient = getNewHttpClient();
                         HttpPost   postRequest = new HttpPost(map[0].get("url"));


                            postRequest.setHeader("Auth", token);

                            reqEntity = new MyCustomMultiPartEntity(new MyCustomMultiPartEntity.ProgressListener() {
                                @Override
                                public void transferred(long num) {
                                }
                            });
                            BasicHttpContext localContext = new BasicHttpContext();
                            for (String key : map[0].keySet()) {
                                System.out.println();
                                System.out.println(key + "====>" + map[0].get(key));
                                if (matchKeysForImages(key)) {
                                    if (map[0].get(key) != null) {
                                        if (map[0].get(key).length() > 1) {
                                            String type = "image/png";
                                            File f = new File(map[0].get(key));
                                            FileBody fbody = new FileBody(f, f.getName(), type, "UTF-8");
                                            reqEntity.addPart(key, fbody);
                                        }
                                    }
                                } else {
                                    if (map[0].get(key) == null) {
                                        reqEntity.addPart(key, new StringBody(""));
                                    } else {
                                        reqEntity.addPart(key, new StringBody(map[0].get(key)));
                                    }
                                }
                            }
                            postRequest.setEntity(reqEntity);
                            HttpResponse responses = httpClient.execute(postRequest, localContext);
                            BufferedReader reader = new BufferedReader(new InputStreamReader(responses.getEntity().getContent(),
                                    "UTF-8"));
                            String sResponse;
                            while ((sResponse = reader.readLine()) != null) {
                                responseBody = responseBody + sResponse;
                            }
                            System.out.println("Response ::" + responseBody);
                            return responseBody;

                        } catch (IOException e) {
                            Log.d("ExPostActivity", e.getMessage());
                        }
                }

            } catch (Exception e) {
                e.printStackTrace();
                aListner.onTaskCompleted(null, request);
            }
            return null;

        }


        @Override
        protected void onPostExecute(String result) {


            aListner.onTaskCompleted(result, request);
            super.onPostExecute(result);
        }
    }

    public boolean matchKeysForImages(String key) {

        if (Pattern.compile(Pattern.quote("image"), Pattern.CASE_INSENSITIVE).matcher(key).find()) {
            return true;
        }

        if (key.contains("image") ||
                key.equalsIgnoreCase("PostImage") ||
                key.equalsIgnoreCase("CompanyURL") ||
                key.equalsIgnoreCase("ResumeURL") ||
                key.equalsIgnoreCase("profile_pic") ||
                key.equalsIgnoreCase("ProfilePic")) {
            return true;
        } else {
            return false;
        }
    }

    public DefaultHttpClient HostVerifier() {
        HostnameVerifier hostnameVerifier = SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER;

        DefaultHttpClient client = new DefaultHttpClient();

        SchemeRegistry registry = new SchemeRegistry();
        SSLSocketFactory socketFactory = SSLSocketFactory.getSocketFactory();
        socketFactory.setHostnameVerifier((X509HostnameVerifier) hostnameVerifier);
        registry.register(new Scheme("https", socketFactory, 443));
        SingleClientConnManager mgr = new SingleClientConnManager(client.getParams(), registry);
        DefaultHttpClient httpClient = new DefaultHttpClient(mgr, client.getParams());

// Set verifier
        HttpsURLConnection.setDefaultHostnameVerifier(hostnameVerifier);
        return httpClient;
    }

    public DefaultHttpClient getNewHttpClient() {
        try {
            KeyStore trustStore = KeyStore.getInstance(KeyStore.getDefaultType());
            trustStore.load(null, null);

            MySSLSocketFactory sf = new MySSLSocketFactory(trustStore);
            sf.setHostnameVerifier(SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);

            HttpParams params = new BasicHttpParams();
            HttpProtocolParams.setVersion(params, HttpVersion.HTTP_1_1);
            HttpProtocolParams.setContentCharset(params, HTTP.UTF_8);

            SchemeRegistry registry = new SchemeRegistry();
            registry.register(new Scheme("http", PlainSocketFactory.getSocketFactory(), 80));
            registry.register(new Scheme("https", sf, 443));

            ClientConnectionManager ccm = new ThreadSafeClientConnManager(params, registry);

            return new DefaultHttpClient(ccm, params);
        } catch (Exception e) {
            return new DefaultHttpClient();
        }
    }
}
