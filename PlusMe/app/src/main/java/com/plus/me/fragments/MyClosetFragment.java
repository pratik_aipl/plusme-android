package com.plus.me.fragments;


import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.core.view.MenuItemCompat;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.SearchView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.plus.me.R;
import com.plus.me.activity.HomeActivity;
import com.plus.me.adapter.CatagoryPopupAdapter;
import com.plus.me.adapter.MyClosetAdapter;
import com.plus.me.listener.CatagoriesPopupListener;
import com.plus.me.listener.GetItemPosition;
import com.plus.me.model.MyCloset;
import com.plus.me.others.App;
import com.plus.me.others.Internet;
import com.plus.me.others.PicassoTrustAll;
import com.plus.me.utils.VerticalSpacingDecoration;
import com.plus.me.ws.AsyncTaskListner;
import com.plus.me.ws.CallRequest;
import com.plus.me.ws.Constant;
import com.plus.me.ws.JsonParserUniversal;
import com.plus.me.ws.MyConstants;
import com.plus.me.ws.Utils;
import com.squareup.picasso.Callback;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static android.view.View.GONE;
import static com.plus.me.activity.HomeActivity.ivDown;
import static com.plus.me.activity.HomeActivity.replaceFragment;
import static com.plus.me.activity.HomeActivity.setupBadge;
import static com.plus.me.activity.HomeActivity.toolbar;
import static com.plus.me.activity.HomeActivity.txtSave;

/**
 * A simple {@link Fragment} subclass.
 */
public class MyClosetFragment extends Fragment implements AsyncTaskListner, CatagoriesPopupListener, GetItemPosition {

    public JsonParserUniversal jParser;
    public TextView txtNoValue;
    private RecyclerView rvCloset, rvCatagoryFilter;
    private GridLayoutManager layoutManager;
    private ArrayList<MyCloset> myClosetList = new ArrayList<>();
    private MyClosetAdapter myClosetAdapter;
    private String[] categoryArray = {"Hot", "New", "Chic", "Casual", "Denim", "Sporty", "Party Style"};
    private PopupWindow popup;
    private CatagoryPopupAdapter categoriesAdapter;
    private String categoryId = "";
    //public Dialog dialog;

    public MyClosetFragment() {
        // Required empty public constructor
    }

    View view;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        view = inflater.inflate(R.layout.fragment_my_closet, container, false);

        setHasOptionsMenu(true);

        bindWidgetReference(view);

        initializeData();

        // TODO comment addData(); and uncomment getData();


        return view;
    }

    private void initializeData() {
        myClosetList.clear();
        jParser = new JsonParserUniversal();
        ivDown.setVisibility(View.VISIBLE);
        setActionSpinner(getActivity(), view);
        toolbar.setText("My Closet");
    }

    private void setActionSpinner(final Activity context, View v) {
        toolbar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int[] location = new int[2];
                v.getLocationOnScreen(location);
                int x = location[0];
                int y = location[1];
                LinearLayout viewGroup = (LinearLayout) context.findViewById(R.id.rlMain);
                LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                View layout = layoutInflater.inflate(R.layout.popup_action_filter, viewGroup);
                popup = new PopupWindow(context);
                popup.setContentView(layout);
                popup.setWidth(LinearLayout.LayoutParams.WRAP_CONTENT);
                popup.setHeight(LinearLayout.LayoutParams.WRAP_CONTENT);
                popup.setFocusable(true);
                popup.setBackgroundDrawable(new BitmapDrawable());
                popup.showAtLocation(layout, Gravity.NO_GRAVITY, x, y);
                rvCatagoryFilter = layout.findViewById(R.id.rvCatagory);
                RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
                rvCatagoryFilter.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
                rvCatagoryFilter.setLayoutManager(mLayoutManager);

                categoriesAdapter = new CatagoryPopupAdapter(getActivity(), MyConstants.categoriesList, MyClosetFragment.this);
                rvCatagoryFilter.setHasFixedSize(true);
                rvCatagoryFilter.setDrawingCacheEnabled(true);
                //rvCompleteChat.addItemDecoration(new DividerItemDecoration(getActivity(), R.drawable.divider));
                rvCatagoryFilter.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
                rvCatagoryFilter.setAdapter(categoriesAdapter);
            }
        });

        ivDown.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toolbar.performClick();
            }
        });
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
       /* txtSave.setVisibility(GONE);
        MyConstants.isEditable = false;
        toolbar.setClickable(true);
        MyConstants.editImageUrl = "";
        MyConstants.isFromCreateLook = false;
        getData();
        menu.getItem(0).setVisible(false);
        menu.getItem(1).setVisible(false);
        menu.getItem(2).setVisible(true);
        menu.getItem(3).setVisible(false);
        super.onPrepareOptionsMenu(menu);*/


        txtSave.setVisibility(View.GONE);
        ivDown.setVisibility(View.GONE);
        toolbar.setClickable(false);
        getData();
        MyConstants.editImageUrl = "";
        MyConstants.isFromCreateLook = false;
        menu.getItem(0).setVisible(false);
        menu.getItem(1).setVisible(false);
        menu.getItem(2).setVisible(true);
        menu.getItem(3).setVisible(false);
        menu.getItem(4).setVisible(false);
        super.onPrepareOptionsMenu(menu);

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);

        menu.clear();
        inflater.inflate(R.menu.home, menu);
        SearchView searchView = (SearchView) MenuItemCompat.getActionView(menu.findItem(R.id.item_search));
        EditText searchEditText = (EditText) searchView.findViewById(R.id.search_src_text);
        searchEditText.setTextColor(getResources().getColor(R.color.black));


        final MenuItem menuItem = menu.findItem(R.id.item_notification);

        View actionView = MenuItemCompat.getActionView(menuItem);
        HomeActivity.tvNotificationItemCount = actionView.findViewById(R.id.notification_badge);
        FrameLayout frameNotification = actionView.findViewById(R.id.frameNotification);
        frameNotification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                replaceFragment(new NotificationFragment(), false);
            }
        });
        setupBadge();

    }

    private void bindWidgetReference(View view) {
        rvCloset = view.findViewById(R.id.rvCloset);
        txtNoValue = view.findViewById(R.id.txtNoValue);
    }

    private void getData() {
        Map<String, String> map = new HashMap<>();
        map.put("url", MyConstants.BASE_URL + "GetMyClosetList");
        map.put("header", "");
        map.put("Auth", App.user.getAuth());
        map.put("CategoryID", categoryId);
        if (!Internet.isAvailable(getActivity())) {
            Internet.showAlertDialog(getActivity(), "Error!", "No Internet Connection", false);
        } else {
            new CallRequest(MyClosetFragment.this).getMyClosetList(map);
        }
    }

    private void setData() {
        layoutManager = new GridLayoutManager(getActivity(), 3, GridLayoutManager.VERTICAL, false); // MAX NUMBER OF SPACES
        rvCloset.setLayoutManager(layoutManager);

        rvCloset.setHasFixedSize(true);
        rvCloset.addItemDecoration(new VerticalSpacingDecoration(5));
        rvCloset.setItemViewCacheSize(20);
        rvCloset.setDrawingCacheEnabled(true);
        rvCloset.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
        myClosetAdapter = new MyClosetAdapter(getActivity(), myClosetList, MyClosetFragment.this);
        rvCloset.setAdapter(myClosetAdapter);
        myClosetAdapter.notifyDataSetChanged();
 /*       rvCloset.addOnItemTouchListener(new RecyclerTouchListener(getContext(), rvCloset, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                // dialogViewImage(position);
                dialogViewImage(position);

            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));*/
    }

    private void dialogViewImage(final int position) {

        final Dialog dialog = new Dialog(getContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.dialog_view_image);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        ImageView ivImage = dialog.findViewById(R.id.ivImage);
        ImageView imgPhotoEditorClose = dialog.findViewById(R.id.close);
        Button ivEdit = dialog.findViewById(R.id.ivEdit);
        Button ivRemove = dialog.findViewById(R.id.btn_Post);
        Button ivdlt = dialog.findViewById(R.id.ivDelete);
        ivRemove.setText("Remove");
        ivdlt.setVisibility(GONE);
        imgPhotoEditorClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        final ProgressBar progressBar = dialog.findViewById(R.id.progressBar);
        myClosetList.get(position).getPostImage();

        try {
            PicassoTrustAll.getInstance(getContext())
                    .load(myClosetList.get(position).getClosetImage())
                    .into(ivImage, new Callback() {
                        @Override
                        public void onSuccess() {
                            progressBar.setVisibility(GONE);
                        }

                        @Override
                        public void onError() {

                        }
                    });
        } catch (Exception e) {
            e.printStackTrace();
        }

        ivEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();

                MyConstants.editImageUrl = myClosetList.get(position).getClosetImage();
                MyConstants.baseImageId = myClosetList.get(position).getClosetID();
                App.inMuCloset = true;

                System.out.println("blogid===" + myClosetList.get(position).getClosetID());
                App.inMuClosetEdit = true;

                Bundle args = new Bundle();
                args.putString("frommyclose", "myclose");
                CreateALookFragmentNew myClosetFragment = new CreateALookFragmentNew();
                myClosetFragment.setArguments(args);

                replaceFragment(myClosetFragment, false);

            }
        });
        ivRemove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();
                Map<String, String> map = new HashMap<>();
                map.put("url", MyConstants.BASE_URL + "RemoveCloset");

                map.put("header", "");
                map.put("Auth", App.user.getAuth());
                map.put("ClosetID", myClosetList.get(position).getClosetID());

                new CallRequest(MyClosetFragment.this).RemoveMyCloset(map);
                dialog.dismiss();

            }
        });
        dialog.show();
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        try {

            Utils.hideProgressDialog();
            if (result != null && !result.isEmpty()) {
                Log.i("TAG", "TAG Result : " + result);

                switch (request) {
                    case getMyClosetList:
                        myClosetList.clear();
                        MyConstants.myClosetList.clear();
                        try {
                            JSONObject object = new JSONObject(result);
                            boolean success = object.getBoolean("success");
                            if (success) {
                                // TODO parse JSON

                                JSONArray data = object.getJSONArray("data");
                                if (data != null && data.length() > 0) {
                                    for (int i = 0; i < data.length(); i++) {
                                        JSONObject jsonObject = data.getJSONObject(i);

                                        MyCloset myCloset = (MyCloset) jParser.parseJson(jsonObject, new MyCloset());
                                        myClosetList.add(myCloset);
                                        MyConstants.myClosetList.add(myCloset);
                                    }

                                    setData();
                                } else {
                                    // Utils.showToast(object.getString("message"), getActivity());
                                }

                            } else {


                                String error_string = object.getString("message");
                                Toast.makeText(getActivity(), error_string, Toast.LENGTH_SHORT).show();
                            }
                            if (myClosetList.size() == 0) {
                                txtNoValue.setVisibility(View.VISIBLE);
                            } else {
                                txtNoValue.setVisibility(View.GONE);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;
                    case RemoveMyCloset:
                        try {
                            JSONObject object = new JSONObject(result);
                            boolean success = object.getBoolean("success");
                            if (success) {
                                String error_string = object.getString("message");
                                Toast.makeText(getActivity(), error_string, Toast.LENGTH_SHORT).show();
                                getData();

                            } else {
                                String error_string = object.getString("message");
                                Toast.makeText(getActivity(), error_string, Toast.LENGTH_SHORT).show();

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onProgressUpdate(String uniqueMessageId, int progres) {

    }

    @Override
    public void onProgressComplete(String uniqueMessageId, String result, Constant.REQUESTS request) {

    }

    @Override
    public void onPopupItemClick(int pos, View v) {
        popup.dismiss();
        categoryId = MyConstants.categoriesList.get(pos).getCategoryID();
        toolbar.setText(MyConstants.categoriesList.get(pos).getCategoryName());
        getData();
    }

    @Override
    public void onIteamPos(int pos) {
        dialogViewImage(pos);
    }
}
