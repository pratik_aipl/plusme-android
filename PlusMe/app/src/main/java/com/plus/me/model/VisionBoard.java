package com.plus.me.model;

import android.graphics.Bitmap;
import android.net.Uri;

import java.io.Serializable;

public class VisionBoard implements Serializable {

    public String BlogId="",PostImage="",PostTitle="",PostCategoryId="",AddVisionBoardID;
    public Bitmap bitmap ;

    public VisionBoard() {
    }

    public VisionBoard(String sdata, String sid, String sname, String sbid, String sbname) {
    }

    public String getBlogId() {
        return BlogId;
    }

    public void setBlogId(String blogId) {
        BlogId = blogId;
    }

    public String getPostImage() {
        return PostImage;
    }

    public void setPostImage(String postImage) {
        PostImage = postImage;
    }

    public String getPostTitle() {
        return PostTitle;
    }

    public void setPostTitle(String postTitle) {
        PostTitle = postTitle;
    }

    public String getPostCategoryId() {
        return PostCategoryId;
    }

    public void setPostCategoryId(String postCategoryId) {
        PostCategoryId = postCategoryId;
    }

    public String getAddVisionBoardID() {
        return AddVisionBoardID;
    }

    public void setAddVisionBoardID(String addVisionBoardID) {
        AddVisionBoardID = addVisionBoardID;
    }
}
