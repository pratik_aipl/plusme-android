package com.plus.me.fragments;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.MultiAutoCompleteTextView;

import com.plus.me.R;
import com.plus.me.activity.MapsActivity;
import com.plus.me.custom_views.CustomTextView;
import com.plus.me.others.App;
import com.plus.me.ws.AsyncTaskListner;
import com.plus.me.ws.CallRequest;
import com.plus.me.ws.Constant;
import com.plus.me.ws.MyConstants;
import com.plus.me.ws.Utils;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import static android.app.Activity.RESULT_OK;
import static com.plus.me.activity.HomeActivity.replaceFragment;
import static com.plus.me.activity.HomeActivity.toolbar;
import static com.plus.me.activity.HomeActivity.txtSave;

public class AddNewStoreFragment extends Fragment implements AsyncTaskListner {
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private String mParam1;
    private String mParam2;
    public AddNewStoreFragment instance;

    public View view;
    public ImageView img_logo;
    public EditText et_comp_name, et_website, et_phone, et_location;
    public MultiAutoCompleteTextView et_about;
    public Uri selectedUri;
    private static String profImagePath = "";
    public Button btnPost;
    public static final int REQUEST_CODE = 1;
    double Lat, Lng;
    String LocAddress;
    CustomTextView tv_Cancel;

    public AddNewStoreFragment() {
        // Required empty public constructor
    }


    // TODO: Rename and change types and number of parameters
    public static AddNewStoreFragment newInstance(String param1, String param2) {
        AddNewStoreFragment fragment = new AddNewStoreFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_add_new_store, container, false);
        setHasOptionsMenu(true);
        toolbar.setText("Add New Store");
        instance = this;
        btnPost = view.findViewById(R.id.btnPost);
        et_comp_name = view.findViewById(R.id.et_comp_name);
        et_about = view.findViewById(R.id.et_about);
        et_location = view.findViewById(R.id.et_location);
        et_phone = view.findViewById(R.id.et_phone);
        tv_Cancel = view.findViewById(R.id.tv_Cancel);
        et_website = view.findViewById(R.id.et_website);
        img_logo = view.findViewById(R.id.img_logo);
        img_logo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                CropImage.startPickImageActivity(getActivity(), instance);
                //  pickImage();
            }
        });
        btnPost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Validation();
            }
        });
        et_location.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getContext(),
                        MapsActivity.class);
                startActivityForResult(intent, REQUEST_CODE);
            }
        });
        tv_Cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                // replaceFragment(new StoresFragment(), true);
                getActivity().onBackPressed();
            }
        });

        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onDetach() {
        super.onDetach();

    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        txtSave.setVisibility(View.GONE);
        MyConstants.editImageUrl = "";
        menu.getItem(0).setVisible(false);
        menu.getItem(1).setVisible(true);
        menu.getItem(2).setVisible(true);
        menu.getItem(3).setVisible(false);
        menu.getItem(4).setVisible(false);
        //tabs.setVisibility(View.VISIBLE);
        super.onPrepareOptionsMenu(menu);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CropImage.PICK_IMAGE_CHOOSER_REQUEST_CODE && resultCode == RESULT_OK) {
            Uri imageUri = CropImage.getPickImageResultUri(getActivity(), data);

            if (CropImage.isReadExternalStoragePermissionsRequired(getActivity(), imageUri)) {
                selectedUri = imageUri;
                profImagePath = selectedUri.getPath().toString();
                requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 0);
            } else {
                startCropImageActivity(imageUri);
            }
        }

        if (requestCode == REQUEST_CODE && resultCode == RESULT_OK) {

            Lat = data.getDoubleExtra("Lat", 0);
            Lng = data.getDoubleExtra("Lng", 0);
            LocAddress = data.getStringExtra("LocAddress");
            et_location.setText(LocAddress);
        }

        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                img_logo.setImageURI(result.getUri());
                selectedUri = result.getUri();
                profImagePath = selectedUri.getPath().toString();
                MyConstants.imageType = "0";
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
            }
        }

    }

    private void startCropImageActivity(Uri imageUri) {
        CropImage.activity(imageUri)
                .setGuidelines(CropImageView.Guidelines.ON)
                .setAspectRatio(1, 1)
                .setFixAspectRatio(false)
                .setMultiTouchEnabled(true)
                .start(getActivity(), instance);

    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        try {

            Utils.hideProgressDialog();
            if (result != null && !result.isEmpty()) {
                Log.i("TAG", "TAG Result : " + result);

                switch (request) {
                    case addCompanyStore:
                        try {
                            JSONObject object = new JSONObject(result);
                            boolean success = object.getBoolean("success");
                            if (success) {
                                // TODO parse JSON
                                Utils.showToast(object.getString("message"), getActivity());
                                replaceFragment(new StoresFragment(), true);
                            } else {
                                MyConstants.selectedImage = "";
                                Utils.showToast(object.getString("message"), getActivity());
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onProgressUpdate(String uniqueMessageId, int progres) {

    }

    @Override
    public void onProgressComplete(String uniqueMessageId, String result, Constant.REQUESTS request) {

    }


    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    public void Validation() {
        if (et_comp_name.getText().toString().equals("")) {
            et_comp_name.setError("Please Enter Post Title.");
            et_comp_name.requestFocus();
        } else if (profImagePath.toString().equals("")) {
            Utils.showToast("Please Select Logo.", getActivity());
        } else if (et_website.getText().toString().equals("")) {
            et_website.setError("Please Enter website url.");
            et_website.requestFocus();
        } else if (et_phone.getText().toString().equals("")) {
            et_phone.setError("Please Enter phone no.");
            et_phone.requestFocus();
        } else if (et_location.getText().toString().equals("")) {
            et_location.setError("Please Enter location/addreaa.");
            et_location.requestFocus();
        } else if (et_about.getText().toString().equals("")) {
            et_about.setError("Please Enter about company.");
            et_about.requestFocus();
        } else {
            AddStore();
        }
    }

    public void AddStore() {
        Map<String, String> map = new HashMap<>();
        map.put("url", MyConstants.BASE_URL + "CompanyStore");
        map.put("CompanyName", et_comp_name.getText().toString());
        map.put("WesiteUrl", et_website.getText().toString());
        map.put("PhoneNo", "+1" + et_phone.getText().toString());
        map.put("StoreLatitude", String.valueOf(Lat));
        map.put("StoreLongiture", String.valueOf(Lng));
        map.put("LocationAdress", LocAddress);
        map.put("AboutCompany", et_about.getText().toString());
        map.put("CompanyLogo", profImagePath);
        map.put("header", "");
        map.put("Auth", App.user.getAuth());
        new CallRequest(instance).addCompanyStore(map);

    }
}
