package com.plus.me.fragments;


import android.Manifest;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.plus.me.R;
import com.plus.me.activity.HomeActivity;
import com.plus.me.adapter.BlogDetailAdapter;
import com.plus.me.model.BlogDetail;
import com.plus.me.model.BlogList;
import com.plus.me.model.CommentDetail;
import com.plus.me.model.MyFeeds;
import com.plus.me.others.App;
import com.plus.me.others.Internet;
import com.plus.me.others.PicassoTrustAll;
import com.plus.me.utils.Utilities;
import com.plus.me.utils.VerticalSpacingDecoration;
import com.plus.me.ws.AsyncTaskListner;
import com.plus.me.ws.CallRequest;
import com.plus.me.ws.Constant;
import com.plus.me.ws.JsonParserUniversal;
import com.plus.me.ws.MyConstants;
import com.plus.me.ws.Utils;
import com.squareup.picasso.Callback;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static com.plus.me.activity.HomeActivity.replaceFragment;
import static com.plus.me.activity.HomeActivity.toolbar;
import static com.plus.me.activity.HomeActivity.txtSave;

/**
 * A simple {@link Fragment} subclass.
 */
public class MyFeedDetailFragment extends Fragment implements AsyncTaskListner {

    public RecyclerView rvBlogDetail;
    public ImageView ivImage, ivUser, ivCloset, ivSend, ivLike;
    public ProgressBar progressBar;
    public TextView tvTitle, tvUserName, tvTime, tvDescription, tvCommentCount,tvLikeCount;
    public LinearLayout llViewAll, llComments;
    public JsonParserUniversal jParser;
    private LinearLayoutManager layoutManager;
    public static ArrayList<CommentDetail> commentDetailsList = new ArrayList<>();
    public static ArrayList<MyFeeds> feedsDetailList = new ArrayList<>();
    private BlogDetailAdapter blogDetailAdapter;
    private BlogList blogList;
    private MyFeeds myFeeds;
    public BlogDetail blogDetail;
    private View line;
    private EditText etComment;
    public String comment, intent, id, uploadpath = "";
    public CommentDetail commentDetail;
    private static final int PICK_IMAGE_PERMISSION_CODE = 1;

    public MyFeedDetailFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_feed_detail, container, false);
        setHasOptionsMenu(true);

        intent = getArguments().getString("intent");
        if (intent.equalsIgnoreCase("blog")) {
            if (getArguments() != null) {
                blogList = (BlogList) getArguments().getSerializable("blog");
                id = blogList.getBlogId();
                System.out.println("id====" + id);
            }
        } else if (intent.equalsIgnoreCase("feeds")) {

            if (getArguments() != null) {
                if (!TextUtils.isEmpty(App.noti_redirect_id)) {
                    id = App.noti_redirect_id;// myFeeds.getBlogId();
                    System.out.println("id====" + id);
                } else {
                    myFeeds = (MyFeeds) getArguments().getSerializable("feeds");
                    id = myFeeds.getBlogId();
                }

            }
        } else if (intent.equalsIgnoreCase("id")) {

            if (getArguments() != null) {

                id = getArguments().getString("id");
                System.out.println("id====" + id);

            }
        }
        toolbar.setText("Blogs");
        commentDetailsList.clear();
        jParser = new JsonParserUniversal();

        bindWidgetReference(view);

        bindWidgetEvents();

        getData();

        // setData();

        return view;
    }

    private void bindWidgetEvents() {
        ivCloset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (blogDetail.getIsCloset().equalsIgnoreCase("0")) {
                    addToCloset();
                } else {
                    Utils.showToast("This item is already added in my closet", getActivity());
                }


            }
        });

        ivLike.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                doLikeDislike();
            }
        });
    }

    private void doLikeDislike() {

        String isLike = null;
        if (blogDetail.getIsLike().equalsIgnoreCase("0")) {
            isLike = "1";
        } else {
            isLike = "0";
        }
        Map<String, String> map = new HashMap<>();
        map.put("url", MyConstants.BASE_URL + "LikePost");
        map.put("PostId", blogDetail.getBlogId()); // TODO add category id
        map.put("isLike", isLike);
        map.put("header", "");
        map.put("Auth", App.user.getAuth());


        new CallRequest(MyFeedDetailFragment.this).getLike(map);
    }

    private void addToCloset() {
        if (isReadStorageAllowed()) {

            new DownloadFileFromURL().execute("");

        }


    }

    public boolean isReadStorageAllowed() {

        //If permission is granted returning true
        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE)
                + ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE)
                == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            requestPermissions(new String[]{Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, PICK_IMAGE_PERMISSION_CODE);
            return false;
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == PICK_IMAGE_PERMISSION_CODE) {
            if (grantResults.length > 0) {
                boolean readExternalFile = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                boolean writeExternalFile = grantResults[1] == PackageManager.PERMISSION_GRANTED;

                if (readExternalFile && writeExternalFile) {
                    new DownloadFileFromURL().execute("");
                }
            }
        }
    }

    private void bindWidgetReference(View view) {
        rvBlogDetail = view.findViewById(R.id.rvBlogDetail);
        ivImage = view.findViewById(R.id.ivImage);
        ivUser = view.findViewById(R.id.ivUser);
        ivCloset = view.findViewById(R.id.ivCloset);
        progressBar = view.findViewById(R.id.progressBar);
        tvTitle = view.findViewById(R.id.tvTitle);
        tvUserName = view.findViewById(R.id.tvUserName);
        tvTime = view.findViewById(R.id.tvTime);
        tvDescription = view.findViewById(R.id.tvDescription);
        tvCommentCount = view.findViewById(R.id.tvCommentCount);
        tvLikeCount = view.findViewById(R.id.tvLikeCount);
        llViewAll = view.findViewById(R.id.llViewAll);
        llComments = view.findViewById(R.id.llComments);
        line = view.findViewById(R.id.view);
        etComment = view.findViewById(R.id.etComment);
        ivSend = view.findViewById(R.id.ivSend);
        ivLike = view.findViewById(R.id.ivLike);
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        txtSave.setVisibility(View.GONE);
        menu.getItem(0).setVisible(true);
        menu.getItem(1).setVisible(true);
        menu.getItem(2).setVisible(true);
        menu.getItem(3).setVisible(false);
        menu.getItem(4).setVisible(false);
        super.onPrepareOptionsMenu(menu);
    }

    private void getData() {
        Map<String, String> map = new HashMap<>();
        map.put("url", MyConstants.BASE_URL + "GetBlogDetail");
        map.put("BlogId", id); // TODO add post id
        map.put("header", "");
        map.put("Auth", App.user.getAuth());

        if (!Internet.isAvailable(getActivity())) {
            Internet.showAlertDialog(getActivity(), "Error!", "No Internet Connection", false);
        } else {
            new CallRequest(MyFeedDetailFragment.this).getBlogDetail(map);
        }
    }

    private void setDataBlog() {

        tvTitle.setText(blogDetail.getPostedByName());
        tvCommentCount.setText(blogDetail.getTotalComment());
        tvCommentCount.setText(blogDetail.getTotalLike());
        tvDescription.setText(Utilities.convertUTF8ToString(blogDetail.getPostTitle()));
        Utils.getDatesDifferenceInDays(blogDetail.getPostTime(), tvTime);
        tvUserName.setText(blogDetail.getRegisterUserTypeByName());
        id = blogDetail.getBlogId();

        if (blogDetail.getIsCloset().equalsIgnoreCase("1")) {
            ivCloset.setImageResource(R.drawable.closet_pink);
        } else {
            ivCloset.setImageResource(R.drawable.closet_gray);
        }

        if (blogDetail.getIsLike().equalsIgnoreCase("0")) {
            ivLike.setImageResource(R.drawable.ic_dislike);
        } else {
            ivLike.setImageResource(R.drawable.ic_like);
        }

        tvTitle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ProfileFragment profileFragment = new ProfileFragment();
                Bundle bundle = new Bundle();
                bundle.putString("intent", "profile");
                bundle.putString("id", blogList.getBlogByID());
                profileFragment.setArguments(bundle);
                HomeActivity.replaceFragment(profileFragment, false);
            }
        });

        ivSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                comment = etComment.getText().toString();

                if (TextUtils.isEmpty(comment)) {
                    Utils.showToast("Please Add Comment", getActivity());
                } else {
                    addComment();
                }
            }
        });
        llViewAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                replaceFragment(new ViewAllCommentFragment(), false);
            }
        });
        try {
            PicassoTrustAll.getInstance(getActivity())
                    .load(blogDetail.getPostImage())
                    .into(ivImage, new Callback() {
                        @Override
                        public void onSuccess() {
                            progressBar.setVisibility(View.GONE);
                        }

                        @Override
                        public void onError() {
                        }
                    });
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            PicassoTrustAll.getInstance(getActivity())
                    .load(blogDetail.getProfileImage())
                    .placeholder(R.drawable.no_image)
                    .error(R.drawable.no_image)
                    .into(ivUser, new Callback() {
                        @Override
                        public void onSuccess() {
                        }

                        @Override
                        public void onError() {
                        }
                    });
        } catch (Exception e) {
            e.printStackTrace();
        }

        layoutManager = new LinearLayoutManager(getActivity());
        rvBlogDetail.setLayoutManager(layoutManager);
        blogDetailAdapter = new BlogDetailAdapter(getActivity(), commentDetailsList);
        rvBlogDetail.setHasFixedSize(true);
        rvBlogDetail.addItemDecoration(new VerticalSpacingDecoration(20));
        rvBlogDetail.setItemViewCacheSize(20);
        rvBlogDetail.setDrawingCacheEnabled(true);
        //rvCompleteChat.addItemDecoration(new DividerItemDecoration(getActivity(), R.drawable.divider));
        rvBlogDetail.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
        rvBlogDetail.setAdapter(blogDetailAdapter);

    }

    private void setDataFeeds() {
        toolbar.setText("My Feeds");

        tvTitle.setText(blogDetail.getPostedByName());
        tvCommentCount.setText(blogDetail.getTotalComment());
        tvLikeCount.setText(blogDetail.getTotalLike());
        tvDescription.setText(Utilities.convertUTF8ToString(blogDetail.getPostTitle()));
        Utils.getDatesDifferenceInDays(blogDetail.getPostTime(), tvTime);
        tvUserName.setText(blogDetail.getRegisterUserTypeByName());
        id = blogDetail.getBlogId();
        if (blogDetail.getIsCloset().equalsIgnoreCase("1")) {
            ivCloset.setImageResource(R.drawable.closet_pink);
        } else {
            ivCloset.setImageResource(R.drawable.closet_gray);
        }

        if (blogDetail.getIsLike().equalsIgnoreCase("0")) {
            ivLike.setImageResource(R.drawable.ic_dislike);
        } else {
            ivLike.setImageResource(R.drawable.ic_like);
        }


        tvTitle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ProfileFragment profileFragment = new ProfileFragment();
                Bundle bundle = new Bundle();
                bundle.putString("intent", "profile");
                bundle.putString("id", blogDetail.getPostedByID());
                profileFragment.setArguments(bundle);
                HomeActivity.replaceFragment(profileFragment, false);
            }
        });

        ivSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                comment = etComment.getText().toString();

                if (TextUtils.isEmpty(comment)) {
                    Utils.showToast("Please Add Comment", getActivity());
                } else {
                    addComment();
                }
            }
        });
        llViewAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                replaceFragment(new ViewFeedAllCommentFragment(), false);
            }
        });
        try {
            PicassoTrustAll.getInstance(getActivity())
                    .load(blogDetail.getPostImage())
                    .into(ivImage, new Callback() {
                        @Override
                        public void onSuccess() {
                            progressBar.setVisibility(View.GONE);
                        }

                        @Override
                        public void onError() {
                        }
                    });
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            PicassoTrustAll.getInstance(getActivity())
                    .load(blogDetail.getProfileImage())
                    .placeholder(R.drawable.no_image)
                    .error(R.drawable.no_image)
                    .into(ivUser, new Callback() {
                        @Override
                        public void onSuccess() {
                        }

                        @Override
                        public void onError() {
                        }
                    });
        } catch (Exception e) {
            e.printStackTrace();
        }

        layoutManager = new LinearLayoutManager(getActivity());
        rvBlogDetail.setLayoutManager(layoutManager);
        blogDetailAdapter = new BlogDetailAdapter(getActivity(), commentDetailsList);
        rvBlogDetail.setHasFixedSize(true);
        rvBlogDetail.addItemDecoration(new VerticalSpacingDecoration(20));
        rvBlogDetail.setItemViewCacheSize(20);
        rvBlogDetail.setDrawingCacheEnabled(true);
        //rvCompleteChat.addItemDecoration(new DividerItemDecoration(getActivity(), R.drawable.divider));
        rvBlogDetail.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
        rvBlogDetail.setAdapter(blogDetailAdapter);


    }

    // convert internal Java String format to UTF-8
    public static String convertStringToUTF8(String s) {
        String out = null;
        try {
            out = new String(s.getBytes("UTF-8"), "ISO-8859-1");
        } catch (java.io.UnsupportedEncodingException e) {
            return null;
        }
        return out;
    }

    private void addComment() {
        Map<String, String> map = new HashMap<>();
        map.put("url", MyConstants.BASE_URL + "add_comment");
        map.put("blog_id", id); // TODO add post id
        map.put("userid", App.user.getId()); // TODO add user id
        map.put("header", "");
        map.put("comment", convertStringToUTF8(comment));
        map.put("Auth", App.user.getAuth());

        if (!Internet.isAvailable(getActivity())) {
            Internet.showAlertDialog(getActivity(), "Error!", "No Internet Connection", false);
        } else {
            new CallRequest(MyFeedDetailFragment.this).addComment(map);
        }

    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        try {

            Utils.hideProgressDialog();
            if (result != null && !result.isEmpty()) {
                Log.i("TAG", "TAG Result : " + result);

                switch (request) {
                    case getBlogDetail:
                        commentDetailsList.clear();
                        try {
                            JSONObject object = new JSONObject(result);
                            boolean success = object.getBoolean("success");
                            if (success) {
                                // TODO parse JSON

                                JSONObject data = object.getJSONObject("data");
                                blogDetail = (BlogDetail) jParser.parseJson(object.getJSONObject("data"), new BlogDetail());
                                JSONArray array = data.getJSONArray("CommentArray");

                                if (array != null && array.length() > 0) {

                                    for (int i = 0; i < array.length(); i++) {
                                        JSONObject jsonObject = array.getJSONObject(i);
                                        commentDetail = (CommentDetail) jParser.parseJson(jsonObject, new CommentDetail());
                                        commentDetailsList.add(commentDetail);

                                    }

                                    rvBlogDetail.setVisibility(View.VISIBLE);
                                    llComments.setVisibility(View.GONE);
                                    line.setVisibility(View.VISIBLE);
                                } else {
                                    //Utils.showToast(object.getString("message"), getActivity());
                                    llComments.setVisibility(View.GONE);
                                    rvBlogDetail.setVisibility(View.GONE);
                                    line.setVisibility(View.GONE);
                                }


                                if (intent.equalsIgnoreCase("blog")) {
                                    setDataBlog();
                                }
                                if (intent.equalsIgnoreCase("feeds")) {
                                    setDataFeeds();
                                }
                                if (intent.equalsIgnoreCase("id")) {
                                    setDataFeeds();
                                }

                            } else {
                                String error_string = object.getString("message");
                                Toast.makeText(getActivity(), error_string, Toast.LENGTH_SHORT).show();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;

                    case addToCloset:

                        try {
                            JSONObject object = new JSONObject(result);
                            boolean success = object.getBoolean("success");
                            if (success) {
                                // TODO parse JSON
                                Utils.showToast(object.getString("message"), getActivity());

                                getData();
                            } else {
                                Utils.showToast(object.getString("message"), getActivity());
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;
                    case addComment:

                        try {
                            JSONObject object = new JSONObject(result);
                            boolean success = object.getBoolean("success");
                            if (success) {
                                // TODO parse JSON
                                etComment.setText("");
                                Utils.showToast(object.getString("message"), getActivity());
                                getData();
                            } else {
                                Utils.showToast(object.getString("message"), getActivity());
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;

                    case getLike:

                        try {
                            JSONObject object = new JSONObject(result);
                            boolean success = object.getBoolean("success");
                            if (success) {
                                // TODO parse JSON
                                //Utils.showToast(object.getString("message"), getActivity());

                                getData();
                                if (ivLike.getDrawable().getConstantState() == getResources().getDrawable(R.drawable.ic_dislike).getConstantState()) {
                                    ivLike.setImageDrawable(getResources().getDrawable(R.drawable.ic_like));
                                } else {
                                    ivLike.setImageDrawable(getResources().getDrawable(R.drawable.ic_dislike));
                                }

                            } else {
                                Utils.showToast(object.getString("message"), getActivity());
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onProgressUpdate(String uniqueMessageId, int progres) {

    }

    @Override
    public void onProgressComplete(String uniqueMessageId, String result, Constant.REQUESTS request) {

    }

    /**
     * Background Async Task to download file
     */
    class DownloadFileFromURL extends AsyncTask<String, String, String> {

        /**
         * Before starting background thread Show Progress Bar Dialog
         */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            Utils.showProgressDialog(getActivity());
        }

        /**
         * Downloading file in background thread
         */
        @Override
        protected String doInBackground(String... f_url) {
            int count;

            String convert = blogDetail.getPostImage();
            String extantion = convert.substring(convert.lastIndexOf("."));
            String fileName = convert.substring(convert.lastIndexOf("/") + 1, convert.lastIndexOf(".")) + extantion;
            uploadpath = getActivity().getExternalCacheDir() + "/" + fileName;
            File file = new File(uploadpath);
            if (file.exists()) {
                return null;
            } else {

                try {
                    URL url = new URL(blogDetail.getPostImage());
                    URLConnection conection = url.openConnection();
                    conection.connect();

                    // this will be useful so that you can show a tipical 0-100%
                    // progress bar
                    int lenghtOfFile = conection.getContentLength();

                    // download the file
                    InputStream input = new BufferedInputStream(url.openStream(),
                            8192);

                    // Output stream
                    uploadpath = getActivity().getExternalCacheDir() + "/" + fileName;
                    OutputStream output = new FileOutputStream(uploadpath);

                    byte data[] = new byte[1024];

                    long total = 0;

                    while ((count = input.read(data)) != -1) {
                        total += count;
                        // publishing the progress....
                        // After this onProgressUpdate will be called
                        publishProgress("" + (int) ((total * 100) / lenghtOfFile));

                        // writing data to file
                        output.write(data, 0, count);
                    }

                    // flushing output
                    output.flush();

                    // closing streams
                    output.close();
                    input.close();

                } catch (Exception e) {
                    Log.e("Error: ", e.getMessage());
                }
            }

            return null;
        }

        /**
         * Updating progress bar
         */
        protected void onProgressUpdate(String... progress) {
            // setting progress percentage

        }

        /**
         * After completing background task Dismiss the progress dialog
         **/
        @Override
        protected void onPostExecute(String file_url) {
            Map<String, String> map = new HashMap<>();
            map.put("url", MyConstants.BASE_URL + "MoveToCloset");
            map.put("PostImage", uploadpath);
            map.put("Title", "");
            map.put("BlogId", blogDetail.getBlogId());
            map.put("BaseImageID", "0");
            map.put("header", "");
            map.put("Auth", App.user.getAuth());
            new CallRequest(MyFeedDetailFragment.this).addToCloset(map);
            // dismiss the dialog after the file was downloaded


        }

    }
}
