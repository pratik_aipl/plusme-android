package com.plus.me.ws;

import android.content.Context;
import androidx.fragment.app.Fragment;

import com.plus.me.others.App;

import java.util.Map;

/**
 * Created by Krupa Kakkad on 13 July 2018
 */
public class CallRequest {

    public App app;
    public Context ct;
    public Fragment ft;
    public static String version = "1";

    public CallRequest(Fragment ft) {
        app = App.getInstance();
        this.ft = ft;
    }

    public CallRequest(Context ct) {
        this.ct = ct;
        app = App.getInstance();
    }

    public CallRequest() {
    }

    public void doLogin(Map<String, String> map) {
        new AsyncHttpRequest(ct, Constant.REQUESTS.login, Constant.POST_TYPE.POST, map);
    }

    public void Isprivate(Map<String, String> map) {
        new AsyncHttpRequest(ct, Constant.REQUESTS.Isprivate, Constant.POST_TYPE.POST, map);
    }

    public void ReportSubmit(Map<String, String> map) {
        new AsyncHttpRequest(ct, Constant.REQUESTS.Report, Constant.POST_TYPE.POST, map);
    }

    public void doRegister(Map<String, String> map) {
        new AsyncHttpRequest(ct, Constant.REQUESTS.register, Constant.POST_TYPE.POST_WITH_IMAGE, map);
    }

    public void forgotPassword(Map<String, String> map) {
        new AsyncHttpRequest(ct, Constant.REQUESTS.forgotPassword, Constant.POST_TYPE.POST, map);
    }

    public void changePassword(Map<String, String> map) {
        new AsyncHttpRequest(ft, Constant.REQUESTS.changePassword, Constant.POST_TYPE.POST, map);
    }

    public void getNotificationList(Map<String, String> map) {
        new AsyncHttpRequest(ft, Constant.REQUESTS.getNotificationList, Constant.POST_TYPE.GET, map);
    }

    public void doLogOut(Map<String, String> map) {
        new AsyncHttpRequest(ft, Constant.REQUESTS.logout, Constant.POST_TYPE.GET, map);
    }

    public void getUserProfile(Map<String, String> map) {
        new AsyncHttpRequest(ct, Constant.REQUESTS.getUserProfile, Constant.POST_TYPE.POST, map);
    }

    public void getUserProfileFragment(Map<String, String> map) {
        new AsyncHttpRequest(ft, Constant.REQUESTS.getUserProfile, Constant.POST_TYPE.POST, map);
    }

    public void editUserProfile(Map<String, String> map) {
        new AsyncHttpRequest(ft, Constant.REQUESTS.editUserProfile, Constant.POST_TYPE.POST_WITH_IMAGE, map);
    }

    public void getMyPosts(Map<String, String> map) {
        new AsyncHttpRequest(ft, Constant.REQUESTS.getMyPosts, Constant.POST_TYPE.POST, map);
    }

    public void getDeletePost(Map<String, String> map) {
        new AsyncHttpRequest(ft, Constant.REQUESTS.getDeletePost, Constant.POST_TYPE.POST, map);
    }

    public void getDeleteBlog(Map<String, String> map) {
        new AsyncHttpRequest(ft, Constant.REQUESTS.getDeleteBlog, Constant.POST_TYPE.POST, map);
    }

    public void getDeleteVisionPost(Map<String, String> map) {
        new AsyncHttpRequest(ft, Constant.REQUESTS.getDeleteVisionPost, Constant.POST_TYPE.POST, map);
    }

    public void getMyFollowers(Map<String, String> map) {
        new AsyncHttpRequest(ft, Constant.REQUESTS.getMyFollowers, Constant.POST_TYPE.POST, map);
    }

    public void getMyFollowing(Map<String, String> map) {
        new AsyncHttpRequest(ft, Constant.REQUESTS.getMyFollowing, Constant.POST_TYPE.POST, map);
    }

    public void getBlogList(Map<String, String> map) {
        new AsyncHttpRequest(ft, Constant.REQUESTS.getBlogList, Constant.POST_TYPE.POST, map);
    }

    public void getBlogDetail(Map<String, String> map) {
        new AsyncHttpRequest(ft, Constant.REQUESTS.getBlogDetail, Constant.POST_TYPE.POST, map);
    }

    public void getCommentDetail(Map<String, String> map) {
        new AsyncHttpRequest(ft, Constant.REQUESTS.getCommentDetail, Constant.POST_TYPE.POST, map);
    }

    public void getMyClosetList(Map<String, String> map) {
        new AsyncHttpRequest(ft, Constant.REQUESTS.getMyClosetList, Constant.POST_TYPE.POST, map);
    }

    public void RemoveMyCloset(Map<String, String> map) {
        new AsyncHttpRequest(ft, Constant.REQUESTS.RemoveMyCloset, Constant.POST_TYPE.POST, map);
    }

    public void getMyFeedList(Map<String, String> map) {
        new AsyncHttpRequest(ft, Constant.REQUESTS.getMyFeedsList, Constant.POST_TYPE.GET, map);
    }

    public void getCategoryList(Map<String, String> map) {
        new AsyncHttpRequest(ft, Constant.REQUESTS.getCategoryList, Constant.POST_TYPE.POST, map);
    }

    public void getStoreList(Map<String, String> map) {
        new AsyncHttpRequest(ft, Constant.REQUESTS.getStoreList, Constant.POST_TYPE.GET, map);
    }

    public void addNewPost(Map<String, String> map) {
        new AsyncHttpRequest(ft, Constant.REQUESTS.addNewPost, Constant.POST_TYPE.POST_WITH_IMAGE, map);
    }

    public void addNewBlog(Map<String, String> map) {
        new AsyncHttpRequest(ft, Constant.REQUESTS.addNewBlog, Constant.POST_TYPE.POST_WITH_IMAGE, map);
    }

    public void addCompanyStore(Map<String, String> map) {
        new AsyncHttpRequest(ft, Constant.REQUESTS.addCompanyStore, Constant.POST_TYPE.POST_WITH_IMAGE, map);
    }


    public void addNewPostImage(Map<String, String> map) {
        new AsyncHttpRequest(ft, Constant.REQUESTS.addNewPost, Constant.POST_TYPE.POST, map);
    }

    public void getCatagory(Map<String, String> map) {

        new AsyncHttpRequest(ct, Constant.REQUESTS.getCategory, Constant.POST_TYPE.GET, map);
    }

    public void addToCloset(Map<String, String> map) {
        new AsyncHttpRequest(ft, Constant.REQUESTS.addToCloset, Constant.POST_TYPE.POST_WITH_IMAGE, map);
    }

    public void FollowUnFollow(Map<String, String> map) {
        new AsyncHttpRequest(ft, Constant.REQUESTS.followUnfollow, Constant.POST_TYPE.POST, map);
    }

    public void addComment(Map<String, String> map) {
        new AsyncHttpRequest(ft, Constant.REQUESTS.addComment, Constant.POST_TYPE.POST, map);
    }

    public void getCountry(Map<String, String> map) {

        new AsyncHttpRequest(ct, Constant.REQUESTS.getCountry, Constant.POST_TYPE.GET, map);
    }

    public void getState(Map<String, String> map) {

        new AsyncHttpRequest(ct, Constant.REQUESTS.getState, Constant.POST_TYPE.POST, map);
    }

    public void getCity(Map<String, String> map) {
        new AsyncHttpRequest(ct, Constant.REQUESTS.getCity, Constant.POST_TYPE.POST, map);
    }

    public void getCountryFragment(Map<String, String> map) {

        new AsyncHttpRequest(ft, Constant.REQUESTS.getCountry, Constant.POST_TYPE.GET, map);
    }

    public void getStateFragment(Map<String, String> map) {

        new AsyncHttpRequest(ft, Constant.REQUESTS.getState, Constant.POST_TYPE.POST, map);
    }

    public void getCityFragment(Map<String, String> map) {
        new AsyncHttpRequest(ft, Constant.REQUESTS.getCity, Constant.POST_TYPE.POST, map);
    }

    public void getDesigner(Map<String, String> map) {
        new AsyncHttpRequest(ft, Constant.REQUESTS.getDesigner, Constant.POST_TYPE.POST, map);
    }

    public void getStylist(Map<String, String> map) {
        new AsyncHttpRequest(ft, Constant.REQUESTS.getStylist, Constant.POST_TYPE.POST, map);
    }

    public void getLike(Map<String, String> map) {
        new AsyncHttpRequest(ft, Constant.REQUESTS.getLike, Constant.POST_TYPE.POST, map);
    }

    public void testLogin(Map<String, String> map) {
        new AsyncHttpRequest(ct, Constant.REQUESTS.testLogin, Constant.POST_TYPE.POST, map);
    }

    public void isAllowNotification(Map<String, String> map) {
        new AsyncHttpRequest(ft, Constant.REQUESTS.allowNotification, Constant.POST_TYPE.POST, map);
    }

    public void isPrivacy(Map<String, String> map) {
        new AsyncHttpRequest(ft, Constant.REQUESTS.isPrivacy, Constant.POST_TYPE.POST, map);
    }

    public void moveToCloset(Map<String, String> map) {
        new AsyncHttpRequest(ft, Constant.REQUESTS.moveTocloset, Constant.POST_TYPE.POST_WITH_IMAGE, map);
    }
    public void moveToClosetusingUrl(Map<String, String> map) {
        new AsyncHttpRequest(ft, Constant.REQUESTS.moveTocloset, Constant.POST_TYPE.POST, map);
    }

    public void moveToClosetAct(Map<String, String> map) {
        new AsyncHttpRequest(ct, Constant.REQUESTS.moveTocloset, Constant.POST_TYPE.POST_WITH_IMAGE, map);
    }

    public void addToVisionBoard(Map<String, String> map) {
        new AsyncHttpRequest(ft, Constant.REQUESTS.addToVisionBoard, Constant.POST_TYPE.POST_WITH_IMAGE, map);
    }
    public void moveimageclosettovision(Map<String, String> map) {
        new AsyncHttpRequest(ft, Constant.REQUESTS.addToVisionBoard, Constant.POST_TYPE.POST, map);
    }

    public void getVisionBoardList(Map<String, String> map) {
        new AsyncHttpRequest(ft, Constant.REQUESTS.getVisionBoardList, Constant.POST_TYPE.POST_WITH_IMAGE, map);
    }

    public void getVisionBoardListActivity(Map<String, String> map) {
        new AsyncHttpRequest(ct, Constant.REQUESTS.getVisionBoardList, Constant.POST_TYPE.POST_WITH_IMAGE, map);
    }
}
