package com.plus.me.activity;

import android.graphics.PorterDuff;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.github.siyamed.shapeimageview.CircularImageView;
import com.plus.me.R;
import com.plus.me.model.MyFeeds;
import com.plus.me.others.App;
import com.plus.me.others.PicassoTrustAll;
import com.plus.me.utils.UtilFunctions;
import com.plus.me.ws.AsyncTaskListner;
import com.plus.me.ws.CallRequest;
import com.plus.me.ws.Constant;
import com.plus.me.ws.MyConstants;
import com.plus.me.ws.Utils;
import com.squareup.picasso.Callback;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class ReportAnIssue extends AppCompatActivity implements AsyncTaskListner {
    public Toolbar toolbar;

    public CircularImageView ivUser;
    public TextView tvTitle, tvUserName, tvTime, tvDescription, tvCommentCount;
    public ImageView ivImage;
    public ProgressBar progressBar;
    public MyFeeds myFeeds;
    Button btn_submit;
    EditText edt_issue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_report_an_issue);

        myFeeds = (MyFeeds) getIntent().getSerializableExtra("data");

        toolbar = findViewById(R.id.toolbar);
        TextView tv_title = toolbar.findViewById(R.id.tv_title);
        tv_title.setText("Report An Issue");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.getNavigationIcon().setColorFilter(getResources().getColor(R.color.black), PorterDuff.Mode.SRC_ATOP);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        ivImage = findViewById(R.id.ivImage);
        ivUser = findViewById(R.id.ivUser);
        tvTitle = findViewById(R.id.tvTitle);
        tvUserName = findViewById(R.id.tvUserName);
        tvTime = findViewById(R.id.tvTime);
        tvDescription = findViewById(R.id.tvDescription);
        progressBar = findViewById(R.id.progressBar);
        btn_submit = findViewById(R.id.btn_submit);
        edt_issue = findViewById(R.id.edt_issue);

        if (myFeeds != null) {
            PicassoTrustAll.getInstance(ReportAnIssue.this)
                    .load(myFeeds.getPostImage())
                    .into(ivImage, new Callback() {
                        @Override
                        public void onSuccess() {
                            progressBar.setVisibility(View.GONE);
                        }

                        @Override
                        public void onError() {
                        }
                    });

            PicassoTrustAll.getInstance(ReportAnIssue.this)
                    .load(myFeeds.getProfileImage())
                    .placeholder(R.drawable.no_image)
                    .error(R.drawable.no_image)
                    .into(ivUser, new Callback() {
                        @Override
                        public void onSuccess() {
                        }

                        @Override
                        public void onError() {
                        }
                    });
            tvTitle.setText(UtilFunctions.convertUTF8ToString(myFeeds.getPostedByName()));
            tvUserName.setText(myFeeds.getRegisterUserTypeByName());
            tvDescription.setText(myFeeds.getPostTitle());
            Utils.getDatesDifferenceInDays(myFeeds.getPostTime(), tvTime);

            btn_submit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Report();
                }
            });
        }
    }

    private void Report() {

        if (TextUtils.isEmpty(edt_issue.getText().toString().trim())) {
            edt_issue.requestFocus();
            edt_issue.setError("Enter Issue");
        } else {
            Map<String, String> map = new HashMap<>();
            map.put("url", MyConstants.BASE_URL + "BlogReport");
            map.put("Message", edt_issue.getText().toString().trim());
            map.put("BlogID", myFeeds.getBlogId());
            map.put("header", "");
            map.put("Auth", App.user.getAuth());

            new CallRequest(ReportAnIssue.this).ReportSubmit(map);
        }
    }


    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        try {

            Utils.hideProgressDialog();
            if (result != null && !result.isEmpty()) {
                Log.i("TAG", "TAG Result : " + result);

                switch (request) {
                    case Report:
                        try {
                            JSONObject object = new JSONObject(result);
                            boolean success = object.getBoolean("success");
                            if (success) {
                                onBackPressed();
                                Toast.makeText(ReportAnIssue.this, object.getString("message"), Toast.LENGTH_SHORT).show();

                            } else {
                                Toast.makeText(ReportAnIssue.this, object.getString("message"), Toast.LENGTH_SHORT).show();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    public void onProgressUpdate(String uniqueMessageId, int progres) {

    }

    @Override
    public void onProgressComplete(String uniqueMessageId, String result, Constant.REQUESTS request) {

    }
}
