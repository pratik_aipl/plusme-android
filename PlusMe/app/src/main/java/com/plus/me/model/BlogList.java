package com.plus.me.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Krupa Kakkad on 04 July 2018
 */
public class BlogList implements Serializable {
      public String BlogId="";
            public String PostedByName="";
            public String PostImage="";
            public String ProfileImage="";
            public String PostTime="";
            public String BlogTitle="";
            public String Description="";
            public String TotalComment="";
            public String TotalLike="";
            public String CompanyName="";
            public String RegisterUserTypeByName="";
            public String BlogByID="";
    public String CreateDate="";   public String PostedByID="";

    public String getCreateDate() {
        return CreateDate;
    }

    public void setCreateDate(String createDate) {
        CreateDate = createDate;
    }

    public String getPostedByID() {
        return PostedByID;
    }

    public void setPostedByID(String postedByID) {
        PostedByID = postedByID;
    }

    public String getBlogId() {
        return BlogId;
    }

    public void setBlogId(String blogId) {
        BlogId = blogId;
    }

    public String getPostedByName() {
        return PostedByName;
    }

    public void setPostedByName(String postedByName) {
        PostedByName = postedByName;
    }

    public String getPostImage() {
        return PostImage;
    }

    public void setPostImage(String postImage) {
        PostImage = postImage;
    }

    public String getProfileImage() {
        return ProfileImage;
    }

    public void setProfileImage(String profileImage) {
        ProfileImage = profileImage;
    }

    public String getPostTime() {
        return PostTime;
    }

    public void setPostTime(String postTime) {
        PostTime = postTime;
    }

    public String getBlogTitle() {
        return BlogTitle;
    }

    public void setBlogTitle(String blogTitle) {
        BlogTitle = blogTitle;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public String getTotalComment() {
        return TotalComment;
    }

    public void setTotalComment(String totalComment) {
        TotalComment = totalComment;
    }

    public String getTotalLike() {
        return TotalLike;
    }

    public void setTotalLike(String totalLike) {
        TotalLike = totalLike;
    }

    public String getCompanyName() {
        return CompanyName;
    }

    public void setCompanyName(String companyName) {
        CompanyName = companyName;
    }

    public String getRegisterUserTypeByName() {
        return RegisterUserTypeByName;
    }

    public void setRegisterUserTypeByName(String registerUserTypeByName) {
        RegisterUserTypeByName = registerUserTypeByName;
    }

    public String getBlogByID() {
        return BlogByID;
    }

    public void setBlogByID(String blogByID) {
        BlogByID = blogByID;
    }

    public ArrayList<BlogComment> commentList = new ArrayList<BlogComment>();
}