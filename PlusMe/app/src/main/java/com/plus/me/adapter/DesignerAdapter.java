package com.plus.me.adapter;

import android.content.Context;
import android.os.Bundle;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.plus.me.R;
import com.plus.me.activity.HomeActivity;
import com.plus.me.fragments.ProfileFragment;
import com.plus.me.model.Designer;
import com.plus.me.others.PicassoTrustAll;
import com.squareup.picasso.Callback;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class DesignerAdapter extends RecyclerView.Adapter<DesignerAdapter.MyViewHolder> {

    Context mContext;
    ArrayList<Designer> designerArrayList;
    private List<Designer> designerFilterList = new ArrayList<Designer>();
    public DesignerAdapter(Context mContext, ArrayList<Designer> designerArrayList) {
        this.mContext = mContext;
        this.designerArrayList = designerArrayList;
        this.designerFilterList.addAll(designerArrayList);
        setHasStableIds(true);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public DesignerAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_designer, parent, false);

        return new DesignerAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final DesignerAdapter.MyViewHolder holder, final int position) {
        final Designer designer = designerArrayList.get(position);
        holder.txtCity.setText(designer.getCityName());
        holder.tvName.setText(designer.getFirstName() + " " + designer.getLastName());
        holder.txtProfetion.setText(designer.getRegisterUserTypeByName());
        try {
            PicassoTrustAll.getInstance(mContext)
                    .load(designer.getUserProfileImage())
                    .placeholder(R.drawable.avatar)
                    .error(R.drawable.avatar)
                    .into(holder.ivImage, new Callback() {
                        @Override
                        public void onSuccess() {
                        }

                        @Override
                        public void onError() {
                        }
                    });
        } catch (Exception e) {
            e.printStackTrace();
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ProfileFragment profileFragment=new ProfileFragment();
                Bundle bundle=new Bundle();
                bundle.putString("intent","profile");
                bundle.putString("id",designer.getId());
                profileFragment.setArguments(bundle);
                HomeActivity.replaceFragment(profileFragment,false);
            }
        });
    }
//    public void filters(String charText) {
//        charText = charText.toLowerCase(Locale.getDefault());
//        designerArrayList.clear();
//        if (charText.length() == 0) {
//            designerArrayList.addAll(designerFilterList);
//        } else {
//            for (Designer bean : designerFilterList) {
//                String Contanint = bean.getCompanyName();
//                if (Contanint.toLowerCase(Locale.getDefault())
//                        .contains(charText)) {
//                    designerArrayList.add(bean);
//                }
//            }
//        }
//        notifyDataSetChanged();
//    }

    public void filters(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());
        designerArrayList.clear();
        if (charText.length() == 0) {
            designerArrayList.addAll(designerFilterList);
        } else {
            for (Designer bean : designerFilterList) {
                String Contanint = bean.getFirstName()+" "+bean.getLastName();
                if (Contanint.toLowerCase(Locale.getDefault())
                        .contains(charText)) {
                    designerArrayList.add(bean);
                }
            }
        }
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return designerArrayList.size();
    }

    public Designer getItem(int position) {
        return designerArrayList.get(position);
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        public ImageView ivImage;
        public TextView tvName, txtProfetion,txtCity;

        public MyViewHolder(View itemView) {
            super(itemView);
            tvName = itemView.findViewById(R.id.txtName);
            ivImage = itemView.findViewById(R.id.ivProfile);
            txtProfetion = itemView.findViewById(R.id.txtProfetion);
            txtCity=itemView.findViewById(R.id.txtCity);
        }
    }
}
