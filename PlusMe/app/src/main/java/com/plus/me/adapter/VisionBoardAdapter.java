package com.plus.me.adapter;

import android.content.Context;

import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.plus.me.R;
import com.plus.me.fragments.VisionBoardLatestFragment;
import com.plus.me.listener.GetItemPosition;
import com.plus.me.model.VisionBoard;
import com.plus.me.others.PicassoTrustAll;
import com.squareup.picasso.Callback;

import java.util.ArrayList;
import java.util.List;

public class VisionBoardAdapter extends RecyclerView.Adapter<VisionBoardAdapter.MyViewHolder> {

    Context mContext;
    LayoutInflater inflater;
    ArrayList<VisionBoard> categoryList;
    private List<VisionBoard> categoryFilterList = new ArrayList<>();
    GetItemPosition getItemPosition;
    public VisionBoardAdapter(Context mContext, ArrayList<VisionBoard> categoryList, VisionBoardLatestFragment visionBoardLatestFragment) {
        this.mContext = mContext;
        this.categoryList = categoryList;
        this.getItemPosition = visionBoardLatestFragment;
        this.categoryFilterList.addAll(categoryList);
        setHasStableIds(true);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public VisionBoardAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.dialog_view_image_visionboard, parent, false);

        return new VisionBoardAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final VisionBoardAdapter.MyViewHolder holder, final int position) {
        VisionBoard visionBoard = categoryList.get(position);
      //  Picasso.with(mContext.getApplicationContext()).load(categoryList.get(position).getPostImage()).into(holder.ivImage);

        try {
            PicassoTrustAll.getInstance(mContext)
                    .load(visionBoard.getPostImage())
                    .into(holder.ivImage, new Callback() {
                        @Override
                        public void onSuccess() {

                            holder.progressBar.setVisibility(View.GONE);
                        }

                        @Override
                        public void onError() {
                        }
                    });
        } catch (Exception e) {
            e.printStackTrace();
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getItemPosition.onIteamPos(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return categoryList.size();
    }

    public VisionBoard getItem(int position) {
        return categoryList.get(position);
    }



    public class MyViewHolder extends RecyclerView.ViewHolder {

        public ImageView ivImage;
        public ProgressBar progressBar;

        public MyViewHolder(View itemView) {
            super(itemView);
            ivImage = itemView.findViewById(R.id.ivImage);
            progressBar = itemView.findViewById(R.id.progressBar);
        }
    }
}
