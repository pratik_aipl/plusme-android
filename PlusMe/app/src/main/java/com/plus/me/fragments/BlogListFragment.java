package com.plus.me.fragments;


import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import androidx.fragment.app.Fragment;
import androidx.core.view.MenuItemCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.SearchView;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.plus.me.R;
import com.plus.me.activity.HomeActivity;
import com.plus.me.adapter.BlogListAdapter;
import com.plus.me.adapter.CatagoryPopupAdapter;
import com.plus.me.listener.BlogListener;
import com.plus.me.listener.CatagoriesPopupListener;
import com.plus.me.listener.DeleteBlogListener;
import com.plus.me.model.BlogComment;
import com.plus.me.model.BlogList;
import com.plus.me.others.App;
import com.plus.me.others.Internet;
import com.plus.me.ws.AsyncTaskListner;
import com.plus.me.ws.CallRequest;
import com.plus.me.ws.Constant;
import com.plus.me.ws.JsonParserUniversal;
import com.plus.me.ws.MyConstants;
import com.plus.me.ws.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static com.plus.me.activity.HomeActivity.ivDown;
import static com.plus.me.activity.HomeActivity.replaceFragment;
import static com.plus.me.activity.HomeActivity.setupBadge;
import static com.plus.me.activity.HomeActivity.toolbar;
import static com.plus.me.activity.HomeActivity.txtSave;

/**
 * A simple {@link Fragment} subclass.
 */
public class BlogListFragment extends Fragment implements AsyncTaskListner, CatagoriesPopupListener, BlogListener, DeleteBlogListener {

    public JsonParserUniversal jParser;
    public SearchView searchView;
    public String categoryId = "";
    public TextView txtNoValue;
    private RecyclerView rvHome, rvCatagoryFilter;
    private BlogListAdapter homeAdapter;
    private CatagoryPopupAdapter categoriesAdapter;
    private ArrayList<BlogList> blogLists = new ArrayList<>();
    private PopupWindow popup;
    public int position = 0;
    public FloatingActionButton fabAdd;
    public String comment, intent, id;

    public BlogListFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_blog_list, container, false);
        //tabs.setVisibility(View.VISIBLE);

        intent = getArguments().getString("intent");
        if (intent.equalsIgnoreCase("blog")) {
            if (getArguments() != null) {
                id = getArguments().getString("user");
                System.out.println("id====" + id);
            }
        }


        setHasOptionsMenu(true);

        bindWidgetReference(view);

        initializeData(view);
        fabAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                replaceFragment(new AddBlogFragment(), false);
            }
        });
        // TODO comment addData(); and uncomment getBlogList();
        getBlogList();
        return view;
    }

    private void initializeData(View view) {
        App.isHomeFragment = true;
        toolbar.setClickable(true);
        if (App.isHomeFragment) {
            ivDown.setVisibility(View.GONE);
        }

        blogLists.clear();
        jParser = new JsonParserUniversal();

        toolbar.setText("Blogs");

        //setActionSpinner(getActivity(), view);

        if (!TextUtils.isEmpty(MyConstants.categoryId)) {
            categoryId = MyConstants.categoryId;
            MyConstants.categoryId = "";
            toolbar.setText(MyConstants.categoryName);
            MyConstants.categoryName = "";
        } else {
            categoryId = "";
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        //super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
        inflater.inflate(R.menu.home, menu);
        searchView = (SearchView) MenuItemCompat.getActionView(menu.findItem(R.id.item_search));
        EditText searchEditText = (EditText) searchView.findViewById(R.id.search_src_text);
        searchEditText.setTextColor(getResources().getColor(R.color.black));


        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                if (TextUtils.isEmpty(newText)) {
                    homeAdapter.filters("");
                } else {
                    homeAdapter.filters(newText);

                }
                return true;
            }
        });


        final MenuItem menuItem = menu.findItem(R.id.item_notification);

        View actionView = MenuItemCompat.getActionView(menuItem);
        HomeActivity.tvNotificationItemCount = actionView.findViewById(R.id.notification_badge);
        FrameLayout frameNotification = actionView.findViewById(R.id.frameNotification);
        frameNotification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println("Notification====");
                replaceFragment(new NotificationFragment(), false);
            }
        });
        setupBadge();
        super.onCreateOptionsMenu(menu, inflater);
    }

    private void setActionSpinner(final Activity context, View v) {
        toolbar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int[] location = new int[2];
                v.getLocationOnScreen(location);
                int x = location[0];
                int y = location[1];
                RelativeLayout viewGroup = context.findViewById(R.id.rlMain);
                LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                View layout = layoutInflater.inflate(R.layout.popup_action_filter, viewGroup);
                popup = new PopupWindow(context);
                popup.setContentView(layout);
                popup.setWidth(LinearLayout.LayoutParams.WRAP_CONTENT);
                popup.setHeight(LinearLayout.LayoutParams.WRAP_CONTENT);
                popup.setFocusable(true);
                popup.setBackgroundDrawable(new BitmapDrawable());
                popup.showAtLocation(layout, Gravity.NO_GRAVITY, x, y);
                rvCatagoryFilter = layout.findViewById(R.id.rvCatagory);
                RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
                rvCatagoryFilter.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
                rvCatagoryFilter.setLayoutManager(mLayoutManager);

                categoriesAdapter = new CatagoryPopupAdapter(getActivity(), MyConstants.categoriesList, BlogListFragment.this);
                rvCatagoryFilter.setHasFixedSize(true);
                rvCatagoryFilter.setDrawingCacheEnabled(true);
                //rvCompleteChat.addItemDecoration(new DividerItemDecoration(getActivity(), R.drawable.divider));
                rvCatagoryFilter.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
                rvCatagoryFilter.setAdapter(categoriesAdapter);
            }
        });

        ivDown.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toolbar.performClick();
            }
        });
    }

    private void bindWidgetReference(View view) {

        fabAdd  =view.findViewById(R.id.fabAdd);
        rvHome = view.findViewById(R.id.rvHome);
        txtNoValue = view.findViewById(R.id.txtNoValue);
    }

    private void addData() {
//        for (int i = 0; i < 3; i++) {
//            BlogList blogList = new BlogList();
//            blogList.setTotalComment("31 Comments");
//            blogList.setPostDescription("Lorem Ipsum is simply dummy text of the printing and typesetting industry.");
//            blogList.setPostTime("1 hour ago");
//            blogList.setTitle("Fashion Hub");
//            blogList.setPostedByName("by Adam Clark");
//            blogList.setPostImageURL("http://urbanmediatoday.com/wp-content/uploads/2016/07/fashion-world-768x371.jpg");
//            blogList.setUserProfileImage("http://urbanmediatoday.com/wp-content/uploads/2016/07/fashion-world-768x371.jpg");
//
//            blogLists.add(blogList);
//        }
    }

    private void getBlogList() {
        Map<String, String> map = new HashMap<>();
        map.put("url", MyConstants.BASE_URL + "UserBlogsListWithDetail");
        // map.put("CategoryID", categoryId); // TODO add category id
        map.put("header", "");
        map.put("Auth", App.user.getAuth());
        map.put("PostedByID", id);
        if (!Internet.isAvailable(getActivity())) {
            Internet.showAlertDialog(getActivity(), "Error!", "No Internet Connection", false);
        } else {
            new CallRequest(BlogListFragment.this).getBlogList(map);
        }
    }

    private void setData() {
        System.out.println("Blog list size==" + blogLists.size());
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        rvHome.setLayoutManager(layoutManager);
        homeAdapter = new BlogListAdapter(getActivity(), blogLists, BlogListFragment.this);
        rvHome.setHasFixedSize(true);
        rvHome.setAdapter(homeAdapter);

        if (blogLists.size() == 0) {
            txtNoValue.setVisibility(View.VISIBLE);
        } else {
            txtNoValue.setVisibility(View.GONE);
        }
        rvHome.getRecycledViewPool().clear();
        homeAdapter.notifyDataSetChanged();
        rvHome.scrollToPosition(position);
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        txtSave.setVisibility(View.GONE);
        menu.getItem(0).setVisible(true);
        menu.getItem(1).setVisible(false);
        menu.getItem(2).setVisible(false);
        menu.getItem(3).setVisible(false);
        menu.getItem(4).setVisible(false);

        super.onPrepareOptionsMenu(menu);
    }



    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        try {

            Utils.hideProgressDialog();
            if (result != null && !result.isEmpty()) {
                Log.i("TAG", "TAG Result : " + result);

                switch (request) {
                    case getBlogList:
                        blogLists.clear();
                        try {
                            JSONObject object = new JSONObject(result);
                            boolean success = object.getBoolean("success");
                            if (success) {
                                // TODO parse JSON
                                JSONArray data = object.getJSONArray("data");
                                if (data != null && data.length() > 0) {
                                    for (int i = 0; i < data.length(); i++) {
                                        JSONObject jsonObject = data.getJSONObject(i);
                                        BlogList blogList = (BlogList) jParser.parseJson(jsonObject, new BlogList());

                                        JSONArray cData = jsonObject.getJSONArray("comments");
                                        if (data != null && cData.length() > 0) {
                                            for (int j = 0; j < cData.length(); j++) {
                                                JSONObject cObject = cData.getJSONObject(j);
                                                BlogComment commetnList = (BlogComment) jParser.parseJson(cObject, new BlogComment());

                                                JSONArray rData = cObject.getJSONArray("reply");
                                                if (rData != null && rData.length() > 0) {
                                                    for (int k = 0; k < rData.length(); k++) {
                                                        JSONObject rObject = rData.getJSONObject(k);
                                                        BlogComment rplyList = (BlogComment) jParser.parseJson(rObject, new BlogComment());

                                                        commetnList.rplyList.add(rplyList);

                                                    }
                                                }
                                                blogList.commentList.add(commetnList);
                                            }
                                        }
                                        blogLists.add(blogList);
                                        setData();
                                    }
                                } else {
                                    //  Utils.showToast(object.getString("message"), getActivity());
                                }
                            } else {
                                String error_string = object.getString("message");
                                Toast.makeText(getActivity(), error_string, Toast.LENGTH_SHORT).show();
                                setData();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;

                    case addToCloset:

                        try {
                            JSONObject object = new JSONObject(result);
                            boolean success = object.getBoolean("success");
                            if (success) {
                                // TODO parse JSON
                                Utils.showToast(object.getString("message"), getActivity());
                                getBlogList();

                            } else {
                                Utils.showToast(object.getString("message"), getActivity());
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;
                    case getLike:

                        try {
                            JSONObject object = new JSONObject(result);
                            boolean success = object.getBoolean("success");
                            if (success) {
                                // TODO parse JSON
                                //Utils.showToast(object.getString("message"), getActivity());
                                getBlogList();

                            } else {
                                Utils.showToast(object.getString("message"), getActivity());
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;
                    case getDeleteBlog:
                        try {
                            JSONObject object = new JSONObject(result);
                            boolean success = object.getBoolean("success");
                            if (success) {
                                blogLists.remove(position);
                                homeAdapter.notifyDataSetChanged();
                                Utils.showToast(object.getString("message"), getActivity());
                            } else {
                                Utils.showToast(object.getString("message"), getActivity());
                            }

                            setData();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    private void getDeleteBlog(String BlogID) {
        Map<String, String> map = new HashMap<>();
        map.put("url", MyConstants.BASE_URL + "RemovePost");
        map.put("header", "");
        map.put("BlogID", BlogID);
        map.put("Auth", App.user.getAuth());

        new CallRequest(BlogListFragment.this).getDeleteBlog(map);
    }
    @Override
    public void onProgressUpdate(String uniqueMessageId, int progres) {

    }

    @Override
    public void onProgressComplete(String uniqueMessageId, String result, Constant.REQUESTS request) {

    }

    @Override
    public void onPopupItemClick(int pos, View v) {
        popup.dismiss();
        categoryId = MyConstants.categoriesList.get(pos).getCategoryID();
        toolbar.setText(MyConstants.categoriesList.get(pos).getCategoryName());
        getBlogList();
    }

    @Override
    public void onClosetClick(int pos) {
        position = pos;
        addToCloset(pos);
    }

    @Override
    public void onLikeClick(int pos) {
        position = pos;
        doLike(pos);
    }

    private void doLike(int pos) {
        String isLike = null;
        if (blogLists.get(pos).getTotalLike().equalsIgnoreCase("0")) {
            isLike = "1";
        } else {
            isLike = "0";
        }
        Map<String, String> map = new HashMap<>();
        map.put("url", MyConstants.BASE_URL + "LikePost");
        map.put("PostId", blogLists.get(pos).getBlogId()); // TODO add category id
        map.put("isLike", isLike);
        map.put("header", "");
        map.put("Auth", App.user.getAuth());


        new CallRequest(BlogListFragment.this).getLike(map);
    }

    private void addToCloset(int pos) {
        Map<String, String> map = new HashMap<>();
        map.put("url", MyConstants.BASE_URL + "AddToVisionBoard");
        map.put("BlogId", blogLists.get(pos).getBlogId()); // TODO add category id
        map.put("header", "");
        map.put("Auth", App.user.getAuth());
        new CallRequest(BlogListFragment.this).addToCloset(map);

    }

    @Override
    public void onDelete(String BlogID, int Pos) {
        getDeleteBlog(BlogID);
        position = Pos;
    }
}
