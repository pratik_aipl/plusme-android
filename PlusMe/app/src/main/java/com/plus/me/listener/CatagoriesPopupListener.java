package com.plus.me.listener;

import android.view.View;

public interface CatagoriesPopupListener {

    public void onPopupItemClick(int pos, View v);
}
