package com.plus.me.model;

public class User {


    public String FirstName = "";
    public String Id = "";
    public String LastName = "";
    public String IsPrivate="";

    public String getIsPrivate() {
        return IsPrivate;
    }

    public void setIsPrivate(String isPrivate) {
        IsPrivate = isPrivate;
    }

    public String IsActive = "";
    public String Email = "";
    public String MobileNo = "";
    public String Password = "";
    public String RegisterUserType = "";
    public String Auth = "";
    public String DeviceToken = "";
    public String Name ="";
    public String isNotificationEnable="";

    public String getFirstName() {
        return FirstName;
    }

    public void setFirstName(String firstName) {
        FirstName = firstName;
    }

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        this.Id = id;
    }

    public String getLastName() {
        return LastName;
    }

    public void setLastName(String lastName) {
        LastName = lastName;
    }

    public String getIsActive() {
        return IsActive;
    }

    public void setIsActive(String isActive) {
        this.IsActive = isActive;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public String getMobileNo() {
        return MobileNo;
    }

    public void setMobileNo(String mobileNo) {
        MobileNo = mobileNo;
    }

    public String getPassword() {
        return Password;
    }

    public void setPassword(String password) {
        Password = password;
    }

    public String getRegisterUserType() {
        return RegisterUserType;
    }

    public void setRegisterUserType(String registerUserType) {
        RegisterUserType = registerUserType;
    }

    public String getAuth() {
        return Auth;
    }

    public void setAuth(String auth) {
        Auth = auth;
    }

    public String getDeviceToken() {
        return DeviceToken;
    }

    public void setDeviceToken(String deviceToken) {
        DeviceToken = deviceToken;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        this.Name = name;
    }

    public String getIsNotificationEnable() {
        return isNotificationEnable;
    }

    public void setIsNotificationEnable(String isNotificationEnable) {
        this.isNotificationEnable = isNotificationEnable;
    }
}