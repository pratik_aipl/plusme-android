package com.plus.me.fragments;


import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.core.view.MenuItemCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.SearchView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.plus.me.R;
import com.plus.me.activity.HomeActivity;
import com.plus.me.adapter.StoresAdapter;
import com.plus.me.model.Stores;
import com.plus.me.others.App;
import com.plus.me.others.Internet;
import com.plus.me.utils.VerticalSpacingDecoration;
import com.plus.me.ws.AsyncTaskListner;
import com.plus.me.ws.CallRequest;
import com.plus.me.ws.Constant;
import com.plus.me.ws.JsonParserUniversal;
import com.plus.me.ws.MyConstants;
import com.plus.me.ws.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static com.plus.me.activity.HomeActivity.ivDown;
import static com.plus.me.activity.HomeActivity.replaceFragment;
import static com.plus.me.activity.HomeActivity.setupBadge;
import static com.plus.me.activity.HomeActivity.toolbar;
import static com.plus.me.activity.HomeActivity.txtSave;

/**
 * A simple {@link Fragment} subclass.
 */
public class StoresFragment extends Fragment implements AsyncTaskListner {

    public RecyclerView rvStores;
    public JsonParserUniversal jParser;
    private StoresAdapter storesAdapter;
    private LinearLayoutManager layoutManager;
    private ArrayList<Stores> storesList = new ArrayList<>();
    public SearchView searchView;

    public StoresFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_stores, container, false);

        setHasOptionsMenu(true);

        bindWidgetReference(view);

        initializeData();


        // TODO comment addData(); and uncomment getData();
        getData();

        // setData();

        return view;
    }

    private void initializeData() {
        toolbar.setText("Stores");
        storesList.clear();
        MyConstants.editImageUrl = "";
        jParser = new JsonParserUniversal();
    }

    private void bindWidgetReference(View view) {
        rvStores = view.findViewById(R.id.rvStore);
    }

    private void getData() {
        Map<String, String> map = new HashMap<>();
        map.put("url", MyConstants.BASE_URL + "GetStoreList");
        map.put("header", "");
        map.put("Auth", App.user.getAuth());

        if (!Internet.isAvailable(getActivity())) {
            Internet.showAlertDialog(getActivity(), "Error!", "No Internet Connection", false);
        } else {
            new CallRequest(StoresFragment.this).getStoreList(map);
        }
    }

    private void setData() {
        layoutManager = new LinearLayoutManager(getActivity());
        rvStores.setLayoutManager(layoutManager);
        storesAdapter = new StoresAdapter(getActivity(), storesList);
        rvStores.setHasFixedSize(true);
        rvStores.addItemDecoration(new VerticalSpacingDecoration(20));
        rvStores.setItemViewCacheSize(20);
        rvStores.setDrawingCacheEnabled(true);
        rvStores.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
        rvStores.setAdapter(storesAdapter);
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        txtSave.setVisibility(View.GONE);
        toolbar.setClickable(false);
        ivDown.setVisibility(View.GONE);
        super.onPrepareOptionsMenu(menu);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
        inflater = getActivity().getMenuInflater();
        inflater.inflate(R.menu.store_menu, menu);

        final MenuItem menuItem = menu.findItem(R.id.item_notification);
        final MenuItem menuadd = menu.findItem(R.id.item_add_new);
        final MenuItem menusearch = menu.findItem(R.id.item_search);
        menuadd.setVisible(true);
        View actionView = MenuItemCompat.getActionView(menuItem);
        HomeActivity.tvNotificationItemCount = actionView.findViewById(R.id.notification_badge);
        FrameLayout frameNotification = actionView.findViewById(R.id.frameNotification);
        frameNotification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                replaceFragment(new NotificationFragment(), false);
            }
        });
        setupBadge();


        searchView = (SearchView) MenuItemCompat.getActionView(menu.findItem(R.id.item_search));
        EditText searchEditText = (EditText) searchView.findViewById(R.id.search_src_text);
        searchEditText.setTextColor(getResources().getColor(R.color.black));
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {


                if (storesList.size() > 0) {
                    Log.i("TAG", "TEXXXT ::-> " + newText);
                    storesAdapter.getFilter().filter(newText);
                }


                return true;
            }
        });


        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

            /*if (id == R.id.item_home) {
                replaceFragment(new HomeFragment(),false);
            } else*/

        if (id == R.id.item_home) {
            replaceFragment(new HomeFragment(), false);
        }
        if (id == R.id.item_add_new) {

            replaceFragment(new AddNewStoreFragment(), false);
            return true;
        } else if (id == R.id.item_notification) {
            System.out.println("******going notification*********");
            replaceFragment(new NotificationFragment(), false);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        try {

            Utils.hideProgressDialog();
            if (result != null && !result.isEmpty()) {
                Log.i("TAG", "TAG Result : " + result);

                switch (request) {
                    case getStoreList:
                        storesList.clear();
                        try {
                            JSONObject object = new JSONObject(result);
                            boolean success = object.getBoolean("success");
                            if (success) {
                                // TODO parse JSON

                                JSONArray data = object.getJSONArray("data");
                                if (data != null && data.length() > 0) {
                                    for (int i = 0; i < data.length(); i++) {
                                        JSONObject jsonObject = data.getJSONObject(i);
//                                        Stores stores = (Stores) jParser.parseJson(jsonObject, new Stores());

                                        Stores stores = new Stores();
                                        stores.setImage(jsonObject.getString("StoreImage"));
                                        stores.setName(jsonObject.getString("StoreName"));
                                        stores.setStoreDescritption(jsonObject.getString("StoreDescritption"));
                                        stores.setStoreAddress(jsonObject.getString("StoreAddress"));
                                        stores.setStoreLatitude(Double.parseDouble(TextUtils.isEmpty(jsonObject.getString("StoreLatitude")) ? "0" : jsonObject.getString("StoreLatitude")));
                                        stores.setStoreLongitude(Double.parseDouble(TextUtils.isEmpty(jsonObject.getString("StoreLongiture")) ? "0" : jsonObject.getString("StoreLongiture")));
                                        stores.setStoreWebLink(jsonObject.getString("StoreWebLink"));
                                        storesList.add(stores);
                                    }
                                } else {
                                    Utils.showToast(object.getString("message"), getActivity());
                                }
                                setData();
                            } else {
                                String error_string = object.getString("message");
                                Toast.makeText(getActivity(), error_string, Toast.LENGTH_SHORT).show();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onProgressUpdate(String uniqueMessageId, int progres) {

    }

    @Override
    public void onProgressComplete(String uniqueMessageId, String result, Constant.REQUESTS request) {

    }
}
