package com.plus.me.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.plus.me.R;
import com.plus.me.activity.GalleryActivityVisionBoard;
import com.plus.me.model.VisionBoard;
import com.plus.me.utils.ImageDownloaderCache;
import com.plus.me.ws.MyConstants;
import com.plus.me.ws.OnLoadMore;
import com.plus.me.ws.Utils;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;

public class GalleryVisionBoardAdapter extends RecyclerView.Adapter<GalleryVisionBoardAdapter.ViewHolder> {
    private static final String TAG = GalleryVisionBoardAdapter.class.getSimpleName();
    private final Context context;
    public ArrayList<Bitmap> bitmapArrayList = new ArrayList<>();
    private ImageDownloaderCache imgDownloader;
    private final OnLoadMore mLoadMoreListener;
    private boolean bSelMultiple = false;
    private ArrayList<VisionBoard> rvItemListSelected = new ArrayList<VisionBoard>();
    private static final int SELECT_MAX = 6;

    public GalleryVisionBoardAdapter(Context context, ArrayList<VisionBoard> items, OnLoadMore listener) {
        this(context, items, listener, false);
    }

    public GalleryVisionBoardAdapter(Context context, ArrayList<VisionBoard> items, OnLoadMore listener, boolean selmulti) {
        this.context = context;

        imgDownloader = new ImageDownloaderCache();
        mLoadMoreListener = listener;
        bSelMultiple = selmulti;


    }

    public static Bitmap getBitmapFromURL(final String src) {

        final Bitmap[] myBitmap = new Bitmap[1];


        Utils.hideProgressDialog();
        return myBitmap[0];

    }

    @Override
    public int getItemCount() {
        return GalleryActivityVisionBoard.adapterList.size();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        //View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.activity_gallery_item, parent, false);
        int layoutId = R.layout.list_gallery_item;
        if (bSelMultiple) {
            layoutId = R.layout.list_gallery_item_multisel;
        }
        //View view = LayoutInflater.from(parent.getContext()).inflate(layoutId, parent, false);
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_gallery_item_multisel, parent, false);
        // set the view's size, margins, paddings and layout parameters

        ViewHolder vh = new ViewHolder(view);
        return vh;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        holder.mItem = GalleryActivityVisionBoard.adapterList.get(position);

        if (holder.mItem.getPostImage() != null && !holder.mItem.getPostImage().isEmpty()) {

            Picasso.with(context).load(holder.mItem.getPostImage()).into(holder.ivGalleryItem);

            holder.ivGalleryItem.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int pos = (Integer) v.getTag();
                    VisionBoard item = GalleryActivityVisionBoard.adapterList.get(pos);
                    if (bSelMultiple) {
                        int itemIdx = rvItemListSelected.indexOf(item);
                        if (itemIdx > -1) {
                            rvItemListSelected.remove(itemIdx);
                            MyConstants.selectedBitmapInString.remove(itemIdx);
                            notifyItemChanged(pos);

                            //update the numbers on the previously selected images
                            for (VisionBoard sgi : rvItemListSelected) {
                                int sgiIdx = GalleryActivityVisionBoard.adapterList.indexOf(sgi);
                                if (sgiIdx > -1) {
                                    notifyItemChanged(sgiIdx);
                                }
                            }
                        } else {
                            if (rvItemListSelected.size() < SELECT_MAX) {
                                rvItemListSelected.add(item);


                                Thread t = new Thread(new Runnable() {
                                    @Override
                                    public void run() {
                                        try {
                                            ((Activity)context).runOnUiThread(new Runnable() {
                                                @Override
                                                public void run() {
                                                    Utils.showProgressDialog(context);
                                                }
                                            });


                                            URL url = new URL(holder.mItem.getPostImage());

                                            holder.bitmap =   BitmapFactory.decodeStream((InputStream)url.getContent());;
                                            MyConstants.selectedBitmapInString.add(BitMapToString(holder.bitmap));
                                        } catch (IOException e) {
                                            // Log exception
                                            holder.mItem.bitmap  = null;
                                            MyConstants.selectedBitmapInString.add(BitMapToString(holder.bitmap));
                                        }
                                        ((Activity)context).runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                Utils.hideProgressDialog();
                                            }
                                        });

                                    }
                                });
                                t.start();

                                notifyItemChanged(pos);
                            }
                        }
                    } else {
                        Intent data = new Intent();
                        data.putExtra("image_path", MyConstants.selectedBitmapInString);
                        data.putExtra("isBitmap", "");
                        //  data.putExtra("BitmapImageList",MyConstants. selectedBitmap);
                        getActivity().setResult(getActivity().RESULT_OK, data);
                        getActivity().finish();
                    }
                }
            });
            if (bSelMultiple) {
                holder.ivGalleryItem.setOnLongClickListener(ivGalleryItem_OnLongClickListener);
            }
            holder.ivGalleryItem.setTag(position);

            holder.rlSelectedBorder.setVisibility(View.GONE);
            if (bSelMultiple) {
                int itemIdx = rvItemListSelected.indexOf(holder.mItem);
                if (itemIdx > -1) {
                    holder.tvSelNum.setText(String.valueOf(itemIdx + 1));
                    holder.rlSelectedBorder.setVisibility(View.VISIBLE);
                }
            }

        }

        //check for last item and load more if displayed
        if ((position >= getItemCount() - 1)) {
            // mLoadMoreListener.onLoadMore();
        }

    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        public VisionBoard mItem;
        public final View mView;
        public final ImageView ivGalleryItem;
        public Bitmap bitmap;
        public final RelativeLayout rlSelectedBorder;
        public final TextView tvSelNum;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            ivGalleryItem = (ImageView) view.findViewById(R.id.ivGalleryItem);
            rlSelectedBorder = (RelativeLayout) view.findViewById(R.id.rlSelectedBorder);
            tvSelNum = (TextView) view.findViewById(R.id.tvSelNum);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + mItem.toString() + "'";
        }
    }

    public AppCompatActivity getActivity() {
        return (AppCompatActivity) context;
    }

    public byte[] BitMapToString(Bitmap bitmap) {
        System.out.println("bitmap===" + bitmap);
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] b = baos.toByteArray();

        return b;
    }

    private View.OnClickListener ivGalleryItem_OnClickListener = new View.OnClickListener() {

        public void onClick(View v) {


        }
    };

    private View.OnLongClickListener ivGalleryItem_OnLongClickListener = new View.OnLongClickListener() {
        public boolean onLongClick(View v) {
            int pos = (Integer) v.getTag();
            VisionBoard item = GalleryActivityVisionBoard.adapterList.get(pos);
            if (item != null) {
                Intent data = new Intent();
                data.putExtra("image_path", item.getPostImage());
                getActivity().setResult(getActivity().RESULT_OK, data);
                getActivity().finish();
                return true;
            }
            return false;
        }
    };


}
