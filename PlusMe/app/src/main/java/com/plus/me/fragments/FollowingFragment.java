package com.plus.me.fragments;


import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.plus.me.R;
import com.plus.me.adapter.FollowingAdapter;
import com.plus.me.model.Following;
import com.plus.me.others.App;
import com.plus.me.utils.VerticalSpacingDecoration;
import com.plus.me.ws.AsyncTaskListner;
import com.plus.me.ws.CallRequest;
import com.plus.me.ws.Constant;
import com.plus.me.ws.JsonParserUniversal;
import com.plus.me.ws.MyConstants;
import com.plus.me.ws.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


/**
 * A simple {@link Fragment} subclass.
 */
public class FollowingFragment extends Fragment implements AsyncTaskListner {

    public JsonParserUniversal jParser;
    private RecyclerView rvFollowers;
    private TextView txtNoValue;
    private GridLayoutManager layoutManager;
    private ArrayList<Following> followingList = new ArrayList<>();
    private FollowingAdapter followingAdapter;

    public FollowingFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_following, container, false);

        setHasOptionsMenu(true);

        bindWidgetReference(view);



        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        initializeData();

        getFollowing();
    }

    private void initializeData() {
        jParser = new JsonParserUniversal();
    }

    private void getFollowing() {
        Map<String, String> map = new HashMap<>();
        map.put("url", MyConstants.BASE_URL + "get_following");
        map.put("header", "");
        if(MyConstants.isUserProfile){
            map.put("UserProfileId", MyConstants.otherUserId);
        }else {
            map.put("UserProfileId", "");
        }
        map.put("Auth", App.user.getAuth());

        new CallRequest(FollowingFragment.this).getMyFollowing(map);
    }

    private void bindWidgetReference(View view) {
        rvFollowers = view.findViewById(R.id.rvFollowing);
        txtNoValue = view.findViewById(R.id.txtNoValue);
    }

    private void setData() {

        layoutManager = new GridLayoutManager(getActivity(), 2, GridLayoutManager.VERTICAL, false); // MAX NUMBER OF SPACES
        rvFollowers.setLayoutManager(layoutManager);

        followingAdapter = new FollowingAdapter(getActivity(), followingList);
        rvFollowers.setHasFixedSize(true);
        rvFollowers.addItemDecoration(new VerticalSpacingDecoration(0));
        rvFollowers.setItemViewCacheSize(20);
        rvFollowers.setDrawingCacheEnabled(true);
        //rvCompleteChat.addItemDecoration(new DividerItemDecoration(getActivity(), R.drawable.divider));
        rvFollowers.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
        rvFollowers.setAdapter(followingAdapter);

        if(followingList.size()==0){
            txtNoValue.setVisibility(View.VISIBLE);
        }else {
            txtNoValue.setVisibility(View.GONE);
        }
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        try {

            Utils.hideProgressDialog();
            if (result != null && !result.isEmpty()) {
                Log.i("TAG", "TAG Result : " + result);

                switch (request) {
                    case getMyFollowing:
                        followingList.clear();
                        try {
                            JSONObject object = new JSONObject(result);
                            boolean success = object.getBoolean("success");
                            if (success) {
                                // TODO parse JSON
                                JSONArray array = object.getJSONArray("data");

                                for (int i = 0; i < array.length(); i++) {
                                    JSONObject jsonObject = array.getJSONObject(i);

                                    Following following = (Following) jParser.parseJson(jsonObject, new Following());

                                    followingList.add(following);
                                }

                            } else {
                                String error_string = object.getString("message");
                                //Utils.showToast(error_string, getActivity());
                            }

                            setData();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onProgressUpdate(String uniqueMessageId, int progres) {

    }

    @Override
    public void onProgressComplete(String uniqueMessageId, String result, Constant.REQUESTS request) {

    }
}
