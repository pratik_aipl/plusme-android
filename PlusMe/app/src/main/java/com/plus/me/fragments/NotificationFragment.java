package com.plus.me.fragments;


import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.core.view.MenuItemCompat;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.SearchView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.plus.me.R;
import com.plus.me.activity.HomeActivity;
import com.plus.me.adapter.NotificationAdapter;
import com.plus.me.model.Notification;
import com.plus.me.others.App;
import com.plus.me.others.Internet;
import com.plus.me.ws.AsyncTaskListner;
import com.plus.me.ws.CallRequest;
import com.plus.me.ws.Constant;
import com.plus.me.ws.JsonParserUniversal;
import com.plus.me.ws.MyConstants;
import com.plus.me.ws.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static com.plus.me.activity.HomeActivity.toolbar;
import static com.plus.me.activity.HomeActivity.txtSave;

/**
 * A simple {@link Fragment} subclass.
 */
public class NotificationFragment extends Fragment implements AsyncTaskListner{
    public RecyclerView rvNotification;
    private NotificationAdapter notificationAdapter;
    private LinearLayoutManager layoutManager;
    public JsonParserUniversal jParser;
    private ArrayList<Notification> notificationList = new ArrayList<>();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_notification, container, false);

        setHasOptionsMenu(true);

        toolbar.setText("Notifications");

        bindWidgetReference(view);

        notificationList.clear();
        jParser = new JsonParserUniversal();

       // addData();
        // TODO comment addData(); and uncomment getData();
        getData();
        return view;
    }

    private void addData() {
        for (int i = 0; i < 3; i++) {
            Notification notification = new Notification();
//            notification.setNotifictionDescription("Lorem Ipsum is simply dummy text of the printing and typesetting industry.");
//            notification.setDate("December 20, 2017");
            notificationList.add(notification);
        }
    }

    private void getData() {
        Map<String, String> map = new HashMap<>();
        map.put("url", MyConstants.BASE_URL + "GetNotificationList");
        map.put("header", "");
        map.put("Auth", App.user.getAuth());
        if (!Internet.isAvailable(getActivity())) {
            Internet.showAlertDialog(getActivity(), "Error!", "No Internet Connection", false);
        } else {
            new CallRequest(NotificationFragment.this).getNotificationList(map);
        }
    }

    private void setData() {
        layoutManager = new LinearLayoutManager(getActivity());
        rvNotification.setLayoutManager(layoutManager);
        notificationAdapter = new NotificationAdapter(getActivity(), notificationList);
        rvNotification.setHasFixedSize(true);
        rvNotification.setItemViewCacheSize(20);
        rvNotification.setDrawingCacheEnabled(true);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(rvNotification.getContext(), layoutManager.getOrientation());
        rvNotification.addItemDecoration(dividerItemDecoration);
        rvNotification.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
        rvNotification.setAdapter(notificationAdapter);
    }

    private void bindWidgetReference(View view) {
        rvNotification = view.findViewById(R.id.rvNotification);
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        txtSave.setVisibility(View.GONE);
        menu.getItem(0).setVisible(true);
        menu.getItem(1).setVisible(true);
        menu.getItem(2).setVisible(false);
        menu.getItem(3).setVisible(false);
        menu.getItem(4).setVisible(false);
        super.onPrepareOptionsMenu(menu);
    }
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        //super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
        inflater.inflate(R.menu.home, menu);
        SearchView searchView = (SearchView) MenuItemCompat.getActionView(menu.findItem(R.id.item_search));
        EditText searchEditText = (EditText) searchView.findViewById(R.id.search_src_text);
        searchEditText.setTextColor(getResources().getColor(R.color.black));

        if (notificationAdapter != null) {
            searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                @Override
                public boolean onQueryTextSubmit(String query) {
                    return false;
                }

                @Override
                public boolean onQueryTextChange(String newText) {
                    if (TextUtils.isEmpty(newText)) {
                        notificationAdapter.filters("");
                    } else {
                        notificationAdapter.filters(newText);

                    }
                    return true;
                }
            });
        }
        super.onCreateOptionsMenu(menu, inflater);
    }
    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        try {

            Utils.hideProgressDialog();
            if (result != null && !result.isEmpty()) {
                Log.i("TAG", "TAG Result : " + result);

                switch (request) {
                    case getNotificationList:
                        notificationList.clear();
                        try {
                            JSONObject object = new JSONObject(result);
                            boolean success = object.getBoolean("status");
                            if (success) {
                                // TODO parse JSON

                                JSONArray data = object.getJSONArray("data");
                                if (data != null && data.length() > 0){
                                    for (int i = 0; i < data.length(); i++) {
                                        JSONObject jsonObject = data.getJSONObject(i);

                                        Notification notification = (Notification) jParser.parseJson(jsonObject, new Notification());

                                        notificationList.add(notification);

                                    }
                                    App.mnotificationItemCount=0;
                                    HomeActivity.tvNotificationItemCount.setText("0");
                                    setData();
                                } else {
                                    Utils.showToast(object.getString("message"), getActivity());
                                }

                            } else {
                                String error_string = object.getString("message");
                                Toast.makeText(getActivity(), error_string, Toast.LENGTH_SHORT).show();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onProgressUpdate(String uniqueMessageId, int progres) {

    }

    @Override
    public void onProgressComplete(String uniqueMessageId, String result, Constant.REQUESTS request) {

    }
}
