package com.plus.me.adapter;

import android.content.Context;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.github.siyamed.shapeimageview.CircularImageView;
import com.plus.me.R;
import com.plus.me.activity.HomeActivity;
import com.plus.me.fragments.BlogListFragment;
import com.plus.me.fragments.ProfileFragment;
import com.plus.me.fragments.UsreWiseBlogListFragment;
import com.plus.me.listener.BlogListener;
import com.plus.me.listener.FilterList;
import com.plus.me.model.UserBlog;
import com.plus.me.others.PicassoTrustAll;
import com.plus.me.utils.Utilities;
import com.squareup.picasso.Callback;

import java.util.ArrayList;
import java.util.List;



public class UserBlogListAdapter extends RecyclerView.Adapter<UserBlogListAdapter.MyViewHolder> implements Filterable {

    private static final String TAG = "UserBlogListAdapter";
    public Context mContext;
    public LayoutInflater inflater;
    public List<UserBlog> blogLists;
    public List<UserBlog> blogFilterList = new ArrayList<UserBlog>();

    public BlogListener blogListener;
    public FilterList filterList;
    public UsreWiseBlogListFragment blogListFragment;
    public SubBlogListAdapter adapter;

    public UserBlogListAdapter(Context mContext, ArrayList<UserBlog> blogLists, UsreWiseBlogListFragment blogListFragment) {
        this.mContext = mContext;
        this.blogLists = blogLists;
        this.blogFilterList.addAll(blogLists);
        this.blogListener = blogListFragment;
        this.filterList = blogListFragment;
        this.blogListFragment = blogListFragment;
        setHasStableIds(true);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item__user_bloag, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final UserBlog blogList = blogLists.get(position);

        //String jobModifieDate = blogList.getPostTime();
        //  Utils.getDatesDifferenceInDays(jobModifieDate, holder.tvTime);

        holder.tvTitle.setText(Utilities.convertUTF8ToString(blogList.getCompanyName()));
        holder.tvUserName.setText("by " + blogList.getPostedByName());
        adapter = new SubBlogListAdapter(mContext, blogList.blogArray);

        LinearLayoutManager layoutManager = new LinearLayoutManager(mContext);
        holder.rvcomment.setLayoutManager(layoutManager);
        holder.rvcomment.setHasFixedSize(true);

        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        holder.rvcomment.setAdapter(adapter);

        holder.tv_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                BlogListFragment profileFragment = new BlogListFragment();
                Bundle bundle = new Bundle();
                bundle.putString("intent", "blog");
                bundle.putSerializable("user", getItem(position).getId());
                profileFragment.setArguments(bundle);
                HomeActivity.replaceFragment(profileFragment, false);
            }
        });

        holder.tvTitle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ProfileFragment profileFragment = new ProfileFragment();
                Bundle bundle = new Bundle();
                bundle.putString("intent", "profile");
                bundle.putString("id", blogList.getId());
                profileFragment.setArguments(bundle);
                HomeActivity.replaceFragment(profileFragment, false);
            }
        });


        if (!TextUtils.isEmpty(blogList.getUserProfileImage())) {
            try {
                PicassoTrustAll.getInstance(mContext)
                        .load(blogList.getUserProfileImage())
                        .placeholder(R.drawable.avatar)
                        .error(R.drawable.avatar)
                        .into(holder.ivUser, new Callback() {
                            @Override
                            public void onSuccess() {
                            }

                            @Override
                            public void onError() {

                            }
                        });
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public int getItemCount() {
        return blogLists.size();
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {

                String charString = charSequence.toString();
                String[] filterData = charString.split(",");

                Log.d(TAG, "filterData>: " + filterData[0] + filterData[1] + filterData[2]);

                if (charString.isEmpty()) {
                    blogLists = blogFilterList;
                } else {
                    List<UserBlog> filteredList = new ArrayList<>();

                    for (UserBlog row : blogFilterList) {
                        if (filterData[2].equalsIgnoreCase("ALL")) {
                            if (row.getSearchtittle().toLowerCase().contains(filterData[1].toLowerCase())
                                    || row.getSearchDesc().toLowerCase().contains(filterData[1].toLowerCase())
                                    || row.getPostedByName().toLowerCase().contains(filterData[0].toLowerCase())) {
                                filteredList.add(row);
                            }
                        } else if (filterData[2].equalsIgnoreCase("NAME")) {
                            if (row.getPostedByName().toLowerCase().contains(filterData[0].toLowerCase())) {
                                filteredList.add(row);
                            }
                        } else if (filterData[2].equalsIgnoreCase("TC")) {
                            if (row.getSearchtittle().toLowerCase().contains(filterData[0].toLowerCase())
                                    || row.getSearchDesc().toLowerCase().contains(filterData[0].toLowerCase())
                            ) {
                                filteredList.add(row);
                            }
                        }

                    }

                    blogLists = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = blogLists;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                blogLists = (List<UserBlog>) filterResults.values;
                Log.d(TAG, "publishResultsarray size: " + blogLists.size());
                filterList.onTotalData(blogLists.size());
                notifyDataSetChanged();
            }
        };
    }


    public UserBlog getItem(int position) {
        return blogLists.get(position);
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {

        public CircularImageView ivUser;
        public TextView tvTitle, tvUserName, tv_view, tvDescription;
        public RecyclerView rvcomment;

        public MyViewHolder(View itemView) {
            super(itemView);

            ivUser = itemView.findViewById(R.id.ivUser);
            tvTitle = itemView.findViewById(R.id.tvTitle);
            tvUserName = itemView.findViewById(R.id.tvUserName);
            tv_view = itemView.findViewById(R.id.tv_view);
            tvDescription = itemView.findViewById(R.id.tvDescription);
            rvcomment = itemView.findViewById(R.id.rvcomment);

        }
    }

}
