package com.plus.me.fragments;


import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.core.view.MenuItemCompat;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.SearchView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import com.plus.me.R;
import com.plus.me.activity.HomeActivity;
import com.plus.me.adapter.CategoriesAdapter;
import com.plus.me.listener.CatagoriesPopupListener;
import com.plus.me.model.Categories;
import com.plus.me.others.App;
import com.plus.me.others.Internet;
import com.plus.me.utils.VerticalSpacingDecoration;
import com.plus.me.ws.AsyncTaskListner;
import com.plus.me.ws.CallRequest;
import com.plus.me.ws.Constant;
import com.plus.me.ws.JsonParserUniversal;
import com.plus.me.ws.MyConstants;
import com.plus.me.ws.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static com.plus.me.activity.HomeActivity.ivDown;
import static com.plus.me.activity.HomeActivity.replaceFragment;
import static com.plus.me.activity.HomeActivity.setupBadge;
import static com.plus.me.activity.HomeActivity.toolbar;
import static com.plus.me.activity.HomeActivity.txtSave;

/**
 * A simple {@link Fragment} subclass.
 */
public class CategoryFragment extends Fragment implements AsyncTaskListner,CatagoriesPopupListener{

    public JsonParserUniversal jParser;
    public TextView txtNoValue;
    private RecyclerView rvCategories;
    private GridLayoutManager layoutManager;
    private ArrayList<Categories> categoriesList = new ArrayList<>();
    private CategoriesAdapter categoriesAdapter;
    public SearchView searchView;
    private PopupWindow popup;
    public String categoryId = "";
    // private String[] categoryArray = {"Trendy", "Formal", "Business", "Casual", "Classic", "Swimwere", "Active/Athleisure"};

    public CategoryFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_category, container, false);

        setHasOptionsMenu(true);

        bindWidgetReference(view);

        categoriesList.clear();
        jParser = new JsonParserUniversal();

        if (MyConstants.categoriesList.size() == 0) {
            getData();
        }

        setData();
       // setActionSpinner(getActivity(), view);
        //  ((HomeActivity)getActivity()).setToolbarTitleFragment("Categories");
        toolbar.setText("Categories");
        return view;
    }
//    private void setActionSpinner(final Activity context, View v) {
//        toolbar.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                int[] location = new int[2];
//                v.getLocationOnScreen(location);
//                int x = location[0];
//                int y = location[1];
//                LinearLayout viewGroup = (LinearLayout) context.findViewById(R.id.rlMain);
//                LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//                View layout = layoutInflater.inflate(R.layout.popup_action_filter, viewGroup);
//                popup = new PopupWindow(context);
//                popup.setContentView(layout);
//                popup.setWidth(LinearLayout.LayoutParams.WRAP_CONTENT);
//                popup.setHeight(LinearLayout.LayoutParams.WRAP_CONTENT);
//                popup.setFocusable(true);
//                popup.setBackgroundDrawable(new BitmapDrawable());
//                popup.showAtLocation(layout, Gravity.NO_GRAVITY, x, y);
//                rvCategories = layout.findViewById(R.id.rvCatagory);
//                RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
//                rvCategories.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
//                rvCategories.setLayoutManager(mLayoutManager);
//                categoriesPopupAdapter = new CatagoryPopupAdapter(getActivity(), MyConstants.categoriesList, CategoryFragment.this);
//                rvCategories.setHasFixedSize(true);
//                rvCategories.setDrawingCacheEnabled(true);
//                //rvCompleteChat.addItemDecoration(new DividerItemDecoration(getActivity(), R.drawable.divider));
//                rvCategories.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
//                rvCategories.setAdapter(categoriesPopupAdapter);
//            }
//        });
//
//        ivDown.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                toolbar.performClick();
//            }
//        });
//    }
    @Override
    public void onPrepareOptionsMenu(Menu menu) {
//        App.isHomeFragment = true;
//        toolbar.setClickable(true);
//        if (App.isHomeFragment) {
//            ivDown.setVisibility(View.VISIBLE);
//        }
        txtSave.setVisibility(View.GONE);
        ivDown.setVisibility(View.GONE);
        toolbar.setClickable(false);
        MyConstants.editImageUrl="";
        menu.getItem(0).setVisible(false);
        menu.getItem(1).setVisible(true);
        menu.getItem(2).setVisible(true);
        menu.getItem(3).setVisible(false);
        menu.getItem(4).setVisible(false);

        super.onPrepareOptionsMenu(menu);
    }
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        //super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
        inflater.inflate(R.menu.home, menu);
        searchView = (SearchView) MenuItemCompat.getActionView(menu.findItem(R.id.item_search));
        EditText searchEditText = (EditText) searchView.findViewById(R.id.search_src_text);
        searchEditText.setTextColor(getResources().getColor(R.color.black));


            searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                @Override
                public boolean onQueryTextSubmit(String query) {
                    return false;
                }

                @Override
                public boolean onQueryTextChange(String newText) {
                    if (TextUtils.isEmpty(newText)) {
                        categoriesAdapter.filters("");
                    } else {
                        categoriesAdapter.filters(newText);

                    }
                    return true;
                }
            });


        final MenuItem menuItem = menu.findItem(R.id.item_notification);

        View actionView = MenuItemCompat.getActionView(menuItem);
        HomeActivity.tvNotificationItemCount = actionView.findViewById(R.id.notification_badge);
        FrameLayout frameNotification = actionView.findViewById(R.id.frameNotification);
        frameNotification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println("Notification====");
                replaceFragment(new NotificationFragment(), false);
            }
        });
        setupBadge();
        super.onCreateOptionsMenu(menu, inflater);
    }

    private void bindWidgetReference(View view) {
        rvCategories = view.findViewById(R.id.rvCategories);
        txtNoValue = view.findViewById(R.id.txtNoValue);
    }

    private void getData() {
        Map<String, String> map = new HashMap<>();
        map.put("url", MyConstants.BASE_URL + "GetCategoriesList");
        map.put("UserId", App.user.getId());
        map.put("LoginToken", App.user.getAuth());
       // map.put("CategoryID", categoryId);
        if (!Internet.isAvailable(getActivity())) {
            Internet.showAlertDialog(getActivity(), "Error!", "No Internet Connection", false);
        } else {
            new CallRequest(CategoryFragment.this).getCategoryList(map);
        }
    }

    private void setData() {
        layoutManager = new GridLayoutManager(getActivity(), 6, GridLayoutManager.VERTICAL, false); // MAX NUMBER OF SPACES
        layoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                if (position == 0) {
                    return 2; // ITEMS AT POSITION 0 OCCUPY 3 SPACES
                }
                if (position == 1) {
                    return 2; // ITEMS AT POSITION 0 OCCUPY 3 SPACES
                }
                if (position == 2) {
                    return 2; // ITEMS AT POSITION 0 OCCUPY 3 SPACES
                } else {
                    return 3; // OTHER ITEMS OCCUPY 2 SPACES
                }
            }
        });
        rvCategories.setLayoutManager(layoutManager);

        rvCategories.setHasFixedSize(true);
        rvCategories.addItemDecoration(new VerticalSpacingDecoration(10));
        rvCategories.setItemViewCacheSize(20);
        rvCategories.setDrawingCacheEnabled(true);
        rvCategories.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);

        categoriesAdapter = new CategoriesAdapter(getActivity(), MyConstants.categoriesList);
        rvCategories.setAdapter(categoriesAdapter);

        if (MyConstants.categoriesList.size() == 0) {
            txtNoValue.setVisibility(View.VISIBLE);
        } else {
            txtNoValue.setVisibility(View.GONE);
        }
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        try {

            Utils.hideProgressDialog();
            if (result != null && !result.isEmpty()) {
                Log.i("TAG", "TAG Result : " + result);

                switch (request) {
                    case getCategoryList:
                        categoriesList.clear();
                        try {
                            JSONObject object = new JSONObject(result);
                            boolean success = object.getBoolean("success");
                            if (success) {
                                // TODO parse JSON

                                JSONArray data = object.getJSONArray("data");
                                if (data != null && data.length() > 0) {
                                    for (int i = 0; i < data.length(); i++) {
                                        JSONObject jsonObject = data.getJSONObject(i);
                                        Categories categories = (Categories) jParser.parseJson(jsonObject, new Categories());
                                        categoriesList.add(categories);
                                    }
                                    setData();
                                } else {
                                  //  Utils.showToast(object.getString("message"), getActivity());
                                }

                            } else {
                                String error_string = object.getString("message");
                                Toast.makeText(getActivity(), error_string, Toast.LENGTH_SHORT).show();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onProgressUpdate(String uniqueMessageId, int progres) {

    }

    @Override
    public void onProgressComplete(String uniqueMessageId, String result, Constant.REQUESTS request) {

    }

    @Override
    public void onPopupItemClick(int pos, View v) {
        popup.dismiss();
        categoryId = MyConstants.categoriesList.get(pos).getCategoryID();
        toolbar.setText(MyConstants.categoriesList.get(pos).getCategoryName());
        getData();
    }
}
