package com.plus.me.activity;

import android.content.Intent;
import androidx.core.app.ActivityCompat;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.plus.me.R;
import com.plus.me.utils.MySharedPref;

public class PreLoginActivity extends AppCompatActivity {
    private Button btnJoinNow, btnSignIn;
    private TextView txtExplore;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pre_login);

        initializeData();

        bindWidgetReference();

        bindWidgetEvents();
    }

    private void initializeData() {
        if (MySharedPref.getIsLogin()) {

            Intent intent = new Intent(PreLoginActivity.this, HomeActivity.class);

            startActivity(intent);
            ActivityCompat.finishAffinity(PreLoginActivity.this);
        }
    }

    private void bindWidgetReference() {

        btnJoinNow = findViewById(R.id.btnJoinNow);
        btnSignIn = findViewById(R.id.btnSignIn);
        txtExplore = findViewById(R.id.txtExplore);
    }

    private void bindWidgetEvents() {

        btnJoinNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(PreLoginActivity.this, PreRegisterActivity.class);
                startActivity(intent);
                //finish();
            }
        });

        btnSignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(PreLoginActivity.this, LoginActivity.class);
                startActivity(intent);
                //finish();
            }
        });

        txtExplore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(PreLoginActivity.this, HomeActivity.class);
                startActivity(intent);
            }
        });
    }
}
