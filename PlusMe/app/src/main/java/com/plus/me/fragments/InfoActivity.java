package com.plus.me.fragments;


import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.appcompat.app.AppCompatActivity;

import android.util.Log;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.plus.me.R;
import com.plus.me.model.UserProfile;
import com.plus.me.others.App;
import com.plus.me.ws.AsyncTaskListner;
import com.plus.me.ws.CallRequest;
import com.plus.me.ws.Constant;
import com.plus.me.ws.MyConstants;
import com.plus.me.ws.Utils;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * A simple {@link Fragment} subclass.
 */
public class InfoActivity extends AppCompatActivity implements AsyncTaskListner {
    public View view;
    public TextView txtCompanyName, txtEmail, txtContacts, txtWorkingHours, txtFbLink, txtInstaLink, txtName, txtProfession;
    public UserProfile userProfile;
    public ImageView ivProfile, icBack;
    public LinearLayout lin_private, lin_email_contact;
    Switch swASwitch;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_info);
        bindWidgetReference();
        initializeData();
        setUserInfo();
        onClickListener();


        swASwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    Isprivate("1");
                } else {
                    Isprivate("0");
                }
            }
        });


    }

    private void onClickListener() {
        icBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    private void setUserInfo() {
        if (userProfile.getIsPrivate().equals("1")) {
            if (userProfile.getId().equals(App.user.getId())) {
                lin_private.setVisibility(View.GONE);
                lin_email_contact.setVisibility(View.VISIBLE);
                swASwitch.setVisibility(View.VISIBLE);

            } else {
                lin_private.setVisibility(View.VISIBLE);
                lin_email_contact.setVisibility(View.GONE);
                swASwitch.setVisibility(View.GONE);
            }

        } else {
                 lin_private.setVisibility(View.GONE);
                lin_email_contact.setVisibility(View.VISIBLE);
                swASwitch.setVisibility(View.GONE);


        }
        if (userProfile.getId().equals(App.user.getId())) {
            swASwitch.setVisibility(View.VISIBLE);

        }

            if(userProfile.getIsPrivate().equalsIgnoreCase("1")){
            swASwitch.setChecked(true);
        }else{
            swASwitch.setChecked(false);
        }
        txtCompanyName.setText(userProfile.getCompanyName());
        txtContacts.setText(userProfile.getMobileNo());
        txtEmail.setText(userProfile.getEmail());
        txtWorkingHours.setText(userProfile.getWorkingHours());
        txtFbLink.setText(userProfile.getFacebookLink());
        txtInstaLink.setText(userProfile.getInstragramLink());
        txtName.setText(userProfile.getFirstName() + " " + userProfile.getLastName());
        txtProfession.setText(userProfile.getRegisterUserTypeByName());
        Picasso.with(getApplicationContext()).load(userProfile.getUserProfileImage()).into(ivProfile);
    }

    private void initializeData() {
        Intent i = getIntent();
        Bundle bundle = i.getExtras();
        userProfile = (UserProfile) bundle.getSerializable("userDetail");
    }

    private void bindWidgetReference() {
        txtInstaLink = findViewById(R.id.txtInstaLink);
        txtFbLink = findViewById(R.id.txtFbLink);
        txtWorkingHours = findViewById(R.id.txtWorkingHours);
        txtContacts = findViewById(R.id.txtContacts);
        txtEmail = findViewById(R.id.txtEmail);
        txtCompanyName = findViewById(R.id.txtCompanyName);
        ivProfile = findViewById(R.id.ivProfile);
        txtName = findViewById(R.id.txtName);
        txtProfession = findViewById(R.id.tvProfession);
        icBack = findViewById(R.id.icBack);

        lin_private = findViewById(R.id.lin_private);
        lin_email_contact = findViewById(R.id.lin_email_contact);
        swASwitch=  findViewById(R.id.switch1);


    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    private void Isprivate(String isprivate) {

            Map<String, String> map = new HashMap<>();
            map.put("url", MyConstants.BASE_URL + "is_private");
            map.put("IsPrivate", isprivate);
            map.put("header", "");
            map.put("Auth", App.user.getAuth());

            new CallRequest(InfoActivity.this).Isprivate(map);
        }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        try {

            Utils.hideProgressDialog();
            if (result != null && !result.isEmpty()) {
                Log.i("TAG", "TAG Result : " + result);

                switch (request) {
                    case Isprivate:
                        try {
                            JSONObject object = new JSONObject(result);
                            boolean success = object.getBoolean("success");
                            if (success) {
                                String error_string = object.getString("message");
                                Toast.makeText(this, error_string, Toast.LENGTH_SHORT).show();

                            } else {
                                String error_string = object.getString("message");
                                Toast.makeText(this, error_string, Toast.LENGTH_SHORT).show();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onProgressUpdate(String uniqueMessageId, int progres) {

    }

    @Override
    public void onProgressComplete(String uniqueMessageId, String result, Constant.REQUESTS request) {

    }

}
