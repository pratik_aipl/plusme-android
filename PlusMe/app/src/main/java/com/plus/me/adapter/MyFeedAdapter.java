package com.plus.me.adapter;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.github.siyamed.shapeimageview.CircularImageView;
import com.jsibbold.zoomage.ZoomageView;
import com.plus.me.R;
import com.plus.me.activity.HomeActivity;
import com.plus.me.activity.ReportAnIssue;
import com.plus.me.fragments.MyFeedDetailFragment;
import com.plus.me.fragments.MyFeedsFragment;
import com.plus.me.fragments.PostCategorywiseFragment;
import com.plus.me.fragments.ProfileFragment;
import com.plus.me.listener.MyFeedsListener;
import com.plus.me.model.MyFeeds;
import com.plus.me.others.PicassoTrustAll;
import com.plus.me.utils.Utilities;
import com.plus.me.ws.Utils;
import com.squareup.picasso.Callback;

import java.util.ArrayList;
import java.util.List;

import static com.plus.me.activity.HomeActivity.replaceFragment;

public class MyFeedAdapter extends RecyclerView.Adapter<MyFeedAdapter.MyViewHolder> implements Filterable {
    private static final String TAG = "MyFeedAdapter";
    Context mContext;
    List<MyFeeds> myFeedList;
    MyFeedsListener myFeedsListener;
    private List<MyFeeds> myFeedFilterList = new ArrayList<MyFeeds>();

    public MyFeedAdapter(Context mContext, ArrayList<MyFeeds> myFeedList, MyFeedsFragment myFeedsFragment) {
        this.mContext = mContext;
        this.myFeedList = myFeedList;
        this.myFeedFilterList.addAll(myFeedList);
        this.myFeedsListener = myFeedsFragment;
        setHasStableIds(true);
    }

    public MyFeedAdapter(Context mContext, ArrayList<MyFeeds> myFeedList, PostCategorywiseFragment myFeedsFragment) {
        this.mContext = mContext;
        this.myFeedList = myFeedList;
        this.myFeedFilterList.addAll(myFeedList);
        this.myFeedsListener = myFeedsFragment;
        setHasStableIds(true);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_home, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final MyFeeds myFeeds = myFeedList.get(position);
        holder.tvCommentCount.setText(myFeeds.getTotalComment());
        holder.tvLikeCount.setText(myFeeds.getTotalLike());
        holder.tvDescription.setText(Utilities.convertUTF8ToString(myFeeds.getPostTitle()));
        Utils.getDatesDifferenceInDays(myFeeds.getPostTime(), holder.tvTime);
        holder.tvTitle.setText(Utilities.convertUTF8ToString(myFeeds.getPostedByName()));
        holder.tvUserName.setText(myFeeds.getRegisterUserTypeByName());
        if (myFeeds.getIsCloset().equalsIgnoreCase("0")) {
            holder.ivCloset.setImageResource(R.drawable.closet_gray);
        } else {
            holder.ivCloset.setImageResource(R.drawable.closet_pink);
        }

        if (myFeeds.getIsLike().equalsIgnoreCase("0")) {
            holder.ivLike.setImageResource(R.drawable.ic_dislike);
        } else {
            holder.ivLike.setImageResource(R.drawable.ic_like);
        }
        holder.ivLike.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myFeedsListener.onLikeClick(position);
            }
        });


        holder.ivCloset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

              /*  Map<String, String> map = new HashMap<>();
                map.put("url", MyConstants.BASE_URL + "MoveToCloset");
                map.put("PostImage", myFeeds.getPostImage());
                map.put("Title", myFeeds.getPostTitle());

                map.put("BaseImageID", "");

                map.put("header", "");
                map.put("Auth", App.user.getAuth());
                new CallRequest(mContext).moveToClosetAct(map);*/

                //myFeedsListener.onClosetClick(position);


                if (myFeeds.getIsCloset().equalsIgnoreCase("0")) {
                    myFeedsListener.onClosetClick(position);
                } else {
                    Utils.showToast("This item is already added in my closet", mContext);
                }

            }
        });
        try {
            PicassoTrustAll.getInstance(mContext)
                    .load(myFeeds.getPostImage())
                    .into(holder.ivImage, new Callback() {
                        @Override
                        public void onSuccess() {
                            holder.progressBar.setVisibility(View.GONE);
                        }

                        @Override
                        public void onError() {
                        }
                    });
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            PicassoTrustAll.getInstance(mContext)
                    .load(myFeeds.getProfileImage())
                    .placeholder(R.drawable.no_image)
                    .error(R.drawable.no_image)
                    .into(holder.ivUser, new Callback() {
                        @Override
                        public void onSuccess() {
                        }

                        @Override
                        public void onError() {
                        }
                    });
        } catch (Exception e) {
            e.printStackTrace();
        }
        holder.tvTitle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ProfileFragment profileFragment = new ProfileFragment();
                Bundle bundle = new Bundle();
                bundle.putString("intent", "profile");
                bundle.putString("id", myFeeds.getPostedByID());
                profileFragment.setArguments(bundle);
                HomeActivity.replaceFragment(profileFragment, false);
            }
        });

        holder.tv_Tdots.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                PopupMenu popup = new PopupMenu(mContext, holder.tv_Tdots);
                popup.getMenuInflater().inflate(R.menu.delete_menu, popup.getMenu());
                popup.getMenu().getItem(0).setTitle("Report An Issue");

                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    public boolean onMenuItemClick(MenuItem item) {
                        //Toast.makeText(mContext, "You Clicked : " + item.getTitle(), Toast.LENGTH_SHORT).show();
                        //deleteBlogListener.onDelete(postList.get(position).getBlogId(), position);
                        Intent mIntent = new Intent(mContext, ReportAnIssue.class);
                        Bundle bundle = new Bundle();
                        bundle.putSerializable("data", myFeeds);
                        mIntent.putExtras(bundle);
                        mContext.startActivity(mIntent);
                        return true;
                    }
                });

                popup.show();
            }
        });
        holder.ivImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createDialog(myFeedList.get(position).getPostImage());
            }
        });
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MyFeedDetailFragment fragment = new MyFeedDetailFragment();
                Bundle b = new Bundle();
                b.putString("intent", "feeds");
                b.putSerializable("feeds", getItem(position));
                fragment.setArguments(b);
                replaceFragment(fragment, false);
            }
        });
    }

    @Override
    public int getItemCount() {
        return myFeedList.size();
    }

    public MyFeeds getItem(int position) {
        return myFeedList.get(position);
    }

    private void createDialog(String image) {
        final Dialog dialog = new Dialog(mContext, R.style.ChatDialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        // Include dialog.xml file
        dialog.setContentView(R.layout.dialog_view_fullimage);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        lp.copyFrom(window.getAttributes());
        //This makes the dialog take up the full width
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
        ImageView imgPhotoEditorClose = dialog.findViewById(R.id.imgPhotoEditorClose);

        imgPhotoEditorClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        ZoomageView zoomageView = dialog.findViewById(R.id.imgDialog);
        try {
            PicassoTrustAll.getInstance(mContext)
                    .load(image)
                    .placeholder(R.drawable.no_image)
                    .error(R.drawable.no_image)
                    .into(zoomageView, new Callback() {
                        @Override
                        public void onSuccess() {
                        }

                        @Override
                        public void onError() {
                        }
                    });
        } catch (Exception e) {
            e.printStackTrace();
        }
        dialog.show();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {

        public CircularImageView ivUser;
        public TextView tvTitle, tvUserName, tvTime, tvDescription, tvCommentCount,tvLikeCount, tv_Tdots;
        public ImageView ivImage, ivCloset, ivCommentIcon, ivLike;
        public ProgressBar progressBar;

        public MyViewHolder(View itemView) {
            super(itemView);
            ivImage = itemView.findViewById(R.id.ivImage);
            ivCloset = itemView.findViewById(R.id.ivCloset);
            ivUser = itemView.findViewById(R.id.ivUser);
            tvTitle = itemView.findViewById(R.id.tvTitle);
            tvUserName = itemView.findViewById(R.id.tvUserName);
            tvTime = itemView.findViewById(R.id.tvTime);
            tv_Tdots = itemView.findViewById(R.id.tv_Tdots);
            tvDescription = itemView.findViewById(R.id.tv_Description);
            tvCommentCount = itemView.findViewById(R.id.tvCommentCount);
            tvLikeCount = itemView.findViewById(R.id.tvLikeCount);
            progressBar = itemView.findViewById(R.id.progressBar);
            ivCommentIcon = itemView.findViewById(R.id.ivCommentIcon);
            ivLike = itemView.findViewById(R.id.ivLike);
        }
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    myFeedList = myFeedFilterList;
                } else {
                    List<MyFeeds> filteredList = new ArrayList<>();
                    for (MyFeeds row : myFeedFilterList) {
                        if (row.getPostedByName().toLowerCase().contains(charString.toLowerCase())) {
                            filteredList.add(row);
                        }
                    }

                    myFeedList = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = myFeedList;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                myFeedList = (ArrayList<MyFeeds>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }
/*    public void filters(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());
        myFeedList.clear();
        if (charText.length() == 0) {
            myFeedList.addAll(myFeedFilterList);
        } else {
            for (MyFeeds bean : myFeedFilterList) {
                String Contanint = bean.getPostedByName();
                Log.d(TAG, "filters: " + Contanint);
                if (Contanint.toLowerCase(Locale.getDefault())
                        .contains(charText)) {
                    myFeedList.add(bean);
                }
            }
        }
        Log.d(TAG, "filters: " + myFeedList.size());
        notifyDataSetChanged();
    }*/
}
