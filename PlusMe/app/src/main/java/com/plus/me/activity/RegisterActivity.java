package com.plus.me.activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.appcompat.app.AppCompatActivity;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.onesignal.OneSignal;
import com.plus.me.R;
import com.plus.me.adapter.NothingSelectedSpinnerAdapter;
import com.plus.me.model.City;
import com.plus.me.model.Country;
import com.plus.me.model.State;
import com.plus.me.model.User;
import com.plus.me.others.App;
import com.plus.me.others.Internet;
import com.plus.me.utils.MySharedPref;
import com.plus.me.ws.AsyncTaskListner;
import com.plus.me.ws.CallRequest;
import com.plus.me.ws.Constant;
import com.plus.me.ws.JsonParserUniversal;
import com.plus.me.ws.MyConstants;
import com.plus.me.ws.Utils;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class RegisterActivity extends AppCompatActivity implements AsyncTaskListner {
    public RegisterActivity instance;
    private EditText etFirstName, etLastName, etEmail, etMobileNumber, etPassword, etConfirmPassword, etZipCode;
    private Button btnRegister;
    private TextView tvSignIn;
    private String registerType = "";
    private Spinner spCountry, spState, spCity;
    private JsonParserUniversal jParser;
    private String stateId = "", cityId = "", countryId = "";

    ImageView ivCamera, ivProfile;
    public Uri selectedUri;
    public CropImage.ActivityResult result;
    public String filePath = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        instance = this;
        getBundleData();
        getOneSignalId();
        intializeData();
        bindWidgetReference();
        getCountry();
        bindWidgetEvents();

        spState.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                try {
                    stateId = MyConstants.stateList.get(position - 1).getId();
                    MyConstants.cityListString.clear();
                    spCity.setSelection(0);
                    cityId = "";
                    getCity();

                } catch (Exception ignored) {
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        spCity.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                try {
                    cityId = MyConstants.cityList.get(position - 1).getId();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        spCountry.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                try {
                    countryId = MyConstants.countryList.get(position - 1).getId();
                    MyConstants.stateListString.clear();
                    spState.setSelection(0);
                    System.out.println("country id==" + countryId);
                    MyConstants.cityListString.clear();
                    spCity.setSelection(0);
                    getState();
                } catch (Exception e) {

                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }


    private void intializeData() {
        jParser = new JsonParserUniversal();
        instance = this;
        MyConstants.IMEI = getDeviceIMEI();
    }

    public String getDeviceIMEI() {
        String deviceUniqueIdentifier = null;
        TelephonyManager tm = (TelephonyManager) this.getSystemService(Context.TELEPHONY_SERVICE);
        if (null != tm) {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                ActivityCompat.requestPermissions(instance, new String[]{android.Manifest.permission.READ_PHONE_STATE}, 123);
            } else {
                deviceUniqueIdentifier = tm.getDeviceId();
            }
        }
        if (null == deviceUniqueIdentifier || 0 == deviceUniqueIdentifier.length()) {
            deviceUniqueIdentifier = Settings.Secure.getString(this.getContentResolver(), Settings.Secure.ANDROID_ID);
        }
        return deviceUniqueIdentifier;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 123) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                MyConstants.IMEI = getDeviceIMEI();
            } else {
                MyConstants.IMEI = "";
            }
        }
    }

    private void getBundleData() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            registerType = bundle.getString("register_type");
        }
    }

    private void getOneSignalId() {
        OneSignal.idsAvailable(new OneSignal.IdsAvailableHandler() {
            @Override
            public void idsAvailable(String userId, String registrationId) {
                if (userId != null)
                    MyConstants.DEVICE_ID = userId;
                Log.d("debug", "User:" + userId);
                Log.d("debug", "registrationId:" + registrationId);
            }
        });
    }

    private void bindWidgetReference() {
        ivCamera = findViewById(R.id.ivCamera);
        ivProfile = findViewById(R.id.ivProfile);
        etFirstName = findViewById(R.id.etFirstName);
        etLastName = findViewById(R.id.etLastName);
        etEmail = findViewById(R.id.etEmail);
        etMobileNumber = findViewById(R.id.etMobileNumber);
        etPassword = findViewById(R.id.etPassword);
        etConfirmPassword = findViewById(R.id.etConfirmPassword);
        //spRegisterAs = findViewById(R.id.spinnerRegisterAs);
        btnRegister = findViewById(R.id.btnRegister);
        tvSignIn = findViewById(R.id.tvSignin);
        spCountry = findViewById(R.id.spCountry);
        spCity = findViewById(R.id.spCity);
        spState = findViewById(R.id.spState);
        etZipCode = findViewById(R.id.etZipCode);
    }

    @SuppressLint("NewApi")
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == CropImage.PICK_IMAGE_CHOOSER_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            Uri imageUri = CropImage.getPickImageResultUri(RegisterActivity.this, data);
            if (CropImage.isReadExternalStoragePermissionsRequired(RegisterActivity.this, imageUri)) {

                selectedUri = imageUri;
                filePath = selectedUri.getPath().toString();

                requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 0);
            } else {
                startCropImageActivity(imageUri);
            }
        }
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                ivProfile.setImageURI(result.getUri());
                selectedUri = result.getUri();
                filePath = selectedUri.getPath();
                System.out.println("filepath===" + filePath);
                ivProfile.setImageBitmap(BitmapFactory.decodeFile(filePath));
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
            }
        }

    }

    private void startCropImageActivity(Uri imageUri) {
        CropImage.activity(imageUri)
                .setGuidelines(CropImageView.Guidelines.ON)
                .setFixAspectRatio(false)
                .setMultiTouchEnabled(true)
                .start(this);
    }

    private void getCountry() {
        Map<String, String> map = new HashMap<>();
        map.put("url", MyConstants.BASE_URL + "GetCountry");
        new CallRequest(RegisterActivity.this).getCountry(map);
    }

    private void getState() {
        Map<String, String> map = new HashMap<>();
        map.put("url", MyConstants.BASE_URL + "GetStates");
        map.put("CountryID", countryId);
        new CallRequest(RegisterActivity.this).getState(map);
    }

    private void getCity() {
        Map<String, String> map = new HashMap<>();
        map.put("url", MyConstants.BASE_URL + "GetCity");
        map.put("StateID", stateId);
        new CallRequest(RegisterActivity.this).getCity(map);
    }

    private void bindWidgetEvents() {

        tvSignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PreRegisterActivity.registerActivity.finish();
                Intent intent = new Intent(RegisterActivity.this, LoginActivity.class);
                startActivity(intent);
                finish();
            }
        });
        ivCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CropImage.startPickImageActivity(RegisterActivity.this);
            }
        });


        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                doRegister();
            }
        });

    }

    private void doRegister() {
        String firstName = etFirstName.getText().toString().trim();
        String lastName = etLastName.getText().toString().trim();
        String email = etEmail.getText().toString().trim();
        String mobileNumber = etMobileNumber.getText().toString().trim();
        String password = etPassword.getText().toString().trim();
        String confirmPassword = etConfirmPassword.getText().toString().trim();
        String zipCode = etZipCode.getText().toString().trim();
        if (TextUtils.isEmpty(firstName)) {
            etFirstName.requestFocus();
            etFirstName.setError("Enter first name");
        } else if (TextUtils.isEmpty(lastName)) {
            etLastName.requestFocus();
            etLastName.setError("Enter last name");
        } else if (!Utils.isValidEmail(email)) {
            etEmail.requestFocus();
            etEmail.setError("Enter email");
        } else if (!Utils.isValidEmail(email)) {
            etEmail.requestFocus();
            etEmail.setError("Enter valid email");
        } /*else if (TextUtils.isEmpty(mobileNumber)) {
            etMobileNumber.requestFocus();
            etMobileNumber.setError("Enter mobile number");
        }*/ else if (TextUtils.isEmpty(password)) {
            etPassword.requestFocus();
            etPassword.setError("Enter password");
        } else if (TextUtils.isEmpty(confirmPassword)) {
            etConfirmPassword.requestFocus();
            etConfirmPassword.setError("Enter confirm password");
        } else if (!password.equals(confirmPassword)) {
            etConfirmPassword.requestFocus();
            etConfirmPassword.setError("Password does not match the confirm password");
        } else if (TextUtils.isEmpty(countryId)) {
            Utils.showToast("Select Country", getApplicationContext());
        } else if (TextUtils.isEmpty(stateId)) {
            Utils.showToast("Select State", getApplicationContext());
        } else if (MyConstants.cityList.size() != 0 && TextUtils.isEmpty(cityId)) {
            Utils.showToast("Select City", getApplicationContext());
        } else if (TextUtils.isEmpty(zipCode)) {
            etZipCode.requestFocus();
            etZipCode.setError("Enter Zip Code");
        } else {
            Map<String, String> map = new HashMap<>();
            map.put("url", MyConstants.BASE_URL + "do_register");
            map.put("FirstName", firstName);
            map.put("LastName", lastName);
            map.put("Email", email);
            map.put("MobileNo", "+1" + mobileNumber);
            map.put("CountryID", countryId);
            map.put("StateID", stateId);
            map.put("ProfileImage", filePath);
            map.put("CityID", cityId);
            map.put("ZipCode", zipCode);
            map.put("Password", password);
            map.put("RegisterUserType", registerType);
            map.put("IMEI", MyConstants.IMEI);
            map.put("DeviceName", MyConstants.MODEL_NAME);
            map.put("OSVersion", "Android");
            map.put("DeviceToken", MyConstants.DEVICE_ID);
            if (!Internet.isAvailable(RegisterActivity.this)) {
                Internet.showAlertDialog(RegisterActivity.this, "Error!", "No Internet Connection", false);
            } else {
                new CallRequest(RegisterActivity.this).doRegister(map);
            }
        }
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        try {

            if (result != null && !result.isEmpty()) {
                Log.i("TAG", "TAG Result : " + result);

                switch (request) {
                    case register:
                        try {
                            JSONObject object = new JSONObject(result);
                            boolean success = object.getBoolean("success");
                            if (success) {
                                // TODO parse JSON
                                Utils.showToast(object.getString("message"), this);
                                Map<String, String> map = new HashMap<>();
                                map.put("url", MyConstants.BASE_URL + "login");
                                map.put("Email", etEmail.getText().toString());
                                map.put("Password", etPassword.getText().toString());
                                map.put("Devicetoken", MyConstants.DEVICE_ID);
                                map.put("IMEI", MyConstants.IMEI);
                                map.put("DeviceName", MyConstants.MODEL_NAME);
                                map.put("OSVersion", "Android");

                                new CallRequest(instance).doLogin(map);

                            } else {
                                String error_string = object.getString("message");
                                Toast.makeText(RegisterActivity.this, error_string, Toast.LENGTH_SHORT).show();
                            }

                            //setData();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;
                    case login:
                        try {
                            Utils.hideProgressDialog();
                            JSONObject object = new JSONObject(result);
                            boolean success = object.getBoolean("status");
                            if (success) {
                                //Utils.showToast(object.getString("message"), this);
                                JSONObject userDataObj = object.getJSONObject("user_data");

                                User user = (User) jParser.parseJson(userDataObj, new User());

                                setUserDataToPreference(user);


                            } else {
                                String error_string = object.getString("message");
                                Toast.makeText(instance, error_string, Toast.LENGTH_SHORT).show();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;
                    case getCountry:
                        MyConstants.countryList.clear();
                        MyConstants.countryListString.clear();
                        int selection = 0;
                        try {
                            Utils.hideProgressDialog();
                            JSONObject object = new JSONObject(result);
                            boolean success = object.getBoolean("status");
                            if (success) {
                                // TODO parse JSON
                                JSONArray userDataObj = object.getJSONArray("data");
                                for (int i = 0; userDataObj.length() > i; i++) {
                                    MyConstants.country = (Country) jParser.parseJson(userDataObj.getJSONObject(i), new Country());
                                    MyConstants.countryList.add(MyConstants.country);
                                    MyConstants.countryListString.add(MyConstants.country.getName());
                                    if (MyConstants.country.getName().equalsIgnoreCase("United States")) {
                                        countryId = MyConstants.country.getId();
                                        selection = i + 1;
                                    }

                                }
                            } else {
                                String error_string = object.getString("message");
                            }
                            ArrayAdapter aaCountry = new ArrayAdapter<>(getApplicationContext(), R.layout.spinner_text, MyConstants.countryListString);
                            aaCountry.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            spCountry.setAdapter(new NothingSelectedSpinnerAdapter(
                                    aaCountry, R.layout.spinner_text,
                                    getApplicationContext(), "Select Country"));
                            spCountry.setSelection(selection);
                            spCountry.setClickable(false);
                            spCountry.setFocusable(false);
                            spCountry.setEnabled(false);
                            System.out.println("country list==" + MyConstants.countryList.size());
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        Utils.hideProgressDialog();
                        break;
                    case getCity:
                        MyConstants.cityList.clear();
                        MyConstants.cityListString.clear();
                        try {
                            Utils.hideProgressDialog();
                            JSONObject object = new JSONObject(result);
                            boolean success = object.getBoolean("status");
                            if (success) {
                                // TODO parse JSON
                                JSONArray userDataObj = object.getJSONArray("data");
                                for (int i = 0; userDataObj.length() > i; i++) {
                                    MyConstants.city = (City) jParser.parseJson(userDataObj.getJSONObject(i), new City());
                                    MyConstants.cityList.add(MyConstants.city);
                                    MyConstants.cityListString.add(MyConstants.city.getName());
                                }
                            } else {
                                String error_string = object.getString("message");
                            }

                            ArrayAdapter aaCity = new ArrayAdapter<>(getApplicationContext(), R.layout.spinner_text, MyConstants.cityListString);
                            aaCity.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            spCity.setAdapter(new NothingSelectedSpinnerAdapter(
                                    aaCity, R.layout.spinner_text,
                                    getApplicationContext(), "Select City"));
                            System.out.println("city list==" + MyConstants.cityList.size());
                            if (MyConstants.cityList.size() == 0) {
                                spCity.setVisibility(View.GONE);
                            } else {
                                spCity.setVisibility(View.VISIBLE);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        Utils.hideProgressDialog();
                        break;
                    case getState:
                        MyConstants.stateList.clear();
                        MyConstants.stateListString.clear();
                        try {
                            Utils.hideProgressDialog();
                            JSONObject object = new JSONObject(result);
                            boolean success = object.getBoolean("status");
                            if (success) {
                                // TODO parse JSON
                                JSONArray userDataObj = object.getJSONArray("data");
                                for (int i = 0; userDataObj.length() > i; i++) {
                                    MyConstants.state = (State) jParser.parseJson(userDataObj.getJSONObject(i), new State());
                                    MyConstants.stateList.add(MyConstants.state);
                                    MyConstants.stateListString.add(MyConstants.state.getName());
                                }
                            } else {
                                String error_string = object.getString("message");
                            }

                            ArrayAdapter aaState = new ArrayAdapter<>(getApplicationContext(), R.layout.spinner_text, MyConstants.stateListString);
                            aaState.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            spState.setAdapter(new NothingSelectedSpinnerAdapter(
                                    aaState, R.layout.spinner_text,
                                    getApplicationContext(), "Select State"));
                            System.out.println("state list==" + MyConstants.stateList.size());
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        Utils.hideProgressDialog();
                        break;
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setUserDataToPreference(User user) {

        MySharedPref.setString(instance, MyConstants.EMAIL, user.getEmail());
        MySharedPref.setString(instance, MyConstants.AUTH, user.getAuth());
        MySharedPref.setString(instance, MyConstants.USER_ID, user.getId());
        MySharedPref.setString(instance, MyConstants.PASSWORD, user.getPassword());
        MySharedPref.setString(instance, MyConstants.FIRST_NAME, user.getFirstName());
        MySharedPref.setString(instance, MyConstants.LAST_NAME, user.getLastName());
        MySharedPref.setString(instance, MyConstants.NAME, user.getFirstName() + " " + user.getLastName());
        MySharedPref.setString(instance, MyConstants.PROFILE_IMAGE, "");
        MySharedPref.setIsLogin(true);

        App.user = user;
        App.user.setName(user.getFirstName() + " " + user.getLastName());

        Intent intent = new Intent(instance, HomeActivity.class);
        startActivity(intent);
        ActivityCompat.finishAffinity(instance);
    }

    @Override
    public void onProgressUpdate(String uniqueMessageId, int progres) {

    }

    @Override
    public void onProgressComplete(String uniqueMessageId, String result, Constant.REQUESTS request) {


    }
}
