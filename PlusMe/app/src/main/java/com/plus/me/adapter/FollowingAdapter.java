package com.plus.me.adapter;

import android.content.Context;
import android.os.Bundle;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.plus.me.R;
import com.plus.me.activity.HomeActivity;
import com.plus.me.fragments.ProfileFragment;
import com.plus.me.model.Following;
import com.plus.me.others.PicassoTrustAll;
import com.squareup.picasso.Callback;

import java.util.ArrayList;

public class FollowingAdapter extends RecyclerView.Adapter<FollowingAdapter.MyViewHolder> {

    Context mContext;
    LayoutInflater inflater;
    ArrayList<Following> followingArrayList;

    public FollowingAdapter(Context mContext, ArrayList<Following> followingArrayList) {
        this.mContext = mContext;
        this.followingArrayList = followingArrayList;
        setHasStableIds(true);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public FollowingAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_following, parent, false);

        return new FollowingAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final FollowingAdapter.MyViewHolder holder, final int position) {
        final Following following = followingArrayList.get(position);

        holder.tvName.setText(following.getFirstName() + " " + following.getLastName());
        holder.txtProfetion.setText(following.getRegisterUserTypeByName());

        try {
            PicassoTrustAll.getInstance(mContext)
                    .load(following.getProfileImage())
                    .placeholder(R.drawable.avatar)
                    .error(R.drawable.avatar)
                    .into(holder.ivImage, new Callback() {
                        @Override
                        public void onSuccess() {
                        }

                        @Override
                        public void onError() {
                        }
                    });
        } catch (Exception e) {
            e.printStackTrace();
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ProfileFragment profileFragment=new ProfileFragment();
                Bundle bundle=new Bundle();
                bundle.putString("intent","profile");
                bundle.putString("id",following.getId());
                profileFragment.setArguments(bundle);
                HomeActivity.replaceFragment(profileFragment,false);
            }
        });

    }

    @Override
    public int getItemCount() {
        return followingArrayList.size();
    }

    public Following getItem(int position) {
        return followingArrayList.get(position);
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        public ImageView ivImage;
        public TextView tvName,txtProfetion;

        public MyViewHolder(View itemView) {
            super(itemView);
            tvName = itemView.findViewById(R.id.txtName);
            ivImage = itemView.findViewById(R.id.ivProfile);
            txtProfetion = itemView.findViewById(R.id.txtProfetion);
        }
    }
}
