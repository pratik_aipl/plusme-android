package com.plus.me.adapter;

import android.content.Context;
import android.os.Bundle;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.plus.me.R;
import com.plus.me.activity.HomeActivity;
import com.plus.me.fragments.TrendsetterFragment;
import com.plus.me.fragments.ProfileFragment;
import com.plus.me.model.Trendsetter;
import com.plus.me.others.PicassoTrustAll;
import com.squareup.picasso.Callback;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class TrendsetterAdapter extends RecyclerView.Adapter<TrendsetterAdapter.MyViewHolder> {

    Context mContext;
    LayoutInflater inflater;
    private List<Trendsetter> trendsettersList;
    private List<Trendsetter> blogFilterList = new ArrayList<Trendsetter>();
    TrendsetterFragment trendsetterFragment;

    public TrendsetterAdapter(Context mContext, ArrayList<Trendsetter> trendsettersList) {
        this.mContext = mContext;
        this.trendsettersList = trendsettersList;
        this.blogFilterList.addAll(trendsettersList);

        setHasStableIds(true);

    }
    public void filters(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());
        trendsettersList.clear();
        if (charText.length() == 0) {
            trendsettersList.addAll(blogFilterList);
        } else {
            for (Trendsetter bean : blogFilterList) {
                String Contanint = bean.getFirstName()+" "+bean.getLastName();
                if (Contanint.toLowerCase(Locale.getDefault())
                        .contains(charText)) {
                    trendsettersList.add(bean);
                }
            }
        }
        notifyDataSetChanged();
    }
    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public TrendsetterAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_designer, parent, false);

        return new TrendsetterAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final TrendsetterAdapter.MyViewHolder holder, final int position) {
        final Trendsetter stylist = trendsettersList.get(position);;
        holder.txtCity.setText(stylist.getCityName());
        holder.tvName.setText(stylist.getFirstName() + " " + stylist.getLastName());
        holder.txtProfetion.setText(stylist.getRegisterUserTypeByName());
        try {
            PicassoTrustAll.getInstance(mContext)
                    .load(stylist.getUserProfileImage())
                    .placeholder(R.drawable.avatar)
                    .error(R.drawable.avatar)
                    .into(holder.ivImage, new Callback() {
                        @Override
                        public void onSuccess() {
                        }

                        @Override
                        public void onError() {
                        }
                    });
        } catch (Exception e) {
            e.printStackTrace();
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ProfileFragment profileFragment=new ProfileFragment();
                Bundle bundle=new Bundle();
                bundle.putString("intent","profile");
                bundle.putString("id",stylist.getId());
                profileFragment.setArguments(bundle);
                HomeActivity.replaceFragment(profileFragment,false);
            }
        });

    }

    @Override
    public int getItemCount() {
        return trendsettersList.size();
    }

    public Trendsetter getItem(int position) {
        return trendsettersList.get(position);
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {

        public ImageView ivImage;
        public TextView tvName, txtProfetion,txtCity;

        public MyViewHolder(View itemView) {
            super(itemView);

            tvName = itemView.findViewById(R.id.txtName);
            ivImage = itemView.findViewById(R.id.ivProfile);
            txtProfetion = itemView.findViewById(R.id.txtProfetion);
            txtCity=itemView.findViewById(R.id.txtCity);
        }
    }
}
