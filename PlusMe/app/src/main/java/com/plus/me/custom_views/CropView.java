package com.plus.me.custom_views;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.DashPathEffect;
import android.graphics.Paint;
import android.graphics.Path;
import android.view.MotionEvent;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.plus.me.R;
import com.plus.me.activity.ChooseImageActivity;
import com.plus.me.others.App;
import com.plus.me.utils.Point;

import java.util.ArrayList;
import java.util.List;

import static com.plus.me.activity.ChooseImageActivity.btnCancle;
import static com.plus.me.activity.ChooseImageActivity.btnOk;
import static com.plus.me.activity.ChooseImageActivity.btnReset;
import static com.plus.me.activity.ChooseImageActivity.llImage;

public class CropView extends View implements View.OnTouchListener {

    public static List<Point> points;
    int DIST = 2;
    boolean flgPathDraw = true;
    Point mfirstpoint = null;
    boolean bfirstpoint = false;
    Point mlastpoint = null;
    Bitmap bitmap, bitmapOriginal;
    Context mContext;
    private Paint paint;
    private int mWidth;
    private int mHeight;
    Bitmap resultingImage;
    boolean iswebP;

    public CropView(Context c, Bitmap bitmap, boolean iswebP) {
        super(c);

        mContext = c;
        this.bitmap = bitmap;
        this.iswebP = iswebP;
        this.bitmapOriginal = bitmap;
        setFocusable(true);
        setFocusableInTouchMode(true);

        paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        paint.setStyle(Paint.Style.STROKE);
        paint.setPathEffect(new DashPathEffect(new float[]{10, 20}, 0));
        paint.setStrokeWidth(5);
        paint.setColor(getResources().getColor(R.color.pink));

        this.setOnTouchListener(this);
        points = new ArrayList<Point>();

        bfirstpoint = false;

        //  showButtons();

        //this.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));

    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        mWidth = View.MeasureSpec.getSize(widthMeasureSpec);
        mHeight = View.MeasureSpec.getSize(heightMeasureSpec);

        setMeasuredDimension(mWidth, mHeight);
        showButtons();
    }

    public void onDraw(Canvas canvas) {
        if (bitmap != null) {
            if (!iswebP) {
                int centreX = (mWidth - bitmap.getWidth());
                int centreY = (mHeight - bitmap.getHeight());
                this.setLayoutParams(new LinearLayout.LayoutParams(bitmap.getWidth(), bitmap.getHeight()));
                canvas.drawBitmap(bitmap, centreX, centreY, null);
            } else {
                this.setLayoutParams(new LinearLayout.LayoutParams(bitmap.getWidth(), bitmap.getHeight()));
                canvas.drawBitmap(bitmap, 0, 0, null);
                btnOk.setVisibility(VISIBLE);
                btnReset.setVisibility(VISIBLE);
            }
        } else {
            btnOk.setVisibility(GONE);
            btnReset.setVisibility(GONE);
            Toast.makeText(mContext, "Selected Image is Crouped", Toast.LENGTH_SHORT).show();
        }


        Path path = new Path();
        boolean first = true;

        for (int i = 0; i < points.size(); i += 2) {
            Point point = points.get(i);
            if (first) {
                first = false;
                path.moveTo(point.x, point.y);
            } else if (i < points.size() - 1) {
                Point next = points.get(i + 1);
                path.quadTo(point.x, point.y, next.x, next.y);
            } else {
                mlastpoint = points.get(i);
                path.lineTo(point.x, point.y);
            }
        }
        canvas.drawPath(path, paint);


    }

    public boolean onTouch(View view, MotionEvent event) {

        Point point = new Point();
        point.x = (int) event.getX();
        point.y = (int) event.getY();
        App.crop = true;
        if (flgPathDraw) {

            if (bfirstpoint) {

                if (comparepoint(mfirstpoint, point)) {
                    points.add(mfirstpoint);
                    flgPathDraw = false;
                    showButtons();
                } else {
                    points.add(point);
                }

            } else {
                points.add(point);
            }

            if (!(bfirstpoint)) {

                mfirstpoint = point;
                bfirstpoint = true;
            }
        }

        invalidate();

        if (event.getAction() == MotionEvent.ACTION_UP) {

            mlastpoint = point;
            if (flgPathDraw) {
                if (points.size() > 12) {
                    if (!comparepoint(mfirstpoint, mlastpoint)) {
                        flgPathDraw = false;
                        points.add(mfirstpoint);
                        showButtons();
                    }
                }
            }
        }

        return true;
    }

    private boolean comparepoint(Point first, Point current) {
        int left_range_x = (int) (current.x - 3);
        int left_range_y = (int) (current.y - 3);

        int right_range_x = (int) (current.x + 3);
        int right_range_y = (int) (current.y + 3);

        if ((left_range_x < first.x && first.x < right_range_x)
                && (left_range_y < first.y && first.y < right_range_y)) {
            if (points.size() < 10) {
                return false;
            } else {
                return true;
            }
        } else {
            return false;
        }

    }

    private void showButtons() {

        btnOk.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                App.bitmap = bitmap;
                App.inMuCloset = false;
                App.edit = true;
                App.firstTimeEdit = true;
                // replaceFragment(new CreateALookFragmentNew(), false);
                ((ChooseImageActivity) mContext).finish();
            }
        });
        btnReset.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                llImage.removeAllViews();
                App.crop = false;
                App.inMuCloset = false;
                App.edit = true;
                App.firstTimeEdit = false;
                llImage.addView(new CropView(mContext, bitmap, iswebP));

            }
        });

        btnCancle.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                App.firstTimeEdit = false;
                ((ChooseImageActivity) mContext).finish();
            }
        });
      /*  DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent intent;
                switch (which) {
                    case DialogInterface.BUTTON_POSITIVE:


                        break;

                    case DialogInterface.BUTTON_NEGATIVE:

                      *//*  intent = new Intent(mContext, CropActivity.class);
                        intent.putExtra("crop", false);
                        mContext.startActivity(intent);

                        bfirstpoint = false;*//*
                        llImage.removeAllViews();
                        llImage.addView(new CropView(instance, bitmap));
                        break;
                }
            }
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setMessage("Do you Want to save Crop or Non-crop image?")
                .setPositiveButton("Crop", dialogClickListener)
                .setNegativeButton("Non-crop", dialogClickListener).show()
                .setCancelable(false);*/
    }
}