package com.plus.me.listener;

/**
 * Created by Krupa Kakkad on 23 August 2018
 */
public interface MyFeedsListener {

    public void onClosetClick(int pos);
    public void onLikeClick(int pos);
}
