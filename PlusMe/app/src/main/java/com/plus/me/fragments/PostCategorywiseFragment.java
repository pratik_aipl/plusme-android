package com.plus.me.fragments;


import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.core.view.MenuItemCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.SearchView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.plus.me.R;
import com.plus.me.activity.HomeActivity;
import com.plus.me.adapter.MyFeedAdapter;
import com.plus.me.listener.MyFeedsListener;
import com.plus.me.model.MyFeeds;
import com.plus.me.others.App;
import com.plus.me.others.Internet;
import com.plus.me.utils.VerticalSpacingDecoration;
import com.plus.me.ws.AsyncTaskListner;
import com.plus.me.ws.CallRequest;
import com.plus.me.ws.Constant;
import com.plus.me.ws.JsonParserUniversal;
import com.plus.me.ws.MyConstants;
import com.plus.me.ws.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static com.plus.me.activity.HomeActivity.replaceFragment;
import static com.plus.me.activity.HomeActivity.setupBadge;
import static com.plus.me.activity.HomeActivity.toolbar;
import static com.plus.me.activity.HomeActivity.txtSave;

/**
 * A simple {@link Fragment} subclass.
 */
public class PostCategorywiseFragment extends Fragment implements AsyncTaskListner, MyFeedsListener {

    public RecyclerView rvFeeds;
    public JsonParserUniversal jParser;
    public TextView txtNoValue;
    public LinearLayout lin_NoValue;
    private MyFeedAdapter myFeedAdapter;
    private LinearLayoutManager layoutManager;
    private ArrayList<MyFeeds> myFeedsList = new ArrayList<>();
    public int position = 0;
    public Button btn_post;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_my_feeds, container, false);

        setHasOptionsMenu(true);

        bindWidgetReference(view);

        myFeedsList.clear();
        jParser = new JsonParserUniversal();
        // TODO comment addData(); and uncomment getData();
        btn_post.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                replaceFragment(new PostNewFragment(), false);
            }
        });
        getData();
        return view;
    }


    private void getData() {
        Map<String, String> map = new HashMap<>();
        map.put("url", MyConstants.BASE_URL + "GetFeedsList");
        map.put("header", "");
        map.put("Auth", App.user.getAuth());
        map.put("CategoryID", MyConstants.categoryId);

        if (!Internet.isAvailable(getActivity())) {
            Internet.showAlertDialog(getActivity(), "Error!", "No Internet Connection", false);
        } else {
            new CallRequest(PostCategorywiseFragment.this).getMyFeedList(map);
        }
    }

    private void setData() {
        layoutManager = new LinearLayoutManager(getActivity());
        rvFeeds.setLayoutManager(layoutManager);

        myFeedAdapter = new MyFeedAdapter(getActivity(), myFeedsList, PostCategorywiseFragment.this);
        rvFeeds.setHasFixedSize(true);
        rvFeeds.addItemDecoration(new VerticalSpacingDecoration(20));
        rvFeeds.setItemViewCacheSize(20);
        rvFeeds.setDrawingCacheEnabled(true);
        rvFeeds.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
        rvFeeds.setAdapter(myFeedAdapter);
        rvFeeds.scrollToPosition(position);
    }

    private void bindWidgetReference(View view) {
        rvFeeds = view.findViewById(R.id.rvFeeds);
        lin_NoValue = view.findViewById(R.id.lin_NoValue);
        txtNoValue = view.findViewById(R.id.txtNoValue);
        btn_post = view.findViewById(R.id.btn_post);
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        txtSave.setVisibility(View.GONE);
        toolbar.setText(MyConstants.categoryName);
        MyConstants.editImageUrl = "";
        menu.getItem(0).setVisible(true);
        menu.getItem(1).setVisible(true);
        menu.getItem(2).setVisible(true);
        menu.getItem(3).setVisible(false);
        menu.getItem(4).setVisible(false);
        //tabs.setVisibility(View.VISIBLE);
        super.onPrepareOptionsMenu(menu);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        //super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
        inflater.inflate(R.menu.home, menu);
        SearchView searchView = (SearchView) MenuItemCompat.getActionView(menu.findItem(R.id.item_search));
        EditText searchEditText = (EditText) searchView.findViewById(R.id.search_src_text);
        searchEditText.setTextColor(getResources().getColor(R.color.black));


        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
              /*  if (TextUtils.isEmpty(newText)) {
                    myFeedAdapter.filters("");
                } else {*/
                    myFeedAdapter.getFilter().filter(newText);

                //}
                return true;
            }
        });


        final MenuItem menuItem = menu.findItem(R.id.item_notification);

        View actionView = MenuItemCompat.getActionView(menuItem);
        HomeActivity.tvNotificationItemCount = actionView.findViewById(R.id.notification_badge);
        FrameLayout frameNotification = actionView.findViewById(R.id.frameNotification);
        frameNotification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println("Notification====");
                replaceFragment(new NotificationFragment(), false);
            }
        });
        setupBadge();
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        try {

            Utils.hideProgressDialog();
            if (result != null && !result.isEmpty()) {
                Log.i("TAG", "TAG Result : " + result);

                switch (request) {
                    case getMyFeedsList:
                        myFeedsList.clear();
                        try {
                            JSONObject object = new JSONObject(result);
                            boolean success = object.getBoolean("success");
                            if (success) {
                                // TODO parse JSON

                                JSONArray data = object.getJSONArray("data");
                                if (data != null && data.length() > 0) {
                                    for (int i = 0; i < data.length(); i++) {
                                        JSONObject jsonObject = data.getJSONObject(i);

                                        MyFeeds myFeeds = (MyFeeds) jParser.parseJson(jsonObject, new MyFeeds());

                                        myFeedsList.add(myFeeds);
                                    }
                                    setData();
                                } else {
                                    //  Utils.showToast(object.getString("message"), getActivity());
                                }

                            } else {
                                String error_string = object.getString("message");
                                // Toast.makeText(getActivity(), error_string, Toast.LENGTH_SHORT).show();
                            }
                            if (myFeedsList.size() == 0) {
                                lin_NoValue.setVisibility(View.VISIBLE);
                            } else {
                                lin_NoValue.setVisibility(View.GONE);
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;

                    case addToCloset:

                        try {
                            JSONObject object = new JSONObject(result);
                            boolean success = object.getBoolean("success");
                            if (success) {
                                // TODO parse JSON
                                Utils.showToast(object.getString("message"), getActivity());
                                getData();

                            } else {
                                Utils.showToast(object.getString("message"), getActivity());
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;
                    case getLike:

                        try {
                            JSONObject object = new JSONObject(result);
                            boolean success = object.getBoolean("success");
                            if (success) {
                                // TODO parse JSON
                                //Utils.showToast(object.getString("message"), getActivity());
                                getData();

                            } else {
                                Utils.showToast(object.getString("message"), getActivity());
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onProgressUpdate(String uniqueMessageId, int progres) {

    }

    @Override
    public void onProgressComplete(String uniqueMessageId, String result, Constant.REQUESTS request) {

    }

    @Override
    public void onClosetClick(int pos) {
        position = pos;
        Map<String, String> map = new HashMap<>();
        map.put("url", MyConstants.BASE_URL + "MoveToCloset");
        map.put("PostImage", myFeedsList.get(pos).getPostImage());
        map.put("Title", "");
        map.put("BaseImageID", "");
        map.put("header", "");
        map.put("Auth", App.user.getAuth());
        new CallRequest(PostCategorywiseFragment.this).addToCloset(map);
    }

    private void moveToMyCloset(String savedPath) {

    }

    @Override
    public void onLikeClick(int pos) {
        position = pos;
        doLike(pos);
    }

    private void doLike(int pos) {
        String isLike = null;
        if (myFeedsList.get(pos).getIsLike().equalsIgnoreCase("0")) {
            isLike = "1";
        } else {
            isLike = "0";
        }
        Map<String, String> map = new HashMap<>();
        map.put("url", MyConstants.BASE_URL + "LikePost");
        map.put("PostId", myFeedsList.get(pos).getBlogId()); // TODO add category id
        map.put("isLike", isLike);
        map.put("header", "");
        map.put("Auth", App.user.getAuth());
        new CallRequest(PostCategorywiseFragment.this).getLike(map);
    }


}
