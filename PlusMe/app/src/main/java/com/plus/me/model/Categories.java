package com.plus.me.model;

import java.io.Serializable;

/**
 * Created by Krupa Kakkad on 05 July 2018
 */
public class Categories implements Serializable{

    public String CategoryImageUrl="", CategoryName ="", CategoryID="";

    public String getCategoryID() {
        return CategoryID;
    }

    public void setCategoryID(String categoryID) {
        CategoryID = categoryID;
    }

    public String getCategoryImageUrl() {
        return CategoryImageUrl;
    }

    public void setCategoryImageUrl(String categoryImageUrl) {
        this.CategoryImageUrl = categoryImageUrl;
    }

    public String getCategoryName() {
        return CategoryName;
    }

    public void setCategoryName(String categoryName) {
        this.CategoryName = categoryName;
    }
}
