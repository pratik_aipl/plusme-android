package com.plus.me.adapter;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.plus.me.R;
import com.plus.me.fragments.BlogListFragment;
import com.plus.me.fragments.MyClosetFragment;
import com.plus.me.fragments.VisionBoardLatestFragment;
import com.plus.me.listener.CatagoriesPopupListener;
import com.plus.me.model.Categories;

import java.util.ArrayList;
import java.util.List;

public class CatagoryPopupAdapter extends RecyclerView.Adapter<CatagoryPopupAdapter.MyViewHolder> {

    Context mContext;
    LayoutInflater inflater;
    ArrayList<Categories> categoryList;
    private List<Categories> categoryFilterList = new ArrayList<>();
    public CatagoriesPopupListener catagoriesPopupListener;
    BlogListFragment blogListFragment;

    public CatagoryPopupAdapter(Context mContext, ArrayList<Categories> categoryList, BlogListFragment blogListFragment) {
        this.mContext = mContext;
        this.categoryList = categoryList;
        this.categoryFilterList.addAll(categoryList);
        this.catagoriesPopupListener = (CatagoriesPopupListener) blogListFragment;
        setHasStableIds(true);
    }

    public CatagoryPopupAdapter(Context mContext, ArrayList<Categories> categoryList, MyClosetFragment blogListFragment) {
        this.mContext = mContext;
        this.categoryList = categoryList;
        this.categoryFilterList.addAll(categoryList);
        this.catagoriesPopupListener = (CatagoriesPopupListener) blogListFragment;
        setHasStableIds(true);
    }

    public CatagoryPopupAdapter(Context mContext, ArrayList<Categories> categoryList, VisionBoardLatestFragment blogListFragment) {
        this.mContext = mContext;
        this.categoryList = categoryList;
        this.categoryFilterList.addAll(categoryList);
        this.catagoriesPopupListener = (CatagoriesPopupListener) blogListFragment;
        setHasStableIds(true);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_categories_poup, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final Categories categories = categoryList.get(position);

        holder.tvCategoryName.setText(categories.getCategoryName());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                catagoriesPopupListener.onPopupItemClick(position, v);
            }
        });
    }

    @Override
    public int getItemCount() {
        return categoryList.size();
    }

    public Categories getItem(int position) {
        return categoryList.get(position);
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {

        public ImageView ivImage;
        public TextView tvCategoryName;
        public ProgressBar progressBar;

        public MyViewHolder(View itemView) {
            super(itemView);

            tvCategoryName = itemView.findViewById(R.id.catagoryItem);
        }
    }
}
