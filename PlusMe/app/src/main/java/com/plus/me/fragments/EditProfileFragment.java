package com.plus.me.fragments;


import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import androidx.fragment.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Switch;

import com.plus.me.R;
import com.plus.me.custom_views.CustomBoldTextView;
import com.plus.me.custom_views.CustomEditText;
import com.plus.me.custom_views.CustomTextView;
import com.plus.me.model.City;
import com.plus.me.model.Country;
import com.plus.me.model.State;
import com.plus.me.model.UserProfile;
import com.plus.me.others.App;
import com.plus.me.others.PicassoTrustAll;
import com.plus.me.utils.MySharedPref;
import com.plus.me.ws.AsyncTaskListner;
import com.plus.me.ws.CallRequest;
import com.plus.me.ws.Constant;
import com.plus.me.ws.JsonParserUniversal;
import com.plus.me.ws.MyConstants;
import com.plus.me.ws.Utils;
import com.squareup.picasso.Callback;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import static android.app.Activity.RESULT_OK;
import static com.plus.me.activity.HomeActivity.toolbar;
import static com.plus.me.activity.HomeActivity.txtSave;

/**
 * A simple {@link Fragment} subclass.
 */
public class EditProfileFragment extends Fragment implements AsyncTaskListner {
    private static final String TAG = "EditProfileFragment";
    public ImageView ivCamera;
    public Uri selectedUri;
    public CropImage.ActivityResult result;
    public String filePath = "";
    public ImageView ivProfile;
    private View view;
    private CustomBoldTextView txtName;
    private CustomTextView tvProfession;
    private CustomEditText etFName;
    private CustomEditText etLName;
    private CustomEditText etCompName;
    private CustomEditText etEmail;
    private CustomEditText etContactNo;
    private CustomEditText etWorkHours;
    private CustomEditText etFacebook;
    private CustomEditText etInstagram, etZipCode;
    private JsonParserUniversal jParser;
    private Spinner spCountry, spState, spCity;
    private String countryId = "", stateId = "", cityId = "";
    private int countryPos, statePos, cityPos = 0;
    public Switch privacy;
    LinearLayout lin_spcity;

    public EditProfileFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_edit_profile, container, false);
        setHasOptionsMenu(true);
        initializeData();
        findViewById();
        setSpinner();
        onClickListener();
        if (MyConstants.userProfile != null) {
            setUserData(MyConstants.userProfile);
        }
        if (MyConstants.countryList.size() == 0) {
            getCountry();
        }
        return view;
    }

    private void getCountry() {
        Map<String, String> map = new HashMap<>();
        map.put("url", MyConstants.BASE_URL + "GetCountry");
        new CallRequest(EditProfileFragment.this).getCountryFragment(map);
    }

    private void getState() {
        Map<String, String> map = new HashMap<>();
        map.put("url", MyConstants.BASE_URL + "GetStates");
        map.put("CountryID", countryId);
        new CallRequest(EditProfileFragment.this).getStateFragment(map);
    }

    private void getCity() {
        Map<String, String> map = new HashMap<>();
        map.put("url", MyConstants.BASE_URL + "GetCity");
        map.put("StateID", stateId);
        new CallRequest(EditProfileFragment.this).getCityFragment(map);
    }

    private void setSpinner() {

    }

    private void initializeData() {
        txtSave.setVisibility(View.VISIBLE);
        jParser = new JsonParserUniversal();
        toolbar.setText("Profile");
        App.isFromEditProfile = true;
    }

    private void onClickListener() {

        ivCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CropImage.startPickImageActivity(getActivity());
            }
        });

        txtSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editProfile();
            }
        });
    }

    private void editProfile() {
        String firstName = etFName.getText().toString().trim();
        String lastName = etLName.getText().toString().trim();
        String email = etEmail.getText().toString().trim();
        String mobileNumber = etContactNo.getText().toString().trim();
        String companyName = etCompName.getText().toString().trim();
        String workingHours = etWorkHours.getText().toString().trim();
        String facebookLink = etFacebook.getText().toString().trim();
        String instagramLink = etInstagram.getText().toString().trim();
        String zipCode = etZipCode.getText().toString().trim();
        if (TextUtils.isEmpty(firstName)) {
            etFName.requestFocus();
            etFName.setError("First name is required");
        } else if (TextUtils.isEmpty(lastName)) {
            etLName.requestFocus();
            etLName.setError("Last name is required");
        } else if (TextUtils.isEmpty(mobileNumber)) {
            etContactNo.requestFocus();
            etContactNo.setError("Contact number is required");
        } else if (TextUtils.isEmpty(zipCode)) {
            etZipCode.requestFocus();
            etZipCode.setError("Zip Code is requried");
        } else {
            Map<String, String> map = new HashMap<>();
            map.put("url", MyConstants.BASE_URL + "edit_profile");
            map.put("header", "");
            map.put("Auth", App.user.getAuth());
            map.put("ProfileImage", filePath);
            map.put("FirstName", firstName);
            map.put("LastName", lastName);
            map.put("MobileNo", "+1" + mobileNumber);
            map.put("CompanyName", companyName);
            map.put("WorkingHours", workingHours);
            map.put("FacebookLink", facebookLink);
            map.put("InstragramLink", instagramLink);
            map.put("CountryID", countryId);
            map.put("StateID", stateId);
            map.put("CityID", cityId);
            map.put("ZipCode", zipCode);
            /*if (privacy.isChecked())
                map.put("IsPrivate", "1");
            else
                map.put("IsPrivate", "0");*/
            new CallRequest(EditProfileFragment.this).editUserProfile(map);
        }
    }

    private void findViewById() {
        ivCamera = view.findViewById(R.id.ivCamera);
        ivProfile = view.findViewById(R.id.ivProfile);
        txtName = view.findViewById(R.id.txtName);
        tvProfession = view.findViewById(R.id.tvProfession);
        etFName = view.findViewById(R.id.etFName);
        etLName = view.findViewById(R.id.etLName);
        etCompName = view.findViewById(R.id.etCompName);
        etEmail = view.findViewById(R.id.etEmail);
        etContactNo = view.findViewById(R.id.etContactNo);
        etWorkHours = view.findViewById(R.id.etWorkHours);
        etFacebook = view.findViewById(R.id.etFacebook);
        etInstagram = view.findViewById(R.id.etInstagram);
        spCity = view.findViewById(R.id.spCity);
        spCountry = view.findViewById(R.id.spCountry);
        spState = view.findViewById(R.id.spState);
        etZipCode = view.findViewById(R.id.etZipCode);
        lin_spcity = view.findViewById(R.id.lin_spcity);
        privacy = view.findViewById(R.id.privacy);
        privacy.setVisibility(View.GONE);
    }

    @SuppressLint("NewApi")
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == CropImage.PICK_IMAGE_CHOOSER_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            Uri imageUri = CropImage.getPickImageResultUri(getActivity(), data);
            if (CropImage.isReadExternalStoragePermissionsRequired(getActivity(), imageUri)) {

                selectedUri = imageUri;
                filePath = selectedUri.getPath().toString();

                requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 0);
            } else {
                startCropImageActivity(imageUri);
            }
        }
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                ivProfile.setImageURI(result.getUri());
                selectedUri = result.getUri();
                filePath = selectedUri.getPath().toString();
                System.out.println("filepath===" + filePath);
                ivProfile.setImageBitmap(BitmapFactory.decodeFile(filePath));
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
            }
        }

    }

    private void startCropImageActivity(Uri imageUri) {
        CropImage.activity(imageUri)
                .setGuidelines(CropImageView.Guidelines.ON)
                .setFixAspectRatio(false)
                .setMultiTouchEnabled(true)
                .start(getActivity());
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        //tabs.setVisibility(View.GONE);
        menu.getItem(0).setVisible(false);
        menu.getItem(1).setVisible(false);
        menu.getItem(2).setVisible(false);
        menu.getItem(3).setVisible(false);
        menu.getItem(4).setVisible(false);

        super.onPrepareOptionsMenu(menu);
    }

    private void setUserData(UserProfile userProfile) {
        etEmail.setText(userProfile.getEmail());
        etCompName.setText(userProfile.getCompanyName());
        etContactNo.setText(userProfile.getMobileNo());
        etFName.setText(userProfile.getFirstName());
        etLName.setText(userProfile.getLastName());
        etWorkHours.setText(userProfile.getWorkingHours());
        etFacebook.setText(userProfile.getFacebookLink());
        etInstagram.setText(userProfile.getInstragramLink());
        etZipCode.setText(userProfile.getZipCode());

        if (userProfile.getIsPrivate().equals("1")) {
            privacy.setChecked(true);
        } else {
            privacy.setChecked(false);
        }
        setSpinner();
        int countId = 0;

        txtName.setText(userProfile.getFirstName() + " " + userProfile.getLastName());
        tvProfession.setText(userProfile.getRegisterUserTypeByName());

        ArrayAdapter aaCountry = new ArrayAdapter<>(getActivity(), R.layout.spinner_text_black, MyConstants.countryListString);
        aaCountry.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spCountry.setAdapter(aaCountry);
        spCountry.setClickable(false);
        spCountry.setFocusable(false);
        spCountry.setEnabled(false);


/*
        ArrayAdapter aaState = new ArrayAdapter<>(getActivity(), R.layout.spinner_text_black, MyConstants.stateListString);
        aaState.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spState.setAdapter(aaState);

        ArrayAdapter aaCity = new ArrayAdapter<>(getActivity(), R.layout.spinner_text_black, MyConstants.cityListString);
        aaCity.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//        spCity.setAdapter(new NothingSelectedSpinnerAdapter(
//                aaCity, R.layout.spinner_text,
//                getActivity(), "Select City"));

        spCity.setAdapter(aaCity);
*/

        for (int i = 0; MyConstants.countryList.size() > i; i++) {
            // System.out.println("country id=="+userProfile.getCountryID());
            if (userProfile.getCountryID().equalsIgnoreCase(MyConstants.countryList.get(i).getId())) {
                countryId = userProfile.getCountryID();
                spCountry.setSelection(i);
                break;

            }
        }
//
//        for (int i = 0; MyConstants.stateList.size() > i; i++) {
//            if (userProfile.getStateID().equalsIgnoreCase(MyConstants.stateList.get(i).getId())) {
//                stateId = userProfile.getStateID();
//                spState.setSelection(i);
//                statePos = i;
//                break;
//            }
//        }
//
//        for (int i = 0; MyConstants.cityList.size() > i; i++) {
//            if (userProfile.getCityID().equalsIgnoreCase(MyConstants.cityList.get(i).getId())) {
//                cityId = userProfile.getCityID();
//                spCity.setSelection(i);
//                cityPos = i;
//                break;
//            }
//        }


        //spCity.setSelection(cityPos+1);
        spState.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                try {
                    statePos = position;
                    cityPos = 0;
                    stateId = MyConstants.stateList.get(position).getId();
                    MyConstants.cityListString.clear();
                    cityId = "";
                    getCity();

                } catch (Exception e) {

                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        spCity.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                try {
                    cityPos = position;
                    cityId = MyConstants.cityList.get(position).getId();

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        spCountry.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                try {
                    countryId = MyConstants.countryList.get(position).getId();
                    getState();

                } catch (Exception e) {

                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        if (!TextUtils.isEmpty(userProfile.getUserProfileImage())) {
            try {
                PicassoTrustAll.getInstance(getActivity())
                        .load(userProfile.getUserProfileImage())
                        .placeholder(R.drawable.avatar)
                        .error(R.drawable.avatar)
                        .into(ivProfile, new Callback() {
                            @Override
                            public void onSuccess() {

                            }

                            @Override
                            public void onError() {

                            }
                        });
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        try {

            Utils.hideProgressDialog();
            if (result != null && !result.isEmpty()) {
                Log.i("TAG", "TAG Result : " + result);

                switch (request) {
                    case editUserProfile:
                        try {
                            JSONObject object = new JSONObject(result);
                            boolean success = object.getBoolean("success");
                            if (success) {
                                // TODO parse JSON
                                Utils.showToast(object.getString("message"), getActivity());

                                JSONObject userDataObj = object.getJSONObject("data");

                                MyConstants.userProfile = (UserProfile) jParser.parseJson(userDataObj, new UserProfile());

                                updateData();

                                if (MyConstants.countryList.size() == 0) {
                                    getCountry();
                                }
                            } else {
                                String error_string = object.getString("message");
                                Utils.showToast(error_string, getActivity());
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;

                    case getCountry:
                        MyConstants.countryList.clear();
                        MyConstants.countryListString.clear();
                        try {
                            JSONObject object = new JSONObject(result);
                            boolean success = object.getBoolean("status");
                            if (success) {
                                // TODO parse JSON
                                JSONArray userDataObj = object.getJSONArray("data");
                                for (int i = 0; userDataObj.length() > i; i++) {
                                    MyConstants.country = (Country) jParser.parseJson(userDataObj.getJSONObject(i), new Country());
                                    MyConstants.countryList.add(MyConstants.country);
                                    MyConstants.countryListString.add(MyConstants.country.getName());
                                    if (MyConstants.country != null) {
                                        if (MyConstants.userProfile.getCountryID().equalsIgnoreCase(MyConstants.country.getId())) {
                                            countryPos = i;
                                            countryId = MyConstants.userProfile.getCountryID();
                                            break;
                                        }
                                    }
                                }
                            } else {
                                String error_string = object.getString("message");
                            }
                            ArrayAdapter aaCountry = new ArrayAdapter<>(getActivity(), R.layout.spinner_text_black, MyConstants.countryListString);
                            aaCountry.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            spCountry.setAdapter(aaCountry);
                            System.out.println("country list==" + MyConstants.countryList.size());
                            spCountry.setSelection(countryPos);
                            getState();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        Utils.hideProgressDialog();
                        break;

                    case getState:
                        MyConstants.stateList.clear();
                        MyConstants.stateListString.clear();
                        try {
                            JSONObject object = new JSONObject(result);
                            boolean success = object.getBoolean("status");
                            if (success) {
                                // TODO parse JSON
                                JSONArray userDataObj = object.getJSONArray("data");
                                for (int i = 0; userDataObj.length() > i; i++) {
                                    MyConstants.state = (State) jParser.parseJson(userDataObj.getJSONObject(i), new State());
                                    MyConstants.stateList.add(MyConstants.state);
                                    MyConstants.stateListString.add(MyConstants.state.getName());
                                    if (MyConstants.state != null) {
                                        if (MyConstants.userProfile.getStateID().equalsIgnoreCase(MyConstants.state.getId())) {
                                            statePos = i;
                                            stateId = MyConstants.userProfile.getStateID();
                                        }
                                    }
                                }
                            } else {
                                String error_string = object.getString("message");
                            }

                            ArrayAdapter aaState = new ArrayAdapter<>(getActivity(), R.layout.spinner_text_black, MyConstants.stateListString);
                            aaState.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            spState.setAdapter(aaState);

                            System.out.println("state list==" + MyConstants.stateList.size());
                            spState.setSelection(statePos);
                            getCity();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        Utils.hideProgressDialog();
                        break;

                    case getCity:
                        MyConstants.cityList.clear();
                        MyConstants.cityListString.clear();
                        try {
                            JSONObject object = new JSONObject(result);
                            boolean success = object.getBoolean("status");
                            if (success) {
                                // TODO parse JSON
                                JSONArray userDataObj = object.getJSONArray("data");
                                Log.d(TAG, "DATA>>: " + userDataObj.toString());
                                for (int i = 0; i < userDataObj.length(); i++) {
                                    MyConstants.city = (City) jParser.parseJson(userDataObj.getJSONObject(i), new City());
                                    MyConstants.cityList.add(MyConstants.city);
                                    MyConstants.cityListString.add(MyConstants.city.getName());
                                    if (MyConstants.city != null) {
                                        if (MyConstants.userProfile.getCityID().equalsIgnoreCase(MyConstants.city.getId())) {
                                            cityPos = i;
                                            cityId = MyConstants.userProfile.getCityID();
                                        }
                                    }
                                }
                            } else {
                                String error_string = object.getString("message");
                            }

                            try {
                                ArrayAdapter aaCity = new ArrayAdapter<>(getActivity(), R.layout.spinner_text_black, MyConstants.cityListString);
                                aaCity.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                spCity.setAdapter(aaCity);
                                System.out.println("city list==" + MyConstants.cityList.size());

                                //   if(cityPos!=0){
                                spCity.setSelection(cityPos);
                                //  }

                                if (MyConstants.cityList.size() == 0) {
                                    lin_spcity.setVisibility(View.GONE);
                                } else {
                                    lin_spcity.setVisibility(View.VISIBLE);
                                }
                            } catch (IndexOutOfBoundsException e) {
                                e.printStackTrace();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        Utils.hideProgressDialog();
                        break;
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void updateData() {


        Handler uiHandler = new Handler(Looper.getMainLooper());
        uiHandler.post(new Runnable() {
            @Override
            public void run() {
                MySharedPref.setString(getActivity(), MyConstants.FIRST_NAME, MyConstants.userProfile.getFirstName());
                MySharedPref.setString(getActivity(), MyConstants.LAST_NAME, MyConstants.userProfile.getLastName());
                MySharedPref.setString(getActivity(), MyConstants.NAME, MyConstants.userProfile.getFirstName() + " " + MyConstants.userProfile.getLastName());
                MySharedPref.setString(getActivity(), MyConstants.PROFILE_IMAGE, MyConstants.userProfile.getUserProfileImage());

                getActivity().onBackPressed();
            }
        });

    }

    @Override
    public void onProgressUpdate(String uniqueMessageId, int progres) {

    }

    @Override
    public void onProgressComplete(String uniqueMessageId, String result, Constant.REQUESTS request) {

    }
}
