package com.plus.me.listener;

import android.view.View;

public interface BlogListener {
    public void onClosetClick(int pos);
    public void onLikeClick(int pos);
}
