package com.plus.me.fragments;


import android.content.Context;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTabHost;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TabWidget;

import com.plus.me.R;
import com.plus.me.custom_views.CustomTextView;
import com.plus.me.ws.Constant;
import com.plus.me.ws.MyConstants;

/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends Fragment {
    public static FragmentTabHost mTabHost;
    public View view;
    public TabWidget tabs;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_home, container, false);

        setHasOptionsMenu(true);

        findViewById();

        initializeData();

        return view;
    }

    private void initializeData() {

        try {

            mTabHost.setup(getActivity(), getChildFragmentManager(), android.R.id.tabcontent);
            mTabHost.getTabWidget().setDividerDrawable(null);
            mTabHost.getTabWidget().setStripEnabled(false);


            mTabHost.addTab(mTabHost.newTabSpec(Constant.TABS.MY_CLOSET.toString())
                    .setIndicator(getTabIndicator(mTabHost.getContext(),
                            R.drawable.tab_my_closet_selector, "My Closet")), MyClosetFragment.class, null);

            mTabHost.addTab(mTabHost.newTabSpec(Constant.TABS.CATEGORIES.toString())
                    .setIndicator(getTabIndicator(mTabHost.getContext(),
                            R.drawable.tab_categories_selector, "Categories")), CategoryFragment.class, null);

            mTabHost.addTab(mTabHost.newTabSpec(Constant.TABS.HOME.toString())
                    .setIndicator(getTabIndicator(mTabHost.getContext(),
                            R.drawable.tab_home_selector, "Home")), MyFeedsFragment.class, null);

            mTabHost.addTab(mTabHost.newTabSpec(Constant.TABS.STORES.toString())
                    .setIndicator(getTabIndicator(mTabHost.getContext(),
                            R.drawable.tab_stores_selector, "Stores")), StoresFragment.class, null);

            Bundle b=new Bundle();

            mTabHost.addTab(mTabHost.newTabSpec(Constant.TABS.VISION_BOARD.toString())
                    .setIndicator(getTabIndicator(mTabHost.getContext(),
                            R.drawable.tab_vision_board_selector, "Create a Look")), CreateALookFragmentNew.class, null);


            if(MyConstants.isFromCreateLook){
                mTabHost.setCurrentTab(0);
            }else {
                mTabHost.setCurrentTab(2);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void findViewById() {
        mTabHost = view.findViewById(R.id.tabhost);
        tabs = view.findViewById(android.R.id.tabs);
        }

    public static View getTabIndicator(Context context, int icon, String tabName) {
        View view = LayoutInflater.from(context).inflate(R.layout.tab_layout, null);

        LinearLayout internalLinear = (LinearLayout) view.findViewById(R.id.internalLinear);
        LinearLayout upperTab = (LinearLayout) view.findViewById(R.id.upperTab);
        ImageView v = (ImageView) view.findViewById(R.id.tab);
        CustomTextView tv = (CustomTextView) view.findViewById(R.id.tvTabName);
        //tvBadgeCount = view.findViewById(R.id.tvBadgeCount);
        tv.setText(tabName);
        v.setBackgroundResource(icon);
        v.setScaleType(ImageView.ScaleType.CENTER_INSIDE);


        /*if (tabName.equalsIgnoreCase("HOME")) {
            tv.setVisibility(View.GONE);
            //tvBadgeCount.setVisibility(View.GONE);
            if (!isTablet()) {
                Log.i("TAG ", "TAG in MOBILE");
                LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
                LinearLayout.LayoutParams layoutParamsImage = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
                upperTab.setPadding(0, 0, 0, 0);
                layoutParamsImage.setMargins((int) getResources().getDimension(R.dimen.margin_8), 0, (int) getResources().getDimension(R.dimen.margin_8), 0);
                v.setLayoutParams(layoutParamsImage);

                v.setScaleType(ImageView.ScaleType.FIT_CENTER);
                internalLinear.setLayoutParams(layoutParams);
                v.invalidate();
                internalLinear.invalidate();
            } else {

                LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
                LinearLayout.LayoutParams layoutParamsImage = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
                upperTab.setPadding(0, 0, 0, 0);
                layoutParamsImage.setMargins((int) getResources().getDimension(R.dimen.margin_8), 0, (int) getResources().getDimension(R.dimen.margin_8), 0);
                v.setLayoutParams(layoutParamsImage);

                v.setScaleType(ImageView.ScaleType.FIT_CENTER);
                internalLinear.setLayoutParams(layoutParams);
                v.invalidate();
                internalLinear.invalidate();
            }

        } else {
            tv.setVisibility(View.VISIBLE);
        }*/

        return view;
    }

   /* @Override
    public void onPrepareOptionsMenu(Menu menu) {
        txtSave.setVisibility(View.GONE);
        menu.getItem(0).setVisible(false);
        menu.getItem(1).setVisible(true);
        menu.getItem(2).setVisible(true);
        menu.getItem(3).setVisible(false);


        super.onPrepareOptionsMenu(menu);
    }*/


}
