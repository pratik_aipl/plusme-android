package com.plus.me.activity;

import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;
import android.widget.TextView;

import com.plus.me.R;
import com.plus.me.model.Categories;
import com.plus.me.model.UserProfile;
import com.plus.me.others.App;
import com.plus.me.utils.FileUtils;
import com.plus.me.utils.MySharedPref;
import com.plus.me.ws.AsyncTaskListner;
import com.plus.me.ws.CallRequest;
import com.plus.me.ws.Constant;
import com.plus.me.ws.JsonParserUniversal;
import com.plus.me.ws.MyConstants;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class SplashActivity extends AppCompatActivity implements AsyncTaskListner {

    private static int SPLASH_TIME_OUT = 2000;
    public String currentVersion = "";
    public int versionCode;
    private TextView tvVersion;
    public JsonParserUniversal jParser;

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        jParser = new JsonParserUniversal();
        tvVersion = findViewById(R.id.tvVersion);

        PackageInfo pInfo = null;
        try {
            pInfo = getPackageManager().getPackageInfo(this.getPackageName(), 0);

            currentVersion = pInfo.versionName;
            versionCode = pInfo.versionCode;

            tvVersion.setText("App Version " + currentVersion);

        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();

        }

        MySharedPref.MySharedPref(getApplicationContext());
        MyConstants.MODEL_NAME = getDeviceName();
        setUserDataFromSharedPreference();
        //  getCatagory();
        // getCompleted();
        //  getUserProfile();
        initializeData();


//        OneSignal.idsAvailable(new OneSignal.IdsAvailableHandler() {
//            @Override
//            public void idsAvailable(String userId, String registrationId) {
//                if (userId != null)
//                    MyConstants.DEVICE_TOKEN = userId;
//                Log.d("debug", "User:" + userId);
//            /*    if (registrationId != null)
//                    MyConstants.DEVICE_ID = registrationId;*/
//                Log.d("debug", "registrationId:" + registrationId);
//            }
//        });


    }


    private void initializeData() {


        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                if (MySharedPref.getIsLogin()) {

                    Intent intent = new Intent(SplashActivity.this, HomeActivity.class);
                    startActivity(intent);
                } else {
                    Intent intent = new Intent(SplashActivity.this, LoginActivity.class);
                    startActivity(intent);
                }
                finish();
            }
        }, SPLASH_TIME_OUT);

    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public void HandelShareIntent() {
        Intent intent = getIntent();
        String action = intent.getAction();
        String type = intent.getType();

        if (Intent.ACTION_SEND.equals(action) && type != null) {
            if ("text/plain".equals(type)) {
                handleSendText(intent); // Handle text being sent
            } else if (type.startsWith("image/")) {
                handleSendImage(intent); // Handle single image being sent
            }
        } else if (Intent.ACTION_SEND_MULTIPLE.equals(action) && type != null) {
            if (type.startsWith("image/")) {
                handleSendMultipleImages(intent); // Handle multiple images being sent
            }
        } else {
            // Handle other intents, such as being started from the home screen
        }


    }

    public void handleSendText(Intent intent) {
        String sharedText = intent.getStringExtra(Intent.EXTRA_TEXT);
        if (sharedText != null) {
            // Update UI to reflect text being shared
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public void handleSendImage(Intent intent) {
        Uri imageUri = (Uri) intent.getParcelableExtra(Intent.EXTRA_STREAM);
        if (imageUri != null) {
            App.intent = "share";
            App.SHARE_URI = FileUtils.getPath(this, imageUri);

            // Update UI to reflect image being shared
        }
    }

    public void handleSendMultipleImages(Intent intent) {
        ArrayList<Uri> imageUris = intent.getParcelableArrayListExtra(Intent.EXTRA_STREAM);
        if (imageUris != null) {


            // Update UI to reflect multiple images being shared
        }
    }

    private void setUserDataFromSharedPreference() {
        if (MySharedPref.getIsLogin()) {
            App.user.setEmail(MySharedPref.getString(this, MyConstants.EMAIL, ""));
            App.user.setAuth(MySharedPref.getString(this, MyConstants.AUTH, ""));
            App.user.setId(MySharedPref.getString(this, MyConstants.USER_ID, ""));
            App.user.setPassword(MySharedPref.getString(this, MyConstants.PASSWORD, ""));
            App.user.setFirstName(MySharedPref.getString(this, MyConstants.FIRST_NAME, ""));
            App.user.setLastName(MySharedPref.getString(this, MyConstants.LAST_NAME, ""));
            App.user.setName(MySharedPref.getString(this, MyConstants.NAME, ""));

            System.out.println("AUTH :::: " + App.user.getAuth());
        }
    }


    private void getCatagory() {
        Map<String, String> map = new HashMap<>();
        map.put("url", MyConstants.BASE_URL + "GetCategoriesList");
        map.put("header", "");
        map.put("Auth", App.user.getAuth());
        map.put("show", "");
        new CallRequest(this).getCatagory(map);
    }

    private void getUserProfile() {
        Map<String, String> map = new HashMap<>();
        map.put("url", MyConstants.BASE_URL + "get_profile");
        map.put("header", "");
        map.put("Auth", App.user.getAuth());
        map.put("show", "");
        new CallRequest(this).getUserProfile(map);
    }

    public String getDeviceName() {
        String manufacturer = Build.MANUFACTURER;
        String model = Build.MODEL;
        if (model.toLowerCase().startsWith(manufacturer.toLowerCase())) {
            return capitalize(model);
        } else {
            return capitalize(manufacturer) + " " + model;
        }
    }


    private String capitalize(String s) {
        if (s == null || s.length() == 0) {
            return "";
        }
        char first = s.charAt(0);
        if (Character.isUpperCase(first)) {
            return s;
        } else {
            return Character.toUpperCase(first) + s.substring(1);
        }
    }

    private void getCompleted() {
        /*new Handler().postDelayed(new Runnable() {

         *//*
         * Showing splash screen with a timer. This will be useful when you
         * want to show case your app logo / company
         *//*

            @Override
            public void run() {
                // This method will be executed once the timer is over
                // Start your app main activity
                System.out.println("going to home");
                Intent i = new Intent(SplashActivity.this, PreLoginActivity.class);
                startActivity(i);

                // close this activity
                finish();
            }
        }, SPLASH_TIME_OUT);*/
        HandelShareIntent();
        Intent i = new Intent(SplashActivity.this, PreLoginActivity.class);
        startActivity(i);
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        try {
            if (result != null && !result.isEmpty()) {
                Log.i("TAG", "TAG Result : " + result);

                switch (request) {
                    case getCategory:
                        MyConstants.categoriesList.clear();
                        MyConstants.categoriesListString.clear();
                        try {
                            JSONObject object = new JSONObject(result);
                            boolean success = object.getBoolean("success");
                            String notificationCount = object.getString("unread_count");
                            App.mnotificationItemCount = Integer.parseInt(notificationCount);
                            if (success) {
                                // TODO parse JSON
                                JSONArray userDataObj = object.getJSONArray("data");
                                for (int i = 0; userDataObj.length() > i; i++) {
                                    MyConstants.category = (Categories) jParser.parseJson(userDataObj.getJSONObject(i), new Categories());
                                    MyConstants.categoriesList.add(MyConstants.category);
                                    MyConstants.categoriesListString.add(MyConstants.category.getCategoryName());
                                }
                            } else {
                                String error_string = object.getString("message");
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                        break;
                    case getUserProfile:
                        try {
                            JSONObject object = new JSONObject(result);
                            boolean success = object.getBoolean("success");
                            if (success) {
                                // TODO parse JSON
                                JSONObject userDataObj = object.getJSONObject("user_data");
                                MyConstants.userProfile = (UserProfile) jParser.parseJson(userDataObj, new UserProfile());

                            } else {
                                String error_string = object.getString("message");
                                //  Toast.makeText(this, error_string, Toast.LENGTH_SHORT).show();
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        getCompleted();
                        // Utils.hideProgressDialog();
                        break;

                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onProgressUpdate(String uniqueMessageId, int progres) {

    }

    @Override
    public void onProgressComplete(String uniqueMessageId, String result, Constant.REQUESTS request) {

    }

}
