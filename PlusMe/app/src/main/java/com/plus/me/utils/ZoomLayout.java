package com.plus.me.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

public class ZoomLayout extends LinearLayout implements ScaleGestureDetector.OnScaleGestureListener {

private enum Mode {
    NONE,
    DRAG,
    ZOOM
}

private static final String TAG = "ZoomLayout";
private static final float MIN_ZOOM = 1.0f;
private static final float MAX_ZOOM = 16.0f;

private Mode mode = Mode.NONE;
private float scale = 1.0f;
private float lastScaleFactor = 0f;

// Where the finger first  touches the screen
private float startX = 0f;
private float startY = 0f;

// How much to translate the canvas
private float dx = 0f;
private float dy = 0f;
private float prevDx = 0f;
private float prevDy = 0f;
ZoomViewListener listener;
public ZoomLayout(Context context) {
    super(context);
    init(context);
    setListner(getListener());
}

public ZoomLayout(Context context, AttributeSet attrs) {
    super(context, attrs);
    init(context);
    setListner(getListener());
}

public ZoomLayout(Context context, AttributeSet attrs, int defStyle) {
    super(context, attrs, defStyle);
    init(context);
    setListner(getListener());

}

public void init(Context context) {
    final ScaleGestureDetector scaleDetector = new ScaleGestureDetector(context, this);
    this.setOnTouchListener(new OnTouchListener() {
        @Override
        public boolean onTouch(View view, MotionEvent motionEvent) {
            switch (motionEvent.getAction() & MotionEvent.ACTION_MASK) {
                case MotionEvent.ACTION_DOWN:






                    /*float eventX = motionEvent.getX();
                    float eventY = motionEvent.getY();
                    float[] eventXY = new float[]{eventX, eventY};

                   //float[] src={motionEvent.getX(),motionEvent.getY()};
                  //float[] dst = new float[2];


                    Matrix invertMatrix = new Matrix();
                    ((ImageView) getChildAt(1)).getImageMatrix().invert(invertMatrix);

                    invertMatrix.mapPoints(eventXY);
                    //invertMatrix.mapPoints(src,dst);
                    int x = Integer.valueOf((int) eventXY[0]);
                    int y = Integer.valueOf((int) eventXY[1]);


                    Log.e("image x", "===img ki x==" + x);
                    Log.e("image y", "===img ki y==" + y);

                    Drawable imgDrawable = ((ImageView) getChildAt(1)).getDrawable();
                    Bitmap bitmap = ((BitmapDrawable) imgDrawable).getBitmap();

                   // int color_value= getHitboxColour(x,y,(ImageView) getChildAt(0));

                   //Limit x, y range within bitmap



                    if (x < 0) {
                        x = 0;
                    } else if (x > bitmap.getWidth() - 1) {
                        x = bitmap.getWidth() - 1;
                    }

                    if (y < 0) {
                        y = 0;
                    } else if (y > bitmap.getHeight() - 1) {
                        y = bitmap.getHeight() - 1;
                    }


                    //Log.e("touched color: ", "" + "#" + Integer.toHexString(color_value));
                   int touchedRGB = bitmap.getPixel(x, y);

                    int redValue = Color.red(touchedRGB);
                    int blueValue = Color.blue(touchedRGB);
                    int greenValue = Color.green(touchedRGB);

                    Log.e("touched color: ", "" + "#" + Integer.toHexString(touchedRGB));

                    listener.onPlaceChosen(touchedRGB);*/

                    Log.i(TAG, "DOWN");
                    if (scale > MIN_ZOOM) {
                        mode = Mode.DRAG;
                        startX = motionEvent.getX() - prevDx;
                        startY = motionEvent.getY() - prevDy;
                    }





                    break;
                case MotionEvent.ACTION_MOVE:
                    if (mode == Mode.DRAG) {
                        dx = motionEvent.getX() - startX;
                        dy = motionEvent.getY() - startY;

                    }
                    break;
                case MotionEvent.ACTION_POINTER_DOWN:
                    mode = Mode.ZOOM;
                    break;
                case MotionEvent.ACTION_POINTER_UP:
                    mode = Mode.DRAG;
                    break;
                case MotionEvent.ACTION_UP:
                    Log.i(TAG, "UP");
                    mode = Mode.NONE;
                    prevDx = dx;
                    prevDy = dy;
                    break;
            }
            scaleDetector.onTouchEvent(motionEvent);

           /* if ((mode == Mode.DRAG && scale >= MIN_ZOOM) || mode == Mode.ZOOM) {
                getParent().requestDisallowInterceptTouchEvent(true);
                float maxDx = (child().getWidth() - (child().getWidth() / scale)) / 2 * scale;
                float maxDy = (child().getHeight() - (child().getHeight() / scale)) / 2 * scale;
                dx = Math.min(Math.max(dx, -maxDx), maxDx);
                dy = Math.min(Math.max(dy, -maxDy), maxDy);
                Log.i(TAG, "Width: " + child().getWidth() + ", scale " + scale + ", dx " + dx
                        + ", max " + maxDx);
                applyScaleAndTranslation();

            }*/

            if (( scale >= MIN_ZOOM) || mode == Mode.ZOOM) {
                getParent().requestDisallowInterceptTouchEvent(true);
                float maxDx = (child().getWidth() - (child().getWidth() / scale)) / 2 * scale;
                float maxDy = (child().getHeight() - (child().getHeight() / scale)) / 2 * scale;
                dx = Math.min(Math.max(dx, -maxDx), maxDx);
                dy = Math.min(Math.max(dy, -maxDy), maxDy);
               // Log.i(TAG, "Width: " + child().getWidth() + ", scale " + scale + ", dx " + dx + ", max " + maxDx);
               // applyScaleAndTranslation();
                child().setScaleX(scale);
                child().setScaleY(scale);

                float maxDx1 = (child2().getWidth() - (child2().getWidth() / scale)) / 2 * scale;
                float maxDy1 = (child2().getHeight() - (child2().getHeight() / scale)) / 2 * scale;
                dx = Math.min(Math.max(dx, -maxDx1), maxDx1);
                dy = Math.min(Math.max(dy, -maxDy1), maxDy1);
               // Log.i(TAG, "Width: " + child2().getWidth() + ", scale " + scale + ", dx " + dx + ", max " + maxDx1);
                // applyScaleAndTranslation();
                child2().setScaleX(scale);
                child2().setScaleY(scale);


            }

            if(mode == Mode.DRAG ){
                getParent().requestDisallowInterceptTouchEvent(true);
                float maxDx = (child().getWidth() - (child().getWidth() / scale)) / 2 * scale;
                float maxDy = (child().getHeight() - (child().getHeight() / scale)) / 2 * scale;
                dx = Math.min(Math.max(dx, -maxDx), maxDx);
                dy = Math.min(Math.max(dy, -maxDy), maxDy);
               // Log.i(TAG, "Width: " + child().getWidth() + ", scale " + scale + ", dx " + dx + ", max " + maxDx);

                child().setTranslationX(dx);
                child().setTranslationY(dy);


                getParent().requestDisallowInterceptTouchEvent(true);
                float maxDx1 = (child2().getWidth() - (child2().getWidth() / scale)) / 2 * scale;
                float maxDy1 = (child2().getHeight() - (child2().getHeight() / scale)) / 2 * scale;
                dx = Math.min(Math.max(dx, -maxDx1), maxDx1);
                dy = Math.min(Math.max(dy, -maxDy1), maxDy1);
               // Log.i(TAG, "Width: " + child2().getWidth() + ", scale " + scale + ", dx " + dx + ", max " + maxDx);

                child2().setTranslationX(dx);
                child2().setTranslationY(dy);
            }



            return true;
        }
    });
}
public int getHitboxColour(int x, int y,ImageView iv) {

   // ImageView iv = (ImageView) findViewById(R.id.img_hitbox);

    Bitmap bmpHotspots;

    int pixel;



   // Fix any offsets by the positioning of screen elements such as Activity titlebar.

   // This part was causing me issues when I was testing out Bill Lahti's code.

    int[] location = new int[2];

    iv.getLocationOnScreen(location);

    x -= location[0];

    y -= location[1];



    // Prevent crashes, return background noise

    if ((x < 0) || (y < 0)) {

        return Color.WHITE;

    }



    // Draw the scaled bitmap into memory

    iv.setDrawingCacheEnabled(true);

    bmpHotspots = Bitmap.createBitmap(iv.getDrawingCache());

    iv.setDrawingCacheEnabled(false);



    pixel = bmpHotspots.getPixel(x, y);

    bmpHotspots.recycle();

    return pixel;

}
// ScaleGestureDetector

@Override
public boolean onScaleBegin(ScaleGestureDetector scaleDetector) {
    Log.i(TAG, "onScaleBegin");
    return true;
}

@Override
public boolean onScale(ScaleGestureDetector scaleDetector) {
    float scaleFactor = scaleDetector.getScaleFactor();
    Log.i(TAG, "onScale" + scaleFactor);
    if (lastScaleFactor == 0 || (Math.signum(scaleFactor) == Math.signum(lastScaleFactor))) {
        scale *= scaleFactor;
        scale = Math.max(MIN_ZOOM, Math.min(scale, MAX_ZOOM));
        lastScaleFactor = scaleFactor;
    } else {
        lastScaleFactor = 0;
    }
    return true;
}

@Override
public void onScaleEnd(ScaleGestureDetector scaleDetector) {
    Log.i(TAG, "onScaleEnd");
}

private void applyScaleAndTranslation() {
    child().setScaleX(scale);
    child().setScaleY(scale);
    child().setTranslationX(dx);
    child().setTranslationY(dy);




}

private View child() {
    return getChildAt(0);
}


private View child2() {
    return getChildAt(1);
}
public interface ZoomViewListener {

    void onZoomStarted(float zoom, float zoomx, float zoomy);

    void onZooming(float zoom, float zoomx, float zoomy);

    void onZoomEnded(float zoom, float zoomx, float zoomy);

    void onPlaceChosen(int color);
}
public ZoomViewListener getListener() {
    return listener;
}

public void setListner(final ZoomViewListener listener) {
    this.listener = listener;
}}