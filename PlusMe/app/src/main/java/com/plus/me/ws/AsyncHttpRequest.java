package com.plus.me.ws;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.Looper;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import android.util.Log;

import com.androidnetworking.common.ANRequest;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interceptors.GzipRequestInterceptor;
import com.androidnetworking.interfaces.AnalyticsListener;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.androidnetworking.interfaces.UploadProgressListener;
import com.plus.me.activity.LoginActivity;
import com.plus.me.utils.MySharedPref;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.security.cert.CertificateException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Executors;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import okhttp3.OkHttpClient;

/**
 * Created by Krupa Kakkad on 25 June 2018
 */
public class AsyncHttpRequest {


    private static final String TAG = "Demo";
    public Fragment ft;
    public Constant.REQUESTS request;
    public Map<String, String> map;
    public AsyncTaskListner aListner;
    public Constant.POST_TYPE post_type;
    public Context ct;
    public boolean header = false;
    public String token;

    public AsyncHttpRequest(Fragment ft, Constant.REQUESTS request, Constant.POST_TYPE post_type, Map<String, String> map) {
        this.ft = ft;
        this.ct = ft.getActivity();
        this.request = request;
        this.map = map;
        this.aListner = (AsyncTaskListner) ft;
        this.post_type = post_type;

        if (Utils.isNetworkAvailable(ft.getActivity())) {

            if (!map.containsKey("show")) {
                Utils.showSimpleSpinProgressDialog(ft.getActivity(), "Please Wait...");
            }

            if (map.containsKey("header")) {
                header = true;
                token = map.get("Auth");
            }
            //new MyRequest().execute(this.map);

            callMyRequest(this.map);

        } else {
            aListner.onTaskCompleted(null, request);
            Utils.showToast("No Internet, Please try again later", ft.getActivity());
        }

    }

    public AsyncHttpRequest(Context ct, Constant.REQUESTS request, Constant.POST_TYPE post_type, Map<String, String> map) {
        this.ct = ct;
        this.request = request;
        this.map = map;
        this.aListner = (AsyncTaskListner) ct;
        this.post_type = post_type;

        if (Utils.isNetworkAvailable(this.ct)) {
            if (!map.containsKey("show")) {
                Utils.showSimpleSpinProgressDialog(ct, "Please Wait...");
            }

            if (map.containsKey("header")) {
                header = true;
                token = map.get("Auth");
            }
            //new MyRequest().execute(this.map);
            callMyRequest(this.map);
        } else {
            aListner.onTaskCompleted(null, request);
            Utils.showToast("No Internet, Please try again later", this.ct);
        }
    }

    private void callMyRequest(Map<String, String> map) {
        OkHttpClient okHttpClient = new OkHttpClient().newBuilder()
                .addInterceptor(new GzipRequestInterceptor())
                .build();
        final String[] responseBody = {""};
        try {
            switch (post_type) {
                case GET:
                    String tempData = "";
                    String query = map.get("url") + "?";
                    tempData += "\n\n\n\n URL : " + map.get("url");

                    map.remove("url");

                    List<String> values = new ArrayList<String>(map.values());
                    List<String> keys = new ArrayList<String>(map.keySet());

                    for (int i = 0; i < values.size(); i++) {
                        System.out.println();

                        System.out.println(keys.get(i) + "====>" + values.get(i));
                        query = query + keys.get(i) + "=" + values.get(i);
                        tempData += "\n" + keys.get(i) + "====>" + values.get(i);
                        if (i < values.size() - 1) {
                            query += "&";
                        }
                    }
                    System.out.println("URL" + "====>" + query);

                    ANRequest.GetRequestBuilder requestBuilder = new ANRequest.GetRequestBuilder(query);
                    if (header) {
                        // TODO As per API key
                        requestBuilder.addHeaders("Auth", token);
                    }


                    //AndroidNetworking.get(query)
                    //.get("https://fierce-cove-29863.herokuapp.com/getAllUsers/{pageNumber}")
                    //.addPathParameter("pageNumber", "0")
                    //.addQueryParameter("limit", "3")
                    //.addHeaders("token", "1234")
                    //.setTag("test")
                    requestBuilder
                            .setPriority(Priority.LOW)
                            .setTag("test")
                            .setOkHttpClient(getUnsafeOkHttpClient()) // passing a custom okHttpClient
                            .build()
                            .getAsJSONObject(new JSONObjectRequestListener() {
                                @Override
                                public void onResponse(JSONObject response) {
                                    // do anything with response

                                    System.out.println("Response -----> " + response);

                                    responseBody[0] = response.toString();
                                    aListner.onTaskCompleted(responseBody[0], request);
                                }

                                @Override
                                public void onError(ANError error) {
                                    // handle error


                                    aListner.onTaskCompleted(null, request);

                                    System.out.println("Error -----> " + error.getErrorBody());

                                    setErrorDetails(error);
                                }
                            });
                    break;

                case POST:
                    tempData = "";
                    System.out.println("new Requested URL >> " + map.get("url"));
                    tempData += "\n\n\n\n URL : " + map.get("url");

                    for (String key : map.keySet()) {
                        System.out.println(key + "====>" + map.get(key));
                        tempData += "\n" + key + "====>" + map.get(key);
                    }

                    ANRequest.PostRequestBuilder postRequestBuilder = new ANRequest.PostRequestBuilder(map.get("url"));

                    if (header) {
                        // TODO As per API key
                        postRequestBuilder.addHeaders("Auth", token);
                    }

                    //AndroidNetworking.post(map.get("url"))
                    postRequestBuilder
                            .addUrlEncodeFormBodyParameter(map) // posting map
                            .setTag("test")
                            .setOkHttpClient(getUnsafeOkHttpClient()) // passing a custom okHttpClient
                            .setPriority(Priority.MEDIUM)
                            .build()
                            .getAsJSONObject(new JSONObjectRequestListener() {
                                @Override
                                public void onResponse(JSONObject response) {
                                    // do anything with response

                                    System.out.println("Response -----> " + response);

                                    responseBody[0] = response.toString();
                                    aListner.onTaskCompleted(responseBody[0], request);
                                }

                                @Override
                                public void onError(ANError error) {
                                    // handle error
                                    aListner.onTaskCompleted(null, request);

                                    System.out.println("Error -----> " + error.getErrorBody());

                                    Utils.hideProgressDialog();
                                    setErrorDetails(error);
                                }
                            });

                    break;

                case POST_WITH_IMAGE:

                    Map<String, File> fileMap = new HashMap<>();

                    for (String key : map.keySet()) {
                        System.out.println();
                        System.out.println(key + "=123====>" + map.get(key));
                        if (matchKeysForImages(key)) {
                            if (map.get(key) != null) {
                                if (map.get(key).length() > 1) {
                                    String type = "image/png";
                                    File f = new File(map.get(key));

                                    fileMap.put(key, f);
                                }
                            }
                        }

                    }

                    ANRequest.MultiPartBuilder multiPartBuilder = new ANRequest.MultiPartBuilder(map.get("url"));

                    if (header) {
                        // TODO As per API key
                        multiPartBuilder.addHeaders("Auth", token);
                    }

                    //AndroidNetworking.upload(map.get("url"))
                    multiPartBuilder
                            .addMultipartFile(fileMap)
                            .addMultipartParameter(map)
                            .setTag("test")
                            .setOkHttpClient(getUnsafeOkHttpClient()) // passing a custom okHttpClient
                            .setPriority(Priority.HIGH)
                            .setExecutor(Executors.newSingleThreadExecutor()) // setting an executor to get response or completion on that executor thread
                            .build()
                            .setAnalyticsListener(new AnalyticsListener() {
                                @Override
                                public void onReceived(long timeTakenInMillis, long bytesSent, long bytesReceived, boolean isFromCache) {
                                    Log.d(TAG, " timeTakenInMillis : " + timeTakenInMillis);
                                    Log.d(TAG, " bytesSent : " + bytesSent);
                                    Log.d(TAG, " bytesReceived : " + bytesReceived);
                                    Log.d(TAG, " isFromCache : " + isFromCache);
                                }
                            })
                            .setUploadProgressListener(new UploadProgressListener() {
                                @Override
                                public void onProgress(long bytesUploaded, long totalBytes) {
                                    // do anything with progress
                                }
                            })/*.getAsString(new StringRequestListener() {
                        @Override
                        public void onResponse(String response) {
                            Log.d(TAG, "onResponse: "+response);
                        }

                        @Override
                        public void onError(ANError anError) {
                            Log.d(TAG, "onError: "+anError.getErrorBody());
                        }
                    });*/
                            .getAsJSONObject(new JSONObjectRequestListener() {
                                @Override
                                public void onResponse(JSONObject response) {
                                    // do anything with response

                                    System.out.println("Response -----> " + response);

                                    responseBody[0] = response.toString();
                                    aListner.onTaskCompleted(responseBody[0], request);
                                }

                                @Override
                                public void onError(ANError error) {
                                    // handle error

                                    if (error.getErrorCode() != 0) {
                                        // received ANError from server
                                        // error.getErrorCode() - the ANError code from server
                                        // error.getErrorBody() - the ANError body from server
                                        // error.getErrorDetail() - just a ANError detail
                                        Log.d(TAG, "onError errorCode : " + error.getErrorCode());
                                        Log.d(TAG, "onError errorBody : " + error.getErrorBody());
                                        Log.d(TAG, "onError errorDetail : " + error.getErrorDetail());
                                    } else {
                                        // error.getErrorDetail() : connectionError, parseError, requestCancelledError
                                        Log.d(TAG, "onError errorDetail : " + error.getErrorDetail());
                                    }

                                    aListner.onTaskCompleted(null, request);
                                    Log.d(TAG, "onError errorCode : " + error.getErrorCode());
                                    Log.d(TAG, "onError errorBody : " + error.getErrorBody());
                                    Log.d(TAG, "onError errorDetail : " + error.getErrorDetail());

                                    Log.d(TAG, "onError StackTrace : " + error.getStackTrace());
                                    Log.d(TAG, "onError Response : " + error.getResponse());
                                    Log.d(TAG, "onError LocalizedMessage : " + error.getLocalizedMessage());

                                    Log.d(TAG, "onError Message : " + error.getMessage());
                                    Log.d(TAG, "onError Cause : " + error.getCause());


                                    setErrorDetails(error);
                                }
                            });

                    break;




            }

        } catch (Exception e) {
            e.printStackTrace();
            new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
                @Override
                public void run() {
                    // Run your task here
                    Utils.showAlert("Something went wrong", ct);
                }
            }, 1000);
            aListner.onTaskCompleted(null, request);
        }
    }

    private void setErrorDetails(ANError error) {
        String error_body = error.getErrorBody(); // {"status":false,"message":"Invalid access token."}
        try {
            final JSONObject object = new JSONObject(error_body);

            if(object.has("error_code")) {
                final String error_message = object.getString("error_code");
                if (error_message.contains("-1")) {

                    Utils.showToast(object.getString("message"), ct);
              /*fdsafds*/
                    MySharedPref.setIsLogin(false);
                    MySharedPref.sharedPrefClear(ct);

                    Intent intent = new Intent(ct, LoginActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    ct.startActivity(intent);
                }
            }else {
                new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        // Run your task here
                        try {
                            Log.d(TAG, "run: ooo ooo "+object.getString("message"));
                            Utils.showAlert(object.getString("message"), ct);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, 1000);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public boolean matchKeysForImages(String key) {


        if (key.equalsIgnoreCase("Image1") ||
                key.equalsIgnoreCase("Image2") ||
                key.equalsIgnoreCase("Image3") ||
                key.equalsIgnoreCase("Image4") ||
                key.equalsIgnoreCase("Image5") ||
                key.equalsIgnoreCase("Image6") ||
                key.equalsIgnoreCase("Image7") ||
                key.equalsIgnoreCase("Image8") ||
                key.equalsIgnoreCase("Image9") ||
                key.equalsIgnoreCase("Image10") ||
                key.equalsIgnoreCase("Image11") ||
                key.equalsIgnoreCase("Image12") ||
                key.equalsIgnoreCase("Image13") ||
                key.equalsIgnoreCase("Image14") ||
                key.equalsIgnoreCase("Image15") ||
                key.equalsIgnoreCase("Image16") ||
                key.equalsIgnoreCase("Image17") ||
                key.equalsIgnoreCase("Image18") ||
                key.equalsIgnoreCase("ProfileImage") ||
                key.equalsIgnoreCase("PostImage") ||
                key.equalsIgnoreCase("profile_picture") ||
                key.equalsIgnoreCase("photo_id") ||
                key.equalsIgnoreCase("Image") ||
                key.equalsIgnoreCase("CompanyLogo") ||
                key.equalsIgnoreCase("profile_pic") ||
                key.equalsIgnoreCase("ProfilePic")) {
            return true;
        } else {
            return false;
        }
    }

    private static OkHttpClient getUnsafeOkHttpClient() {
        try {
            // Create a trust manager that does not validate certificate chains
            final TrustManager[] trustAllCerts = new TrustManager[] {
                    new X509TrustManager() {
                        @Override
                        public void checkClientTrusted(java.security.cert.X509Certificate[] chain, String authType) throws CertificateException {
                        }

                        @Override
                        public void checkServerTrusted(java.security.cert.X509Certificate[] chain, String authType) throws CertificateException {
                            try {
                                chain[0].checkValidity();
                            } catch (Exception e) {
                                throw new CertificateException("Certificate not valid or trusted.");
                            }
                        }

                        @Override
                        public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                            return new java.security.cert.X509Certificate[]{};
                        }
                    }
            };

            // Install the all-trusting trust manager
            final SSLContext sslContext = SSLContext.getInstance("SSL");
            sslContext.init(null, trustAllCerts, new java.security.SecureRandom());
            // Create an ssl socket factory with our all-trusting manager
            final SSLSocketFactory sslSocketFactory = sslContext.getSocketFactory();

            OkHttpClient.Builder builder = new OkHttpClient.Builder();
            builder.sslSocketFactory(sslSocketFactory, (X509TrustManager)trustAllCerts[0]);
            builder.hostnameVerifier(new HostnameVerifier() {
                @Override
                public boolean verify(String hostname, SSLSession session) {
                    return true;
                }
            });

            OkHttpClient okHttpClient = builder.build();
            return okHttpClient;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}