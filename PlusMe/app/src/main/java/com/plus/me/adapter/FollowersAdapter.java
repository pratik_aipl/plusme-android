package com.plus.me.adapter;

import android.content.Context;
import android.os.Bundle;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.plus.me.R;
import com.plus.me.activity.HomeActivity;
import com.plus.me.fragments.ProfileFragment;
import com.plus.me.listener.FollowerListener;
import com.plus.me.model.Followers;
import com.plus.me.others.PicassoTrustAll;
import com.squareup.picasso.Callback;

import java.util.ArrayList;

public class FollowersAdapter extends RecyclerView.Adapter<FollowersAdapter.MyViewHolder> {

    Context mContext;
    LayoutInflater inflater;
    ArrayList<Followers> followersArrayList;
    public FollowerListener followerListener;

    public FollowersAdapter(Context mContext, ArrayList<Followers> followersArrayList) {
        this.mContext = mContext;
        this.followersArrayList = followersArrayList;
        setHasStableIds(true);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public FollowersAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_followers, parent, false);

        return new FollowersAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final FollowersAdapter.MyViewHolder holder, final int position) {
        final Followers followers = followersArrayList.get(position);

        holder.tvName.setText(followers.getFirstName() + " " + followers.getLastName());
        holder.txtProfetion.setText(followers.getRegisterUserTypeByName());

        try {
            PicassoTrustAll.getInstance(mContext)
                    .load(followers.getProfileImage())
                    .placeholder(R.drawable.avatar)
                    .error(R.drawable.avatar)
                    .into(holder.ivImage, new Callback() {
                        @Override
                        public void onSuccess() {
                        }

                        @Override
                        public void onError() {
                        }
                    });
        } catch (Exception e) {
            e.printStackTrace();
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ProfileFragment profileFragment=new ProfileFragment();
                Bundle bundle=new Bundle();
                bundle.putString("intent","profile");
                bundle.putString("id",followers.getId());
                profileFragment.setArguments(bundle);
                HomeActivity.replaceFragment(profileFragment,false);
            }
        });
    }

    @Override
    public int getItemCount() {
        return followersArrayList.size();
    }

    public Followers getItem(int position) {
        return followersArrayList.get(position);
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        public ImageView ivImage;
        public TextView tvName, txtProfetion;

        public MyViewHolder(View itemView) {
            super(itemView);
            tvName = itemView.findViewById(R.id.txtName);
            ivImage = itemView.findViewById(R.id.ivProfile);
            txtProfetion = itemView.findViewById(R.id.txtProfetion);
        }
    }
}
