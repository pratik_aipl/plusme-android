package com.plus.me.adapter;

import android.content.Context;
import android.os.Bundle;
import androidx.recyclerview.widget.RecyclerView;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.plus.me.R;
import com.plus.me.fragments.BlogDetailFragment;
import com.plus.me.fragments.MyFeedDetailFragment;
import com.plus.me.fragments.ProfileFragment;
import com.plus.me.model.Notification;
import com.plus.me.others.App;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import static com.plus.me.activity.HomeActivity.replaceFragment;

public class NotificationAdapter extends RecyclerView.Adapter<NotificationAdapter.MyViewHolder> {

    Context mContext;
    ArrayList<Notification> myNotificationList;
    private List<Notification> categoryFilterList = new ArrayList<>();

    public NotificationAdapter(Context mContext, ArrayList<Notification> myNotificationList) {
        this.mContext = mContext;
        this.myNotificationList = myNotificationList;
        this.categoryFilterList.addAll(myNotificationList);
        setHasStableIds(true);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public NotificationAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_notification, parent, false);

        return new NotificationAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final NotificationAdapter.MyViewHolder holder, final int position) {
        final Notification notification = myNotificationList.get(position);
        holder.txtNotiDesc.setText(notification.getMessage());

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");

        Date myDate = null;
        try {
            myDate = dateFormat.parse(notification.getCreatedDate());

        } catch (ParseException e) {
            e.printStackTrace();
        }
        // String dayOfTheWeek = (String) DateFormat.format("EEEE", date); // Thursday
        String day = (String) DateFormat.format("dd", myDate); // 20
        String monthString = (String) DateFormat.format("MMMM", myDate); // Jun
        // String monthNumber  = (String) DateFormat.format("MM",   date); // 06
        String year = (String) DateFormat.format("yyyy", myDate);
        holder.txtDate.setText(monthString + " " + day + ", " + year);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (notification.getType().equals("6")) {
                    ProfileFragment profileFragment = new ProfileFragment();
                    Bundle bundle = new Bundle();
                    bundle.putString("intent", "profile");
                    bundle.putString("id", notification.getTypeID());
                    profileFragment.setArguments(bundle);
                    replaceFragment(profileFragment, false);
                    App.noti_type = "";
                    App.noti_redirect_id = "";
                } else if (notification.getType().equals("8")) {
                    MyFeedDetailFragment fragment = new MyFeedDetailFragment();
                    Bundle b = new Bundle();
                    b.putString("intent", "id");
                    b.putSerializable("id", notification.getTypeID());
                    fragment.setArguments(b);
                    replaceFragment(fragment, false);
                } else if (notification.getType().equals("9")) {
                    MyFeedDetailFragment fragment = new MyFeedDetailFragment();
                    Bundle b = new Bundle();
                    b.putString("intent", "id");
                    b.putSerializable("id", notification.getTypeID());
                    fragment.setArguments(b);
                    replaceFragment(fragment, false);

                } else if (notification.getType().equals("12")) {
                    MyFeedDetailFragment fragment = new MyFeedDetailFragment();
                    Bundle b = new Bundle();
                    b.putString("intent", "id");
                    b.putSerializable("id", notification.getTypeID());
                    fragment.setArguments(b);
                    replaceFragment(fragment, false);
                } else if (notification.getType().equals("10")) {
                    BlogDetailFragment fragment = new BlogDetailFragment();
                    Bundle b = new Bundle();
                    b.putString("intent", "blog");
                    b.putSerializable("blog", notification.getTypeID());
                    b.putSerializable("user", notification.getReceiverId());
                    fragment.setArguments(b);
                    replaceFragment(fragment, false);
                } /*else if (notification.getType().equals("11")) {
                    MyFeedDetailFragment fragment = new MyFeedDetailFragment();
                    Bundle b = new Bundle();
                    b.putString("intent", "feeds");
                    b.putSerializable("feeds", notification.getTypeID());
                    fragment.setArguments(b);
                    replaceFragment(fragment, false);
                }else if (notification.getType().equals("12")) {
                    MyFeedDetailFragment fragment = new MyFeedDetailFragment();
                    Bundle b = new Bundle();
                    b.putString("intent", "feeds");
                    b.putSerializable("feeds", notification.getTypeID());
                    fragment.setArguments(b);
                    replaceFragment(fragment, false);
                }*/
            }
        });

    }

    @Override
    public int getItemCount() {
        return myNotificationList.size();
    }

    public Notification getItem(int position) {
        return myNotificationList.get(position);
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {

        public TextView txtNotiDesc, txtDate;

        public MyViewHolder(View itemView) {
            super(itemView);
            txtNotiDesc = itemView.findViewById(R.id.txtNotiDesc);
            txtDate = itemView.findViewById(R.id.txtDate);
        }
    }

    public void filters(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());
        myNotificationList.clear();
        if (charText.length() == 0) {
            myNotificationList.addAll(categoryFilterList);
        } else {
            for (Notification bean : categoryFilterList) {
                String Contanint = bean.getMessage();
                if (Contanint.toLowerCase(Locale.getDefault())
                        .contains(charText)) {
                    myNotificationList.add(bean);
                }
            }
        }
        notifyDataSetChanged();
    }
}
