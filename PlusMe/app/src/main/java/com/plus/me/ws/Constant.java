package com.plus.me.ws;


public class Constant {
    public static String image = "image";
    public static String video = "video";
    public static String audio = "audio";
    public static String content = "content";
    public static String file = "file";

    public enum POST_TYPE {
        GET, POST, POST_WITH_IMAGE, POST_WITH_JSON;
    }

    public enum TABS {
        VISION_BOARD, STORES, ADD_OWN, CATEGORIES, MY_CLOSET, HOME;
    }

    public enum REQUESTS {
        login, Report, register, forgotPassword, changePassword, getBlogList, getBlogDetail, getMyClosetList, getMyFeedsList, getCategoryList,
        getStoreList, getNotificationList, logout, getUserProfile, editUserProfile, getMyPosts, getDeletePost,getDeleteBlog, getDeleteVisionPost, getMyFollowers, getMyFollowing, addNewPost,
        getCategory,Isprivate, addToCloset, followUnfollow, addComment, getCountry, getState, getCity, getDesigner, getStylist, getLike, testLogin,
        allowNotification, moveTocloset, addToVisionBoard, getVisionBoardList, RemoveMyCloset, addNewBlog, getCommentDetail, isPrivacy, addCompanyStore;


    }
}
