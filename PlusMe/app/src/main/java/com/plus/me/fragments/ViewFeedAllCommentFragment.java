package com.plus.me.fragments;


import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;

import com.plus.me.R;
import com.plus.me.adapter.BlogDetailAdapter;
import com.plus.me.model.CommentDetail;
import com.plus.me.utils.VerticalSpacingDecoration;

import static com.plus.me.activity.HomeActivity.toolbar;
import static com.plus.me.activity.HomeActivity.txtSave;

/**
 * A simple {@link Fragment} subclass.
 */
public class ViewFeedAllCommentFragment extends Fragment {


    public ViewFeedAllCommentFragment() {
        // Required empty public constructor
    }
    private RecyclerView rvViewAllComment;
    private CommentDetail blogDetail;
    BlogDetailAdapter blogDetailAdapter;
    View view;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view= inflater.inflate(R.layout.fragment_view_feed_all_comment, container, false);
        setHasOptionsMenu(true);
        rvViewAllComment=view.findViewById(R.id.rvComment);
        setData();
        return view;
    }
    private void setData() {
        toolbar.setText("Comments");
        System.out.println("Blog list size==" + MyFeedDetailFragment.commentDetailsList.size());
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        rvViewAllComment.setLayoutManager(layoutManager);
        blogDetailAdapter = new BlogDetailAdapter(getActivity(), MyFeedDetailFragment.commentDetailsList);
        rvViewAllComment.setHasFixedSize(true);
        rvViewAllComment.addItemDecoration(new VerticalSpacingDecoration(20));
        rvViewAllComment.setItemViewCacheSize(20);
        rvViewAllComment.setDrawingCacheEnabled(true);
        rvViewAllComment.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
        rvViewAllComment.setAdapter(blogDetailAdapter);
        }
    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        txtSave.setVisibility(View.GONE);
        menu.getItem(0).setVisible(true);
        menu.getItem(1).setVisible(false);
        menu.getItem(2).setVisible(false);
        menu.getItem(3).setVisible(false);
        menu.getItem(4).setVisible(false);

        super.onPrepareOptionsMenu(menu);
    }
}
