package com.plus.me.fragments;


import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import androidx.core.view.MenuItemCompat;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.plus.me.R;
import com.plus.me.activity.HomeActivity;
import com.plus.me.adapter.CatagoryPopupAdapter;
import com.plus.me.adapter.NothingSelectedSpinnerAdapter;
import com.plus.me.adapter.VisionBoardAdapter;
import com.plus.me.listener.CatagoriesPopupListener;
import com.plus.me.listener.GetItemPosition;
import com.plus.me.model.Categories;
import com.plus.me.model.MyCloset;
import com.plus.me.model.VisionBoard;
import com.plus.me.others.App;
import com.plus.me.others.Internet;
import com.plus.me.others.PicassoTrustAll;
import com.plus.me.utils.VerticalSpacingDecoration;
import com.plus.me.ws.AsyncTaskListner;
import com.plus.me.ws.CallRequest;
import com.plus.me.ws.Constant;
import com.plus.me.ws.JsonParserUniversal;
import com.plus.me.ws.MyConstants;
import com.plus.me.ws.Utils;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import static android.app.Activity.RESULT_OK;
import static android.view.View.GONE;
import static com.plus.me.activity.HomeActivity.ivDown;
import static com.plus.me.activity.HomeActivity.replaceFragment;
import static com.plus.me.activity.HomeActivity.setupBadge;
import static com.plus.me.activity.HomeActivity.toolbar;
import static com.plus.me.activity.HomeActivity.txtSave;

public class VisionBoardLatestFragment extends BaseFragment implements AsyncTaskListner, CatagoriesPopupListener, GetItemPosition {
    private static final String TAG = "VisionBoardLatestFragme";
    public ArrayList<Categories> categoriesList = new ArrayList<>();

    public VisionBoardLatestFragment() {
        // Required empty public constructor
    }

    public static Dialog dialog, dialog1;
    private LinearLayout llCamera, llGallery;
    private View view;
    private RecyclerView rvImages;
    private LinearLayoutManager layoutManager;
    public ArrayList<VisionBoard> visionBoards = new ArrayList<>();
    private ArrayList<MyCloset> myClosetList = new ArrayList<>();
    private String catagory;
    public JsonParserUniversal jParser;
    public VisionBoardAdapter myClosetAdapter;
    public TextView txtNoValue;
    public Uri selectedUri;
    public String filePath;
    Dialog dialogMyVisionboard;
    public LinearLayout llFromMyCloset;
    public LinearLayout llCollageMaker;
    //Dialog dialogZoomImage;
    private String categoryId = "";
    private PopupWindow popup;
    public RecyclerView rvCatagoryFilter;
    CatagoryPopupAdapter categoriesAdapter;
    int removeposition;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_vision_board_latest, container, false);

        setHasOptionsMenu(true);

        bindWidgetReference(view);

        bindWidgetEvents();

        initializeData();

        //  setData();

        return view;
    }

    private void getData() {
        Map<String, String> map = new HashMap<>();
        map.put("url", MyConstants.BASE_URL + "GetMyVisionBoardList");
        map.put("header", "");
        map.put("CategoryID", categoryId);
        map.put("Auth", App.user.getAuth());
        Log.d(TAG, "getData: " + MyConstants.USER_ID);
        if (!Internet.isAvailable(getActivity())) {
            Internet.showAlertDialog(getActivity(), "Error!", "No Internet Connection", false);
        } else {
            new CallRequest(VisionBoardLatestFragment.this).getVisionBoardList(map);
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
        inflater = getActivity().getMenuInflater();
        inflater.inflate(R.menu.menu_vision_board_new, menu);

        final MenuItem menuItem = menu.findItem(R.id.item_notification);
        final MenuItem menuadd = menu.findItem(R.id.item_add_new);
        menuadd.setVisible(true);
        View actionView = MenuItemCompat.getActionView(menuItem);
        HomeActivity.tvNotificationItemCount = actionView.findViewById(R.id.notification_badge);
        FrameLayout frameNotification = actionView.findViewById(R.id.frameNotification);
        frameNotification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                replaceFragment(new NotificationFragment(), false);
            }
        });
        setupBadge();
    }


    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        txtSave.setVisibility(View.GONE);
        toolbar.setClickable(true);
        ivDown.setVisibility(View.VISIBLE);

        MyConstants.editImageUrl = "";
        getData();
        toolbar.setText("Vision Board");

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

            /*if (id == R.id.item_home) {
                replaceFragment(new HomeFragment(),false);
            } else*/

        if (id == R.id.item_home) {
            MyConstants.isFromCreateLook = false;
            replaceFragment(new HomeFragment(), false);
        }
        if (id == R.id.item_add_new) {

            openImageChooserDialog();
            return true;
        } else if (id == R.id.item_notification) {
            System.out.println("******going notification*********");
            replaceFragment(new NotificationFragment(), false);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    private void openImageChooserDialog() {
        dialog = new Dialog(getActivity(), R.style.ChatDialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        // Include dialog.xml file
        dialog.setContentView(R.layout.dialog_add_image_tovisionboard);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        lp.copyFrom(window.getAttributes());
        //This makes the dialog take up the full width
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
        dialog.show();

        initDialogComponents();
    }


    private void initDialogComponents() {
        // llCamera = dialog.findViewById(R.id.llCamera);
        llGallery = dialog.findViewById(R.id.llGallery);
        llFromMyCloset = dialog.findViewById(R.id.llFromMyCloset);
        //  llCollageMaker = dialog.findViewById(R.id.llCollageMaker);
        //  llCollageMaker.setVisibility(GONE);


        llGallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                App.isFromCreateALook = false;
                App.isFromNewPost = false;
                App.isFromEditProfile = false;
                App.isAppFromVisionBoard = true;
                CropImage.startPickImageActivity(getActivity());
                dialog.dismiss();
            }
        });


        llFromMyCloset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                replaceFragment(new SelectClosetImageFragment(), false);
                dialog.dismiss();
            /*    dialog.dismiss();
                flCropImage.setVisibility(View.GONE);
                mLayout.setVisibility(View.VISIBLE);
                isCollageMaker = false;
                openImageChooserDialog();*/

            }
        });

    }

    private void setData() {
        getActivity().runOnUiThread(new Runnable() {

            @Override
            public void run() {

                layoutManager = new GridLayoutManager(getActivity(), 3, GridLayoutManager.VERTICAL, false); // MAX NUMBER OF SPACES
                rvImages.setLayoutManager(layoutManager);

                rvImages.setHasFixedSize(true);
                rvImages.addItemDecoration(new VerticalSpacingDecoration(5));
                rvImages.setItemViewCacheSize(20);
                rvImages.setDrawingCacheEnabled(true);
                rvImages.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
                myClosetAdapter = new VisionBoardAdapter(getActivity(), visionBoards, VisionBoardLatestFragment.this);
                rvImages.setAdapter(myClosetAdapter);

                if (visionBoards.size() != 0) {
                    txtNoValue.setVisibility(GONE);
                } else {
                    txtNoValue.setVisibility(View.VISIBLE);
                }

                /*rvImages.addOnItemTouchListener(new RecyclerTouchListener(getContext(), rvImages, new RecyclerTouchListener.ClickListener() {
                    @Override
                    public void onClick(View view, int position) {
                        displayZoomImageDialouge(position);

                    }

                    @Override
                    public void onLongClick(View view, int position) {

                    }
                }));*/
            }
        });


    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            if (MyConstants.imageType == "") {
                openMoveToVisionBoardDialogue(MyConstants.selectedImage);
            }
        }
    }

    private void bindWidgetEvents() {
        jParser = new JsonParserUniversal();
    }

    private void bindWidgetReference(View view) {
        rvImages = view.findViewById(R.id.rvImages);
        txtNoValue = view.findViewById(R.id.txtNoValue);
    }

    public void displayZoomImageDialouge(final int position) {

        final Dialog dialogZoomImage = new Dialog(Objects.requireNonNull(getActivity()));
        dialogZoomImage.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogZoomImage.setCancelable(true);
        dialogZoomImage.setContentView(R.layout.dialog_view_image);
        dialogZoomImage.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        ImageView ivImage = dialogZoomImage.findViewById(R.id.ivImage);
        final ProgressBar progressBar = dialogZoomImage.findViewById(R.id.progressBar);
        Button btnShare = dialogZoomImage.findViewById(R.id.ivEdit);
        Button ivDelete = dialogZoomImage.findViewById(R.id.ivDelete);
        ImageView close = dialogZoomImage.findViewById(R.id.close);
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogZoomImage.dismiss();
            }
        });
        Button btn_Post = dialogZoomImage.findViewById(R.id.btn_Post);

        btnShare.setText("Share");
        btn_Post.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogZoomImage.dismiss();
                displayPostDialouge(position);

            }
        });

        ivDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogZoomImage.dismiss();
                Map<String, String> map = new HashMap<>();
                map.put("url", MyConstants.BASE_URL + "RemoveVisionBoard");
                map.put("header", "");
                map.put("VisionBoardID", visionBoards.get(position).getAddVisionBoardID());
                map.put("Auth", App.user.getAuth());
                removeposition = position;
                new CallRequest(VisionBoardLatestFragment.this).getDeleteVisionPost(map);
                Toast.makeText(getActivity(), "delt", Toast.LENGTH_SHORT).show();

            }

        });

        btnShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Thread thread = new Thread(new Runnable() {

                    @Override
                    public void run() {
                        try {
                            String path = visionBoards.get(position).getPostImage();
                            Bitmap image = null;
                            try {
                                URL url = new URL(path);
                                image = BitmapFactory.decodeStream(url.openConnection().getInputStream());
                            } catch (IOException e) {
                                System.out.println(e);
                            }

                            Bitmap icon = image;
                            Intent share = new Intent(Intent.ACTION_SEND);
                            share.setType("image/jpeg");
                            ContentValues values = new ContentValues();
                            values.put(MediaStore.Images.Media.TITLE, "title");
                            values.put(MediaStore.Images.Media.MIME_TYPE, "image/jpeg");
                            Uri uri = getActivity().getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                                    values);
                            OutputStream outstream;
                            try {
                                outstream = getActivity().getContentResolver().openOutputStream(uri);
                                icon.compress(Bitmap.CompressFormat.JPEG, 100, outstream);
                                outstream.close();
                            } catch (Exception e) {
                                System.err.println(e.toString());
                            }
                            share.putExtra(Intent.EXTRA_STREAM, uri);
                            startActivity(Intent.createChooser(share, "Share Image"));
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });
                thread.start();
            }
        });

        try {
            PicassoTrustAll.getInstance(getContext())
                    .load(visionBoards.get(position).getPostImage())
                    .into(ivImage, new Callback() {
                        @Override
                        public void onSuccess() {
                            progressBar.setVisibility(GONE);
                        }

                        @Override
                        public void onError() {

                        }
                    });
        } catch (Exception e) {
            e.printStackTrace();
        }


        dialogZoomImage.show();
    }

    private void displayPostDialouge(final int position) {

        final Dialog dialogZoomImage = new Dialog(getActivity());
        dialogZoomImage.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogZoomImage.setCancelable(true);
        dialogZoomImage.setContentView(R.layout.dialog_new_post);
        ImageView ivImage = dialogZoomImage.findViewById(R.id.ivImage);
        final ProgressBar progressBar = dialogZoomImage.findViewById(R.id.progressBar);
        Button btn_cancel = dialogZoomImage.findViewById(R.id.btn_cancel);
        Button btn_Post = dialogZoomImage.findViewById(R.id.btn_Post);
        final EditText edtPostTitle = dialogZoomImage.findViewById(R.id.edtPostTitle);
        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogZoomImage.dismiss();
                displayZoomImageDialouge(position);
            }
        });

        btn_Post.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (TextUtils.isEmpty(edtPostTitle.getText().toString())) {
                    edtPostTitle.requestFocus();
                    Utils.showToast("Please Enter Post Title.", getActivity());
                } else {
                    dialogZoomImage.dismiss();
                    Map<String, String> map = new HashMap<>();
                    map.put("url", MyConstants.BASE_URL + "add_new_blog");
                    map.put("PostTitle", edtPostTitle.getText().toString());
                    map.put("CategoryID", visionBoards.get(position).getPostCategoryId());
                    //Todo
                    map.put("Description", "NA");
                    map.put("header", "");
                    map.put("Auth", App.user.getAuth());

                    map.put("ImageType", "1");
                    map.put("image_url", visionBoards.get(position).getPostImage());
                    new CallRequest(VisionBoardLatestFragment.this).addNewPost(map);

                }
            }
        });
        dialogZoomImage.show();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        App.isAppFromVisionBoard = false;

        assert getFragmentManager() != null;
        getFragmentManager().popBackStack();

//        getFragmentManager().getBackStackEntryCount();


    }

    public void startCropImageActivity(Uri imageUri) {
        CropImage.activity(imageUri)
                .setGuidelines(CropImageView.Guidelines.ON)
                .setFixAspectRatio(false)
                .setMultiTouchEnabled(true)
                .start(getActivity());
    }

    @SuppressLint("NewApi")
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CropImage.PICK_IMAGE_CHOOSER_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            Uri imageUri = CropImage.getPickImageResultUri(getActivity(), data);
            if (CropImage.isReadExternalStoragePermissionsRequired(getActivity(), imageUri)) {
                selectedUri = imageUri;
                filePath = selectedUri.getPath();
                requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 0);
                openMoveToVisionBoardDialogue(filePath);
            } else {
                startCropImageActivity(imageUri);
            }
        }
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                selectedUri = result.getUri();
                filePath = selectedUri.getPath();
                System.out.println("file path===" + filePath);
                //   ivPostImage.setImageBitmap(BitmapFactory.decodeFile(filePath));
                openMoveToVisionBoardDialogue(filePath);

            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
            }
        }
    }

    private void openMoveToVisionBoardDialogue(String image) {
        dialogMyVisionboard = new Dialog(getActivity());
        dialogMyVisionboard.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogMyVisionboard.setCancelable(true);
        dialogMyVisionboard.setContentView(R.layout.dialog_image_move_tocloset);
        dialogMyVisionboard.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        final ImageView ivImage = dialogMyVisionboard.findViewById(R.id.ivImage);
        Button btnMoveToVisionBoard = dialogMyVisionboard.findViewById(R.id.btnMoveToCloset);
        Spinner spCatagory = dialogMyVisionboard.findViewById(R.id.spCatagory);

        ArrayAdapter aaCountry = new ArrayAdapter<>(getActivity(), R.layout.spinner_text_black, MyConstants.categoriesListString);
        aaCountry.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spCatagory.setAdapter(new NothingSelectedSpinnerAdapter(
                aaCountry, R.layout.spinner_text_black,
                getContext(), "Select Category"));
        try {
            if (!TextUtils.isEmpty(MyConstants.selectedImage)) {
                Picasso.with(getActivity()).load(MyConstants.selectedImage).into(ivImage);
                Bitmap bitmapImage = getBitmapFromURL(MyConstants.selectedImage);
                image = BitmapToString(bitmapImage);

            } else {
                Bitmap bitmap = BitmapFactory.decodeFile(image);
                ivImage.setImageBitmap(bitmap);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        final String finalImage = image;
        btnMoveToVisionBoard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (catagory != null) {
                    moveToVisionBoard(finalImage);
                    dialogMyVisionboard.dismiss();
                } else {
                    Toast.makeText(getActivity(), "Please Select Category", Toast.LENGTH_SHORT).show();
                }

            }
        });


        spCatagory.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                try {

                    catagory = MyConstants.categoriesList.get(position - 1).getCategoryID();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        dialogMyVisionboard.show();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        MyConstants.selectedImage = "";
    }

    public static String BitmapToString(Bitmap bitmap) {
        try {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, baos);
            byte[] b = baos.toByteArray();
            String temp = Base64.encodeToString(b, Base64.DEFAULT);
            return temp;
        } catch (NullPointerException e) {
            return null;
        } catch (OutOfMemoryError e) {
            return null;
        }
    }

    public static Bitmap getBitmapFromURL(String src) {
        try {
            URL url = new URL(src);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap myBitmap = BitmapFactory.decodeStream(input);
            return myBitmap;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    private void moveToVisionBoard(String selectedOutputPath) {
        Map<String, String> map = new HashMap<>();
        map.put("url", MyConstants.BASE_URL + "MoveToVisionBoard");
        map.put("CategoryID", catagory);
        if (!TextUtils.isEmpty(MyConstants.selectedImage)) {
            map.put("ClosetID", MyConstants.baseImageId);
            map.put("PostImage", "");
        } else {
            map.put("PostImage", selectedOutputPath);
        }
        map.put("header", "");
        map.put("Type", "IMAGE");
        map.put("Auth", App.user.getAuth());
        new CallRequest(VisionBoardLatestFragment.this).addToVisionBoard(map);
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        try {

            Utils.hideProgressDialog();
            if (result != null && !result.isEmpty()) {
                Log.i("TAG", "TAG Result : " + result);

                switch (request) {
                    case getVisionBoardList:
                        visionBoards.clear();
                        try {
                            JSONObject object = new JSONObject(result);
                            boolean success = object.getBoolean("success");
                            if (success) {
                                JSONArray data = object.getJSONArray("data");
                                if (data != null && data.length() > 0) {
                                    for (int i = 0; i < data.length(); i++) {
                                        JSONObject jsonObject = data.getJSONObject(i);
                                        VisionBoard visionBoard = (VisionBoard) jParser.parseJson(jsonObject, new VisionBoard());
                                        visionBoards.add(visionBoard);
                                        //  MyConstants.selectedImage = "";
                                    }
                                    setData();
                                } else {
                                    Utils.showToast(object.getString("message"), getActivity());
                                }

                            } else {
                                Utils.showToast(object.getString("message"), getActivity());

                                getActivity().runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        if (visionBoards.size() == 0) {
                                            visionBoards.clear();
                                            myClosetList.clear();
                                            txtNoValue.setVisibility(View.VISIBLE);
                                            if (myClosetAdapter != null)
                                                myClosetAdapter.notifyDataSetChanged();
                                        } else {
                                            txtNoValue.setVisibility(View.GONE);
                                            if (myClosetAdapter != null)
                                                myClosetAdapter.notifyDataSetChanged();
                                        }
                                    }
                                });
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;
                    case addNewPost:
                        try {
                            JSONObject object = new JSONObject(result);
                            boolean success = object.getBoolean("success");
                            if (success) {
                                // TODO parse JSON
                                Utils.showToast(object.getString("message"), getActivity());
                                MyConstants.selectedImage = "";
                                getActivity().runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        replaceFragment(new HomeFragment(), true);
                                    }
                                });

                            } else {
                                MyConstants.selectedImage = "";
                                Utils.showToast(object.getString("message"), getActivity());
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;
                    case addToVisionBoard:
                        try {
                            JSONObject object = new JSONObject(result);
                            boolean success = object.getBoolean("success");
                            if (success) {
                                // TODO parse JSON
                                Utils.showToast(object.getString("message"), getContext());
                                dialogMyVisionboard.dismiss();
                                //  dialog.dismiss();
                                //  dialogZoomImage.dismiss();
                                MyConstants.selectedImage = "";
                                getData();
                            } else {
                                Utils.showToast(object.getString("message"), getContext());
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;

                    case moveTocloset:
                        try {
                            JSONObject object = new JSONObject(result);
                            boolean success = object.getBoolean("success");
                            if (success) {
                                // TODO parse JSON
                                Utils.showToast(object.getString("message"), getActivity());
                                dialogMyVisionboard.dismiss();
                                dialog.dismiss();
                                //dialogZoomImage.dismiss();
                                getData();
                            } else {
                                Utils.showToast(object.getString("message"), getActivity());
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;

                    case getDeleteVisionPost:
                        try {
                            JSONObject object = new JSONObject(result);
                            boolean success = object.getBoolean("success");
                            if (success) {
                                Toast.makeText(getActivity(), "" + removeposition, Toast.LENGTH_SHORT).show();
                                visionBoards.remove(removeposition);
                                myClosetAdapter.notifyDataSetChanged();
                                if (visionBoards.size() != 0) {
                                    txtNoValue.setVisibility(GONE);
                                } else {
                                    txtNoValue.setVisibility(View.VISIBLE);
                                }

                            } else {

                                String error_string = object.getString("message");
                                Utils.showToast(error_string, getActivity());
                            }

                            setData();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;

                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (!TextUtils.isEmpty(MyConstants.selectedImage)) {
            openMoveToVisionBoardDialogue(MyConstants.selectedImage);
        }
    }

    @Override
    public void onProgressUpdate(String uniqueMessageId, int progres) {

    }

    @Override
    public void onProgressComplete(String uniqueMessageId, String result, Constant.REQUESTS request) {

    }

    @Override
    public void onPopupItemClick(int pos, View v) {
        popup.dismiss();
        if (pos == 0) {
            categoryId = "";
        } else {
            categoryId = categoriesList.get(pos).getCategoryID();
        }
        toolbar.setText(categoriesList.get(pos).getCategoryName());
        getData();
    }

    private void initializeData() {
        myClosetList.clear();
        jParser = new JsonParserUniversal();
        ivDown.setVisibility(View.VISIBLE);
        setActionSpinner(getActivity(), view);
        toolbar.setText("My Closet");
    }


    private void setActionSpinner(final Activity context, View v) {
        toolbar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int[] location = new int[2];
                v.getLocationOnScreen(location);
                int x = location[0];
                int y = location[1];
                LinearLayout viewGroup = (LinearLayout) context.findViewById(R.id.rlMain);
                LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                View layout = layoutInflater.inflate(R.layout.popup_action_filter, viewGroup);
                popup = new PopupWindow(context);
                popup.setContentView(layout);
                popup.setWidth(LinearLayout.LayoutParams.WRAP_CONTENT);
                popup.setHeight(LinearLayout.LayoutParams.WRAP_CONTENT);
                popup.setFocusable(true);
                popup.setBackgroundDrawable(new BitmapDrawable());
                popup.showAtLocation(layout, Gravity.NO_GRAVITY, x, y);
                rvCatagoryFilter = layout.findViewById(R.id.rvCatagory);
                RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
                rvCatagoryFilter.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
                rvCatagoryFilter.setLayoutManager(mLayoutManager);

                // New Implement

                if (categoriesList.size() == 0) {
                    Categories cate = new Categories();
                    cate.setCategoryID("0");
                    cate.setCategoryName("ALL");
                    categoriesList.add(cate);

                    categoriesList.addAll(MyConstants.categoriesList);
                }

                categoriesAdapter = new CatagoryPopupAdapter(getActivity(), categoriesList, VisionBoardLatestFragment.this);
                rvCatagoryFilter.setHasFixedSize(true);
                rvCatagoryFilter.setDrawingCacheEnabled(true);
                //rvCompleteChat.addItemDecoration(new DividerItemDecoration(getActivity(), R.drawable.divider));
                rvCatagoryFilter.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
                rvCatagoryFilter.setAdapter(categoriesAdapter);
            }
        });

        ivDown.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toolbar.performClick();
            }
        });
    }

    @Override
    public void onIteamPos(int pos) {
        displayZoomImageDialouge(pos);
    }
}
