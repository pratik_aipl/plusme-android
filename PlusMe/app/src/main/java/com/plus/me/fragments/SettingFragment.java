package com.plus.me.fragments;


import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.appcompat.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.plus.me.R;
import com.plus.me.activity.AboutActivity;
import com.plus.me.activity.HomeActivity;
import com.plus.me.activity.LoginActivity;
import com.plus.me.activity.TermsConditionActivity;
import com.plus.me.others.App;
import com.plus.me.utils.MySharedPref;
import com.plus.me.ws.AsyncTaskListner;
import com.plus.me.ws.CallRequest;
import com.plus.me.ws.Constant;
import com.plus.me.ws.MyConstants;
import com.plus.me.ws.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import static com.plus.me.activity.HomeActivity.toolbar;
import static com.plus.me.activity.HomeActivity.txtSave;

/**
 * A simple {@link Fragment} subclass.
 */
public class SettingFragment extends Fragment implements AsyncTaskListner {


    public SettingFragment() {
        // Required empty public constructor
    }

    View view;
    String isEnable;
    public TextView txtChangePassword, txtAbout, txtLogout, txtNotification, txtPrivacyPolicy, txtTermsCondition;
    public Switch switchNotification, switchPrivate;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_setting, container, false);
        setHasOptionsMenu(true);
        bindWidgetReference();
        initializeData();
        onClickListener();
        return view;
    }

    private void onClickListener() {
        txtChangePassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                HomeActivity.replaceFragment(new ChangePasswordFragment(), false);
            }
        });
        txtAbout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getActivity(), AboutActivity.class);
                startActivity(i);

                //HomeActivity.replaceFragment(new AboutUsFragment(), false);
            }
        });
        switchNotification.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                allowNotification(isChecked);

            }
        });
        switchPrivate.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                is_private(isChecked);

            }
        });
        txtTermsCondition.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getActivity(), TermsConditionActivity.class);
                startActivity(i);
            }
        });
        txtLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                doLogOut();
            }
        });
    }

    private void allowNotification(boolean isChecked) {

        Map<String, String> map = new HashMap<>();
        map.put("url", MyConstants.BASE_URL + "AllowNotification");

        System.out.println("notification enable===" + App.user.getIsNotificationEnable());
        if (isChecked) {
            isEnable = "1";
            map.put("isNotificationEnable", isEnable);// TODO add category id
        } else {
            isEnable = "0";
            map.put("isNotificationEnable", isEnable);
        }
        map.put("header", "");
        map.put("Auth", App.user.getAuth());

        new CallRequest(SettingFragment.this).isAllowNotification(map);
    }

    private void is_private(boolean isChecked) {

        Map<String, String> map = new HashMap<>();
        map.put("url", MyConstants.BASE_URL + "is_private");

        System.out.println("notification enable===" + App.user.getIsNotificationEnable());
        if (isChecked) {
            map.put("IsPrivate", "1");// TODO add category id
        } else {

            map.put("IsPrivate", "0");
        }
        map.put("header", "");
        map.put("Auth", App.user.getAuth());

        new CallRequest(SettingFragment.this).isPrivacy(map);
    }

    private void initializeData() {
        toolbar.setText("SETTINGS");
        if (App.user.getIsNotificationEnable().equalsIgnoreCase("0")) {
            switchNotification.setChecked(false);
        } else {
            switchNotification.setChecked(true);
        }

        if (MyConstants.userProfile.getIsPrivate().equalsIgnoreCase("0")) {
            switchPrivate.setChecked(false);
        } else {
            switchPrivate.setChecked(true);
        }
    }

    private void doLogOut() {

        new AlertDialog.Builder(getContext())
                .setTitle("Alert!")
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setMessage("Would you like to logout?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        Map<String, String> map = new HashMap<>();
                        map.put("url", MyConstants.BASE_URL + "logout");
                        map.put("header", "");
                        map.put("Auth", App.user.getAuth());

                        new CallRequest(SettingFragment.this).doLogOut(map);
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .show();
    }

    private void bindWidgetReference() {
        txtChangePassword = view.findViewById(R.id.txtChangePassword);
        txtAbout = view.findViewById(R.id.txtAbout);
        txtLogout = view.findViewById(R.id.txtLogout);
        txtNotification = view.findViewById(R.id.txtNotification);
        switchNotification = view.findViewById(R.id.switchNotification);
        txtTermsCondition = view.findViewById(R.id.txtTermsCondition);
        switchPrivate = view.findViewById(R.id.switchPrivate);
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        txtSave.setVisibility(View.GONE);
        menu.getItem(0).setVisible(true);
        menu.getItem(1).setVisible(false);
        menu.getItem(2).setVisible(false);
        menu.getItem(3).setVisible(false);
        menu.getItem(4).setVisible(false);

        super.onPrepareOptionsMenu(menu);
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        try {

            Utils.hideProgressDialog();
            if (result != null && !result.isEmpty()) {
                Log.i("TAG", "TAG Result : " + result);

                switch (request) {
                    case logout:
                        try {
                            JSONObject object = new JSONObject(result);
                            boolean success = object.getBoolean("success");
                            if (success) {
                                Utils.showToast(object.getString("message"), getActivity());
                                MySharedPref.setIsLogin(false);
                                MySharedPref.sharedPrefClear(getActivity());
                                App.isAppFromVisionBoard = false;
                                App.isFromHomeFragment = false;
                                MyConstants.categoriesList.clear();
                                MyConstants.stateList.clear();
                                MyConstants.cityList.clear();
                                MyConstants.countryList.clear();
                                Intent intent = new Intent(getActivity(), LoginActivity.class);
                                startActivity(intent);
                                ActivityCompat.finishAffinity(getActivity());
                            } else {
                                String error_string = object.getString("message");
                                Toast.makeText(getActivity(), error_string, Toast.LENGTH_SHORT).show();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;

                    case allowNotification:
                        try {
                            JSONObject object = new JSONObject(result);
                            boolean success = object.getBoolean("status");
                            if (success) {
                                Utils.showToast(object.getString("message"), getActivity());

                                if (isEnable.equalsIgnoreCase("0")) {
                                    App.user.setIsNotificationEnable("0");


                                } else if (isEnable.equalsIgnoreCase("1")) {
                                    App.user.setIsNotificationEnable("1");

                                }
                                System.out.println("notification enable===" + App.user.getIsNotificationEnable());

                            } else {
                                String error_string = object.getString("message");
                                Toast.makeText(getActivity(), error_string, Toast.LENGTH_SHORT).show();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;

                    case isPrivacy:
                        try {
                            JSONObject object = new JSONObject(result);
                            if (object.getBoolean("success")) {
                                Utils.showToast(object.getString("message"), getActivity());

                                if (switchPrivate.isChecked()) {
                                    MyConstants.userProfile.setIsPrivate("1");


                                } else {
                                    MyConstants.userProfile.setIsPrivate("0");

                                }
                                System.out.println("notification enable===" + App.user.getIsNotificationEnable());

                            } else {
                                String error_string = object.getString("message");
                                Toast.makeText(getActivity(), error_string, Toast.LENGTH_SHORT).show();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;

                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onProgressUpdate(String uniqueMessageId, int progres) {

    }

    @Override
    public void onProgressComplete(String uniqueMessageId, String result, Constant.REQUESTS request) {

    }
}
