package com.plus.me.fragments;


import android.Manifest;
import android.annotation.SuppressLint;
import android.content.pm.PackageManager;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import androidx.annotation.NonNull;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import androidx.fragment.app.Fragment;
import androidx.core.content.ContextCompat;
import androidx.core.view.MenuItemCompat;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.SearchView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

import com.plus.me.R;
import com.plus.me.activity.HomeActivity;
import com.plus.me.adapter.MyFeedAdapter;
import com.plus.me.listener.MyFeedsListener;
import com.plus.me.model.MyFeeds;
import com.plus.me.others.App;
import com.plus.me.others.Internet;
import com.plus.me.utils.VerticalSpacingDecoration;
import com.plus.me.ws.AsyncTaskListner;
import com.plus.me.ws.CallRequest;
import com.plus.me.ws.Constant;
import com.plus.me.ws.JsonParserUniversal;
import com.plus.me.ws.MyConstants;
import com.plus.me.ws.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static com.plus.me.activity.HomeActivity.ivDown;
import static com.plus.me.activity.HomeActivity.replaceFragment;
import static com.plus.me.activity.HomeActivity.setupBadge;
import static com.plus.me.activity.HomeActivity.toolbar;
import static com.plus.me.activity.HomeActivity.txtSave;

/**
 * A simple {@link Fragment} subclass.
 */
public class MyFeedsFragment extends Fragment implements AsyncTaskListner, MyFeedsListener {
    private static final String TAG = "MyFeedsFragment";
    public RecyclerView rvFeeds;
    public JsonParserUniversal jParser;
    public LinearLayout lin_NoValue;
    private MyFeedAdapter myFeedAdapter;
    private LinearLayoutManager layoutManager;
    public Button btn_post;
    private ArrayList<MyFeeds> myFeedsList = new ArrayList<>();
    public int position = 0;
    String uploadpath;
    String fileName = "";
    public static boolean sIsDiskCacheAvailable = false;
    public static File sRootDir = null;
    private static final int PICK_IMAGE_PERMISSION_CODE = 1;
    public SearchView searchView;
    FloatingActionButton fabRefresh;
    private SwipeRefreshLayout mSwipeRefreshLayout;

    @SuppressLint("RestrictedApi")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_my_feeds, container, false);

        setHasOptionsMenu(true);

        bindWidgetReference(view);

        myFeedsList.clear();
        jParser = new JsonParserUniversal();
        // TODO comment addData(); and uncomment getData();
        btn_post.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                replaceFragment(new PostNewFragment(), false);
            }
        });

        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getData();
                mSwipeRefreshLayout.setRefreshing(false);


            }
        });

        fabRefresh.setVisibility(View.GONE);
/*
        fabRefresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getData();
            }
        });*/
        getData();
        return view;
    }


    private void getData() {
        Map<String, String> map = new HashMap<>();
        map.put("url", MyConstants.BASE_URL + "GetFeedsList");
        map.put("header", "");
        map.put("Auth", App.user.getAuth());

        if (!Internet.isAvailable(getActivity())) {
            Internet.showAlertDialog(getActivity(), "Error!", "No Internet Connection", false);
        } else {
            new CallRequest(MyFeedsFragment.this).getMyFeedList(map);
        }
    }

    private void setData() {
        layoutManager = new LinearLayoutManager(getActivity());
        rvFeeds.setLayoutManager(layoutManager);

        myFeedAdapter = new MyFeedAdapter(getActivity(), myFeedsList, MyFeedsFragment.this);
        rvFeeds.setHasFixedSize(true);
        rvFeeds.addItemDecoration(new VerticalSpacingDecoration(20));
        rvFeeds.setItemViewCacheSize(20);
        rvFeeds.setDrawingCacheEnabled(true);
        rvFeeds.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
        rvFeeds.setAdapter(myFeedAdapter);
        rvFeeds.scrollToPosition(position);
    }

    private void bindWidgetReference(View view) {
        rvFeeds = view.findViewById(R.id.rvFeeds);
        btn_post = view.findViewById(R.id.btn_post);
        lin_NoValue = view.findViewById(R.id.lin_NoValue);
        fabRefresh = view.findViewById(R.id.fabrefresh);
        mSwipeRefreshLayout = view.findViewById(R.id.swiperefresh_items);

    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        txtSave.setVisibility(View.GONE);
        toolbar.setClickable(false);
        ivDown.setVisibility(View.GONE);

        toolbar.setText("MY FEEDS");

        MyConstants.editImageUrl = "";
        menu.getItem(0).setVisible(true);
        menu.getItem(1).setVisible(true);
        menu.getItem(2).setVisible(true);
        menu.getItem(3).setVisible(false);
        menu.getItem(4).setVisible(false);

        //tabs.setVisibility(View.VISIBLE);
        super.onPrepareOptionsMenu(menu);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        //super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
        inflater.inflate(R.menu.home, menu);

        searchView = (SearchView) MenuItemCompat.getActionView(menu.findItem(R.id.item_search));
        EditText searchEditText = searchView.findViewById(R.id.search_src_text);
        searchEditText.setTextColor(getResources().getColor(R.color.black));



        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                Log.d(TAG, "onQueryTextChange: " + newText);

                if (myFeedAdapter != null && myFeedsList.size() > 0)
                    myFeedAdapter.getFilter().filter(newText);
                return true;
            }
        });
        final MenuItem menuItem = menu.findItem(R.id.item_notification);


        MenuItem home = menu.findItem(R.id.item_home);
        Drawable icon = getResources().getDrawable(R.drawable.ic_refresh_black_24dp);
        icon.setColorFilter(getResources().getColor(R.color.default_font_colour), PorterDuff.Mode.SRC_IN);
        home.setIcon(icon);


        View actionView = MenuItemCompat.getActionView(menuItem);
        HomeActivity.tvNotificationItemCount = actionView.findViewById(R.id.notification_badge);
        FrameLayout frameNotification = actionView.findViewById(R.id.frameNotification);
        frameNotification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println("Notification====");
                replaceFragment(new NotificationFragment(), false);
            }
        });
        setupBadge();
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.item_home:
                getData();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        try {

            Utils.hideProgressDialog();
            if (result != null && !result.isEmpty()) {
                Log.i("TAG", "TAG Result : " + result);

                switch (request) {
                    case getMyFeedsList:
                        myFeedsList.clear();
                        try {
                            JSONObject object = new JSONObject(result);
                            boolean success = object.getBoolean("success");
                            if (success) {
                                // TODO parse JSON

                                JSONArray data = object.getJSONArray("data");
                                if (data != null && data.length() > 0) {
                                    for (int i = 0; i < data.length(); i++) {
                                        JSONObject jsonObject = data.getJSONObject(i);

                                        MyFeeds myFeeds = (MyFeeds) jParser.parseJson(jsonObject, new MyFeeds());

                                        myFeedsList.add(myFeeds);
                                    }
                                    setData();
                                } else {
                                    //  Utils.showToast(object.getString("message"), getActivity());
                                }

                            } else {
                                String error_string = object.getString("message");
                                // Toast.makeText(getActivity(), error_string, Toast.LENGTH_SHORT).show();
                            }
                            if (myFeedsList.size() == 0) {
                                lin_NoValue.setVisibility(View.VISIBLE);
                            } else {
                                lin_NoValue.setVisibility(View.GONE);
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;

                    case addToCloset:

                        try {
                            JSONObject object = new JSONObject(result);
                            boolean success = object.getBoolean("success");
                            if (success) {
                                // TODO parse JSON
                                Utils.showToast(object.getString("message"), getActivity());
                                getData();

                            } else {
                                Utils.showToast(object.getString("message"), getActivity());
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;
                    case getLike:

                        try {
                            JSONObject object = new JSONObject(result);
                            boolean success = object.getBoolean("success");
                            if (success) {
                                // TODO parse JSON
                                //Utils.showToast(object.getString("message"), getActivity());
                                getData();

                            } else {
                                Utils.showToast(object.getString("message"), getActivity());
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onProgressUpdate(String uniqueMessageId, int progres) {

    }

    @Override
    public void onProgressComplete(String uniqueMessageId, String result, Constant.REQUESTS request) {

    }

    @Override
    public void onClosetClick(int pos) {
        position = pos;
        doCloset(pos);
    }

    private void doCloset(int pos) {
/*        String isCloset = null;
        if (myFeedsList.get(pos).getIsCloset().equalsIgnoreCase("0")) {
            isCloset = "1";
            myFeedsList.get(pos).setIsCloset("1");
        } else {
            isCloset = "0";
            myFeedsList.get(pos).setIsCloset("0");
            myFeedAdapter.notifyDataSetChanged();
        }*/
        if (isReadStorageAllowed()) {

            new DownloadFileFromURL().execute("");
        }

    }

    public boolean isReadStorageAllowed() {

        //If permission is granted returning true
        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE)
                + ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE)
                == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            requestPermissions(new String[]{Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, PICK_IMAGE_PERMISSION_CODE);
            return false;
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == PICK_IMAGE_PERMISSION_CODE) {
            if (grantResults.length > 0) {
                boolean readExternalFile = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                boolean writeExternalFile = grantResults[1] == PackageManager.PERMISSION_GRANTED;

                if (readExternalFile && writeExternalFile) {
                    new DownloadFileFromURL().execute("");
                }
            }
        }
    }

    @Override
    public void onLikeClick(int pos) {
        position = pos;
        doLike(pos);
    }

    private void doLike(int pos) {
        String isLike = null;
        if (myFeedsList.get(pos).getIsLike().equalsIgnoreCase("0")) {
            isLike = "1";
        } else {
            isLike = "0";
        }
        Map<String, String> map = new HashMap<>();
        map.put("url", MyConstants.BASE_URL + "LikePost");
        map.put("PostId", myFeedsList.get(pos).getBlogId()); // TODO add category id
        map.put("isLike", isLike);
        map.put("header", "");
        map.put("Auth", App.user.getAuth());
        new CallRequest(MyFeedsFragment.this).getLike(map);
    }


    /**
     * Background Async Task to download file
     */
    class DownloadFileFromURL extends AsyncTask<String, String, String> {

        /**
         * Before starting background thread Show Progress Bar Dialog
         */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            Utils.showProgressDialog(getActivity());
        }

        /**
         * Downloading file in background thread
         */
        @Override
        protected String doInBackground(String... f_url) {
            int count;

            String convert = myFeedsList.get(position).getPostImage();
            String extantion = convert.substring(convert.lastIndexOf("."));
            fileName = convert.substring(convert.lastIndexOf("/") + 1, convert.lastIndexOf(".")) + extantion;
            uploadpath = getActivity().getExternalCacheDir() + "/" + fileName;
            File file = new File(uploadpath);
            if (file.exists()) {
                return null;
            } else {

                try {
                    URL url = new URL(myFeedsList.get(position).getPostImage());
                    URLConnection conection = url.openConnection();
                    conection.connect();

                    // this will be useful so that you can show a tipical 0-100%
                    // progress bar
                    int lenghtOfFile = conection.getContentLength();

                    // download the file
                    InputStream input = new BufferedInputStream(url.openStream(),
                            8192);

                    // Output stream
                    uploadpath = getActivity().getExternalCacheDir() + "/" + fileName;
                    OutputStream output = new FileOutputStream(uploadpath);

                    byte[] data = new byte[1024];

                    long total = 0;

                    while ((count = input.read(data)) != -1) {
                        total += count;
                        // publishing the progress....
                        // After this onProgressUpdate will be called
                        publishProgress("" + (int) ((total * 100) / lenghtOfFile));

                        // writing data to file
                        output.write(data, 0, count);
                    }

                    // flushing output
                    output.flush();

                    // closing streams
                    output.close();
                    input.close();

                } catch (Exception e) {
                    Log.e("Error: ", e.getMessage());
                }
            }

            return null;
        }

        /**
         * Updating progress bar
         */
        protected void onProgressUpdate(String... progress) {
            // setting progress percentage

        }

        /**
         * After completing background task Dismiss the progress dialog
         **/
        @Override
        protected void onPostExecute(String file_url) {
            Map<String, String> map = new HashMap<>();
            map.put("url", MyConstants.BASE_URL + "MoveToCloset");
            map.put("PostImage", uploadpath);
            map.put("Title", "");
            map.put("BlogId", myFeedsList.get(position).getBlogId());

            map.put("BaseImageID", "0");
            map.put("Type", "IMAGE");
            map.put("header", "");
            map.put("Auth", App.user.getAuth());
            new CallRequest(MyFeedsFragment.this).addToCloset(map);
            // dismiss the dialog after the file was downloaded


        }

    }
}
