package com.plus.me.fragments;


import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.core.view.GravityCompat;
import androidx.core.view.MenuItemCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.SearchView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.plus.me.R;
import com.plus.me.activity.HomeActivity;
import com.plus.me.adapter.DesignerAdapter;
import com.plus.me.model.Designer;
import com.plus.me.others.App;
import com.plus.me.utils.VerticalSpacingDecoration;
import com.plus.me.ws.AsyncTaskListner;
import com.plus.me.ws.CallRequest;
import com.plus.me.ws.Constant;
import com.plus.me.ws.JsonParserUniversal;
import com.plus.me.ws.MyConstants;
import com.plus.me.ws.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static com.plus.me.activity.HomeActivity.replaceFragment;
import static com.plus.me.activity.HomeActivity.toolbar;
import static com.plus.me.activity.HomeActivity.txtSave;

/**
 * A simple {@link Fragment} subclass.
 */
public class DesignersFragment extends Fragment implements AsyncTaskListner {


    public DesignersFragment() {
        // Required empty public constructor
    }

    public ArrayList<Designer> designerList = new ArrayList<>();
    public JsonParserUniversal jParser = new JsonParserUniversal();
    public View view;
    private RecyclerView rlDesigner;
    private TextView txtNoValue;
    private DesignerAdapter designerAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_designers, container, false);
        setHasOptionsMenu(true);
        bindWidgetReference(view);
        intializeData();
        getData();
        return view;
    }

    private void intializeData() {
        HomeActivity.unlockDrawer();
        toolbar.setText("Designers");

        if (MyConstants.isStylistScreen) {
            MyConstants.cityFilterId = "";
            MyConstants.stateFilterId = "";
            MyConstants.zipCode = "";
        }
        MyConstants.isStylistScreen=false;
        MyConstants.isDesignerScreen = true;


    }

    public void getData() {

        Map<String, String> map = new HashMap<>();
        map.put("url", MyConstants.BASE_URL + "GetStylists");
        map.put("Auth", App.user.getAuth());
        map.put("CountryID", MyConstants.countryFilterId);
        map.put("StateID", MyConstants.stateFilterId);
        map.put("CityID", MyConstants.cityFilterId);
        map.put("ZipCode", MyConstants.zipCode);
        map.put("Auth", App.user.getAuth());

        new CallRequest(DesignersFragment.this).getDesigner(map);
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        txtSave.setVisibility(View.GONE);
       /* menu.getItem(0).setVisible(true);
        menu.getItem(1).setVisible(false);
        menu.getItem(2).setVisible(false);
        menu.getItem(3).setVisible(false);*/
        super.onPrepareOptionsMenu(menu);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
        inflater.inflate(R.menu.designer, menu);

        SearchView searchView =
                (SearchView) menu.findItem(R.id.search).getActionView();
        EditText searchEditText = (EditText) searchView.findViewById(R.id.search_src_text);
        searchEditText.setTextColor(getResources().getColor(R.color.black));


        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                if (TextUtils.isEmpty(newText)) {
                    designerAdapter.filters("");
                } else {
                    designerAdapter.filters(newText);

                }
                return true;
            }
        });

        super.onCreateOptionsMenu(menu, inflater);


    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.home) {
            replaceFragment(new HomeFragment(), true);
            return true;
        } else if (id == R.id.search) {

            return true;
        } else if (id == R.id.filter) {
            HomeActivity.openDraer();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void bindWidgetReference(View view) {
        rlDesigner = view.findViewById(R.id.rlDesigner);
        txtNoValue = view.findViewById(R.id.txtNoValue);
    }


    private void setData() {

        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        rlDesigner.setLayoutManager(layoutManager);
        designerAdapter = new DesignerAdapter(getActivity(), designerList);
        rlDesigner.addItemDecoration(new VerticalSpacingDecoration(15));
        rlDesigner.setItemViewCacheSize(20);
        rlDesigner.setDrawingCacheEnabled(true);
        //rvCompleteChat.addItemDecoration(new DividerItemDecoration(getActivity(), R.drawable.divider));
        rlDesigner.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
        rlDesigner.setAdapter(designerAdapter);
        if (designerList.size() == 0) {
            txtNoValue.setVisibility(View.VISIBLE);
        } else {
            txtNoValue.setVisibility(View.GONE);
        }
    }


    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        try {

            Utils.hideProgressDialog();
            if (result != null && !result.isEmpty()) {
                Log.i("TAG", "TAG Result : " + result);

                switch (request) {
                    case getDesigner:
                        designerList.clear();
                        try {
                            JSONObject object = new JSONObject(result);
                            boolean success = object.getBoolean("status");
                            if (success) {
                                // TODO parse JSON
                                JSONArray array = object.getJSONArray("data");

                                for (int i = 0; i < array.length(); i++) {
                                    JSONObject jsonObject = array.getJSONObject(i);
                                    Designer designer = (Designer) jParser.parseJson(jsonObject, new Designer());
                                    designerList.add(designer);
                                }

                            } else {
                                String error_string = object.getString("message");
                                //Utils.showToast(error_string, getActivity());
                            }

                            setData();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onProgressUpdate(String uniqueMessageId, int progres) {

    }

    @Override
    public void onProgressComplete(String uniqueMessageId, String result, Constant.REQUESTS request) {

    }
}
