package com.plus.me.adapter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.plus.me.R;
import com.plus.me.model.Stores;
import com.plus.me.others.PicassoTrustAll;
import com.squareup.picasso.Callback;

import java.util.ArrayList;
import java.util.Locale;

public class StoresAdapter extends RecyclerView.Adapter<StoresAdapter.MyViewHolder> implements Filterable {

    Context mContext;
    ArrayList<Stores> myStoresList;
    ArrayList<Stores> myStoresFilterList = new ArrayList<>();

    public StoresAdapter(Context mContext, ArrayList<Stores> myStoresList) {
        this.mContext = mContext;
        this.myStoresList = myStoresList;
        this.myStoresFilterList = myStoresList;
        setHasStableIds(true);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public StoresAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_stores, parent, false);

        return new StoresAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final StoresAdapter.MyViewHolder holder, final int position) {
        final Stores stores = myStoresList.get(position);
        holder.txtName.setText(stores.getName());
        holder.txtDesc.setText(stores.getStoreDescritption());
        holder.txtLocation.setText(stores.getStoreAddress());
        try {
            PicassoTrustAll.getInstance(mContext)
                    .load(stores.getImage())
                    .into(holder.ivImage, new Callback() {
                        @Override
                        public void onSuccess() {
                        }

                        @Override
                        public void onError() {
                        }
                    });
        } catch (Exception e) {
            e.printStackTrace();
        }
        holder.txtLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Uri gmmIntentUri = Uri.parse("geo:" + stores.getStoreLatitude() + "," + stores.getStoreLongitude() + "?z=10&q=" + stores.getStoreAddress());
                Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                mapIntent.setPackage("com.google.android.apps.maps");
                if (mapIntent.resolveActivity(mContext.getPackageManager()) != null) {
                    mContext.startActivity(mapIntent);
                }


            }
        });

        holder.ivImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (stores.getStoreWebLink() != null) {
                  //  if (stores.getStoreWebLink().startsWith("https://") || stores.getStoreWebLink().startsWith("http://")) {

                    String url =stores.getStoreWebLink().contains("http") ? stores.getStoreWebLink() : "http://" + stores.getStoreWebLink();
                    mContext.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(url)));

                   /* Uri uri = Uri.parse(stores.getStoreWebLink());
                        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                        mContext.startActivity(intent);*/
                   /* } else {
                        Toast.makeText(mContext, "Invalid Url", Toast.LENGTH_SHORT).show();
                    }*/
                } else {
                    Toast.makeText(mContext, "Web Link Not Available...!", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return myStoresList.size();
    }

    public Stores getItem(int position) {
        return myStoresList.get(position);
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {

                String charString = charSequence.toString();

                if (charString.isEmpty()) {

                    myStoresList = myStoresFilterList;
                } else {

                    ArrayList<Stores> filteredList = new ArrayList<>();
                    final String filterPattern = charString.toLowerCase().trim();
                    for (Stores bean : myStoresFilterList) {

                        String Contanint = bean.getName();
                        String address = bean.getStoreAddress();
                        if (Contanint.toLowerCase(Locale.getDefault()).contains(filterPattern) ||
                                address.toLowerCase(Locale.getDefault()).contains(filterPattern)) {
                            filteredList.add(bean);
                        }


                    }

                    myStoresList = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = myStoresList;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                myStoresList = (ArrayList<Stores>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        public ImageView ivImage;
        public TextView txtName, txtDesc, txtLocation;

        public MyViewHolder(View itemView) {
            super(itemView);
            txtName = itemView.findViewById(R.id.txtName);
            txtDesc = itemView.findViewById(R.id.txtDesc);
            txtLocation = itemView.findViewById(R.id.txtLocation);
            ivImage = itemView.findViewById(R.id.ivImage);
        }
    }
}
