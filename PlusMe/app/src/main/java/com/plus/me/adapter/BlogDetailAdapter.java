package com.plus.me.adapter;

import android.content.Context;
import android.os.Bundle;
import androidx.recyclerview.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.github.siyamed.shapeimageview.CircularImageView;
import com.plus.me.R;
import com.plus.me.fragments.ViewAllCommentFragment;
import com.plus.me.model.CommentDetail;
import com.plus.me.others.PicassoTrustAll;
import com.plus.me.utils.Utilities;
import com.plus.me.ws.Utils;
import com.squareup.picasso.Callback;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import static com.plus.me.activity.HomeActivity.replaceFragment;


public class BlogDetailAdapter extends RecyclerView.Adapter<BlogDetailAdapter.MyViewHolder> {

    Context mContext;
    LayoutInflater inflater;
    ArrayList<CommentDetail> blogDetailList;
    private List<CommentDetail> blogFilterList = new ArrayList<>();
    int unicode = 0x1F60A;

    public BlogDetailAdapter(Context mContext, ArrayList<CommentDetail> blogDetailList) {
        this.mContext = mContext;
        this.blogDetailList = blogDetailList;
        this.blogFilterList.addAll(blogDetailList);
        setHasStableIds(true);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_bloag_comment, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final CommentDetail blogList = blogDetailList.get(position);


        holder.tvUserName.setText(blogList.getCommentedByName());
        holder.tvChatTime.setText(Utils.ConvertTime(blogList.getTime()));
        holder.tvComment.setText(Utilities.convertUTF8ToString(blogList.getComment()));
        if (!TextUtils.isEmpty(blogList.getUserProfileImage())) {
            try {
                PicassoTrustAll.getInstance(mContext)
                        .load(blogList.getUserProfileImage())
                        .placeholder(R.drawable.avatar)
                        .error(R.drawable.avatar)
                        .into(holder.ivUser, new Callback() {
                            @Override
                            public void onSuccess() {
                            }

                            @Override
                            public void onError() {

                            }
                        });
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ViewAllCommentFragment fragment = new ViewAllCommentFragment();
                Bundle b = new Bundle();
                b.putString("intent", "blog");
                b.putString("FROM", "FEED");
                b.putString("BLOGID", blogList.getBlogId());
                b.putString("COMMENTID", blogList.getBlogCommentId());
                b.putSerializable("c_id", getItem(position).getBlogCommentId());
                fragment.setArguments(b);
                replaceFragment(fragment, false);
            }
        });

    /*    holder.tvComment.setText(convertUTF8ToString(blogList.getComment()));
        holder.tvName.setText(blogList.getCommentedByName());

        try {
            PicassoTrustAll.getInstance(mContext)
                    .load(blogList.getUserProfileImage())
                    .into(holder.ivUser, new Callback() {
                        @Override
                        public void onSuccess() {

                        }
                        @Override
                        public void onError() {
                        }
                    });
        } catch (Exception e) {
            e.printStackTrace();
        }*/
/*
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ProfileFragment profileFragment=new ProfileFragment();
                Bundle bundle=new Bundle();
                bundle.putString("intent","profile");
                bundle.putString("id",blogList.getUserId());
                profileFragment.setArguments(bundle);
                HomeActivity.replaceFragment(profileFragment,false);

            }
        });*/
    }

    @Override
    public int getItemCount() {
        return blogDetailList.size();
    }

    public CommentDetail getItem(int position) {
        return blogDetailList.get(position);
    }

    public static String convertUTF8ToString(String s) {
        String out = null;
        try {
            out = new String(s.getBytes("ISO-8859-1"), "UTF-8");
        } catch (java.io.UnsupportedEncodingException e) {
            return null;
        }
        return out;
    }

    public void filters(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());
        blogDetailList.clear();
        if (charText.length() == 0) {
            blogDetailList.addAll(blogFilterList);
        } else {
            for (CommentDetail bean : blogFilterList) {
                /*String Contanint = bean.getFirstName();
                if (bean.getFirstName().toLowerCase(Locale.getDefault())
                        .contains(charText)) {
                    blogDetailList.add(bean);
                }*/
            }
        }
        notifyDataSetChanged();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        public CircularImageView ivUser;
        public TextView tvUserName, tvComment, tvChatTime;
        public ProgressBar progressBar;
        public ImageView img_rply;

        public MyViewHolder(View itemView) {
            super(itemView);

            ivUser = itemView.findViewById(R.id.ivUser);
            tvUserName = itemView.findViewById(R.id.tvUserName);
            tvComment = itemView.findViewById(R.id.tvComment);
            tvChatTime = itemView.findViewById(R.id.tvdatetime);
            progressBar = itemView.findViewById(R.id.progressBar);
            img_rply = itemView.findViewById(R.id.img_rply);
        }
    }

    public String getEmojiByUnicode(int unicode) {
        return new String(Character.toChars(unicode));
    }
}
