package com.plus.me.fragments;


import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.text.util.Linkify;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.github.siyamed.shapeimageview.CircularImageView;
import com.plus.me.R;
import com.plus.me.activity.HomeActivity;
import com.plus.me.adapter.BlogCommentAdapter;
import com.plus.me.listener.BlogListener;
import com.plus.me.model.BlogComment;
import com.plus.me.model.BlogList;
import com.plus.me.others.App;
import com.plus.me.others.Internet;
import com.plus.me.others.PicassoTrustAll;
import com.plus.me.utils.AutoImageView;
import com.plus.me.utils.Utilities;
import com.plus.me.ws.AsyncTaskListner;
import com.plus.me.ws.CallRequest;
import com.plus.me.ws.Constant;
import com.plus.me.ws.JsonParserUniversal;
import com.plus.me.ws.MyConstants;
import com.plus.me.ws.Utils;
import com.squareup.picasso.Callback;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import static com.plus.me.activity.HomeActivity.toolbar;
import static com.plus.me.activity.HomeActivity.txtSave;

/**
 * A simple {@link Fragment} subclass.
 */
public class BlogDetailFragment extends Fragment implements AsyncTaskListner, BlogListener {

    public ImageView ivSend;
    public AutoImageView ivImage;
    public CircularImageView ivUser;
    public TextView tvTitle, tvUserName, tvTime, tvDescription, tvCommentCount;
    public ProgressBar progressBar;
    public RecyclerView rvcomment;
    public JsonParserUniversal jParser;
    private EditText etComment;
    public BlogList blogDetail;
    public String comment, intent, id, user_id;

    public BlogDetailFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_blog_detail, container, false);
        setHasOptionsMenu(true);

        if (getArguments() != null) {
            intent = getArguments().getString("intent");
        }
        if (intent != null && intent.equalsIgnoreCase("blog")) {
            if (getArguments() != null) {
                id = getArguments().getString("blog");
                System.out.println("id====" + id);
                user_id = getArguments().getString("user");
            }
        }


        toolbar.setText("Blogs");
        jParser = new JsonParserUniversal();

        bindWidgetReference(view);

        getData();


        return view;
    }


    private void bindWidgetReference(View view) {
        ivImage = view.findViewById(R.id.ivImage);
        ivUser = view.findViewById(R.id.ivUser);

        progressBar = view.findViewById(R.id.progressBar);
        tvTitle = view.findViewById(R.id.tvTitle);
        tvUserName = view.findViewById(R.id.tvUserName);
        tvTime = view.findViewById(R.id.tvTime);
        tvDescription = view.findViewById(R.id.tvDescription);
        tvCommentCount = view.findViewById(R.id.tvCommentCount);
        rvcomment = view.findViewById(R.id.rvcomment);
        etComment = view.findViewById(R.id.etComment);
        ivSend = view.findViewById(R.id.ivSend);

    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        txtSave.setVisibility(View.GONE);
        menu.getItem(0).setVisible(true);
        menu.getItem(1).setVisible(false);
        menu.getItem(2).setVisible(true);
        menu.getItem(3).setVisible(false);
        menu.getItem(4).setVisible(false);

        super.onPrepareOptionsMenu(menu);
    }

    private void getData() {
        Map<String, String> map = new HashMap<>();
        map.put("url", MyConstants.BASE_URL + "UserBlogsListWithDetail");
        map.put("PostedByID", user_id);
        map.put("BlogId", id); // TODO add post id
        map.put("header", "");
        map.put("Auth", App.user.getAuth());

        if (!Internet.isAvailable(getActivity())) {
            Internet.showAlertDialog(getActivity(), "Error!", "No Internet Connection", false);
        } else {
            new CallRequest(BlogDetailFragment.this).getBlogDetail(map);
        }
    }

    private void setDataBlog() {

        tvTitle.setText(Utilities.convertUTF8ToString(blogDetail.getBlogTitle()));
        tvCommentCount.setText(blogDetail.getTotalComment());
        tvDescription.setText(Utilities.convertUTF8ToString(blogDetail.getDescription()));
        tvDescription.setMovementMethod(LinkMovementMethod.getInstance());
        Linkify.addLinks(tvDescription, Linkify.WEB_URLS);
        Utils.getDatesDifferenceInDays(blogDetail.getPostTime(), tvTime);
        tvUserName.setText(Utilities.convertUTF8ToString(blogDetail.getRegisterUserTypeByName()));
        id = blogDetail.getBlogId();


        tvTitle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ProfileFragment profileFragment = new ProfileFragment();
                Bundle bundle = new Bundle();
                bundle.putString("intent", "profile");
                bundle.putString("id", blogDetail.getBlogId());
                profileFragment.setArguments(bundle);
                HomeActivity.replaceFragment(profileFragment, false);
            }
        });

        ivSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                comment = etComment.getText().toString();

                if (TextUtils.isEmpty(comment)) {
                    Utils.showToast("Please Add BlogComment", getActivity());
                } else {
                    addComment();
                }
            }
        });

        try {
            PicassoTrustAll.getInstance(getActivity())
                    .load(blogDetail.getPostImage())
                    .into(ivImage, new Callback() {
                        @Override
                        public void onSuccess() {
                            progressBar.setVisibility(View.GONE);
                        }

                        @Override
                        public void onError() {
                        }
                    });
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            PicassoTrustAll.getInstance(getActivity())
                    .load(blogDetail.getProfileImage())
                    .placeholder(R.drawable.no_image)
                    .error(R.drawable.no_image)
                    .into(ivUser, new Callback() {
                        @Override
                        public void onSuccess() {
                        }

                        @Override
                        public void onError() {
                        }
                    });
        } catch (Exception e) {
            e.printStackTrace();
        }

        BlogCommentAdapter adapter = new BlogCommentAdapter(getActivity(), blogDetail.commentList, BlogDetailFragment.this);

        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        rvcomment.setLayoutManager(layoutManager);
        rvcomment.setHasFixedSize(true);
        rvcomment.setAdapter(adapter);

    }


    private void addComment() {
        Map<String, String> map = new HashMap<>();
        map.put("url", MyConstants.BASE_URL + "add_blogfeed_comment");
        map.put("blog_id", id); // TODO add post id
        map.put("header", "");
        map.put("FROM", "BLOG"); // TODO add post id

        map.put("comment", Utilities.convertStringToUTF8(comment));
        map.put("Auth", App.user.getAuth());

        if (!Internet.isAvailable(getActivity())) {
            Internet.showAlertDialog(getActivity(), "Error!", "No Internet Connection", false);
        } else {
            new CallRequest(BlogDetailFragment.this).addComment(map);
        }

    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        try {

            Utils.hideProgressDialog();
            if (result != null && !result.isEmpty()) {
                Log.i("TAG", "TAG Result : " + result);

                switch (request) {
                    case getBlogDetail:

                        try {
                            JSONObject object = new JSONObject(result);
                            boolean success = object.getBoolean("success");
                            if (success) {
                                // TODO parse JSON
                                JSONArray data = object.getJSONArray("data");
                                if (data != null && data.length() > 0) {
                                    for (int i = 0; i < data.length(); i++) {
                                        JSONObject jsonObject = data.getJSONObject(i);
                                        blogDetail = (BlogList) jParser.parseJson(jsonObject, new BlogList());

                                        JSONArray cData = jsonObject.getJSONArray("comments");
                                        if (data != null && cData.length() > 0) {
                                            for (int j = 0; j < cData.length(); j++) {
                                                JSONObject cObject = cData.getJSONObject(j);
                                                BlogComment commetnList = (BlogComment) jParser.parseJson(cObject, new BlogComment());

                                                JSONArray rData = cObject.getJSONArray("reply");
                                                if (rData != null && rData.length() > 0) {
                                                    for (int k = 0; k < rData.length(); k++) {
                                                        JSONObject rObject = rData.getJSONObject(k);
                                                        BlogComment rplyList = (BlogComment) jParser.parseJson(rObject, new BlogComment());

                                                        commetnList.rplyList.add(rplyList);

                                                    }
                                                }
                                                blogDetail.commentList.add(commetnList);
                                            }
                                        }


                                    }
                                    setDataBlog();
                                } else {
                                    //  Utils.showToast(object.getString("message"), getActivity());
                                }
                            } else {
                                String error_string = object.getString("message");
                                Toast.makeText(getActivity(), error_string, Toast.LENGTH_SHORT).show();

                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;


                    case addComment:

                        try {
                            JSONObject object = new JSONObject(result);
                            boolean success = object.getBoolean("success");
                            if (success) {
                                // TODO parse JSON
                                etComment.setText("");
                                Utils.showToast(object.getString("message"), getActivity());
                                getData();
                            } else {
                                Utils.showToast(object.getString("message"), getActivity());
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;


                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onProgressUpdate(String uniqueMessageId, int progres) {

    }

    @Override
    public void onProgressComplete(String uniqueMessageId, String result, Constant.REQUESTS request) {

    }

    @Override
    public void onClosetClick(int pos) {

    }

    @Override
    public void onLikeClick(int pos) {

    }
}
