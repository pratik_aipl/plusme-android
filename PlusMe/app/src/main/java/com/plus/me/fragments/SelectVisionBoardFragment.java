package com.plus.me.fragments;


import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.plus.me.R;
import com.plus.me.adapter.SelectVisionBoardAdapter;
import com.plus.me.model.VisionBoard;
import com.plus.me.others.App;
import com.plus.me.others.Internet;
import com.plus.me.utils.RecyclerTouchListener;
import com.plus.me.utils.VerticalSpacingDecoration;
import com.plus.me.ws.AsyncTaskListner;
import com.plus.me.ws.CallRequest;
import com.plus.me.ws.Constant;
import com.plus.me.ws.JsonParserUniversal;
import com.plus.me.ws.MyConstants;
import com.plus.me.ws.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import static com.plus.me.activity.HomeActivity.toolbar;
import static com.plus.me.activity.HomeActivity.txtSave;
import static com.plus.me.ws.MyConstants.myVisionBoardList;

/**
 * A simple {@link Fragment} subclass.
 */
public class SelectVisionBoardFragment extends Fragment implements AsyncTaskListner {


    public SelectVisionBoardFragment() {
        // Required empty public constructor
    }

    private RecyclerView rvCloset;
    public View view;
    private LinearLayoutManager layoutManager;
    private SelectVisionBoardAdapter myVisionboardAdapter;
    private String categoryId = "";
    private JsonParserUniversal jParser;
    private TextView txtNoValue;
    public String catagoryId;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_select_vision_board, container, false);
        setHasOptionsMenu(true);

        bindWidgetReference(view);

        bindWidgetEvents();

        initializeData();


        return view;
    }

    private void initializeData() {
        toolbar.setText("Select Visionboard");
        jParser = new JsonParserUniversal();
        catagoryId = getArguments().getString("CatagoryId");
        if (!TextUtils.isEmpty(catagoryId)) {
            getData();
        } else {
            setData();
        }
    }

    private void bindWidgetEvents() {

    }

    private void bindWidgetReference(View view) {
        rvCloset = view.findViewById(R.id.rvVisionboard);
        txtNoValue = view.findViewById(R.id.txtNoValue);
    }

    private void getData() {
        Map<String, String> map = new HashMap<>();
        map.put("url", MyConstants.BASE_URL + "GetMyVisionBoardList");
        map.put("header", "");
        map.put("CategoryID", catagoryId);
        map.put("Auth", App.user.getAuth());
        if (!Internet.isAvailable(getActivity())) {
            Internet.showAlertDialog(getActivity(), "Error!", "No Internet Connection", false);
        } else {
            new CallRequest(SelectVisionBoardFragment.this).getVisionBoardList(map);
        }
    }

    private void setData() {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                layoutManager = new GridLayoutManager(getActivity(), 3, GridLayoutManager.VERTICAL, false); // MAX NUMBER OF SPACES
                rvCloset.setLayoutManager(layoutManager);
                rvCloset.setHasFixedSize(true);
                rvCloset.addItemDecoration(new VerticalSpacingDecoration(5));
                rvCloset.setItemViewCacheSize(20);
                rvCloset.setDrawingCacheEnabled(true);
                rvCloset.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
                myVisionboardAdapter = new SelectVisionBoardAdapter(getActivity(), myVisionBoardList);
                rvCloset.setAdapter(myVisionboardAdapter);

                rvCloset.addOnItemTouchListener(new RecyclerTouchListener(getContext(), rvCloset, new RecyclerTouchListener.ClickListener() {
                    @Override
                    public void onClick(View view, int position) {
                        // dialogViewImage(position);
                        MyConstants.selectedImage = myVisionBoardList.get(position).getPostImage();
                        catagoryId = myVisionBoardList.get(position).getPostCategoryId();
                        // MyConstants.baseImageId=myClosetList.get(position).getClosetID();
                        MyConstants.imageType = "1";

          /*      Bundle b = new Bundle();
                b.putBoolean("isFromCloset", true);
                b.putString("imagePath",myClosetList.get(position).getPostImage() );
                CreateALookFragment crFragment = new CreateALookFragment();
                crFragment.setArguments(b);
                replaceFragment(crFragment,false);*/

                        getActivity().onBackPressed();
                    }

                    @Override
                    public void onLongClick(View view, int position) {

                    }
                }));
            }
        });

    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        txtSave.setVisibility(View.GONE);
        menu.getItem(0).setVisible(true);
        menu.getItem(1).setVisible(false);
        menu.getItem(2).setVisible(true);
        menu.getItem(3).setVisible(false);
        menu.getItem(4).setVisible(false);

    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        try {

            Utils.hideProgressDialog();
            if (result != null && !result.isEmpty()) {
                Log.i("TAG", "TAG Result : " + result);

                switch (request) {
                    case getVisionBoardList:
                        myVisionBoardList.clear();
                        try {
                            JSONObject object = new JSONObject(result);
                            boolean success = object.getBoolean("success");
                            if (success) {
                                JSONArray data = object.getJSONArray("data");
                                if (data != null && data.length() > 0) {
                                    for (int i = 0; i < data.length(); i++) {
                                        JSONObject jsonObject = data.getJSONObject(i);
                                        VisionBoard visionBoard = (VisionBoard) jParser.parseJson(jsonObject, new VisionBoard());
                                        myVisionBoardList.add(visionBoard);
                                    }
                                    setData();
                                } else {
                                    Utils.showToast(object.getString("message"), getActivity());
                                }

                            }
                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    if (myVisionBoardList.size() == 0) {
                                        txtNoValue.setVisibility(View.VISIBLE);
                                    } else {
                                        txtNoValue.setVisibility(View.GONE);
                                    }
                                }
                            });

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onProgressUpdate(String uniqueMessageId, int progres) {

    }

    @Override
    public void onProgressComplete(String uniqueMessageId, String result, Constant.REQUESTS request) {

    }
}
