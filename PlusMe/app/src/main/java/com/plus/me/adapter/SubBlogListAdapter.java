package com.plus.me.adapter;

import android.content.Context;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.plus.me.R;
import com.plus.me.fragments.BlogDetailFragment;
import com.plus.me.model.BlogList;
import com.plus.me.utils.Utilities;
import com.plus.me.ws.Utils;

import java.util.ArrayList;
import java.util.List;

import static com.plus.me.activity.HomeActivity.replaceFragment;

public class SubBlogListAdapter extends RecyclerView.Adapter<SubBlogListAdapter.MyViewHolder>  {
    private static final String TAG = "SubBlogListAdapter";
    public Context mContext;
    public List<BlogList> ArrayLists;

    public SubBlogListAdapter(Context mContext, ArrayList<BlogList> blogLists) {
        this.mContext = mContext;
        this.ArrayLists = blogLists;
        setHasStableIds(true);
    }


    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item__sub_bloag, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        final BlogList blogList = ArrayLists.get(position);

        holder.tv_Tdots.setVisibility(View.GONE);
        String jobModifieDate = blogList.getCreateDate();
        Utils.getDatesDifferenceInDays(jobModifieDate, holder.tvTime);
        holder.tvDescription.setText(Utilities.convertUTF8ToString(blogList.getDescription()));
        holder.tv_title.setText(Utilities.convertUTF8ToString(blogList.getBlogTitle()));
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                BlogDetailFragment fragment = new BlogDetailFragment();
                Bundle b = new Bundle();
                b.putString("intent", "blog");
                b.putSerializable("blog", blogList.getBlogId());
                b.putSerializable("user", blogList.getPostedByID());
                fragment.setArguments(b);
                replaceFragment(fragment, false);
            }
        });

    }

    @Override
    public int getItemCount() {
        return ArrayLists.size();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {

        public TextView tvTime, tvDescription, tv_title,tv_Tdots;

        public MyViewHolder(View itemView) {
            super(itemView);
            tv_title = itemView.findViewById(R.id.tv_title);
            tvTime = itemView.findViewById(R.id.tvTime);
            tvDescription = itemView.findViewById(R.id.tvDescription);
            tv_Tdots = itemView.findViewById(R.id.tv_Tdots);

        }
    }

}