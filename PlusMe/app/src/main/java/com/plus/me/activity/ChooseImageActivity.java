package com.plus.me.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.plus.me.R;
import com.plus.me.custom_views.CropView;
import com.plus.me.utils.UtilFunctions;
import com.plus.me.utils.ZoomLayout;
import com.plus.me.ws.MyConstants;

import java.io.File;
import java.io.IOException;
import java.net.URL;

import static com.plus.me.fragments.CreateALookFragment.subImage;


public class ChooseImageActivity extends Activity {
    private static final String TAG = "ChooseImageActivity";
    public static ZoomLayout llImage;
    public static ChooseImageActivity instance;
    public Bitmap bitmap;
    protected boolean _taken, isCamera;
    View view;
    public static Button btnOk, btnCancle, btnReset;
    boolean iswebP = false;

    @Override
    protected void onResume() {
        super.onResume();

        try {
//            if(isRefresh) {
//                llImage.setVisibility(View.GONE);
//                llImage.removeAllViews();
//                ivCamera.setVisibility(View.VISIBLE);
//                ivGallery.setVisibility(View.VISIBLE);
//            }
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_image);
        instance = this;

        llImage = findViewById(R.id.llImage);
        llImage.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                llImage.init(ChooseImageActivity.this);
                return false;
            }
        });
        btnOk = findViewById(R.id.btnOk);
        btnReset = findViewById(R.id.btnReset);
        btnCancle = findViewById(R.id.btnCancel);
        llImage.removeAllViews();
        llImage.setVisibility(View.VISIBLE);
        String isEdit = getIntent().getExtras().getString("isEdit");

        subImage = getIntent().getExtras().getString("image");
        isCamera = getIntent().getExtras().getBoolean("isCamera");
        Log.d(TAG, "onCreate: " + subImage);
        File image = new File(subImage);
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        Bitmap largeBitmap = BitmapFactory.decodeFile(image.getAbsolutePath(), bmOptions);

        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);

        int height = displayMetrics.heightPixels;
        int width = displayMetrics.widthPixels;

        // Calculates the new dimensions
        //  Last Chnages here 18/12/2019 by ayaz for webp file
     /*   float scale = (float) width / largeBitmap.getWidth();
        int newHeight = (int) Math.round(largeBitmap.getHeight() * scale);*/

        // This is new one
        float scale = (float) width / height;
        int newHeight = (int) Math.round(height * scale);


//         int height = (int) (largeBitmap.getHeight() / 3.5); ///displayMetrics.heightPixels;
//        int width = (int) (largeBitmap.getWidth() / 3.5); // displayMetrics.widthPixels;

        Bitmap bitmap = null;
        if (largeBitmap != null) {
            if (!subImage.contains(".webp")) {
                bitmap = Bitmap.createScaledBitmap(largeBitmap, width, newHeight, true);
                iswebP = false;
            } else {
                bitmap = largeBitmap;
                iswebP = true;
            }
        } else {
            Toast.makeText(ChooseImageActivity.this, "Selected Image is Crouped", Toast.LENGTH_SHORT).show();
        }



       /* if (isCamera) {
            bitmap = Bitmap.createScaledBitmap(largeBitmap, width, height, true);
        } else
            bitmap = largeBitmap;
*/

        System.out.println("image path:::" + subImage);
        if (UtilFunctions.stringIsNotEmpty(subImage)) {


            if (isEdit.equalsIgnoreCase("yes")) {

                UploadAsyncTask uploadAsyncTask = new UploadAsyncTask(this);
                uploadAsyncTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

            } else {
                this.bitmap = bitmap;
                _taken = true;
                System.out.println("bitmap:::" + bitmap);

                addViewToLayout();

                System.gc();
            }


        } else {
            this.bitmap = bitmap;
            _taken = true;
            System.out.println("bitmap:::" + bitmap);

            addViewToLayout();

            System.gc();
        }
    }

    private void addViewToLayout() {

        runOnUiThread(new Runnable() {

            @Override
            public void run() {

                // Stuff that updates the UI
                llImage.setVisibility(View.VISIBLE);
                llImage.addView(new CropView(instance, bitmap, iswebP));
                if (!TextUtils.isEmpty(MyConstants.editImageUrl)) {
                    MyConstants.isEditable = true;
                }
//                bitmap.recycle();
                bitmap = null;
                MyConstants.editImageUrl = "";
            }
        });

    }


    /*private void setViewLayout(int id){
        LayoutInflater inflater = (LayoutInflater) instance.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        view = inflater.inflate(id, null);
        ViewGroup rootView = (ViewGroup) getView();
        rootView.removeAllViews();
        rootView.addView(view);
    }*/
    private class UploadAsyncTask extends AsyncTask<Void, Integer, String> {
        private ProgressDialog mProgressDialog;
        Context context;

        private UploadAsyncTask(Context context) {
            this.context = context;
        }

        @Override
        protected String doInBackground(Void... params) {
            try {
                URL url = new URL(subImage);
                bitmap = BitmapFactory.decodeStream(url.openConnection().getInputStream());

            } catch (IOException e) {
                System.out.println(e);
            }
            return "";
        }

        @Override
        protected void onPreExecute() {
            mProgressDialog = new ProgressDialog(context);
            mProgressDialog.setMessage("Loading.....");
            mProgressDialog.show();
        }

        @Override
        protected void onPostExecute(String result) {
            if (mProgressDialog != null) {
                mProgressDialog.cancel();
                mProgressDialog.dismiss();
                mProgressDialog = null;
            }
            addViewToLayout();

            System.gc();
        }

    }

    @Override
    public void onDestroy() {
        if (bitmap != null) {
            bitmap.recycle();
            bitmap = null;
        }
        System.gc();
        super.onDestroy();

    }
}
