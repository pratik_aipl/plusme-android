package com.plus.me.fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.AssetFileDescriptor;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.media.ExifInterface;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.transition.TransitionManager;
import android.util.Log;
import android.view.View;

import com.plus.me.R;
import com.plus.me.activity.CropActivity;
import com.plus.me.activity.CropMultiActivity;
import com.plus.me.utils.ImageDownloaderCache;
import com.plus.me.utils.UtilFunctions;
import com.plus.me.ws.MyConstants;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;

import static android.app.Activity.RESULT_CANCELED;
import static android.app.Activity.RESULT_OK;
import static com.plus.me.fragments.CreateALookFragment.imgView;
import static com.plus.me.fragments.CreateALookFragment.masterImagePath;
import static com.plus.me.fragments.CreateALookFragment.vSelect;
import static com.plus.me.fragments.CreateALookFragment.workingBitmap;
import static com.plus.me.fragments.CreateALookFragment.workingImagePath;
import static com.plus.me.utils.ImagePicker.minWidthQuality;
import static com.plus.me.ws.MyConstants.INTENT_RESULT_GALLERY;

/**
 * Created by Krupa Kakkad on 28 July 2018
 */
public abstract class MediaFragment extends BaseFragment {

    private Uri selectedImageUri;
    protected String selectedImagePath;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (resultCode) {
            case RESULT_CANCELED:
                break;
            case RESULT_OK:
                try {
                    if (requestCode == GALLERY_INTENT_CALLED || requestCode == CAMERA_CODE || requestCode == GALLERY_KITKAT_INTENT_CALLED) {
                        if (requestCode == GALLERY_INTENT_CALLED) {
                            selectedImageUri = data.getData();
                            selectedImagePath = getPath(selectedImageUri);
                        } else if (requestCode == GALLERY_KITKAT_INTENT_CALLED) {
                            selectedImageUri = data.getData();
                            final int takeFlags = data.getFlags()
                                    & (Intent.FLAG_GRANT_READ_URI_PERMISSION | Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
                            // Check for the freshest data.
                            if (selectedImageUri != null) {
                                getActivity().getContentResolver().takePersistableUriPermission(
                                        selectedImageUri, takeFlags);
                                selectedImagePath = getPath(selectedImageUri);
                            }
                        } else {
                            selectedImagePath = selectedOutputPath;
                        }
                     /*   if (requestCode == CAMERA_CODE) {

                            bitmap = getImageResized(getContext(), selectedImageUri);
                            int rotation = getRotation(getContext(), selectedImageUri, true);
                            bitmap = rotate(bitmap, rotation);
                            _taken = true;
                            onPhotoTaken();
                            bitmap.recycle();
                            System.gc();
                        } else {*/
                        if (UtilFunctions.stringIsNotEmpty(selectedImagePath)) {
                            // decode image size
                         /*   BitmapFactory.Options o = new BitmapFactory.Options();
                            o.inJustDecodeBounds = true;
                            BitmapFactory.decodeFile(selectedImagePath, o);
                            // Find the correct scale value. It should be the power of
                            // 2.
                            int width_tmp = o.outWidth, height_tmp = o.outHeight;
                            Log.d("MediaActivity", "MediaActivity : image size : "
                                    + width_tmp + " ; " + height_tmp);
                            final int MAX_SIZE = getResources().getDimensionPixelSize(
                                    R.dimen.image_loader_post_width);
                            int scale = 1;
                            // while (true) {
                            // if (width_tmp / 2 < MAX_SIZE
                            // || height_tmp / 2 < MAX_SIZE)
                            // break;
                            // width_tmp /= 2;
                            // height_tmp /= 2;
                            // scale *= 2;
                            // }
                            if (height_tmp > MAX_SIZE || width_tmp > MAX_SIZE) {
                                if (width_tmp > height_tmp) {
                                    scale = Math.round((float) height_tmp
                                            / (float) MAX_SIZE);
                                } else {
                                    scale = Math.round((float) width_tmp
                                            / (float) MAX_SIZE);
                                }
                            }
                            Log.d("MediaActivity", "MediaActivity : scaling image by factor : " + scale);
                            BitmapFactory.Options options = new BitmapFactory.Options();
                            options.inSampleSize = scale;*/
                            int rotation = 0;
                            if (requestCode == CAMERA_CODE) {
                                rotation = getRotation(getContext(), selectedImageUri, true);
                            } else {
                                rotation = getRotation(getContext(), selectedImageUri, false);
                            }

                            bitmap = rotate(bitmap, rotation);
                            // bitmap = BitmapFactory.decodeFile(selectedImagePath, options);
                            _taken = true;
                            onPhotoTaken();
                            bitmap.recycle();
                            System.gc();
                        }
                    }
                    //}
                } catch (Exception e) {

                }

                if (requestCode == INTENT_RESULT_GALLERY) {
                    if (data != null) {
                        CreateALookFragment.masterImageList = null;
                        masterImagePath = null;
                        String imgpath = null;
                        if (data.hasExtra("image_path")) {
                            imgpath = data.getStringExtra("image_path");
                        } else if (data.hasExtra("image_path_list")) {
                            CreateALookFragment.masterImageList = data.getStringArrayExtra("image_path_list");
                            if (CreateALookFragment.masterImageList != null && CreateALookFragment.masterImageList.length == 1) {
                                //only one image
                                imgpath = CreateALookFragment.masterImageList[0];
                            }
                        }


                        if (imgpath != null && !imgpath.isEmpty()) {
                            masterImagePath = imgpath;
                            workingBitmap = ImageDownloaderCache.decodeSampledBitmapFromPath(masterImagePath, ImageDownloaderCache.maxImageSize, ImageDownloaderCache.maxImageSize);
                        } else if (CreateALookFragment.masterImageList != null && CreateALookFragment.masterImageList.length > 0) {

                            //  adjust image demisions to fit
                            if (!getActivity().isFinishing()) {
                                ProgressDialog progressDialog1 = ProgressDialog.show(getActivity(), "", "Merging Photos...", true, false);
                                //int borderSize = 10;
                                int borderSize = getActivity().getResources().getDimensionPixelSize(R.dimen.margin_4);
                                int count = CreateALookFragment.masterImageList.length;
                                //don't allow more than 6 images
                                if (count > 6) {
                                    count = 6;
                                }
                                Bitmap[] bitmapList = new Bitmap[count];
                                for (int i = 0; i < count; i++) {
                                    Bitmap bmImage = ImageDownloaderCache.decodeSampledBitmapFromPath(CreateALookFragment.masterImageList[i], ImageDownloaderCache.maxImageSize, ImageDownloaderCache.maxImageSize, false);
                                    if (bmImage != null) {
                                        int imgW = Math.min(bmImage.getWidth(), bmImage.getHeight()) - borderSize;
                                        int imgH = imgW;
                                        if (count <= 3) {
                                            //int dimension = Math.min(bmImage.getWidth(), bmImage.getHeight());
                                            imgW = (ImageDownloaderCache.maxImageSize - ((count - 1) * borderSize)) / count;
                                            imgH = ImageDownloaderCache.maxImageSize;
                                            //bitmapList[i] = ThumbnailUtils.extractThumbnail(bmImage, imgW, imgDownloader.maxImageSize, ThumbnailUtils.OPTIONS_RECYCLE_INPUT);
                                        } else if (count == 4) {
                                            //int dimension = Math.min(bmImage.getWidth(), bmImage.getHeight()) - borderSize;
                                            imgW = ((ImageDownloaderCache.maxImageSize - borderSize) / 2);
                                            imgH = imgW;
                                            //bitmapList[i] = ThumbnailUtils.extractThumbnail(bmImage, imgWH, imgWH, ThumbnailUtils.OPTIONS_RECYCLE_INPUT);
                                        } else if (count == 5) {
                                            if (i < 2) {
                                                imgW = ((ImageDownloaderCache.maxImageSize - borderSize) / 2);
                                                imgH = imgW;
                                                //bitmapList[i] = ThumbnailUtils.extractThumbnail(bmImage, imgWH, imgWH, ThumbnailUtils.OPTIONS_RECYCLE_INPUT);
                                            } else {
                                                imgW = ((ImageDownloaderCache.maxImageSize - borderSize) / 2);
                                                //imgH = (imgDownloader.maxImageSize - (2 * borderSize)) / 3;
                                                //note this formula returns the ceil value instead of the floor - from https://stackoverflow.com/questions/7139382/java-rounding-up-to-an-int-using-math-ceil
                                                int a = (ImageDownloaderCache.maxImageSize - (2 * borderSize));
                                                int b = 3;
                                                imgH = (a - 1) / b + 1;
                                                //bitmapList[i] = ThumbnailUtils.extractThumbnail(bmImage, imgW, imgH, ThumbnailUtils.OPTIONS_RECYCLE_INPUT);
                                            }
                                        } else if (count == 6) {
                                            if (i < 1) {
                                                int img3d = ((ImageDownloaderCache.maxImageSize - borderSize) / 3);
                                                //int a = (imgDownloader.maxImageSize - borderSize);
                                                //int b = 3;
                                                //int img3d = (a - 1) / b + 1;
                                                //imgW = (img3d*2) - borderSize; //size is 2/3
                                                imgW = (img3d * 2); //size is 2/3
                                                //imgH = imgW + borderSize;
                                                //imgW = (img3d*2) + (borderSize / 2); //size is 2/3
                                                //calculate the differnce facter and add it to the large image
                                                int imgW1 = (ImageDownloaderCache.maxImageSize - (2 * borderSize)) / 3;
                                                int dif1 = ImageDownloaderCache.maxImageSize - (imgW + borderSize + imgW1);
                                                //int dif2 = imgDownloader.maxImageSize - ((imgW1*3)+(borderSize*2));
                                                //add the extra to the single large image
                                                //imgW += dif1+dif2;
                                                imgW += dif1;
                                                imgH = imgW;
                                                //if (MyConstants.DEBUG) Log.d(TAG, "borderSize : "+borderSize);
                                                //if (MyConstants.DEBUG) Log.d(TAG, "one third : "+img3d);
                                                //if (MyConstants.DEBUG) Log.d(TAG, "two thirds - border : "+imgW);
                                                //if (MyConstants.DEBUG) Log.d(TAG, "dif1 : "+dif1);
                                                //if (MyConstants.DEBUG) Log.d(TAG, "dif2 : "+dif2);
                                            } else {
                                                imgW = (ImageDownloaderCache.maxImageSize - (2 * borderSize)) / 3;
                                                if (i == 5) {
                                                    int dif2 = ImageDownloaderCache.maxImageSize - ((imgW * 3) + (borderSize * 2));
                                                    if (dif2 > 0) {
                                                        //add extra into last image
                                                        imgW += dif2;
                                                    }
                                                }
                                                //note this formula returns the ceil value instead of the floor - from https://stackoverflow.com/questions/7139382/java-rounding-up-to-an-int-using-math-ceil
                                                //int a = (imgDownloader.maxImageSize - (2 * borderSize));
                                                //int b = 3;
                                                //imgW = (a - 1) / b + 1;
                                                //imgW = (img3d) - (borderSize*2/3);  //size is 1/3;
                                                //imgW = img3d - borderSize;  //size is 1/3;
                                                //imgW = img3d;  //size is 1/3;
                                                imgH = imgW;
                                            }
                                        }
                                        bitmapList[i] = ThumbnailUtils.extractThumbnail(bmImage, imgW, imgH, ThumbnailUtils.OPTIONS_RECYCLE_INPUT);
                                        //if (MyConstants.DEBUG) {
                                        //    Log.d(TAG, "Image : "+(i+1)+" width: " +bitmapList[i].getWidth());
                                        //    Log.d(TAG, "Image : "+(i+1)+" height: " + bitmapList[i].getHeight());
                                        //}
                                    }
                                }
                                //workingBitmap = Bitmap.createBitmap(bitmapList[0].getWidth() * 2, bitmapList[0].getHeight() * 2, Bitmap.Config.ARGB_8888);
                                workingBitmap = Bitmap.createBitmap(ImageDownloaderCache.maxImageSize, ImageDownloaderCache.maxImageSize, Bitmap.Config.ARGB_8888);
                                Canvas canvas = new Canvas(workingBitmap);
                                canvas.drawColor(Color.TRANSPARENT);
                                Paint paint = new Paint();
                                int xPos = 0;
                                int yPos = 0;
                                count = bitmapList.length;
                                for (int i = 0; i < bitmapList.length; i++) {
                                    //if (MyConstants.DEBUG) {
                                    //    Log.d(TAG, "Image : "+(i)+" xPos: " +xPos);
                                    //    Log.d(TAG, "Image : "+(i)+" yPos: " + yPos);
                                    //    //Log.d(TAG, "even? : "+(i % 2 ));
                                    //    Log.d(TAG, "even? : "+(i % 2 ));
                                    //}
                                    //canvas.drawBitmap(bitmapList[i], bitmapList[i].getWidth() * (i % 2), bitmapList[i].getHeight() * (i / 2), paint);
                                    if (bitmapList[i] != null) {
                                        canvas.drawBitmap(bitmapList[i], xPos, yPos, paint);
                                    }

                                    if (count == 4) {

                                        if (i % 2 == 0) {
                                            //move next row
                                            yPos += bitmapList[i].getHeight() + borderSize;
                                        } else {
                                            //move to next column
                                            yPos = 0;
                                            xPos += bitmapList[i].getWidth() + borderSize;
                                        }
                                    } else if (count == 5) {
                                        if (i == 1) {
                                            //move to next column
                                            yPos = 0;
                                            xPos += bitmapList[i].getWidth() + borderSize;
                                        } else {
                                            //move next row
                                            yPos += bitmapList[i].getHeight() + borderSize;
                                        }
                                    } else if (count == 6) {
                                        if (i == 1) {
                                            //move to next column in the same row
                                            xPos += bitmapList[i].getWidth() + borderSize;
                                        } else if (i == 2) {
                                            //move to next column
                                            yPos = 0;
                                            xPos += bitmapList[i].getWidth() + borderSize;
                                            int xPos2 = bitmapList[0].getWidth() + borderSize;
                                            if (xPos2 > xPos) {
                                                xPos = xPos2;
                                            }
                                        } else {
                                            //move next row
                                            yPos += bitmapList[i].getHeight() + borderSize;
                                        }
                                    } else {
                                        //this is for 2 or 3 images
                                        //move to next column
                                        xPos += bitmapList[i].getWidth() + borderSize;
                                    }
                                }

                                if (progressDialog1 != null && progressDialog1.isShowing()) {
                                    progressDialog1.dismiss();
                                }

                            }
                        }

                        if (workingBitmap != null) {
                            TransitionManager.beginDelayedTransition(CreateALookFragment.flCropImage);

                            if (imgView != null) {
                                if (vSelect != null) {
                                    vSelect.setVisibility(View.INVISIBLE);
                                }
                                imgView.setImageBitmap(workingBitmap);
                                imgView.setVisibility(View.VISIBLE);
                            }


                            //save the working image
                            try {
                                FileOutputStream out = getActivity().openFileOutput(MyConstants.IMAGE_WORKING_FILE, Context.MODE_PRIVATE);
                                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                                workingBitmap.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
                                out.write(bytes.toByteArray());
                                out.close();
                                File workingImageFile = getActivity().getFileStreamPath(MyConstants.IMAGE_WORKING_FILE);
                                if (workingImageFile != null) {
                                    workingImagePath = workingImageFile.getAbsolutePath();
                                }
                            } catch (Exception e) {

                            }

                            //call to crop image - maybe do as button later
                            if (masterImagePath != null && !masterImagePath.isEmpty()) {
                                Intent pcIntent = new Intent(getActivity(), CropActivity.class);
                                pcIntent.putExtra(MyConstants.INTENT_KEY_CROP_IMGPATH, masterImagePath);
                                startActivityForResult(pcIntent, MyConstants.INTENT_RESULT_CROP);
                            } else if (CreateALookFragment.masterImageList != null && CreateALookFragment.masterImageList.length > 0) {
                                //open multi image crop
                                Intent pcIntent = new Intent(getActivity(), CropMultiActivity.class);
                                pcIntent.putExtra(MyConstants.INTENT_KEY_CROP_IMGPATHLIST, CreateALookFragment.masterImageList);
                                startActivityForResult(pcIntent, MyConstants.INTENT_RESULT_CROP);

                            }


                        }


                    }
                } else if (requestCode == MyConstants.INTENT_RESULT_CROP) {
                    try {
                        File workingImageFile = getActivity().getFileStreamPath(MyConstants.IMAGE_WORKING_FILE);
                        if (workingImageFile != null) {
                            workingImagePath = workingImageFile.getAbsolutePath();
                            if (workingImagePath != null && !workingImagePath.isEmpty()) {
                                workingBitmap = BitmapFactory.decodeStream(getActivity().openFileInput(MyConstants.IMAGE_WORKING_FILE));
                                if (workingBitmap != null && !workingBitmap.isRecycled()) {
                                    TransitionManager.beginDelayedTransition(CreateALookFragment.flCropImage);

                                    if (imgView != null) {
                                        if (vSelect != null) {
                                            vSelect.setVisibility(View.INVISIBLE);
                                        }
                                        imgView.setImageBitmap(workingBitmap);
                                        imgView.setVisibility(View.VISIBLE);
                                    }
                                }

                                workingBitmap.recycle();

                            }
                        }

                    } catch (Exception e) {
                        //Log.d(TAG,e.getMessage()+e.getStackTrace());
                        //return null;
                    }

                }

                break;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private static Bitmap getImageResized(Context context, Uri selectedImage) {
        Bitmap bm = null;
        int[] sampleSizes = new int[]{5, 3, 2, 1};
        int i = 0;
        do {
            bm = decodeBitmap(context, selectedImage, sampleSizes[i]);
            Log.d("TAG", "resizer: new bitmap width = " + bm.getWidth());
            i++;
        } while (bm.getWidth() < minWidthQuality && i < sampleSizes.length);
        return bm;
    }

    private static Bitmap decodeBitmap(Context context, Uri theUri, int sampleSize) {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inSampleSize = sampleSize;

        AssetFileDescriptor fileDescriptor = null;
        try {
            fileDescriptor = context.getContentResolver().openAssetFileDescriptor(theUri, "r");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        Bitmap actuallyUsableBitmap = BitmapFactory.decodeFileDescriptor(
                fileDescriptor.getFileDescriptor(), null, options);

        Log.d("TAG", options.inSampleSize + " sample method bitmap ... " +
                actuallyUsableBitmap.getWidth() + " " + actuallyUsableBitmap.getHeight());

        return actuallyUsableBitmap;
    }

    public static int getRotation(Context context, Uri imageUri, boolean isCamera) {
        int rotation;
        if (isCamera) {
            rotation = getRotationFromCamera(context, imageUri);
        } else {
            rotation = getRotationFromGallery(context, imageUri);
        }
        Log.d("TAG", "Image rotation: " + rotation);
        return rotation;
    }

    public static int getRotationFromGallery(Context context, Uri imageUri) {
        int result = 0;
        String[] columns = {MediaStore.Images.Media.ORIENTATION};
        Cursor cursor = null;
        try {
            cursor = context.getContentResolver().query(imageUri, columns, null, null, null);
            if (cursor != null && cursor.moveToFirst()) {
                int orientationColumnIndex = cursor.getColumnIndex(columns[0]);
                result = cursor.getInt(orientationColumnIndex);
            }
        } catch (Exception e) {
            //Do nothing
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }//End of try-catch block
        return result;
    }

    private static Bitmap rotate(Bitmap bm, int rotation) {
        if (rotation != 0) {
            Matrix matrix = new Matrix();
            matrix.postRotate(rotation);
            Bitmap bmOut = Bitmap.createBitmap(bm, 0, 0, bm.getWidth(), bm.getHeight(), matrix, true);
            return bmOut;
        }
        return bm;
    }

    public static int getRotationFromCamera(Context context, Uri imageFile) {
        int rotate = 0;
        try {
            context.getContentResolver().notifyChange(imageFile, null);
            ExifInterface exif = new ExifInterface(imageFile.getPath());
            int orientation = exif.getAttributeInt(
                    ExifInterface.TAG_ORIENTATION,
                    ExifInterface.ORIENTATION_NORMAL);
            switch (orientation) {
                case ExifInterface.ORIENTATION_ROTATE_270:
                    rotate = 270;
                    break;
                case ExifInterface.ORIENTATION_ROTATE_180:
                    rotate = 180;
                    break;
                case ExifInterface.ORIENTATION_ROTATE_90:
                    rotate = 90;
                    break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return rotate;
    }

    protected abstract void onPhotoTaken();
}
