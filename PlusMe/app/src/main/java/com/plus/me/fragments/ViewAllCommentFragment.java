package com.plus.me.fragments;


import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.github.siyamed.shapeimageview.CircularImageView;
import com.plus.me.R;
import com.plus.me.adapter.CommentRplyAdapter;
import com.plus.me.listener.BlogListener;
import com.plus.me.model.BlogComment;
import com.plus.me.model.CommentDetail;
import com.plus.me.others.App;
import com.plus.me.others.Internet;
import com.plus.me.others.PicassoTrustAll;
import com.plus.me.utils.Utilities;
import com.plus.me.utils.VerticalSpacingDecoration;
import com.plus.me.ws.AsyncTaskListner;
import com.plus.me.ws.CallRequest;
import com.plus.me.ws.Constant;
import com.plus.me.ws.JsonParserUniversal;
import com.plus.me.ws.MyConstants;
import com.plus.me.ws.Utils;
import com.squareup.picasso.Callback;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import static com.plus.me.activity.HomeActivity.toolbar;
import static com.plus.me.activity.HomeActivity.txtSave;

/**
 * A simple {@link Fragment} subclass.
 */
public class ViewAllCommentFragment extends Fragment implements AsyncTaskListner, BlogListener {

    private static final String TAG = "ViewAllCommentFragment";

    public ViewAllCommentFragment() {
        // Required empty public constructor
    }

    public JsonParserUniversal jParser;
    private RecyclerView rvViewAllComment;
    CommentRplyAdapter blogDetailAdapter;
    View view;
    public String intent, id;
    private EditText etComment;
    public CircularImageView ivUser;
    public TextView tvUserName, tvComment,tvdatetime;
    public BlogComment blogDetail;
    CommentDetail commentDetail;
    public ImageView ivSend;
    public String comment, From, BLOGID,COMMENTID;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_view_all_comment, container, false);
        setHasOptionsMenu(true);
        toolbar.setText("Reply");
        jParser = new JsonParserUniversal();
        blogDetail = new BlogComment();
        commentDetail = new CommentDetail();
        Log.d(TAG, "BLOG DATA>>: " + blogDetail.getBlogId());
        Log.d(TAG, "FEED DATA>>: " + commentDetail.getBlogId());

        intent = getArguments().getString("intent");
        From = getArguments().getString("FROM");
        BLOGID = getArguments().getString("BLOGID");
        COMMENTID = getArguments().getString("COMMENTID");

        if (intent.equalsIgnoreCase("blog")) {
            if (getArguments() != null) {
                id = getArguments().getString("c_id");
                System.out.println("id====" + id);
            }
        }
        etComment = view.findViewById(R.id.etComment);
        ivSend = view.findViewById(R.id.ivSend);
        ivUser = view.findViewById(R.id.ivUser);
        tvComment = view.findViewById(R.id.tvComment);
        tvdatetime = view.findViewById(R.id.tvdatetime);
        tvUserName = view.findViewById(R.id.tvUserName);
        rvViewAllComment = view.findViewById(R.id.rvComment);
        getData();

        ivSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                comment = etComment.getText().toString();

                if (TextUtils.isEmpty(comment)) {
                    Utils.showToast("Please Add BlogComment", getActivity());
                } else {
                    addComment();
                }
            }
        });
        return view;


    }

    private void addComment() {
        Map<String, String> map = new HashMap<>();
        map.put("url", MyConstants.BASE_URL + "add_blogfeed_comment");
        map.put("header", "");
        map.put("FROM", From); // TODO add post id
        map.put("comment", Utilities.convertStringToUTF8(comment));
        map.put("Auth", App.user.getAuth());
        map.put("blog_id", BLOGID); // TODO add post id
        map.put("ParentCommentID", COMMENTID);

       /* if (From.equalsIgnoreCase("BLOG")) {
        } else {
            map.put("ParentCommentID", commentDetail.getBlogCommentId());
        }
*/
        if (!Internet.isAvailable(getActivity())) {
            Internet.showAlertDialog(getActivity(), "Error!", "No Internet Connection", false);
        } else {
            new CallRequest(ViewAllCommentFragment.this).addComment(map);
        }

    }

    private void getData() {
        Map<String, String> map = new HashMap<>();
        map.put("url", MyConstants.BASE_URL + "get_comment");
        map.put("CommentID", id); // TODO add post id
        map.put("FROM", From); // TODO add post id
        map.put("header", "");
        map.put("Auth", App.user.getAuth());

        if (!Internet.isAvailable(getActivity())) {
            Internet.showAlertDialog(getActivity(), "Error!", "No Internet Connection", false);
        } else {
            new CallRequest(ViewAllCommentFragment.this).getCommentDetail(map);
        }
    }

    private void setData() {
        tvUserName.setText(blogDetail.getPostedByName());
        tvdatetime.setText(Utils.ConvertTime(blogDetail.getTime()));
        tvComment.setText(Utilities.convertUTF8ToString(blogDetail.getComment()));
        if (!TextUtils.isEmpty(blogDetail.getProfileImage())) {
            try {
                PicassoTrustAll.getInstance(getActivity())
                        .load(blogDetail.getProfileImage())
                        .placeholder(R.drawable.avatar)
                        .error(R.drawable.avatar)
                        .into(ivUser, new Callback() {
                            @Override
                            public void onSuccess() {
                            }

                            @Override
                            public void onError() {

                            }
                        });
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        //System.out.println("Blog list size==" + BlogDetailFragment.commentDetailsList.size());
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        rvViewAllComment.setLayoutManager(layoutManager);
        blogDetailAdapter = new CommentRplyAdapter(getActivity(), blogDetail.rplyList, ViewAllCommentFragment.this);
        rvViewAllComment.setHasFixedSize(true);
        rvViewAllComment.addItemDecoration(new VerticalSpacingDecoration(20));
        rvViewAllComment.setItemViewCacheSize(20);
        rvViewAllComment.setDrawingCacheEnabled(true);
        rvViewAllComment.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
        rvViewAllComment.setAdapter(blogDetailAdapter);
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        txtSave.setVisibility(View.GONE);
        menu.getItem(0).setVisible(true);
        menu.getItem(1).setVisible(false);
        menu.getItem(2).setVisible(false);
        menu.getItem(3).setVisible(false);
        menu.getItem(4).setVisible(false);

        super.onPrepareOptionsMenu(menu);
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        try {

            Utils.hideProgressDialog();
            if (result != null && !result.isEmpty()) {
                Log.i("TAG", "TAG Result : " + result);

                switch (request) {
                    case getCommentDetail:

                        try {
                            JSONObject object = new JSONObject(result);
                            boolean success = object.getBoolean("success");
                            if (success) {
                                // TODO parse JSON
                                JSONArray data = object.getJSONArray("data");
                                if (data != null && data.length() > 0) {
                                    for (int i = 0; i < data.length(); i++) {
                                        JSONObject jsonObject = data.getJSONObject(i);
                                        blogDetail = (BlogComment) jParser.parseJson(jsonObject, new BlogComment());

                                        JSONArray rData = jsonObject.getJSONArray("reply");
                                        if (rData != null && rData.length() > 0) {
                                            for (int k = 0; k < rData.length(); k++) {
                                                JSONObject rObject = rData.getJSONObject(k);
                                                BlogComment rplyList = (BlogComment) jParser.parseJson(rObject, new BlogComment());

                                                blogDetail.rplyList.add(rplyList);

                                            }
                                        }

                                    }
                                    setData();
                                } else {
                                    //  Utils.showToast(object.getString("message"), getActivity());
                                }
                            } else {
                                String error_string = object.getString("message");
                                Toast.makeText(getActivity(), error_string, Toast.LENGTH_SHORT).show();

                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;


                    case addComment:

                        try {
                            JSONObject object = new JSONObject(result);
                            boolean success = object.getBoolean("success");
                            if (success) {
                                // TODO parse JSON
                                etComment.setText("");
                                Utils.showToast(object.getString("message"), getActivity());
                                getData();
                            } else {
                                Utils.showToast(object.getString("message"), getActivity());
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;


                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onProgressUpdate(String uniqueMessageId, int progres) {

    }

    @Override
    public void onProgressComplete(String uniqueMessageId, String result, Constant.REQUESTS request) {

    }

    @Override
    public void onClosetClick(int pos) {

    }

    @Override
    public void onLikeClick(int pos) {

    }
}
