package com.plus.me.listener;

/**
 * Created by Krupa Kakkad on 23 August 2018
 */
public interface FollowerListener {
    public void followerItemClick(int pos);
}
