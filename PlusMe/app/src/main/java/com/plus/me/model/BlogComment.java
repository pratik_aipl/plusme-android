package com.plus.me.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class BlogComment implements Serializable {
    
       public String BlogCommentId="";
               public String ParentCommentID="";
               public String UserId="";
               public String BlogId="";
               public String Comment="";
               public String IsActive="";
               public String CreateDate="";
               public String PostedByName="";
               public String ProfileImage="";
               public String Time="";

    public String getTime() {
        return Time;
    }

    public void setTime(String time) {
        Time = time;
    }

    public String getBlogCommentId() {
        return BlogCommentId;
    }

    public void setBlogCommentId(String blogCommentId) {
        BlogCommentId = blogCommentId;
    }

    public String getParentCommentID() {
        return ParentCommentID;
    }

    public void setParentCommentID(String parentCommentID) {
        ParentCommentID = parentCommentID;
    }

    public String getUserId() {
        return UserId;
    }

    public void setUserId(String userId) {
        UserId = userId;
    }

    public String getBlogId() {
        return BlogId;
    }

    public void setBlogId(String blogId) {
        BlogId = blogId;
    }

    public String getComment() {
        return Comment;
    }

    public void setComment(String comment) {
        Comment = comment;
    }

    public String getIsActive() {
        return IsActive;
    }

    public void setIsActive(String isActive) {
        IsActive = isActive;
    }

    public String getCreateDate() {
        return CreateDate;
    }

    public void setCreateDate(String createDate) {
        CreateDate = createDate;
    }

    public String getPostedByName() {
        return PostedByName;
    }

    public void setPostedByName(String postedByName) {
        PostedByName = postedByName;
    }

    public String getProfileImage() {
        return ProfileImage;
    }

    public void setProfileImage(String profileImage) {
        ProfileImage = profileImage;
    }
    public ArrayList<BlogComment> rplyList = new ArrayList<>();
}
