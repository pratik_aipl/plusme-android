package com.plus.me.fragments;


import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.plus.me.R;
import com.plus.me.activity.LoginActivity;
import com.plus.me.others.App;
import com.plus.me.others.Internet;
import com.plus.me.utils.MySharedPref;
import com.plus.me.ws.AsyncTaskListner;
import com.plus.me.ws.CallRequest;
import com.plus.me.ws.Constant;
import com.plus.me.ws.MyConstants;
import com.plus.me.ws.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import static com.plus.me.activity.HomeActivity.toolbar;
import static com.plus.me.activity.HomeActivity.txtSave;

/**
 * A simple {@link Fragment} subclass.
 */
public class ChangePasswordFragment extends Fragment implements AsyncTaskListner {

    private EditText etOldPassword, etNewPassword, etConfirmPassword;
    private TextView tvCancel;
    private Button btnOK;

    public ChangePasswordFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_change_password, container, false);

        setHasOptionsMenu(true);

        bindWidgetReference(view);

        bindWidgetEvents();

        toolbar.setText("Change Password");
        return view;
    }

    private void bindWidgetReference(View view) {
        etOldPassword = view.findViewById(R.id.etOldPassword);
        etNewPassword = view.findViewById(R.id.etNewPassword);
        etConfirmPassword = view.findViewById(R.id.etConfirmPassword);
        tvCancel = view.findViewById(R.id.tvCancel);
        btnOK = view.findViewById(R.id.btnOK);
    }

    private void bindWidgetEvents() {
        btnOK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changePassword();
            }
        });

        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });
    }

    private void changePassword() {
        String oldPassword = etOldPassword.getText().toString().trim();
        String newPassword = etNewPassword.getText().toString().trim();
        String confirmPassword = etConfirmPassword.getText().toString().trim();

        if (TextUtils.isEmpty(oldPassword)) {
            etOldPassword.requestFocus();
            etOldPassword.setError("Enter old password");
        } else if (TextUtils.isEmpty(newPassword)) {
            etNewPassword.requestFocus();
            etNewPassword.setError("Enter new password");
        } else if (TextUtils.isEmpty(confirmPassword)) {
            etConfirmPassword.requestFocus();
            etConfirmPassword.setError("Enter confirm password");
        } else if (!newPassword.equalsIgnoreCase(confirmPassword)) {
            etConfirmPassword.requestFocus();
            etConfirmPassword.setError("Password does not match the confirm password");
        } else {
            Map<String, String> map = new HashMap<>();
            map.put("url", MyConstants.BASE_URL + "change_password");
            map.put("OldPassword", oldPassword);
            map.put("NewPassword", newPassword);
            map.put("header", "");
            map.put("Auth", App.user.getAuth());

            if (!Internet.isAvailable(getActivity())) {
                Internet.showAlertDialog(getActivity(), "Error!", "No Internet Connection", false);
            } else {
                new CallRequest(ChangePasswordFragment.this).changePassword(map);
            }
        }
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        txtSave.setVisibility(View.GONE);
        //  tabs.setVisibility(View.GONE);
        menu.getItem(0).setVisible(true);
        menu.getItem(1).setVisible(false);
        menu.getItem(2).setVisible(false);
        menu.getItem(3).setVisible(false);
        menu.getItem(4).setVisible(false);

        super.onPrepareOptionsMenu(menu);
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        try {

            Utils.hideProgressDialog();
            if (result != null && !result.isEmpty()) {
                Log.i("TAG", "TAG Result : " + result);

                switch (request) {
                    case changePassword:
                        try {
                            JSONObject object = new JSONObject(result);
                            boolean success = object.getBoolean("success");
                            if (success) {
                                // TODO parse JSON
                                Utils.showToast(object.getString("message"), getActivity());

                                updateData();
                            } else {
                                String error_string = object.getString("message");
                                Utils.showToast(error_string, getActivity());
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void updateData() {

        Handler uiHandler = new Handler(Looper.getMainLooper());
        uiHandler.post(new Runnable(){
            @Override
            public void run() {
                MySharedPref.setIsLogin(false);
                MySharedPref.sharedPrefClear(getActivity());

                Intent intent = new Intent(getActivity(), LoginActivity.class);
                startActivity(intent);
                ActivityCompat.finishAffinity(getActivity());

            }
        });


    }

    @Override
    public void onProgressUpdate(String uniqueMessageId, int progres) {

    }

    @Override
    public void onProgressComplete(String uniqueMessageId, String result, Constant.REQUESTS request) {

    }
}
