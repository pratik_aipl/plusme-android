package com.plus.me.model;

import java.io.Serializable;

/**
 * Created by Krupa Kakkad on 22 August 2018
 */
public class UserProfile implements Serializable {

    public String Id = "";
    public String Email = "";
    public String IsPrivate="";

    public String getIsPrivate() {
        return IsPrivate;
    }

    public void setIsPrivate(String isPrivate) {
        IsPrivate = isPrivate;
    }

    public String UserProfileImage = "";
    public String FirstName = "";
    public String LastName = "";
    public String CompanyName = "";
    public String MobileNo = "";
    public String WorkingHours = "";
    public String FacebookLink = "";
    public String InstragramLink = "";
    public String RegisterUserType = "";
    public String RegisterUserTypeByName ="";
    public String CountryID="";
    public String StateID="";
    public String CityID="";
    public String ZipCode="";
    public int isFollowing, UserProfileID;

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public String getUserProfileImage() {
        return UserProfileImage;
    }

    public void setUserProfileImage(String userProfileImage) {
        UserProfileImage = userProfileImage;
    }

    public String getFirstName() {
        return FirstName;
    }

    public void setFirstName(String firstName) {
        FirstName = firstName;
    }

    public String getLastName() {
        return LastName;
    }

    public void setLastName(String lastName) {
        LastName = lastName;
    }

    public String getCompanyName() {
        return CompanyName;
    }

    public void setCompanyName(String companyName) {
        CompanyName = companyName;
    }

    public String getMobileNo() {
        return MobileNo;
    }

    public void setMobileNo(String mobileNo) {
        MobileNo = mobileNo;
    }

    public String getWorkingHours() {
        return WorkingHours;
    }

    public void setWorkingHours(String workingHours) {
        WorkingHours = workingHours;
    }

    public String getFacebookLink() {
        return FacebookLink;
    }

    public void setFacebookLink(String facebookLink) {
        FacebookLink = facebookLink;
    }

    public String getInstragramLink() {
        return InstragramLink;
    }

    public void setInstragramLink(String instragramLink) {
        InstragramLink = instragramLink;
    }

    public int getIsFollowing() {
        return isFollowing;
    }

    public void setIsFollowing(int isFollowing) {
        this.isFollowing = isFollowing;
    }

    public int getUserProfileID() {
        return UserProfileID;
    }

    public void setUserProfileID(int userProfileID) {
        UserProfileID = userProfileID;
    }

    public String getRegisterUserType() {
        return RegisterUserType;
    }

    public void setRegisterUserType(String registerUserType) {
        RegisterUserType = registerUserType;
    }

    public String getCountryID() {
        return CountryID;
    }

    public void setCountryID(String countryID) {
        CountryID = countryID;
    }

    public String getStateID() {
        return StateID;
    }

    public void setStateID(String stateID) {
        StateID = stateID;
    }

    public String getCityID() {
        return CityID;
    }

    public void setCityID(String cityID) {
        CityID = cityID;
    }

    public String getZipCode() {
        return ZipCode;
    }

    public void setZipCode(String zipCode) {
        ZipCode = zipCode;
    }

    public String getRegisterUserTypeByName() {
        return RegisterUserTypeByName;
    }

    public void setRegisterUserTypeByName(String registerUserTypeByName) {
        RegisterUserTypeByName = registerUserTypeByName;
    }
}
