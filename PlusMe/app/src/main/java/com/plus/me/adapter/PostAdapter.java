package com.plus.me.adapter;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.github.siyamed.shapeimageview.CircularImageView;
import com.jsibbold.zoomage.ZoomageView;
import com.plus.me.R;
import com.plus.me.activity.HomeActivity;
import com.plus.me.fragments.PostFragment;
import com.plus.me.fragments.ProfileFragment;
import com.plus.me.listener.DeleteBlogListener;
import com.plus.me.model.Post;
import com.plus.me.others.App;
import com.plus.me.others.PicassoTrustAll;
import com.plus.me.utils.Utilities;
import com.plus.me.ws.Utils;
import com.squareup.picasso.Callback;

import java.util.ArrayList;

public class PostAdapter extends RecyclerView.Adapter<PostAdapter.MyViewHolder> {

    Context mContext;
    LayoutInflater inflater;
    ArrayList<Post> postList;
    public DeleteBlogListener deleteBlogListener;

    public PostAdapter(Context mContext, ArrayList<Post> categoryList, PostFragment postFragment) {
        this.mContext = mContext;
        this.postList = categoryList;
        setHasStableIds(true);
        this.deleteBlogListener = postFragment;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public PostAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_post, parent, false);

        return new PostAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final PostAdapter.MyViewHolder holder, final int position) {
        final Post post = postList.get(position);
        holder.progressBar.setVisibility(View.VISIBLE);

        //   holder.tvCommentCount.setText(post.getTotalComment());
        holder.tvUserName.setText(post.getRegisterUserTypeByName());
        holder.tvTitle.setText(post.getPostedByName());
        holder.tvDescription.setText(Utilities.convertUTF8ToString(post.getPostTitle()));
        Utils.getDatesDifferenceInDays(post.getPostTime(), holder.tvTime);

        try {
            PicassoTrustAll.getInstance(mContext)
                    .load(post.getPostImage())
                    .placeholder(R.drawable.no_image)
                    .error(R.drawable.no_image)
                    .into(holder.ivImage, new Callback() {
                        @Override
                        public void onSuccess() {
                            holder.progressBar.setVisibility(View.GONE);
                        }

                        @Override
                        public void onError() {
                            holder.progressBar.setVisibility(View.GONE);
                        }
                    });
        } catch (Exception e) {
            e.printStackTrace();
        }
        holder.tvTitle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ProfileFragment profileFragment = new ProfileFragment();
                Bundle bundle = new Bundle();
                bundle.putString("intent", "profile");
                bundle.putString("id", post.getPostedByID());
                profileFragment.setArguments(bundle);
                HomeActivity.replaceFragment(profileFragment, false);
            }
        });

        holder.ivImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createDialog(postList.get(position).getPostImage());
            }
        });

        if (!App.user.getId().equalsIgnoreCase(postList.get(position).getPostedByID())) {
            holder.tv_Tdots.setVisibility(View.GONE);
        }
        holder.tv_Tdots.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                PopupMenu popup = new PopupMenu(mContext, holder.tv_Tdots);
                popup.getMenuInflater().inflate(R.menu.delete_menu, popup.getMenu());

                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    public boolean onMenuItemClick(MenuItem item) {
                        //Toast.makeText(mContext, "You Clicked : " + item.getTitle(), Toast.LENGTH_SHORT).show();
                        deleteBlogListener.onDelete(postList.get(position).getBlogId(), position);
                        return true;
                    }
                });

                popup.show();
            }
        });
        try {
            PicassoTrustAll.getInstance(mContext)
                    .load(post.getProfileImage())
                    .placeholder(R.drawable.avatar)
                    .error(R.drawable.avatar)
                    .into(holder.ivUser, new Callback() {
                        @Override
                        public void onSuccess() {
                        }

                        @Override
                        public void onError() {
                        }
                    });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void createDialog(String image) {
        final Dialog dialog = new Dialog(mContext, R.style.ChatDialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        // Include dialog.xml file
        dialog.setContentView(R.layout.dialog_view_fullimage);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        lp.copyFrom(window.getAttributes());
        //This makes the dialog take up the full width
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
        ImageView imgPhotoEditorClose = dialog.findViewById(R.id.imgPhotoEditorClose);
        imgPhotoEditorClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        ZoomageView zoomageView = dialog.findViewById(R.id.imgDialog);
        try {
            PicassoTrustAll.getInstance(mContext)
                    .load(image)
                    .placeholder(R.drawable.no_image)
                    .error(R.drawable.no_image)
                    .into(zoomageView, new Callback() {
                        @Override
                        public void onSuccess() {
                        }

                        @Override
                        public void onError() {
                        }
                    });
        } catch (Exception e) {
            e.printStackTrace();
        }

        dialog.show();
    }

    @Override
    public int getItemCount() {
        return postList.size();
    }

    public Post getItem(int position) {
        return postList.get(position);
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        public CircularImageView ivUser;
        public TextView tvTitle, tvUserName, tvTime, tvDescription, tvCommentCount, tv_Tdots;
        public ImageView ivImage, ivCloset;
        public ProgressBar progressBar;

        public MyViewHolder(View itemView) {
            super(itemView);

            ivImage = itemView.findViewById(R.id.ivImage);
            //  ivCloset = itemView.findViewById(R.id.ivCloset);
            ivUser = itemView.findViewById(R.id.ivUser);
            tvTitle = itemView.findViewById(R.id.tvTitle);
            tv_Tdots = itemView.findViewById(R.id.tv_Tdots);
            tvUserName = itemView.findViewById(R.id.tvUserName);
            tvTime = itemView.findViewById(R.id.tvTime);
            tvDescription = itemView.findViewById(R.id.tvDescription);
            //    tvCommentCount = itemView.findViewById(R.id.tvCommentCount);
            progressBar = itemView.findViewById(R.id.progressBar);
        }
    }
}
