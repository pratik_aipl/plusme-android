package com.plus.me.fragments;


import android.content.Intent;
import android.os.Bundle;
import com.google.android.material.tabs.TabLayout;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.plus.me.R;
import com.plus.me.adapter.ProfileTabsPagerAdapter;
import com.plus.me.model.UserProfile;
import com.plus.me.others.App;
import com.plus.me.others.PicassoTrustAll;
import com.plus.me.ws.AsyncTaskListner;
import com.plus.me.ws.CallRequest;
import com.plus.me.ws.Constant;
import com.plus.me.ws.JsonParserUniversal;
import com.plus.me.ws.MyConstants;
import com.plus.me.ws.Utils;
import com.squareup.picasso.Callback;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import static com.plus.me.activity.HomeActivity.toolbar;
import static com.plus.me.activity.HomeActivity.txtSave;

/**
 * A simple {@link Fragment} subclass.
 */
public class ProfileFragment extends Fragment implements AsyncTaskListner {

    public TextView tvProfession, txtName, txtInfo, txtPlush;
    public TabLayout tabLayout;
    public ViewPager viewPager;
    public ImageView ivProfile, ivPlush,ivInfo;
    public  ProfileTabsPagerAdapter adapter;
    public String intent;
    public JsonParserUniversal jParser;
    private UserProfile userProfile;
    public Menu menu;

    public ProfileFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view;

        view = inflater.inflate(R.layout.fragment_profile, container, false);

        setHasOptionsMenu(true);

        bindwidgetReferrence(view);
        initializeData();
        getUserProfile();
        onClickListener();
        return view;
    }

    private void onClickListener() {
        ivPlush.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                userFollowUnfollow();
            }
        });
        ivInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent yourIntent = new Intent(getContext(), InfoActivity.class);
                Bundle b = new Bundle();
                b.putSerializable("userDetail", userProfile);
                yourIntent.putExtras(b); //pass bundle to your intent
                startActivity(yourIntent);
                }
        });
    }


    private void userFollowUnfollow() {
        Map<String, String> map = new HashMap<>();
        map.put("url", MyConstants.BASE_URL + "follow_unfollow_user");
        map.put("FollowUserID", MyConstants.otherUserId);
        if (userProfile.getIsFollowing() == 1) {
            map.put("isFollow", "0");// TODO add category id
        } else {
            map.put("isFollow", "1");
        }
        map.put("header", "");
        map.put("Auth", App.user.getAuth());

        new CallRequest(ProfileFragment.this).FollowUnFollow(map);
    }

    private void getUserProfile() {
        Map<String, String> map = new HashMap<>();
        map.put("url", MyConstants.BASE_URL + "get_profile");
        map.put("header", "");
        map.put("Auth", App.user.getAuth());
        if (MyConstants.isUserProfile) {
            map.put("UserProfileID", MyConstants.otherUserId);
        } else {
            map.put("UserProfileID", App.user.getId());
        }

        new CallRequest(ProfileFragment.this).getUserProfileFragment(map);
    }

    private void setUserData() {

        if (userProfile.getIsFollowing() == 0) {
            txtPlush.setText("Follow");
        } else {
            txtPlush.setText("Unfollow");
        }
        txtName.setText(userProfile.getFirstName() + " " + userProfile.getLastName());
        tvProfession.setText(userProfile.getRegisterUserTypeByName());
        if (!TextUtils.isEmpty(userProfile.getUserProfileImage())) {
            try {
                PicassoTrustAll.getInstance(getActivity())
                        .load(userProfile.getUserProfileImage())
                        .placeholder(R.drawable.avatar)
                        .error(R.drawable.avatar)
                        .into(ivProfile, new Callback() {
                            @Override
                            public void onSuccess() {

                            }

                            @Override
                            public void onError() {

                            }
                        });
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }


    @Override
    public void onResume() {
        super.onResume();
        // setUserData();
        // ((HomeActivity) getActivity()).setUserData();
    }

    private void initializeData() {
        toolbar.setText("My Profile");
        jParser = new JsonParserUniversal();
        intent = getArguments().getString("intent");
        if (intent.equalsIgnoreCase("profile")) {
            MyConstants.otherUserId = getArguments().getString("id");
            MyConstants.isUserProfile = true;
            MyConstants.profileCount = MyConstants.profileCount + 1;

        } else {
            MyConstants.isUserProfile = false;
            MyConstants.otherUserId = App.user.getId();
        }

        if (!App.user.getId().equalsIgnoreCase(MyConstants.otherUserId)) {
            ivPlush.setVisibility(View.VISIBLE);
            txtPlush.setVisibility(View.VISIBLE);
        } else {
            ivPlush.setVisibility(View.GONE);
            txtPlush.setVisibility(View.GONE);
        }
        tabLayout.addTab(tabLayout.newTab().setText("Posts"));
        tabLayout.addTab(tabLayout.newTab().setText("Followers"));
        tabLayout.addTab(tabLayout.newTab().setText("Followings"));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

        adapter = new ProfileTabsPagerAdapter
                (getChildFragmentManager(), tabLayout.getTabCount());
        viewPager.setAdapter(adapter);

        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    private void bindwidgetReferrence(View view) {
        viewPager = view.findViewById(R.id.pager);
        tabLayout = view.findViewById(R.id.tabs);
        tvProfession = view.findViewById(R.id.tvProfession);
        txtName = view.findViewById(R.id.txtName);
        txtInfo = view.findViewById(R.id.txtInfo);
        txtPlush = view.findViewById(R.id.txtPlush);
        ivPlush = view.findViewById(R.id.ivPlush);
        ivInfo=view.findViewById(R.id.ivInfo);
        ivProfile = view.findViewById(R.id.ivProfile);
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        txtSave.setVisibility(View.GONE);
        this.menu  = menu;
        menu.getItem(0).setVisible(true);
        menu.getItem(1).setVisible(false);
        menu.getItem(2).setVisible(false);
        menu.getItem(3).setVisible(false);

        /*try {
            if( App.user.getId().equalsIgnoreCase(userProfile.getId())){
                menu.getItem(3).setVisible(true);
            }
        }catch (Exception e){
            e.printStackTrace();
        }*/


        super.onPrepareOptionsMenu(menu);
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        try {

            Utils.hideProgressDialog();
            if (result != null && !result.isEmpty()) {
                Log.i("TAG", "TAG Result : " + result);

                switch (request) {
                    case followUnfollow:
                        try {
                            JSONObject object = new JSONObject(result);
                            boolean success = object.getBoolean("success");
                            if (success) {
                                // TODO parse JSON
                               // Utils.showToast(object.getString("message"), getActivity());

                                if (userProfile.getIsFollowing() == 0) {
                                    txtPlush.setText("Unfollow");
                                } else {
                                    txtPlush.setText("Follow");
                                }
                                getUserProfile();
                                adapter.notifyDataSetChanged();
                                viewPager.invalidate();
                            } else {
                                Utils.showToast(object.getString("message"), getActivity());
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;

                    case getUserProfile:
                        try {
                            JSONObject object = new JSONObject(result);
                            boolean success = object.getBoolean("success");
                            if (success) {
                                // TODO parse JSON
                                JSONObject userDataObj = object.getJSONObject("user_data");

                                userProfile = (UserProfile) jParser.parseJson(userDataObj, new UserProfile());
                                try {
                                    if( App.user.getId().equalsIgnoreCase(userProfile.getId())){
                                        menu.getItem(4).setVisible(true);
                                    }else{
                                        menu.getItem(4).setVisible(false);
                                    }
                                }catch (Exception e){
                                    e.printStackTrace();
                                }

                            } else {
                                String error_string = object.getString("message");
                                Toast.makeText(getContext(), error_string, Toast.LENGTH_SHORT).show();
                            }
                            setUserData();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    @Override
    public void onProgressUpdate(String uniqueMessageId, int progres) {

    }

    @Override
    public void onProgressComplete(String uniqueMessageId, String result, Constant.REQUESTS request) {

    }
}
