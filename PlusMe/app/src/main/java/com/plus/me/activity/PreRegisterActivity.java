package com.plus.me.activity;

import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.plus.me.R;

public class PreRegisterActivity extends AppCompatActivity {

    private Button btnTrendSetter, btnFashionista;
    public static PreRegisterActivity registerActivity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pre_register);

        registerActivity = this;
        bindWidgetReference();
        bindWidgetEvents();
    }

    private void bindWidgetReference() {
        btnTrendSetter = findViewById(R.id.btnTrendSetter);
        btnFashionista = findViewById(R.id.btnFashionista);
    }

    private void bindWidgetEvents() {
        btnTrendSetter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(PreRegisterActivity.this, RegisterActivity.class);
                intent.putExtra("register_type", "0");
                startActivity(intent);
            }
        });

        btnFashionista.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(PreRegisterActivity.this, RegisterActivity.class);
                intent.putExtra("register_type", "1");
                startActivity(intent);
            }
        });
    }
}
