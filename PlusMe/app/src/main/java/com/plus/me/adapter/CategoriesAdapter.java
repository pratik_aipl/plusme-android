package com.plus.me.adapter;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.plus.me.R;
import com.plus.me.fragments.PostCategorywiseFragment;
import com.plus.me.model.Categories;
import com.plus.me.others.PicassoTrustAll;
import com.plus.me.ws.MyConstants;
import com.squareup.picasso.Callback;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import static com.plus.me.activity.HomeActivity.replaceFragment;

/**
 * Created by Krupa Kakkad on 04 July 2018
 */

public class CategoriesAdapter extends RecyclerView.Adapter<CategoriesAdapter.MyViewHolder> {

    Context mContext;
    ArrayList<Categories> categoryList;
    private List<Categories> categoryFilterList = new ArrayList<>();

    public CategoriesAdapter(Context mContext, ArrayList<Categories> categoryList) {
        this.mContext = mContext;
        this.categoryList = categoryList;
        this.categoryFilterList.addAll(categoryList);
        setHasStableIds(true);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_categories, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final Categories categories = categoryList.get(position);

        holder.tvCategoryName.setText(categories.getCategoryName());

        try {
            PicassoTrustAll.getInstance(mContext)
                    .load(categories.getCategoryImageUrl())
                    .into(holder.ivImage, new Callback() {
                        @Override
                        public void onSuccess() {
                            holder.progressBar.setVisibility(View.GONE);
                        }

                        @Override
                        public void onError() {
                        }
                    });
        } catch (Exception e) {
            e.printStackTrace();
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MyConstants.categoryId = categories.getCategoryID();
                MyConstants.categoryName = categories.getCategoryName();
                //HomeFragment.mTabHost.setCurrentTab(2);

                replaceFragment(new PostCategorywiseFragment(),false);
            }
        });
    }

    @Override
    public int getItemCount() {
        return categoryList.size();
    }

    public Categories getItem(int position) {
        return categoryList.get(position);
    }

    public void filters(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());
        categoryList.clear();
        if (charText.length() == 0) {
            categoryList.addAll(categoryFilterList);
        } else {
            for (Categories bean : categoryFilterList) {
                String Contanint = bean.getCategoryName();
                if (Contanint.toLowerCase(Locale.getDefault())
                        .contains(charText)) {
                    categoryList.add(bean);
                }
            }
        }
        notifyDataSetChanged();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        public ImageView ivImage;
        public TextView tvCategoryName;
        public ProgressBar progressBar;

        public MyViewHolder(View itemView) {
            super(itemView);

            tvCategoryName = itemView.findViewById(R.id.tvCategoryName);
            ivImage = itemView.findViewById(R.id.ivImage);
            progressBar = itemView.findViewById(R.id.progressBar);
        }
    }
}
