package com.plus.me.fragments;


import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.plus.me.R;
import com.plus.me.adapter.SelectClosetAdapter;
import com.plus.me.model.MyCloset;
import com.plus.me.others.App;
import com.plus.me.others.Internet;
import com.plus.me.utils.RecyclerTouchListener;
import com.plus.me.utils.VerticalSpacingDecoration;
import com.plus.me.ws.AsyncTaskListner;
import com.plus.me.ws.CallRequest;
import com.plus.me.ws.Constant;
import com.plus.me.ws.JsonParserUniversal;
import com.plus.me.ws.MyConstants;
import com.plus.me.ws.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import static com.plus.me.activity.HomeActivity.toolbar;
import static com.plus.me.activity.HomeActivity.txtSave;
import static com.plus.me.ws.MyConstants.myClosetList;

/**
 * A simple {@link Fragment} subclass.
 */
public class SelectClosetImageFragment extends Fragment implements AsyncTaskListner {


    public SelectClosetImageFragment() {
        // Required empty public constructor
    }

    private RecyclerView rvCloset;
    public View view;
    private GridLayoutManager layoutManager;
    private SelectClosetAdapter myClosetAdapter;
    private String categoryId = "";
    private JsonParserUniversal jParser;
    private TextView txtNoValue;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_select_closet_image, container, false);

        setHasOptionsMenu(true);

        bindWidgetReference(view);

        bindWidgetEvents();

        initializeData();


        return view;
    }

    private void initializeData() {
        toolbar.setText("Select Closet");
        jParser = new JsonParserUniversal();
        if (MyConstants.myClosetList.size() == 0) {
            getData();
        } else {
            setData();
        }
    }

    private void bindWidgetEvents() {

    }

    private void bindWidgetReference(View view) {
        rvCloset = view.findViewById(R.id.rvCloset);
        txtNoValue = view.findViewById(R.id.txtNoValue);
    }

    private void getData() {
        Map<String, String> map = new HashMap<>();
        map.put("url", MyConstants.BASE_URL + "GetMyClosetList");
        map.put("header", "");
        map.put("Auth", App.user.getAuth());
        map.put("CategoryID", categoryId);
        if (!Internet.isAvailable(getActivity())) {
            Internet.showAlertDialog(getActivity(), "Error!", "No Internet Connection", false);
        } else {
            new CallRequest(SelectClosetImageFragment.this).getMyClosetList(map);
        }
    }

    private void setData() {
        layoutManager = new GridLayoutManager(getActivity(), 3, GridLayoutManager.VERTICAL, false); // MAX NUMBER OF SPACES
        rvCloset.setLayoutManager(layoutManager);
        rvCloset.setHasFixedSize(true);
        rvCloset.addItemDecoration(new VerticalSpacingDecoration(5));
        rvCloset.setItemViewCacheSize(20);
        rvCloset.setDrawingCacheEnabled(true);
        rvCloset.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
        myClosetAdapter = new SelectClosetAdapter(getActivity(), myClosetList);
        rvCloset.setAdapter(myClosetAdapter);

        rvCloset.addOnItemTouchListener(new RecyclerTouchListener(getContext(), rvCloset, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                // dialogViewImage(position);
                MyConstants.selectedImage = myClosetList.get(position).getClosetImage();
                MyConstants.baseImageId = myClosetList.get(position).getClosetID();
                MyConstants.imageType = "1";

          /*      Bundle b = new Bundle();
                b.putBoolean("isFromCloset", true);
                b.putString("imagePath",myClosetList.get(position).getPostImage() );
                CreateALookFragment crFragment = new CreateALookFragment();
                crFragment.setArguments(b);
                replaceFragment(crFragment,false);*/

                getActivity().onBackPressed();
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        txtSave.setVisibility(View.GONE);
        menu.getItem(0).setVisible(true);
        menu.getItem(1).setVisible(false);
        menu.getItem(2).setVisible(true);
        menu.getItem(3).setVisible(false);
        menu.getItem(4).setVisible(false);

    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        try {

            Utils.hideProgressDialog();
            if (result != null && !result.isEmpty()) {
                Log.i("TAG", "TAG Result : " + result);

                switch (request) {
                    case getMyClosetList:
                        myClosetList.clear();
                        try {
                            JSONObject object = new JSONObject(result);
                            boolean success = object.getBoolean("success");
                            if (success) {
                                // TODO parse JSON

                                JSONArray data = object.getJSONArray("data");
                                if (data != null && data.length() > 0) {
                                    for (int i = 0; i < data.length(); i++) {
                                        JSONObject jsonObject = data.getJSONObject(i);

                                        MyCloset myCloset = (MyCloset) jParser.parseJson(jsonObject, new MyCloset());

                                        myClosetList.add(myCloset);
                                    }

                                    setData();
                                } else {
                                    Utils.showToast(object.getString("message"), getActivity());
                                }

                            } else {
                                String error_string = object.getString("message");
                                Toast.makeText(getActivity(), error_string, Toast.LENGTH_SHORT).show();
                            }
                            if (myClosetList.size() == 0) {
                                txtNoValue.setVisibility(View.VISIBLE);
                            } else {
                                txtNoValue.setVisibility(View.GONE);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onProgressUpdate(String uniqueMessageId, int progres) {

    }

    @Override
    public void onProgressComplete(String uniqueMessageId, String result, Constant.REQUESTS request) {

    }
}
