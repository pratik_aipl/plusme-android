package com.plus.me.model;

import java.io.Serializable;

public class Stores implements Serializable {

    public String StoreImage = "",
            StoreName = "", StoreDescritption = "",
            StoreAddress = "", StoreID = "", StoreWebLink = "";

   public double StoreLatitude, StoreLongitude;

    public String getStoreImage() {
        return StoreImage;
    }

    public void setStoreImage(String storeImage) {
        StoreImage = storeImage;
    }

    public String getStoreName() {
        return StoreName;
    }

    public void setStoreName(String storeName) {
        StoreName = storeName;
    }

    public double getStoreLatitude() {
        return StoreLatitude;
    }

    public void setStoreLatitude(double storeLatitude) {
        StoreLatitude = storeLatitude;
    }

    public double getStoreLongitude() {
        return StoreLongitude;
    }

    public void setStoreLongitude(double storeLongitude) {
        StoreLongitude = storeLongitude;
    }

    public String getImage() {
        return StoreImage;
    }

    public void setImage(String image) {
        this.StoreImage = image;
    }

    public String getName() {
        return StoreName;
    }

    public void setName(String name) {
        this.StoreName = name;
    }

    public String getStoreDescritption() {
        return StoreDescritption;
    }

    public void setStoreDescritption(String storeDescritption) {
        this.StoreDescritption = storeDescritption;
    }

    public String getStoreAddress() {
        return StoreAddress;
    }

    public void setStoreAddress(String storeAddress) {
        this.StoreAddress = storeAddress;
    }

    public String getStoreID() {
        return StoreID;
    }

    public void setStoreID(String storeID) {
        StoreID = storeID;
    }

    public String getStoreWebLink() {
        return StoreWebLink;
    }

    public void setStoreWebLink(String storeWebLink) {
        StoreWebLink = storeWebLink;
    }
}
